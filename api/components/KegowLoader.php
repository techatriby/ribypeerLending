<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use app\modules\api\modules\v1\models\KegowLoader as KegowModel;
use app\modules\api\modules\v1\models\KegowCards as KegowCardModel;



class KegowLoader extends Component {

  // Test Merchant Details
  // public $merchantId = 297;
  // public $privateKey = '4F979B996A149779C50C399A686DEF38';
  //Production Merchant details
  private $merchantId = '';
  private $privateKey = '';
  private $baseUrl = '';
  public $currency = 'NGN';
  public $address = 'Lagos';
  public $country = 'NG';
  public $city = 'Ikeja';
  public $zip = '23401';
  public $successUrl = 'http://www.peerlending.riby.me';
  public $failureUrl = 'http://www.riby.me/';


  function __construct($env = 'prod'){
    if ($env == 'prod') {
      $this->merchantId = 577;
      $this->privateKey = '2B612340211D6D79F4D0C354220FD00D';
      $this->baseUrl = 'https://kegow.com/';
    }
    elseif($env == 'dev') {
      $this->merchantId = 297;
      $this->privateKey = '4F979B996A149779C50C399A686DEF38';
      $this->baseUrl = 'https://kegow.getitcard.net/';
    }
    else {
      return [
        'status' => false,
        'message' => 'Invalid Environment Set for Kegow',
      ];
    }
  }

  public function loadUserAccount($phone,$amount,$user_id) {
    if (substr( $phone, 0, 3 ) === "234") {
      $this->getUserCardID($phone, $user_id);
      $orderId = uniqid();
      $real_amount = $amount * 100;
      $hash2 = Yii::$app->encryption->getEncryptionKeyFive([$phone,$real_amount,$this->currency,$this->merchantId,$this->privateKey]);
      $url = $this->baseUrl."getit/api/merchant/bankTopup.do?accountID=".$phone."&merchantID=".$this->merchantId."&currency=".$this->currency."&amount=".$real_amount."&orderID=".$orderId."&encKey=".$hash2;
      $filename = getcwd(). '/curl_content.txt';
      if(file_exists($filename)){
        $fp = fopen($filename, 'w+');
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        fwrite($fp, $result);
        if( strpos(file_get_contents($filename),'result=30') !== false) {
          $file = file_get_contents('curl_content.txt');
          $values = [];
          $result = explode('&', $file);
          foreach ($result as $key) {
            $eachValues = explode('=', $key);
            array_push($values, $eachValues[1]);
          }
          $kegow = new KegowModel;
          $kegow->user_id = $user_id;
          $kegow->result = $values[0];
          $kegow->transactID = $values[1];
          $kegow->checkSUM = $values[2];
          $kegow->orderID = $values[3];
          $kegow->transaction_key = $phone;
          $kegow->amount = $amount;
          $kegow->transaction_type = 'CARD TRANSFER';
          if ($kegow->save()) {
            file_put_contents($filename, "");
            return [
              'status' => true,
              'message' => 'Funds Transfer Successful',
              'data' => [
                'transaction_id' => $values[1],
                'amount_sent' => $amount,
                'accountId' => $phone,
                'orderId' => $values[3],
              ],
            ];
          }
        }
        else {
          file_put_contents($filename, "");
          return [
            'status' => false,
            'message' => 'Funds Transfer UnSuccessful, Kegow Payment not Available. Please Try other Options',
          ];
        }
        curl_close($curl);
        fclose($fp);

      }
      else {
        return [
          'status' => false,
          'message' => 'Transaction not Succesful, Write File not Found',
        ];
      }
    }
    else {
      return [
        'status' => false,
        'message' => 'Invalid Phone number. Phone number must start with 234',
      ];
    }
  }

  public function kegowRegisterUser($phone,$name,$address,$city)
  {
    if (substr( $phone, 0, 3 ) === "234") {
      $hash4 =  Yii::$app->encryption->getEncryptionKeyThree([$this->merchantId,$phone,$this->privateKey]);
      //Prod Url
      $url = $this->baseUrl."getit/api/merchant/registration.do?merchantID=".$this->merchantId."&phone=".$phone."&name=".$name."&address=".$address."&city=".$city."&country=".$this->country."&zip=".$this->zip."&encKey=".$hash4;
      // https://kegow.com/getit/
      //Test Url
      // $url = "https://kegow.getitcard.net/getit/api/merchant/registration.do?merchantID=".$this->merchantId."&phone=".$phone."&name=".$name."&address=".$address."&city=".$city."&country=".$this->country."&zip=".$this->zip."&encKey=".$hash4;
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_FAILONERROR, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($curl);
      // return [$result, $url,$this->merchantId,$phone,$this->privateKey];
    }
    else {
      return [
        'status' => false,
        'message' => 'Invalid Phone number. Phone number must start with 234',
      ];
    }

  }

  public function KegowWithdraw($phone, $amount, $user_id)
  {
    $orderId = uniqid();
    $amount *= 100;
    $card = $this->getUserCardID($phone, $user_id);
    $hash2 = Yii::$app->encryption->getEncryptionKeyFive([$card['data']['CardID'],$amount,$this->currency,$this->merchantId,$this->privateKey]);
    $url = $this->baseUrl."getit/api/merchant/withdraw.do?cardID=".$card['data']['CardID']."&merchantID=".$this->merchantId."&currency=".$this->currency."&amount=".$amount."&orderID=".$orderId."&encKey=".$hash2;
    $filename = getcwd(). '/curl_content.txt';
    if(file_exists($filename)){
      $fp = fopen($filename, 'w+');
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_FAILONERROR, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($curl);
      fwrite($fp, $result);
      if( strpos(file_get_contents($filename),'result=30') !== false) {
        $file = file_get_contents('curl_content.txt');
        $values = [];
        $result = explode('&', $file);
        foreach ($result as $key) {
          $eachValues = explode('=', $key);
          array_push($values, $eachValues[1]);
        }
        $kegow = new KegowModel;
        $kegow->user_id = $user_id;
        $kegow->result = $values[0];
        $kegow->transactID = $values[1];
        $kegow->checkSUM = $values[2];
        $kegow->orderID = $values[3];
        $kegow->transaction_key = $phone;
        $kegow->amount = ($amount/100);
        $kegow->cardID = $card['data']['CardID'];
        $kegow->transaction_type = 'CARD WITHDRAW';
        if ($kegow->save()) {
          file_put_contents($filename, "");
          return [
            'status' => true,
            'message' => 'Funds Withdrawal Successful',
            'data' => [
              'transaction_id' => $values[1],
              'amount_withdraw' => $amount,
              'card' => $card['data']['CardID'],
              'orderId' => $values[3],
            ],
          ];
        }
      }
      else {
        file_put_contents($filename, "");
        return [
          'status' => false,
          'message' => 'Funds Transfer UnSuccessful, Kegow Payment not Available. Please Try other Options',
        ];
      }
      curl_close($curl);
      fclose($fp);


  }
}

  public function getUserCardID($phone, $user_id) {
    $hasCard = KegowCardModel::findOne(['user_id' => $user_id]);
    // return $hasCard;
    if (count($hasCard) >= 1) {
      return [
        'status' => true,
        'message' => 'Card Details Pulled Successfully',
        'data' => [
          'CardID' => $hasCard->cardID,
        ],
      ];
    }
    if (substr( $phone, 0, 3 ) === "234") {
      $hash3 = Yii::$app->encryption->getEncryptionKeyThree([$phone,$this->merchantId,$this->privateKey]);
      $url = $this->baseUrl."getit/api/merchant/card.do?accountID=".$phone."&merchantID=".$this->merchantId."&encKey=".$hash3;
      $filename = getcwd(). '/curl_content.txt';
      if(file_exists($filename)){
        $fp = fopen($filename, 'w+');
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        fwrite($fp, $result);
        if( strpos(file_get_contents($filename),'result=30') !== false) {
          $file = file_get_contents('curl_content.txt');
          $values = [];
          $result = explode('&', $file);
          foreach ($result as $key) {
            $eachValues = explode('=', $key);
            array_push($values, $eachValues[1]);
          }
          $kegow = new KegowCardModel;
          $kegow->user_id = $user_id;
          $kegow->result = $values[0];
          $kegow->cardID = $values[1];
          $kegow->checkSUM = $values[2];
          if ($kegow->save()) {
            file_put_contents($filename, "");
            return [
              'status' => true,
              'message' => 'Card Details Pulled Successfully',
              'data' => [
                'CardID' => $values[1],
              ],
            ];
          }
        }
        else {
          file_put_contents($filename, "");
          return [
            'status' => false,
            'message' => 'Card Details Pull Unsuccessfully, Kegow not Available. Please Try Again',
            'data' => [$result,$user_id,$phone,$url]
          ];
        }
        curl_close($curl);
        fclose($fp);

      }
      else {
        return [
          'status' => false,
          'message' => 'Pull not Succesful, Write File not Found',
        ];
      }
    }
    else {
      return [
        'status' => false,
        'message' => 'Invalid Phone number. Phone number must start with 234',
      ];
    }
  }
  public function getWalletBalance($cardID)
  {
    $hash5 = Yii::$app->encryption->getEncryptionKeyThree([$cardID,$this->merchantId,$this->privateKey]);
    $url = $this->baseUrl."getit/api/merchant/balance.do?cardID=".$cardID."&merchantID=".$this->merchantId."&encKey=".$hash5;
    $filename = getcwd(). '/curl_content.txt';
    if(file_exists($filename)){
      $fp = fopen($filename, 'w+');
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_FAILONERROR, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($curl);
      fwrite($fp, $result);
      if( strpos(file_get_contents($filename),'result=30') !== false) {
        $file = file_get_contents('curl_content.txt');
        $values = [];
        $result = explode('&', $file);
        foreach ($result as $key) {
          $eachValues = explode('=', $key);
          array_push($values, $eachValues[1]);
        }
        if (count($values)) {
          file_put_contents($filename, "");
          return [
            'status' => true,
            'message' => 'Account Balance  Pulled Successfully',
            'data' => [
              'amount' => $values[1],
            ],
          ];
        }
      }
      else {
        file_put_contents($filename, "");
        return [
          'status' => false,
          'message' => 'Account Balance Pull Unsuccessfully, Kegow not Available. Please Try Again',
          'data' => [
            'amount' => 'Not Available',
          ],
        ];
      }
      curl_close($curl);
      fclose($fp);

    }
    else {
      return [
        'status' => false,
        'message' => 'Account Balance not Succesful, Write File not Found',
      ];
    }
  }

  public function VerifyBank($acctNum, $bankCode)
  {
    $merchantId = 577;
    $privateKey = '2B612340211D6D79F4D0C354220FD00D';
    $hash7 = Yii::$app->encryption->getEncryptionKeyFour([$bankCode,$merchantId,$acctNum,$privateKey]);
    // $url = "https://kegow.com/getit/api/merchant/verifybank.do?bankAccountNumber=3065546223&merchantID=577&bankCode=000016&encKey=A7DF1154478A93EFB9202EDE10F4CDDAE2DCF69802B99B37233F614F955BAF12";
    $url = $this->baseUrl."getit/api/merchant/verifybank.do?bankAccountNumber=".$acctNum."&merchantID=".$merchantId."&bankCode=".$bankCode."&encKey=".$hash7;
    $filename = getcwd(). '/curl_content.txt';
    if(file_exists($filename)){
      $fp = fopen($filename, 'w+');
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_FAILONERROR, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($curl);
      fwrite($fp, $result);
      if( strpos(file_get_contents($filename),'result=30') !== false) {
        $file = file_get_contents('curl_content.txt');
        $values = [];
        $result = explode('&', $file);
        foreach ($result as $key) {
          $eachValues = explode('=', $key);
          array_push($values, $eachValues[1]);
        }
        if (count($values)) {
          file_put_contents($filename, "");
          return [
            'status' => true,
            'message' => 'Bank Account Verification',
            'data' => [
              'accountname' => $values[1],
            ],
          ];
        }
      }
      else {
        file_put_contents($filename, "");
        return [
          'status' => false,
          'message' => 'Bank Verification Not Successful',
          // 'data' => $result,
          // 'url' => $url
        ];
      }
      curl_close($curl);
      fclose($fp);

    }
    else {
      return [
        'status' => false,
        'message' => 'Bank Verification not Succesful, Write File not Found',
      ];
    }
  }


  public function Transfertobank($acctNum, $bankSortCode, $amount, $user_id)
  {
    $merchantId = 577;
    $privateKey = '2B612340211D6D79F4D0C354220FD00D';
    $orderID = uniqid();
    $real_amount = $amount * 100;
    $hash7 = Yii::$app->encryption->getEncryptionKeySeven([$merchantId,$orderID,$real_amount,$this->currency,$acctNum,$bankSortCode,$privateKey]);
    // $url = "https://kegow.com/getit/api/merchant/verifybank.do?bankAccountNumber=3065546223&merchantID=577&bankCode=000016&encKey=A7DF1154478A93EFB9202EDE10F4CDDAE2DCF69802B99B37233F614F955BAF12";
    $url = $this->baseUrl."getit/api/merchant/bankmerchantwithdraw.do?bankAccountNumber=".$acctNum."&merchantID=".$merchantId."&bankSortCode=".$bankSortCode."&amount=".$real_amount."&currency=".$this->currency."&orderID=".$orderID."&encKey=".$hash7;
    $filename = getcwd(). '/curl_content.txt';
    if(file_exists($filename)){
      $fp = fopen($filename, 'w+');
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_FAILONERROR, true);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      $result = curl_exec($curl);
      fwrite($fp, $result);
      if( strpos(file_get_contents($filename),'result=30') !== false) {
        $file = file_get_contents('curl_content.txt');
        $values = [];
        $result = explode('&', $file);
        foreach ($result as $key) {
          $eachValues = explode('=', $key);
          array_push($values, $eachValues[1]);
        }
        $kegow = new KegowModel;
        $kegow->user_id = $user_id;
        $kegow->result = $values[0];
        $kegow->transactID = $values[2];
        $kegow->checkSUM = $values[1];
        $kegow->orderID = $orderID;
        $kegow->transaction_key = $acctNum;
        $kegow->amount = $amount;
        $kegow->transaction_type = 'BANK TRANSFER';

        if ($kegow->save()) {
          file_put_contents($filename, "");
          return [
            'status' => true,
            'message' => 'Transfer Successful',
            'data' => [
              'transacion_details' => $values,
            ],
          ];
        }
      }
      else {
        file_put_contents($filename, "");
        return [
          'status' => false,
          'message' => 'Transfer Not Successful',
        ];
      }
      curl_close($curl);
      fclose($fp);

    }
    else {
      return [
        'status' => false,
        'message' => 'Transfer not Succesful, Write File not Found',
      ];
    }
  }

}
?>
