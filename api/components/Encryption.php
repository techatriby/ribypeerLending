<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class Encryption extends Component {

  public $key = '';
  public $sha256 = '';
  public $merchantID = '';
  public $amount = '';
  public $orderID = '';
  public $accountID = '';
  public $encryption_type = '';

  function __construct($encryption_type = 'sha256')
  {
    $this->encryption_type = $encryption_type;
  }

  public function getEncryptionKeyThree(Array $value, $upperCase = true)
  {
    $value = $value[0] . $value[1] . $value[2];
    $this->key = hash($this->encryption_type, $value);
    return strtoupper($this->key);
  }

  public function getEncryptionKeyFour(Array $value, $upperCase = true)
  {
    $value = $value[0] . $value[1] . $value[2] . $value[3];
    $this->key = hash($this->encryption_type, $value);
    return strtoupper($this->key);
  }

  public function getEncryptionKeyFive(Array $value, $upperCase = true)
  {
    $value = $value[0] . $value[1] . $value[2] . $value[3] . $value[4];
    $this->key = hash($this->encryption_type, $value);
    return strtoupper($this->key);
  }

  public function getEncryptionKeySeven(Array $value, $upperCase = true)
  {
    $value = $value[0] . $value[1] . $value[2] . $value[3] . $value[4] . $value[5] . $value[6];
    $this->key = hash($this->encryption_type, $value);
    return strtoupper($this->key);
  }


}

 ?>
