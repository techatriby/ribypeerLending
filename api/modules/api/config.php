<?php
/**
 * Created by PhpStorm.
 * User: Judekayode
 * Date: 4/16/16
 * Time: 4:17 PM
 */


return [
    'modules' => [
        'v1' => [
            'class' => 'app\modules\api\modules\v1\Module',
        ],

    ],
    'components' => [
        'request' => [
            'class' => 'yii\web\Response',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
    ],

    'params' => [

    ]
];