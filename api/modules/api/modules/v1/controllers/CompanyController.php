<?php

namespace app\modules\api\modules\v1\controllers;

use app\modules\api\modules\v1\models\Company;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;

class CompanyController extends BaseController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'login'     => ['POST'],
                    'company'   => ['GET'],

                ],
            ],
        ]);
    }
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCompany(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $company = $this->getCompanies();

        if (!is_array($company)) {
            $message = "Error retrieving company name";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Company Fetched Successfully',
                'data' => $company
            ];
        }
    }

    public function actionCompanieswithid(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $company = $this->getCompaniesid();

        if (!is_array($company)) {
            $message = "Error retrieving company name";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Companies Fetched Successfully',
                'data' => $company
            ];
        }
    }

    public function getCompanies(){
        return Company::find()->select(['company_name'])->asArray()->all();
    }

    public function getCompaniesid(){
        return Company::find()->select(['company_id','company_name','company_address','company_info','is_active', 'is_approved'])->where(['is_active' => 1])->asArray()->all();
    }
}
