<?php

namespace app\modules\api\modules\v1\controllers;


use app\modules\api\common\models\FileUploader;
use app\modules\api\common\models\ImageFolder;
use app\modules\api\common\models\ResponseCode;
use app\modules\api\modules\v1\models\Borrow;
use app\modules\api\modules\v1\models\Bank;
use app\modules\api\modules\v1\models\PaymentOptionType;
use app\modules\api\modules\v1\models\BorrowReason;
use app\modules\api\modules\v1\models\BorrowerLenderApiKeyHelper;
use app\modules\api\modules\v1\models\EmployeeStatus;
use app\modules\api\modules\v1\models\Lend;
use app\modules\api\modules\v1\models\UserBankDetails;
use app\modules\api\modules\v1\models\UserBankDetailsApiKeyHelper;
use app\modules\api\modules\v1\models\UserLogin;
use app\modules\api\modules\v1\models\AdminLog;
use app\modules\api\modules\v1\models\UserPayment;
use app\modules\api\modules\v1\controllers\FPDF;
use app\modules\api\modules\v1\models\LoanNotes;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;
use app\modules\api\modules\v1\models\Company;
use app\modules\api\modules\v1\models\CompanyApiKeyHelper;
use app\modules\api\modules\v1\models\UserDetails;
use app\modules\api\modules\v1\models\UserDetailsApiKeyHelper;
use app\modules\api\modules\v1\models\UserLoginApiKeyHelper;
use yii\base\Exception;
use yii\web\User;
use Flutterwave\Bvn;
use Flutterwave\Flutterwave;


class UserController extends BaseController
{

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                                'create'    =>          ['POST'],
                                'updateprofile' => ['POST'],
                            ],
                        ],
        ]);
    }

    public function actionIndex()
    {

        return json_encode(['kayode']);


    }

    public function Generateloannote($borrow_id=null)
    {

      $apikey = Yii::$app->request->getQueryParam('key');
      $user_id = ApiAuthenticator::getUserIdFromKey($apikey);
      $user_details = UserDetails::find()->where(['user_login_id' => $user_id])->one();
      $company_details = Company::find()->where(['company.company_id' => $user_details['company_id'], 'user_details.user_type_id' => 2])->leftJoin('user_details', 'user_details.company_id = company.company_id')->one();
      $get_super_admin = UserDetails::find()->where(['user_details.company_id' => $user_details['company_id'], 'user_details.user_type_id' => 2])->one();

      $amount_to_pay = Yii::$app->request->post('amount_to_pay');
      $period = Yii::$app->request->post('period');
      $interest_rate = Yii::$app->request->post('interest_rate');
      $fixed_charge = Yii::$app->request->post('fixed_charge');
      $insurance_rate = Yii::$app->request->post('insurance_rate');
      $priority = Yii::$app->request->post('priority');
      $fine = Yii::$app->request->post('fine');
      $loan_amount = Yii::$app->request->post('loan_amount');

      $principal_repayment_holder = Yii::$app->request->post('principal_repayment_holder');
      $monthly_interest_holder = Yii::$app->request->post('monthly_interest_holder');
      $insurance_charges_holder = Yii::$app->request->post('insurance_charges_holder');
      $priority_charges_holder = Yii::$app->request->post('priority_charges_holder');
      $fines_charges_holder = Yii::$app->request->post('fines_charges_holder');
      $transaction_charges_holder = Yii::$app->request->post('transaction_charges_holder');
      $monthly = Yii::$app->request->post('monthly');
      $principal_charge = Yii::$app->request->post('principal_charge');

      $payment_total_charges = Yii::$app->request->post('payment_total_charges');
      $loan_repayment = Yii::$app->request->post('loan_repayment');
      $total_repayment_rate = Yii::$app->request->post('total_repayment_rate');
      $effrate = Yii::$app->request->post('effrate');

      $new_loan_note = new LoanNotes;
      $connection = Yii::$app->db;
      $transaction = $connection->beginTransaction();
      $new_loan_note->amount_to_pay = serialize($amount_to_pay);
      $new_loan_note->user_id = $user_id;
      $new_loan_note->period = serialize($period);
      $new_loan_note->interest_rate = serialize($interest_rate);
      $new_loan_note->fixed_charge = serialize($fixed_charge);
      $new_loan_note->insurance_rate = serialize($insurance_rate);
      $new_loan_note->priority = serialize($priority);
      $new_loan_note->fine = serialize($fine);

      $new_loan_note->principal_repayment_holder = serialize($principal_repayment_holder);
      $new_loan_note->monthly_interest_holder = serialize($monthly_interest_holder);
      $new_loan_note->insurance_charges_holder = serialize($insurance_charges_holder);
      $new_loan_note->priority_charges_holder = serialize($priority_charges_holder);
      $new_loan_note->fines_charges_holder = serialize($fines_charges_holder);
      $new_loan_note->transaction_charges_holder = serialize($transaction_charges_holder);
      $new_loan_note->monthly = serialize($monthly);
      $new_loan_note->principal_charge = serialize($principal_charge);

      $new_loan_note->payment_total_charges = serialize($payment_total_charges);
      $new_loan_note->loan_repayment = serialize($loan_repayment);
      $new_loan_note->total_repayment_rate= serialize($total_repayment_rate);
      $new_loan_note->effrate = serialize($effrate);
      $new_loan_note->borrow_id = $borrow_id;
      $new_loan_note->date_of_request = date('d/m/Y');
      $new_loan_note->date_disbursed = '0/0/0';

      if ($new_loan_note->save()) {
        $new_loan_note_id = $new_loan_note->loan_note_id;
        // return $new_loan_note_id;
      $transaction->commit();
      $pdf = new FPDF();
      $pdf->AddPage();
      $name = strtoupper($user_details->firstname." ".$user_details->lastname);
      $date = date('l \, jS \of F Y');
      $pdf->SetTitle($name.' Loan Note - '.$date);
      $pdf->SetFont('Times','B');
      $pdf->SetFontSize(20);
      $pdf->Cell(40,10,$name);
      $pdf->Ln(10);
      $pdf->SetFont('Times','',14);
      $pdf->SetTextColor(0);
      $pdf->SetFontSize(12);
      $pdf->Cell(30,5,'Loan Note',0,1);
      $pdf->Ln(5);
      $pdf->SetFontSize(9);
      $pdf->SetTextColor(0);
      $pdf->Cell(75,5,"Date: ".$date,0,0);
      $eir = $effrate."%";
      $pdf->Cell(73,5,'Effective Interest Rate: '.$eir,1,0,'C');
      $pdf->Cell(42,5,'Total Repayment: N'.number_format($amount_to_pay, 2),1,1,'C');

      $pdf->Ln(5);
      $loan_type = ucfirst('Standard');
      if ($priority == 0.085) {
        $loan_type = ucfirst('Priority');
      }

      $pdf->Cell(75,5,'Loan Type: '.$loan_type,0,0);
      $tr = number_format(89063);
      $pdf->Cell(73,5,'Total Repayment Rate Inclusive of All Charges: '.$total_repayment_rate.'%',1,0,'L');
      $pdf->Cell(42,5,'Loan Repayment: N'.number_format($loan_repayment,2),1,1);
      $pdf->SetTextColor(0);

      $pdf->Cell(75,5,'',0,0);
      $pdf->Cell(73,5,'Loan Tenure in Months: '.$period,1,0,'C');
      $pdf->Cell(42,5,'Total Charges: N'.number_format($payment_total_charges,2),1,1,'C');
      $pdf->Ln(5);

      $pdf->SetFontSize(20);
      $pdf->SetTextColor(182, 40, 38);


      $pdf->Cell(60,5,'',0,0);
      $pdf->Cell(70,5,'PRELIMINARY LOAN NOTE',0,0,'C');
      $pdf->Cell(42,5,'',0,1,'C');

      $pdf->SetFontSize(9);
      $pdf->SetTextColor(0);

      $pdf->Cell(190,5,'Loan Amount',0,1,'R');
      $pdf->SetFontSize(20);
      $loan_amount = number_format($loan_amount);
      $pdf->Cell(190,5,'N'.$loan_amount,0,1,'R');
      $pdf->SetFontSize(9);

      $pdf->Image(getcwd().'/logo.png',150,10,-100);
      $pdf->Ln(5);
      $pdf->SetFont('Times','',9);

      /** Loan Details Configurations **/

      $pdf->Cell(30,15,'Description',1,0,'C');
      $tr = number_format(95875);
      $pdf->Cell(30,15,'Amount / Rate',1,0,'C');
      $pdf->Cell(100,10,'Repayments',1,0,'C');
      $pdf->SetFillColor(230, 119, 22);
      $pdf->Cell(30,20,'Total',1,0,'C','***1***');
      $pdf->Ln(10);
      $pdf->Cell(30,5,'',0,0,'C');
      $pdf->Cell(30,5,'',0,0,'R');
      $i = 0;
      $len = count($monthly);
      $rW = 100/$len;
      foreach ($monthly as $key => $value) {
        if(++$i === $len) {
          $pdf->Cell($rW,5,($key+1),1,1,'C');
        }
        else {
          $pdf->Cell($rW,5,($key+1),1,0,'C');
        }
      }

      $pdf->SetFillColor(131, 146, 132);
      $pdf->Cell(30,5,'Principal',1,0,'C','***1***');
      $principal = $loan_amount;
      $pdf->Cell(30,5,'N'.$principal,1,0,'R','***1***');
      $i = 0;
      $len = count($principal_charge);
      $rW = 100/$len;
      foreach ($principal_charge as $key => $value) {
        if(++$i === $len) {
          $pdf->Cell($rW,5,'N'.number_format($value,2),1,1,'C','***1***');
        }
        else {
          $pdf->Cell($rW,5,'N'.number_format($value,2),1,0,'C','***1***');
        }
      }


      $pdf->SetFillColor(131, 146, 132);
      $pdf->Cell(30,10,'Principal Repayment',1,0,'C');
      $pdf->Cell(30,10,$period,1,0,'R');
      foreach ($principal_repayment_holder as $key => $value) {
          $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
      }
      $principal_total = number_format(array_sum($principal_repayment_holder), 2);
      $pdf->Cell(30,10,$principal_total,1,1,'R');

      $pdf->Cell(30,10,'Monthly Interest Rate',1,0,'C');
      $pdf->Cell(30,10,($interest_rate*100).'%',1,0,'R');
      foreach ($monthly_interest_holder as $key => $value) {
          $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
      }
      $monthly_int_total = number_format(array_sum($monthly_interest_holder), 2);
      $pdf->Cell(30,10,$monthly_int_total,1,1,'R');

      $pdf->Cell(30,10,'Priority Interest',1,0,'L');
      $pdf->Cell(30,10,($priority*100).'%',1,0,'R');
      $tr = number_format(89063);
      foreach ($priority_charges_holder as $key => $value) {
          $pdf->Cell($rW,10,number_format($value,2),1,0,'R');
      }
      $priority_total = number_format(array_sum($priority_charges_holder), 2);
      $pdf->Cell(30,10,$priority_total,1,1,'R');

      $pdf->Cell(30,10,'Fines',1,0,'L');
      $pdf->Cell(30,10,($fine*100).'%',1,0,'R');
      foreach ($fines_charges_holder as $key => $value) {
          $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
      }
      $fines_total = number_format(array_sum($fines_charges_holder), 2);
      $pdf->Cell(30,10,$fines_total,1,1,'R');

      $pdf->Cell(30,10,'Transaction Charges',1,0,'L');
      $pdf->Cell(30,10,'N'.$transaction_charges_holder[0],1,0,'R');
      foreach ($transaction_charges_holder as $key => $value) {
          $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
      }

      $transaction_charges_total = number_format(array_sum($transaction_charges_holder), 2);
      $pdf->Cell(30,10,'N'.$transaction_charges_total,1,1,'R');

      $pdf->Cell(30,10,'Assurance Charges',1,0,'L');
      $pdf->Cell(30,10,($insurance_rate*100).'%',1,0,'R');
      $tr = number_format(89063);
      foreach ($insurance_charges_holder as $key => $value) {
          $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
      }
      $assurance_charges_total = number_format(array_sum($insurance_charges_holder), 2);
      $pdf->Cell(30,10,'N'.$assurance_charges_total,1,1,'R');

      $pdf->Cell(60,10,'Monthly/Total Repayments',1,0,'L');
      $pdf->SetFillColor(131, 146, 132);
      foreach ($monthly as $key => $value) {
          $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
      }
      $pdf->SetFillColor(228, 24, 9);
      $montly_repayment_total = number_format($amount_to_pay, 2);
      $pdf->Cell(30,10,'N'.$montly_repayment_total,1,1,'R','***1***');
      $pdf->Ln(5);
      $pdf->SetFillColor(255,255,255);
      $pdf->Cell(70,5,'Approvals, Repayment Mode, Ts&Cs:',0,0,'L');
      $pdf->Cell(90,5,'All loans / lending are based on an intra-company peer-to-peer marketplace where one',0,1);
      $pdf->Cell(70,5,'',0,0,'L');
      $pdf->Cell(90,5,'colleague lends to another. Loans are approved by the Human Resources and Accounts ',0,1);
      $pdf->Cell(70,5,'',0,0,'L');
      $pdf->Cell(90,5,'Department and Repayment are done through Salary Deductions. By accepting this loan,',0,1);
      $pdf->Cell(70,5,'',0,0,'L');
      $pdf->Cell(90,5,'you hereby also accept all applicable charges and rates stated above and the terms and ',0,1);
      $pdf->Cell(70,5,'',0,0,'L');
      $pdf->Cell(90,5,'conditions of service',0,1);
      $pdf->Cell(70,5,'',0,1,'L');
      $pdf->Cell(40,5,'For Service & Inquiries:',1,1,'C');
      $pdf->Cell(40,5,$company_details->company_name,1,0,'L');
      $pdf->Cell(40,5,'Operations',1,0,'L');
      $pdf->Cell(60,5,$get_super_admin->email,1,0,'L');
      $pdf->Cell(40,5,$get_super_admin->phone_number,1,1,'L');

      $pdf->Cell(40,5,$company_details->company_name,1,0,'L');
      $pdf->Cell(40,5,'Operations',1,0,'L');
      $pdf->Cell(60,5,'-',1,0,'L');
      $pdf->Cell(40,5,'-',1,1,'L');


      $pdf->Cell(40,5,'Platform Service by Riby',1,0,'L');
      $pdf->Cell(40,5,'Operations Manager',1,0,'L');
      $pdf->Cell(60,5,'peerlending@riby.me',1,0,'L');
      $pdf->Cell(40,5,'01.291.4247',1,0,'L');

      $getDate = date('jS \, F Y');
      $fileBuffer = 'PRELIM Loan Note - '.$name.'-'.$date.'.pdf';
      $filename= getcwd().'/loannotes/'.$fileBuffer;
      $pdf->Output($filename, 'F');
      return [$fileBuffer, $fileBuffer,$new_loan_note_id];

    }
    else {
      exit();
    }

    }
    public function actionGetusercardid()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyApiKey();

      $access_token = ApiKeyHelper::ACCESS_TOKEN;
      $get_access_token = $this->checkApiKey();
      $user_id = Yii::$app->request->getQueryParam('user_id');
      $user_details = UserDetails::findOne(['user_login_id' => $user_id]);
      if (count($user_details) > 0) {
        /** Confirm Payment to User Account **/
        $getCard = Yii::$app->kegowloader->getUserCardID($user_details->phone_number, $user_id);
        return $getCard;

      }
      else {
        return
            [
                'status' => false,
                'message' => 'User Not Found',
                'data' => null
            ];
      }
      return $getCard;
    }

    public function actionGetwalletbalance()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyApiKey();

      $access_token = ApiKeyHelper::ACCESS_TOKEN;
      $get_access_token = $this->checkApiKey();
      $user_id = Yii::$app->request->getQueryParam('user_id');
      // $user_id = Yii::$app->request->post('user_id');
      $user_details = UserDetails::findOne(['user_login_id' => $user_id]);
      if (count($user_details) > 0) {
        /** Confirm Payment to User Account **/
        $getCard = Yii::$app->kegowloader->getUserCardID($user_details->phone_number, $user_id);
        if (isset($getCard['data'])) {
          return Yii::$app->kegowloader->getWalletBalance($getCard['data']['CardID']);
        }
        else {
          return $getCard;
        }

      }
      else {
        return
            [
                'status' => false,
                'message' => 'User Not Found',
                'data' => null
            ];
      }


    }

    public function actionGetallbank()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyApiKey();
      $banks = Bank::find()->all();
      return [
          'status' => true,
          'message' => 'Bank Fetched Successfully',
          'data' => $banks
      ];
    }

    public function actionGetpaymentoptiontype()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyApiKey();
      $payment_option_id = Yii::$app->request->getQueryParam('payment_option_id');
      $banks = PaymentOptionType::find(['payment_option_id' => $payment_option_id])->all();
      return [
          'status' => true,
          'message' => 'Payment Options fetched successfully',
          'data' => $banks
      ];
    }

    public function actionVerifybank()
    {
      // $this->setHeader(200);
      // ApiAuthenticator::verifyApiKey();
      // return "HEY";
      $account = Yii::$app->request->getQueryParam('account');
      $bankCode = Yii::$app->request->getQueryParam('bankcode');
      $getCard = Yii::$app->kegowloader->VerifyBank($account, $bankCode);
      return $getCard;
    }

    public function actionVerifybvn()
    {
      $merchantKey = "tk_PIwcm06Nh3"; //can be found on flutterwave dev portal
      $apiKey = "tk_CYcyIq1g0h3tO9UHNXQf"; //can be found on flutterwave dev portal
      $env = "staging"; //this can be production when ready for deployment
      Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
      $session = Yii::$app->session;
      $bvn = Yii::$app->request->getQueryParam('bvn');
      if (!isset($bvn)) {
        return [
            'status' => false,
            'message' => 'BVN Missing from Request',
            'data' => null
        ];
      }
      // Wale
      // $bvn = "22143074076";
      //WIlliams
      // $bvn = "22370072647";
      if (!$session->has('ongoing_validation')) {
        $result = Bvn::verify($bvn, Flutterwave::SMS); //this will send otp to the telephone used for the bvn registration
        //methods like getResponseData(), getStatusCode(), getResponseCode(), isSuccessfulResponse()
        if ($result->isSuccessfulResponse()) {
          $session->set('ongoing_validation', 1);
          $session->set('tras_ref', $result->getResponseData()['data']['transactionReference']);
          // print_r($result->getResponseData());

          return [
              'status' => true,
              'message' => 'BVN was Successfully Confirmed, OTP Sent',
              'data' => $result->getResponseData(),
          ];
        } else {
          return [
              'status' => false,
              'message' => 'Incorrect BVN',
              'data' => null
          ];
        }
      }
      else {
        $otp = Yii::$app->request->post('otp');
        if (!$session->has('tras_ref')) {
            return [
                'status' => false,
                'message' => 'Missing Transaction Reference',
                'data' => null
            ];
          }
          else {
            if (!isset($otp) || empty($otp)) {
              return [
                  'status' => false,
                  'message' => 'Missing OTP in Request',
                  'data' => null
              ];
            }
            else {
              $transactionRef = $session->get('tras_ref');
              // return [$transactionRef, $otp, $bvn];
              $result = Bvn::validate($bvn, $otp, $transactionRef);
              if ($result->isSuccessfulResponse()) {
                $session->remove('ongoing_validation');
                return [
                    'status' => true,
                    'message' => 'Authentication Completed',
                    'data' => $result
                ];
            }
            else {
              $session->remove('ongoing_validation');
              return [
                  'status' => false,
                  'message' => 'Authentication could not be completed, Incorrect OTP. Please try Again',
                  'data' => $result
              ];
            }
          }

        }
      }

    }

    public function actionVerifybvnwithotp()
    {

      $merchantKey = "tk_PIwcm06Nh3"; //can be found on flutterwave dev portal
      $apiKey = "tk_CYcyIq1g0h3tO9UHNXQf"; //can be found on flutterwave dev portal
      $env = "staging"; //this can be production when ready for deployment
      Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);



    }

    public function actionResendbvnotp()
    {
      $merchantKey = "tk_PIwcm06Nh3"; //can be found on flutterwave dev portal
      $apiKey = "tk_CYcyIq1g0h3tO9UHNXQf"; //can be found on flutterwave dev portal
      $env = "staging"; //this can be production when ready for deployment
      Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);

      $transactionRef = Yii::$app->request->post('transaction_reference');

      $result = Bvn::resendOtp($transactionRef, Flutterwave::SMS);
      return $result;
    }

    public function actionUpdateprofile()
    {
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsValuePair = [
            UserDetailsApiKeyHelper::TITLE => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::FIRSTNAME => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::LASTNAME => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::MIDDLENAME => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::PHONE_NUMBER => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::ADDRESS => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::BIRTHDATE => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::GENDER => ApiUtility::TYPE_STRING,
            UserDetailsApiKeyHelper::EMPLOYEE_STATUS_ID => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsValuePair, $params);

        $firstname = $params[UserDetailsApiKeyHelper::FIRSTNAME];
        $lastname = $params[UserDetailsApiKeyHelper::LASTNAME];
        $midname = $params[UserDetailsApiKeyHelper::MIDDLENAME];
        $phone = $params[UserDetailsApiKeyHelper::PHONE_NUMBER];
        $address = $params[UserDetailsApiKeyHelper::ADDRESS];
        $title = $params[UserDetailsApiKeyHelper::TITLE];
        $gender = $params[UserDetailsApiKeyHelper::GENDER];
        $birthdate = $params[UserDetailsApiKeyHelper::BIRTHDATE];
        $employee_status_id = $params[UserDetailsApiKeyHelper::EMPLOYEE_STATUS_ID];

        $get_access_token = $this->checkApiKey();
        //$get_access_token = $params[UserLoginApiKeyHelper::ACCESS_TOKEN];
        $update = $this->updateUsers($get_access_token, $firstname, $lastname, $midname, $phone, $address, $title, $gender, $birthdate, $employee_status_id);

        // saveAudit()

        if ($update)
        {
            return [

                'status' => true,
                'message' => 'Profile updated',

            ];
        }
        else
        {
            return[

                'status' => false,
                'message' => 'Error: Profile not updated, Please try again',


            ];
        }

    }

    public function actionGetuserdetail(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        $userdetail = $this->getUserDetails($user->user_id);
        //saveAudit($user->user_id,"New user Log in",$user->company_id);

        if (!is_array($userdetail)) {
            $message = "Error retrieving User";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'User Fetched Successfully',
                'data' => $userdetail
            ];
        }
    }

   //    public function actionPaymentDeadLine(){


  //  }

    public function actionGetemployeestatus(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

       $employeestatus = EmployeeStatus::find()->select(['employee_id', 'employee_status_name'])->asArray()->all();

        if (!is_array($employeestatus)) {
            $message = "Error retrieving Employee Status";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Employee Status Fetched Successfully',
                'data' => $employeestatus
            ];
        }
    }

    public function actionConfirmpassword()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyApiKey();
      $params = $_REQUEST;
      $access_token = ApiKeyHelper::ACCESS_TOKEN;

      $paramsPairValue = [
          UserLoginApiKeyHelper::USER_PASSWORD  => ApiUtility::TYPE_STRING,
      ];

      $this->paramCheck($paramsPairValue, $params);

      $get_access_token = $this->checkApiKey();
      $password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
      $password =  ApiUtility::generatePasswordHash($password);
      $get_password = UserLogin::findOne(['access_token' => $get_access_token, 'user_password' => $password]);
        if(count($get_password) > 0){
            return [
              'status' => true,
              'message' => 'Password Confirmed'
            ];
        }else{
          return [
            'status' => false,
            'message' => 'Incorrect Passsword'
          ];
        }
    }

    public function actionChangepassword()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $passwordlength = 7;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsPairValue = [
            UserLoginApiKeyHelper::USER_PASSWORD  => ApiUtility::TYPE_STRING,
            UserLoginApiKeyHelper::CONFIRM_PASSWORD  => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsPairValue, $params);

        $get_access_token = $this->checkApiKey();
        $password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
        $cpassword = $params[UserLoginApiKeyHelper::CONFIRM_PASSWORD];

        if(strlen($password)>=$passwordlength){
            if($password == $cpassword){
                $password =  ApiUtility::generatePasswordHash($password);

                $upass = $this->updatePassword($get_access_token, $password);
                // return [
                //   $upass, $get_access_token
                // ];
                if($upass){
                    $this->setHeader(200);
                    return[
                        'status' => true,
                        'message' => 'Password Updated',
                    ];
                }else{
                    return[
                        'status' => false,
                        'message' => 'Password Not Updated',
                    ];
                }

            }else{
                return[
                    'status' => false,
                    'message' => 'Passwords do not match',
                ];
            }
        }else{
            return[
                'status' => false,
                'message' => 'Password Length must be at least 7 characters',
            ];
        }
    }

    public function actionLend(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
//        die($_REQUEST);
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsPairValue = [
            BorrowerLenderApiKeyHelper::LEND_AMOUNT => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::AMOUNTTORECEIVE => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsPairValue, $params);

        $get_access_token = $this->checkApiKey();
        $lend_amount = $params[BorrowerLenderApiKeyHelper::LEND_AMOUNT];
        $amount_received = $params[BorrowerLenderApiKeyHelper::AMOUNTTORECEIVE];
        $lend_tenure = Yii::$app->request->post('period');
        $user = UserLogin::findOne(['access_token'=>$get_access_token]);
        $userdetails = UserDetails::findOne(['user_login_id'=>$user->user_login_id]);
        $company = Company::findOne(['company_id'=>$userdetails['company_id']]);
        $account = UserBankDetails::findOne(['user_id'=>$userdetails['user_id']]);
        $account_id = $account['bvn'];
        $transaction_code = rand(0,1000000);
        $company_id = $userdetails['company_id'];
        $user_id = $userdetails['user_id'];
        $transaction_id = $company_id.$user_id.$transaction_code;
        $currency = "NGN";

        $encript = $account_id.$lend_amount.$currency.$transaction_id."1086AC8958C6A237092437CBA0FDE870";
        $sha256 = hash('sha256',$encript);
        //$sha256 = crypt($encript,'$5$rounds=5000$ribypeerlending$');

      //   saveAudit($user->user_id,"New Lending record",$user->company_id);
        //$lend = new BorrowerLender();
        $checklend = $this->saveLendamount($lend_amount, $amount_received, $userdetails->user_id, $userdetails->company_id, $lend_tenure);

        // return $checklend;
        if($checklend){
            $this->setHeader(200);

            $message = "Your request to Lend ".$lend_amount." on our platform has been received. <br>
                        Your request is being processed and you will be contacted within the next 24hrs.<br/>
                                                Thank you.<br/><br>
                                                 ";


            $this->sendEmail('mailtemplate',$this->getLendEmail(),$message,'Request To Lend | RibyPeerLending', $userdetails->email, $this->getLendEmail(),$userdetails->email,$company['company_name']);


            return[
                'status' => true,
                'message' => 'Lending Successful',
                'data' => $sha256,
                'encript' => $encript,
            ];
        }else{

            return[
                'status' => false,
                'message' => 'Oops!, Please try Again',
            ];
        }
    }

    public function actionBorrow(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();
        $log = new AdminLog();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $userbankdetails = UserBankDetails::find()->where(['user_id'=>$userlog->user_login_id])->asArray()->one();
        if (count($userbankdetails) <= 0) {
          return[
                'status' => false,
                'message' => 'Please update your bank details, you cannot apply for this loan',
                'data' => null,
            ];
        }
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        $userdetail = $this->getUserDetails($user->user_id);
        $company_id = $userdetail['company_id'];
        $user_id = $userdetail['user_id'];
        $transaction_code = rand(0,1000000);


        $Transaction_ID = $company_id.$user_id.$transaction_code;

        // return $Transaction_ID;
        $AllLog = (new \yii\db\Query())
        ->select(['*'])
        ->from('admin_log')
        ->all();

        $admin_log_id = count($AllLog) + 1;


        $log->user_id = $user->user_id;
        $log->admin_log_details = "New Loan Requested";
        $log->company_id = $user->company_id;
        $log->admin_log_id = $admin_log_id;
        $log->transaction_id =  $Transaction_ID;


        // Check if all user details is complete. Needed for Kegow KYC
        $userdetails = $this->getUserDetails($user->user_id);
        // return $userdetails['phone_number'];
        // if (empty($userdetails['phone_number'])) {
        //   return[
        //       'status' => false,
        //       'message' => 'Phone number cannot be empty, Please update your profile',
        //       'data' => null,
        //   ];
        // }
        //
        // if (empty($userdetails['address'])) {
        //   return[
        //       'status' => false,
        //       'message' => 'Address cannot be empty, Please update your profile',
        //       'data' => null,
        //   ];
        // }
        //
        // if (empty($userdetails['city'])) {
        //   return[
        //       'status' => false,
        //       'message' => 'City cannot be empty, Please update your profile',
        //       'data' => null,
        //   ];
        // }

        //ONly proceed if user has no active or pending approval loan
        $active_loan = $this->getuseractiveloan($user->user_id);
        $pending_loan = $this->getUserLoanHistory($user->user_id);

        $active = 0;
        foreach($pending_loan as $one){
            if($one['status_id'] == 2 && $one['approve_flag'] != 4)$active++;
        }
        $log->save();
        if($active > 0 || isset($active_loan)) {
            $msg = 'Loan not created, Your already have an active/pending Loan.';

            return ApiUtility::errorResponse($msg);
                }else{
                     $log->save();
                     // return;
                }

        $paramsPairValue = [
            BorrowerLenderApiKeyHelper::LOAN_AMOUNT => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::AMOUNTTOPAY => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::PERIOD => ApiUtility::TYPE_STRING,
            // BorrowerLenderApiKeyHelper::DATETOPAY => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::INSURANCE_RATE => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::INTEREST_RATE => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::FIXED_CHARGE => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::FINE => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::PRIORITY => ApiUtility::TYPE_STRING,
            // BorrowerLenderApiKeyHelper::REASON_ID => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsPairValue, $params);

        $get_access_token = $this->checkApiKey();
        $loan_amount = $params[BorrowerLenderApiKeyHelper::LOAN_AMOUNT];
        $amount_to_pay = $params[BorrowerLenderApiKeyHelper::AMOUNTTOPAY];
        $period = $params[BorrowerLenderApiKeyHelper::PERIOD];
        //$date_to_pay = $params[BorrowerLenderApiKeyHelper::DATETOPAY];
        $date_to_pay = date("Y-m-d H:i:s");;
        $insurance_rate = $params[BorrowerLenderApiKeyHelper::INSURANCE_RATE];
        $interest_rate = $params[BorrowerLenderApiKeyHelper::INTEREST_RATE];
        $fixed_charge = $params[BorrowerLenderApiKeyHelper::FIXED_CHARGE];
        $fine = $params[BorrowerLenderApiKeyHelper::FINE];
        $priority = $params[BorrowerLenderApiKeyHelper::PRIORITY];
        // $reason_id = $params[BorrowerLenderApiKeyHelper::REASON_ID];
        $reason_id = Yii::$app->request->post('reason_id');
        $other_reasons = Yii::$app->request->post('other_reasons');

        $user = UserLogin::findOne(['access_token'=>$get_access_token]);
        $userdetails = UserDetails::find(['company_id',  'user_id'])->where(['user_login_id'=>$user->user_login_id])->one();


        $loan_note = $this->Generateloannote();
        // return $loan_note;
        $lend = $this->saveBorrowamount($loan_amount, $amount_to_pay, $interest_rate, $insurance_rate, $fixed_charge, $fine, $priority, $userdetails->user_id, $userdetails->company_id, $period, $date_to_pay,$Transaction_ID,$reason_id,$other_reasons,$loan_note[0],$loan_note[2]);

        if($lend['status']){
            $admin = UserDetails::find()->innerJoin('user_login','user_details.user_login_id=user_login.user_login_id')->where(['company_id'=>$userdetails['company_id'],'user_details.user_type_id'=>2])->asArray()->one();
            $company = Company::findOne(['company_id'=>$userdetails['company_id']]);
            $time = date('Y-m-d H:i:s');

            //approve email to Admin ;

            $message = "A new loan request has been sent for approval.<br><br>
                            Amount: $loan_amount<br>
                            Date Requested: $time<br><br>
                            Kindly login to approve request<br/>
                            ";
            $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan awaiting Approval', $admin['email'], $this->getReplyEmail(), $admin['email'],$company['company_name']);


            $processed_message = "Your loan request of ₦ {$loan_amount} has been Queued!.<br>
            You will receive a confirmation email immediately your loan is confirmed. <br/>
            If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a><br/>
            Please check attached file your loan note.<br/>
            Thank you for using Peerlengine<br/><br>
            ";


            $this->sendEmail('mailtemplate', $this->getReplyEmail(), $processed_message, 'Loan Processing', $userdetails->email, $this->getReplyEmail(), $userdetails->email, $company['company_name'],$loan_note[1]);


            $this->setHeader(200);
            return[
                'status' => true,
                'message' => 'Your Transaction is being processed',
                'data' => null,
            ];
        }else{

            return[
                'status' => false,
                'message' => $lend['message'],
            ];
        }
    }

    public function actionGetborrowreason()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyApiKey();
        $reasons = BorrowReason::find()->asArray()->all();
        if (is_array($reasons)) {
          return
              [
                  'status' => true,
                  'message' => 'Reason fetched successfully',
                  'data' => $reasons
              ];
        }
        else {
          return
              [
                  'status' => false,
                  'message' => 'Error Getting Reasons, Please try again',
                  'data' => null
              ];
        }
    }

    public function actionUpdatepicture()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $apikey = Yii::$app->request->getQueryParam('key');
        $user_id = ApiAuthenticator::getUserIdFromKey($apikey);

        $fileuploader = new FileUploader('uploads/user');

        $checkset = $fileuploader->verifyIsset('user_pics');
        if($checkset) {
            $check = $fileuploader->verifyFileSizeIndependent('user_pics');
            if ($check == ResponseCode::FILE_SIZE_OK && $fileuploader->verifyFileExtension($_FILES['user_pics']['name'])) {
                $uploads = $fileuploader->upload('user_pics', ImageFolder::USER_PROFILE.'_' . $user_id);
                if($uploads == ResponseCode::FILE_UPLOAD_SUCCESSFUL)
                {
                    $userdetails = UserDetails::findOne(['user_login_id'=> $user_id]);

                    if($userdetails)
                    {
                        $file_name = $_FILES['user_pics']['name'];
                        $extension = substr($file_name, strpos($file_name, '.') + 1);
                        $extension = strtolower($extension);

                        $userdetails->user_image_url =  ImageFolder::USER_PROFILE.'_'.$user_id.'.'.$extension;
                        $userdetails->update();

                        $this->setHeader(200);
                        return [
                            'status'=> true,
                            'message'=> 'user image updated successfully',
                            'data'=> null
                        ];
                    }
                }
                else {

                }

            } else {
                $msg = "Error, image size must not exceed 3MB. Also, check that you are uploading a valid image with extension of either
                    jpeg, png, gif, JPEG";

                return ApiUtility::errorResponse($msg);
            }

        }
        else{
            return ApiUtility::errorResponse("Image not successfully saved,Please try again");
        }

    }

    public function fetchuserimage($user_id)
    {
        $user = UserDetails::find()->select(['CONCAT("http://localhost/ribypeerlending/api/web/uploads/user/",user_image_url) AS user_image'
        ])
            ->where(['user_login_id' => $user_id])
            ->asArray()->one();
        ;

        return $user;
    }

    public function actionUserimage()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $user_id = ApiAuthenticator::getUserIdFromKey($apikey);

        $userimage = $this->fetchuserimage($user_id);
        if(!empty($userimage))
        {
            return [
                'status' => true,
                'message'=> "User image fetched successfully",
                'data'=> $userimage
            ];
        }
        else
        {
            $errormsg = "Error fetching user image";
            return ApiUtility::errorResponse($errormsg);
        }

    }

    public function actionGetuserbankdetails(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $user = UserLogin::findOne(['access_token'=>$get_access_token]);
        // $userdetail = UserDetails::findOne(['user_login_id'=>$user->user_login_id]);
        $userbankdetails = UserBankDetails::findOne(['user_id'=>$user->user_login_id]);
        // return $userbankdetails;
        if (!is_object($userbankdetails)) {
            $message = "Error getting User Bank details";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'User Bank details Fetched Successfully',
                'data' => $userbankdetails
            ];
        }

    }

    public function actionGetuserloanhistory(){

        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $user = UserLogin::findOne(['access_token'=>$get_access_token]);
        $userdetail = UserDetails::findOne(['user_login_id'=>$user->user_login_id]);
        $userhist = $this->getUserLoanHistory($userdetail->user_id);

        if (!is_array($userhist)) {
            $message = "Error retrieving Loan";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Loan History Fetched Successfully',
                'data' => $userhist,
            ];
        }

    }

    public function actionGetuserlendhistory(){

        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $user = UserLogin::findOne(['access_token'=>$get_access_token]);
        $userdetail = UserDetails::findOne(['user_login_id'=>$user->user_login_id]);
        $userhist = $this->getUserLendHistory($userdetail->user_id);

        if (!is_array($userhist)) {
            $message = "Error retrieving Lend history";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Lend History Fetched Successfully',
                'data' => $userhist
            ];
        }

    }

    public function actionGettotaluserborrow(){

        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $info = array();
        $user = UserLogin::findOne(['access_token'=>$get_access_token]);
        $userdetail = UserDetails::findOne(['user_login_id'=>$user->user_login_id]);
        $numberofborrow = $this->getUserTotalBorrow($userdetail->user_id);
        $lasttransact = $this->getBorrowLastTransaction($userdetail->user_id);

        $info["total_amount_of_borrow"] = $numberofborrow;
        $info["last_transaction_date"] = $lasttransact["date_of_request"];

        if($numberofborrow<0 && !is_array($info)){
            $message = "Error retrieving Total Borrow";
            return ApiUtility::errorResponse($message);
        }
        if ($numberofborrow==0 && !is_array($info)) {
            return [
                'status' => true,
                'message' => 'Total Borrow is Zero',
                'data' => $info
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Fetched Successfully',
                'data' => $info
            ];
        }

    }

    public function actionGettotaluserlend(){

        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $info = array();
        $user = UserLogin::findOne(['access_token'=>$get_access_token]);
        $userdetail = UserDetails::findOne(['user_login_id'=>$user->user_login_id]);
        $numberoflend = $this->getUserTotalLend($userdetail->user_id);

        $lasttransact = $this->getLendLastTransaction($userdetail->user_id);
        $info["total_amount_of_lend"] = $numberoflend;
        $info["last_transaction_date"] = $lasttransact["date_lend"];
        if($numberoflend<0 && !is_array($info)){
            $message = "Error retrieving Total Lend";
            return ApiUtility::errorResponse($message);
        }
        if ($numberoflend==0 && !is_array($info)) {
            return [
                'status' => true,
                'message' => 'Total Lend is Zero',
                'data' => $info
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Lend Fetched Successfully',
                'data' => $info
            ];
        }

    }

    /*
    public function actionResetpassword()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsValuePair = [
            UserLoginApiKeyHelper::USER_EMAIL => ApiUtility::TYPE_STRING
        ];
        $this->paramCheck($paramsValuePair, $params);

        $email = $params[UserLoginApiKeyHelper::USER_EMAIL];

        $user = UserLogin::findOne(['user_email'=>$email]);

        if($user){
            if($user->status_id == 3){
                $errormsg = "Sorry, your account has not been activated, continue to check within the next 24 hours";

                return ApiUtility::errorResponse($errormsg);
            }

            else if($user->status_id == 2) {
                $curdate = date(time());
                $expirydate = strtotime('+4 hours',$curdate);
                $code = sha1(rand(100000,1000000));
                $verification = generateverificationcode();
                $message = "";
                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message, "Request For Password Reset", $email, $this->getReplyEmail(), $email);
                $access_token = $user->access_token;
                $user_type_id = $user->user_type_id;
                $username = $user->user_email;
                return [
                    'status' => true,
                    'message' => "Check your email",
                    'data' => [
                        'user_access_token' => $access_token,
                        'user_type_id'=> $user_type_id,
                        'user_email' => $username
                    ]
                ];
            }
            else{
                $errormsg = "sorry, you do not have an account with us";
                return ApiUtility::errorResponse($errormsg);
            }
        }
        else
        {
            $msg = "Invalid username/password";
            return ApiUtility::errorResponse($msg);
        }

    }
    */

    public function saveLendamount($lend_amount, $amount_received, $user_id, $company_id, $lend_tenure){
        $lend = new Lend();
        date_default_timezone_set('Africa/Lagos');
        $time = date('Y-m-d H:i:s');
        $lend->lend_amount = $lend_amount;
        $lend->amount_to_receive = (int)$amount_received;
        $lend->company_id = $company_id;
        $lend->user_id = $user_id;
        $lend->status_id = 2;
        $lend->approve_flag = 1;
        $lend->modified_date = $time;
        $lend->date_lend = $time;
        $lend->lend_tenure = $lend_tenure;

        // return $lend->getErrors();
        if($lend->save()){
            return true;
        }else{
            return false;
        }
    }
       // Constructor for saveBorrowamount ....       /////////

    public function saveBorrowamount($loan_amount, $amount_to_pay, $interest_rate, $insurance_rate, $fixed_charge, $fine, $priority, $user_id, $company_id, $period, $date_to_pay,$Transaction_ID,$reason_id,$other_reasons,$loan_note_url,$loan_note_id){
        $borrow = new Borrow();
        $connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
        date_default_timezone_set('Africa/Lagos');
        $time = date('Y-m-d H:i:s');
        $date_to_pay= strtotime($date_to_pay);
        $date_to_pay = date("Y-m-d H:i:s", $date_to_pay);

        $borrow->company_id = $company_id;
        $borrow->user_id = $user_id;
        $borrow->loan_amount = $loan_amount;
        $borrow->amount_to_pay = $amount_to_pay;
        $borrow->interest_rate = $interest_rate;
        $borrow->insurance_rate = $insurance_rate;
        $borrow->fixed_charge = $fixed_charge;
        $borrow->fine = $fine;
        $borrow->priority = $priority;
        $borrow->date_of_request = $time;
        $borrow->period = $period;
        $borrow->date_to_pay = $date_to_pay;
        $borrow->date_updated = $time;
        $borrow->status_id = 2;
        $borrow->transaction_id = $Transaction_ID;
        $borrow->reason_id = $reason_id;
        $borrow->other_reasons = $other_reasons;
        $borrow->loan_note_url = $loan_note_url;


        if($borrow->validate()) {
            if ($borrow->save()) {
                $update_loan_note = LoanNotes::findOne(['loan_note_id' => $loan_note_id]);
                $update_loan_note->borrow_id = $borrow->borrow_id;
                if ($update_loan_note->save()) {
                  $transaction->commit();
                  return ['status' => true];
                }
                else {
                  $transaction->rollBack();
                  return ['status' => false, 'message' => 'Loan Note Generation Not Completed. Please contact Admin'];
                }

            } else {
              $transaction->rollBack();
                return [
                    'status' => false,
                    'message' => 'Oops! Network error, Please try Again'];
            }
        }
        else{
          $transaction->rollBack();
            return [
                'status'=>false,
                'message' => $borrow->getErrors()];
        }
    }

    public function updateUsers($access_token, $firstname, $lastname, $midname, $phone, $address, $title, $gender, $birthdate, $employee_status_id){

        $userlogin = UserLogin::findOne(['access_token'=>$access_token]);
        $update = UserDetails::findOne(['user_login_id' => $userlogin->user_login_id]);

        date_default_timezone_set("Africa/Lagos");
        $time = date("Y-m-d H:i:s");

        $change_user = UserDetails::updateAll(

            [
                'title' => $title,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'middlename' => $midname,
                'phone_number' => $phone,
                'gender' => $gender,
                'address' => $address,
                'birthdate' => $birthdate,
                'employee_status_id' => $employee_status_id,
                'modified_date' => $time,

            ],
            [
                'user_id' => $update->user_id,

            ]
        );

        $time_user = UserLogin::updateAll(

            [

                'modified_date' => $time,

            ],
            [
                'user_login_id' => $userlogin->user_login_id,

            ]
        );

        if ($change_user != null || $time_user != null)
        {
            return true;
        }else{
            return false;
        }
    }

    public function updateBank($access_token, $acct,$acct_name, $bank,$bank_code, $bvn){

        $userlogin = UserLogin::findOne(['access_token'=>$access_token]);
        $update = UserDetails::findOne(['user_login_id' => $userlogin->user_login_id]);
        $bankdetails = UserBankDetails::findOne(['user_id'=>$userlogin->user_login_id]);

        date_default_timezone_set("Africa/Lagos");
        $time = date("Y-m-d H:i:s");
        // return $update->user_login_id;
        if(count($bankdetails)){
            $change_bank = UserBankDetails::updateAll(

                [
                    'account_name' => $acct_name,
                    'bank_name' => $bank,
                    'bank_code' => $bank_code,
                    'account_number' => $acct,
                    'bvn' => $bvn,
                    'modified_date' => $time,
                ],
                [
                    'user_id' => $userlogin->user_login_id,

                ]
            );

            if ($change_bank != null )
            {
                return true;
            }else{
                return false;
            }
        }else{
            $userbank = new UserBankDetails();

            $userbank->account_name = $acct_name;
            $userbank->account_number = $acct;
            $userbank->bank_name = $bank;
            $userbank->bank_code = $bank_code;
            $userbank->bvn = $bvn;
            $userbank->user_id = $userlogin->user_login_id;
            $userbank->created_date = $time;
            $userbank->modified_date = $time;
            // $userbank->save();
            // return $userbank->getErrors();

            if ($userbank->save()) {

                return true;
            }else{
                return false;
            }
        }



    }

    public function getUserDetails($user_id){
        return UserDetails::find()->where(['user_id'=>$user_id])->asArray()->one();
    }


    public function updatePassword($access_token, $password)
    {
        $update = UserLogin::findOne(['access_token' => $access_token]);

        $update->user_password = $password;
        if($update->save()){
            return true;
        }else{
            return false;
        }
    }

    public function getUserActiveLoan($user_id){
        return Borrow::find()->where(['user_id'=>$user_id, 'approve_flag'=>3, 'status_id'=>2])->orderBy(['date_of_request'=> SORT_DESC])->asArray()->one();
    }

    public function getUserLoanAwaitingApproval($user_id){
        return Borrow::find()->where(['user_id'=>$user_id, 'approve_flag'=>0, 'status_id'=>2])->orderBy(['date_of_request'=> SORT_DESC])->asArray()->one();
    }

    public function getUserLoanHistory($user_id){
        $hist = Borrow::findBySql("
            SELECT br.company_id, br.user_id,br.transaction_id,br.borrow_id,br.loan_note_url, br.loan_amount, br.period, br.amount_to_pay, br.priority, br.date_of_request,br.other_reasons, br.date_to_pay, br.status_id, br.approve_flag, us.firstname, us.middlename, us.lastname, us.phone_number, us.email, us.address, cm.company_name, reason.title as reason FROM borrow br left join borrow_reason reason ON br.reason_id=reason.reason_id inner join user_details us ON us.user_id=br.user_id inner join company cm
            ON cm.company_id=br.company_id WHERE us.user_id = $user_id
        ")->asArray()->all();

        return $hist;
        //return Borrow::find()->where(['user_id'=>$user_id])->asArray()->all();
    }

    public function getUserLendHistory($user_id){
        $hist = Lend::findBySql("
            SELECT br.company_id, br.user_id, br.lend_amount, br.amount_to_receive, br.date_lend, br.date_to_receive, br.status_id, br.approve_flag, us.firstname, us.middlename, us.lastname, us.phone_number, us.email, us.address, cm.company_name FROM lend br inner join user_details us ON us.user_id=br.user_id inner join company cm
            ON cm.company_id=br.company_id WHERE us.user_id = $user_id
        ")->asArray()->all();

        return $hist;
        //return Borrow::find()->where(['user_id'=>$user_id])->asArray()->all();
    }

    public function getUserTotalBorrow($user_id){
        $totalborrow = Borrow::find()->where(['user_id'=>$user_id, 'approve_flag'=>2])->asArray()->all();
        return count($totalborrow);
    }

    public function getUserTotalLend($user_id){
        $totallend = Lend::find()->where(['user_id'=>$user_id, 'approve_flag'=>2])->asArray()->all();
        return count($totallend);
    }

    public function  getBorrowLastTransaction($user_id){
        return Borrow::find()->where(['user_id'=>$user_id, 'approve_flag'=>1])->orderBy(['date_of_request'=> SORT_DESC])->asArray()->one();
    }

    public function  getLendLastTransaction($user_id){
        return Lend::find()->where(['user_id'=>$user_id, 'approve_flag'=>1])->orderBy(['date_lend'=> SORT_DESC])->asArray()->one();
    }

    //get Active Loan
    public  function actionActiveloan(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        //return $user;
        $activeloan = $this->getuseractiveloan($user->user_id);

        if(!is_array($activeloan)){
            $message = "No Active Loan";
            return ApiUtility::errorResponse($message);
        }
        if (empty($activeloan)) {
            return [
                'status' => true,
                'message' => 'No Active Loan',
                'data' => $activeloan
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Active Loan Fetched Successfully',
                'data' => $activeloan
            ];
        }
    }

    public  function actionCancelloan(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();
        $transaction_id = Yii::$app->request->post('transaction_id');
        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        //return $user;
        $borrow_update = Borrow::updateAll(
            ['status_id' => 0],
            ['user_id' => $user->user_id, 'transaction_id' => $transaction_id]);
        if ($borrow_update) {
            return [
                'status' => true,
                'message' => 'Loan Canceled Successfully',
                'data' => null
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => false,
                'message' => 'Unable Cancel loan at the moment',
                'data' => null
            ];
        }
    }

    //get Active Loan
    public  function actionActivelend(){
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        //return $user;
        $activeloan = Lend::find()->where(['user_id'=>$user->user_id, 'approve_flag'=>1, 'status_id'=>2])->orderBy(['date_of_request'=> SORT_DESC])->asArray()->one();

        if(!is_array($activeloan)){
            $message = "Error, No Active Lend";
            return ApiUtility::errorResponse($message);
        }
        if (empty($activeloan)) {
            return [
                'status' => true,
                'message' => 'No Active Lend',
                'data' => $activeloan
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Active Lend Fetched Successfully',
                'data' => $activeloan
            ];
        }
    }

    //forgot password
    public function actionForgotpassword()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsValuePair = [
            UserLoginApiKeyHelper::USER_EMAIL => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsValuePair, $params);

        $email = $params[UserLoginApiKeyHelper::USER_EMAIL];

        if(ApiUtility::isValidEmail($email)){

            $user = new UserLogin();
            $user = UserLogin::findOne(['user_email'=> $email, 'status_id'=> 2]);

            //$checkmailexist = UserLogin::findOne(['user_email'=>$email]);

            if($user)
            {

                $password_reset_key = ApiUtility::generatepass_reset_key();

                //$uniqid = (90 * 13 + $user->user_login_id);
                $uniqid = $user->user_email;

                $date = date('Y-m-d H:i:s', time() + 24 * 60 * 60);
                $encodedate = base64_encode($date);


                if($this->sendForgotMail($password_reset_key,$uniqid, $encodedate, $email)){
                    $user->reset_password_key = $password_reset_key;
                    $user->expiration_date = $encodedate;
                    $user->update();

                    return [
                        'status' => true,
                        'message'=>'instructions on password retrieval has been sent to your mail',
                        'data'=>$user
                    ];
                }
                else{
                    $errormsg = "Error sending mail, try again";
                    return ApiUtility::errorResponse($errormsg);
                }

            }
            else{
                $errormsg = "Email does not exist on our system";
                return ApiUtility::errorResponse($errormsg);
            }

        }
        else{
            $errormsg = "Invalid email address";

            return ApiUtility::errorResponse($errormsg);
        }

    }



    /*method to confirm credentials are correct */

        #TODO Delete Once Full forgot password process completed
    /*
     * public function checkactivationlink()
    {
        $this->setHeader(200);

        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $activation_key = Yii::$app->getRequest()->getQueryParam('activation_key');
        $uniqid = Yii::$app->getRequest()->getQueryParam('uniqid');
        $expire = Yii::$app->getRequest()->getQueryParam('expire');

        $currentdate = date("Y-m-d H:i:s");
        $decodedate = base64_decode($expire);

        //return $activation_key;



        $user_id = $uniqid - (90 * 13);

        //check activation key
        $user = UserLogin::findOne(['reset_password_key'=> $activation_key
        ]);
        if($user){

            if($currentdate - $decodedate <= 48 * 60 * 60){
                return [
                    'status' => true,
                    'message'=> 'All activation credentials are correct',
                    'data' => null
                ];

            }
            else{
                $errormsg = "Reset password link has expired. Please request for a new password";
                return ApiUtility::errorResponse($errormsg);
            }

        }
        else{
            $errormsg = "Sorry, we could not locate your account. If it's been more than 24 hours since you asked to reset your password, you'll need to reset your password again. Otherwise, you
            may try to copy and paste the URL from your email directly to the browser or use the 'forgot password' in the sign in window.";
            return ApiUtility::errorResponse($errormsg);

        }

    }
    */

    public function actionCheckactivationlink()
    {
        $this->setHeader(200);

        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsValuePair = [
            'activation_key' => ApiUtility::TYPE_STRING,
            'uniqid' => ApiUtility::TYPE_STRING,
            'expire' => ApiUtility::TYPE_STRING,
        ];


        $this->paramCheck($paramsValuePair, $params);

        $activation_key = $params['activation_key'];
        $uniqid = $params['uniqid'];
        $expire = $params['expire'];

        $currentdate = date("Y-m-d H:i:s");
        $decodedate = base64_decode($expire);

        //check activation key
        $user = UserLogin::findOne(['reset_password_key'=> $activation_key,'user_email'=>$uniqid
        ]);
        if($user){

            if((strtotime($currentdate) - strtotime($decodedate)) <= (48 * 60 * 60)){
                return [
                    'status' => true,
                    'message'=> 'All activation credentials are correct',
                    ];
            }
            else{
                $errormsg = "Reset password link has expired. Please request for a new password";
                return ApiUtility::errorResponse($errormsg);
            }

        }
        else{
            $errormsg = "Sorry, we could not locate your account. If it's been more than 24 hours since you asked to reset your password, you'll need to reset your password again. Otherwise, you
            may try to copy and paste the URL from your email directly to the browser or use the 'forgot password' in the sign in window.";
            return ApiUtility::errorResponse($errormsg);

        }

    }


    // public function actionResetoldpassword()
    // {
    //     $this->setHeader(200);
    //     ApiAuthenticator::verifyApiKey();
    //
    //     $params = $_REQUEST;
    //     $access_token = ApiKeyHelper::ACCESS_TOKEN;
    //
    //     //$apikey = Yii::$app->request->getQueryParam('key');
    //
    //
    //     $activation_key = Yii::$app->getRequest()->getQueryParam('activation_key');
    //     $email = Yii::$app->getRequest()->getQueryParam('user_email');
    //
    //
    //
    //     $paramsValuePair = [
    //         UserLoginApiKeyHelper::USER_PASSWORD => ApiUtility::TYPE_STRING,
    //         UserLoginApiKeyHelper::CONFIRM_PASSWORD => ApiUtility::TYPE_STRING,
    //         // 'activation_key' => ApiUtility::TYPE_STRING,
    //         'user_email' => ApiUtility::TYPE_STRING,
    //         // 'expire' => ApiUtility::TYPE_STRING,
    //     ];
    //
    //     $this->paramCheck($paramsValuePair, $params);
    //
    //     $new_password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
    //     $confirm_password = $params[UserLoginApiKeyHelper::CONFIRM_PASSWORD];
    //     // $activation_key = $params['activation_key'];
    //     $email = $params['user_email'];
    //     // $expire = $params['expire'];
    //
    //
    //     $user = UserLogin::findOne(['user_email'=> $email]);
    //     if($user){
    //
    //
    //         if(strcmp($new_password ,$confirm_password)== 0) {
    //
    //             $user->user_password = $this->hashpassword($new_password);
    //             $user->reset_password_key = '';
    //             $user->expiration_date = date('Y-m-d H:i:s',0);
    //             $user->modified_date = date('Y-m-d H:i:s');
    //
    //             //grant new Access Token
    //             $auth = new ApiAuthenticator();
    //             $user->access_token = $auth->generateApiKey();
    //
    //
    //             if ($user->save()) {
    //                 return [
    //                     'status' => true,
    //                     'message' => "Password reset successfully",
    //                     'data' => null
    //                 ];
    //             } else {
    //                 $errormsg = "Error changing user password";
    //                 return ApiUtility::errorResponse($errormsg);
    //             }
    //         }
    //         else{
    //             $errormsg = "new password field and confirm password field must be the same";
    //             return ApiUtility::errorResponse($errormsg);
    //         }
    //
    //     }
    //     else{
    //         $errormsg = "Sorry, we could not locate your account. If it's been more than 24 hours since you asked to reset your password, you'll need to reset your password again. Otherwise, you
    //         may try to copy and paste the URL from your email directly to the browser or use the 'forgot password' in the sign in window.";
    //         return ApiUtility::errorResponse($errormsg);
    //     }
    //
    //
    // }

    public function actionResetoldpassword()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        //$apikey = Yii::$app->request->getQueryParam('key');


        $activation_key = Yii::$app->getRequest()->getQueryParam('activation_key');
        $uniqid = Yii::$app->getRequest()->getQueryParam('uniqid');



        $paramsValuePair = [
            UserLoginApiKeyHelper::USER_PASSWORD => ApiUtility::TYPE_STRING,
            UserLoginApiKeyHelper::CONFIRM_PASSWORD => ApiUtility::TYPE_STRING,
            'activation_key' => ApiUtility::TYPE_STRING,
            'uniqid' => ApiUtility::TYPE_STRING,
            'expire' => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsValuePair, $params);

        $new_password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
        $confirm_password = $params[UserLoginApiKeyHelper::CONFIRM_PASSWORD];
        $activation_key = $params['activation_key'];
        $uniqid = $params['uniqid'];
        $expire = $params['expire'];


        $user = UserLogin::findOne(['user_email'=> $uniqid]);
        if($user){


            if(strcmp($new_password ,$confirm_password)== 0) {

                $user->user_password = $this->hashpassword($new_password);
                $user->reset_password_key = '';
                $user->expiration_date = date('Y-m-d H:i:s',0);
                $user->modified_date = date('Y-m-d H:i:s');

                //grant new Access Token
                $auth = new ApiAuthenticator();
                $user->access_token = $auth->generateApiKey();


                if ($user->save()) {
                    return [
                        'status' => true,
                        'message' => "Password reset successfully",
                        'data' => null
                    ];
                } else {
                    $errormsg = "Error changing user password";
                    return ApiUtility::errorResponse($errormsg);
                }
            }
            else{
                $errormsg = "new password field and confirm password field must be the same";
                return ApiUtility::errorResponse($errormsg);
            }

        }
        else{
            $errormsg = "Sorry, we could not locate your account. If it's been more than 24 hours since you asked to reset your password, you'll need to reset your password again. Otherwise, you
            may try to copy and paste the URL from your email directly to the browser or use the 'forgot password' in the sign in window.";
            return ApiUtility::errorResponse($errormsg);
        }


    }

    public function actionSupplybankdetails(){
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        // return $params;
        $paramsValuePair = [
            UserBankDetailsApiKeyHelper::ACCOUNT_NUMBER => ApiUtility::TYPE_INT,
            UserBankDetailsApiKeyHelper::ACCOUNT_NAME => ApiUtility::TYPE_STRING,
            UserBankDetailsApiKeyHelper::BANK_NAME => ApiUtility::TYPE_STRING,
            UserBankDetailsApiKeyHelper::BVN => ApiUtility::TYPE_INT,
            UserBankDetailsApiKeyHelper::BANK_CODE => ApiUtility::TYPE_INT
        ];

        $this->paramCheck($paramsValuePair, $params);

        $acct = $params[UserBankDetailsApiKeyHelper::ACCOUNT_NUMBER];
        $acct_name = $params[UserBankDetailsApiKeyHelper::ACCOUNT_NAME];
        $bank_code = $params[UserBankDetailsApiKeyHelper::BANK_CODE];
        $bank = $params[UserBankDetailsApiKeyHelper::BANK_NAME];
        $bvn = $params[UserBankDetailsApiKeyHelper::BVN];

        // return $bank_code;
        $get_access_token = $this->checkApiKey();
        //$get_access_token = $params[UserLoginApiKeyHelper::ACCESS_TOKEN];
        $update = $this->updateBank($get_access_token, $acct,$acct_name, $bank,$bank_code, $bvn);

        // return $update;

        if ($update)
        {
            return [

                'status' => true,
                'message' => 'Bank Details updated',

            ];
        }
        else
        {
            return[

                'status' => false,
                'message' => 'Error: Bank Details not updated.',

            ];
        }
    }


    public function actionPayment(){

        $params = $_REQUEST;
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        $id = $user->user_id;
         $Transaction_ID = rand(1000000,1000000000);
           $payment= new UserPayment();

            $payment->transaction_id = $Transaction_ID;
            $payment->user_id = $id;
            $payment->payment_amount = 100000;
            $payment->teller_number = 27455759;
            $payment->status_id = 1;

               $payment->save();

              return[

                'status' => true,
                'message' => 'Successfully Paid',
                'data'    =>  $id

            ];
    }



    public function actionGetaudit(){

          $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        $id = $user->user_id;

    $rows = (new \yii\db\Query())
    ->select(['*'])
    ->from('admin_log')
    ->where(['user_id' => $id])
    ->all();

   return [
            'status' => true,
            'message' => 'Query successfully',
            'data' => $rows
        ];
    }


public function saveAudit($user,$logData,$company_id ){


      $Transaction_ID = rand(10000000,1000000000);

          $AllLog = (new \yii\db\Query())
    ->select(['*'])
    ->from('admin_log')
    ->all();

    $admin_log_id = count($AllLog) + 1;


        $log->user_id = $user;
        $log->admin_log_details = $logData;
        $log->company_id = $company_id;
        $log->admin_log_id = $admin_log_id;
        $log->transaction_id =  $Transaction_ID;
}


}
