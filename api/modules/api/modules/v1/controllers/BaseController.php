<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/21/15
 * Time: 12:38 PM
 */

namespace app\modules\api\modules\v1\controllers;

use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;
use app\modules\api\modules\v1\models\PaymentType;
use app\modules\api\modules\v1\models\PaymentOption;

use Yii;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\modules\v1\models\PaymentTypeApiKeyHelper;
use app\modules\api\modules\v1\models\PaymentOptionApiKeyHelper;
class BaseController extends Controller{

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(),  [
            'pageCache' => [
                'class' => 'yii\filters\PageCache',
                'only' => [''],
                'duration' => 0,
                'dependency' => [
                    'class' => 'yii\caching\DummyCache',
                ],
                'variations' => [
                    \Yii::$app->language,
                ]
            ],
        ]);
    }

    public function setHeader($status)
    {

        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        $content_type="application/x-www-form-urlencoded; charset=utf-8";

        header($status_header);
        header('Access-Control-Allow-Origin: *');
        header('Content-type: ' . $content_type);
        header('X-LiteSpeed-Cache-Control: no-cache');
        header('X-Powered-By: ' . "RibyPeerLending");
    }



    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    public static function getAppParams($param){
        return Yii::$app->params[$param];
    }

    public function getadminadminusertypes(){
        return $this->getAppParams('company_user_types');
    }

    public function getadminusertypes(){
        return $this->getAppParams('company_admin_user_type');
    }

    public function paramCheck($paramsKeyValuePair, $params)
    {
        $keys = array_keys($paramsKeyValuePair);
        $paramsCheckResult = ApiUtility::paramCheck($keys, $params);
        if (is_array($paramsCheckResult)){
            $this->setHeader(400);
            $errorMsg = 'Missing params: ' . implode(', ', $paramsCheckResult);
            echo json_encode(ApiUtility::errorResponse($errorMsg));
            exit;
        }

        $paramsTypeCheckResult = ApiUtility::paramTypeCheck($paramsKeyValuePair, $params);
        if (is_array($paramsTypeCheckResult))
        {
            $this->setHeader(400);
            $errorMsg = 'The following param had wrong types: ' . implode(', ', $paramsTypeCheckResult);
            echo json_encode(ApiUtility::errorResponse($errorMsg));
            exit;
        }

        return true;
    }

    public static function getpaymenttypes(){

        $payments = PaymentType::find()->asArray()->all();
        return $payments;
    }

    public static function getdisbursementpaymenttype(){
        $payments = PaymentType::find()->where(['payment_type_name'=>'disbursement'])->asArray()->one();
        return $payments;
    }

    public static function getrepaymentpaymenttype(){
        $payments = PaymentType::find()->where(['payment_type_name'=>'repayment'])->asArray()->one();
        return $payments;
    }

    public function get_payment_options($payment){
        $alloptions = self::getAppParams('allpaymenttypes');
        if($payment == 'disbursement')
            $specs = 'disbursementonly';
        else if($payment == 'repayment')
            $specs = 'repaymentonly';
        else $specs = '';

        $options_id =[$alloptions];
        if($specs != '')
            $options_id[] = self::getAppParams($specs);

        $options = PaymentOption::find()->where(['payment_option_type'=>$options_id])->asArray()->all();
        return $options;
    }


    public function getReplyEmail()
    {
        return $this->getAppParams('noreplyEmail');
    }

    public function getAdminEmail()
    {
        return $this->getAppParams('adminEmail');
    }

    public function getLendEmail()
    {
        return $this->getAppParams('lendEmail');
    }

    public function hashpassword($password)
    {
        return sha1($password);
    }

    function checkApiKey()
    {

        if (!isset($_GET['key'])) {

            $msg = "Permission Denied, You need an api key to access this!";
            return json_encode(ApiUtility::errorResponse($msg));

        } else {

            $apiKey = $_GET['key'];


            return $apiKey;
        }
    }

    public function getcurrentdatetime()
    {
        return date("Y-m-d h:m:s");
    }

    public function currentDate()
    {
        return date("Y-m-d h:i:s");

    }

    public function adddaystoDate($days)
    {
        return date('Y-m-d h:i:s', strtotime("+".$days."days"));
    }

    //simple template to send email
    public static function sendEmail($mail_template_name, $reply_to = null,$message, $subject, $email_to, $email_from, $username,$company=null,$loan_note_url=null)
    {
        $check =  Yii::$app->mailer->compose($mail_template_name,
            ['site_url'=>self::getAppParams('site_url'),'username'=>$username,'loan_note_url' => getcwd().'/loannotes/'.$loan_note_url, 'msg'=>$message,'email_from'=>$email_from
            , 'company'=>$company,'no_reply'=>self::getAppParams('noreplyEmail'),
                'support'=>self::getAppParams('adminEmail'),'phone'=>self::getAppParams('adminPhone')])

            ->setFrom($email_from)
            ->setTo($email_to)
            ->setReplyTo($reply_to)
            ->setSubject($subject)
            ->send();
        return $check;

    }

    public function sendForgotMail($activation_key,$uniqid,$expire,$to){

        $from = $this->getReplyEmail();


        $check =  Yii::$app->mailer->compose('forgot', ['site_url'=>$this->getAppParams('site_url'),'activation_key'=> $activation_key,
            'uniqid'=> $uniqid, 'expire'=>$expire,'no_reply'=>$this->getAppParams('noreplyEmail'),'support'=>$this->getAppParams('adminEmail'),'phone'=>$this->getAppParams('adminPhone')])

            ->setFrom($from)
            ->setTo($to)
            ->setReplyTo($from)
            ->setSubject("Forgot Password")
            ->send();
        return $check;
    }



    public function timeagoString($giventime)
    {
        $timediff = strtotime(date("Y-m-d h:i:s")) - $giventime;



        if ($timediff < 1)
        {
            return '0 seconds';
        }
        else {

            $period = [
                365 * 24 * 60 * 60 => 'year',
                30 * 24 * 60 * 60 => 'month',
                24 * 60 * 60 => 'day',
                60 * 60 => 'hour',
                60 => 'minute',
                1 => 'second'
            ];
            $periods = array(
                'year' => 'years',
                'month' => 'months',
                'day' => 'days',
                'hour' => 'hours',
                'minute' => 'minutes',
                'second' => 'seconds'
            );

            foreach ($period as $secs => $str) {
                if($secs > $timediff){
                    continue;
                }

                $d = $timediff / $secs;


                if ($d >= 1) {
                    $r = round($d);
                    return $r . ' ' . ($r > 1 ? $periods[$str] : $str) . ' ago';
                } else {
                    return '0 seconds';
                }
            }
        }
    }


}
