<?php

namespace app\modules\api\modules\v1\controllers;
use app\modules\api\modules\v1\models\ContactUs;
use app\modules\api\modules\v1\models\RequestInvite;
use Exception;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;
use app\modules\api\modules\v1\models\ContactApiKeyHelper;


class ContactController extends BaseController
{

  public function behaviors()
  {
      return ArrayHelper::merge(parent::behaviors(), [

          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'login' => ['POST'],

              ],
          ],
      ]);
  }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionContactus()
    {
      $params = $_REQUEST;

      $paramsValuePair = [
          ContactApiKeyHelper::NAME => ApiUtility::TYPE_STRING,
          ContactApiKeyHelper::EMAIL => ApiUtility::TYPE_EMAIL,
          ContactApiKeyHelper::PHONE => ApiUtility::TYPE_INT,
          ContactApiKeyHelper::MESSAGE => ApiUtility::TYPE_STRING
      ];
      $this->paramCheck($paramsValuePair, $params);

      $name = $params[ContactApiKeyHelper::NAME];
      $email = $params[ContactApiKeyHelper::EMAIL];
      $phone = $params[ContactApiKeyHelper::PHONE];
      $message = $params[ContactApiKeyHelper::MESSAGE];

      $contact = new ContactUs;
      $contact->name = $name;
      $contact->email = $email;
      $contact->phone = $phone;
      $contact->message = $message;

      if ($contact->save()) {
        $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Peerlending Contact Notification', 'peerlending@riby.me', $email, 'Admin');
        return [
            'status' => true,
            'message' => "Your Message has been sent Successfully",
        ];
      }
      else {
        return ApiUtility::errorResponse($msg);
      }
    }

    public function actionRequestinvite()
    {
      $params = $_REQUEST;

      $paramsValuePair = [
          ContactApiKeyHelper::FULLNAME => ApiUtility::TYPE_STRING,
          ContactApiKeyHelper::EMAIL => ApiUtility::TYPE_EMAIL,
          ContactApiKeyHelper::PHONE => ApiUtility::TYPE_INT,
          ContactApiKeyHelper::COMPANY_NAME => ApiUtility::TYPE_STRING,
          ContactApiKeyHelper::HR_EMAIL => ApiUtility::TYPE_EMAIL,
          ContactApiKeyHelper::HR_PHONE => ApiUtility::TYPE_INT
      ];
      $this->paramCheck($paramsValuePair, $params);

      $fullname = $params[ContactApiKeyHelper::FULLNAME];
      $email = $params[ContactApiKeyHelper::EMAIL];
      $phone = $params[ContactApiKeyHelper::PHONE];
      $company_name = $params[ContactApiKeyHelper::COMPANY_NAME];
      $hr_email = $params[ContactApiKeyHelper::HR_EMAIL];
      $hr_phone = $params[ContactApiKeyHelper::HR_PHONE];

      $contact = RequestInvite::findOne(['company_name' => $company_name]);
      if (count($contact)) {
        return [
            'status' => false,
            'message' => "An Invite has Already been submitted for this company",
        ];
      }

      $invite = new RequestInvite;
      $invite->email = $email;
      $invite->phone = $phone;
      $invite->fullname = $fullname;
      $invite->company_name = $company_name;
      $invite->hr_email = $hr_email;
      $invite->hr_phone = $hr_phone;

      $message = "{$company_name} is Requesting Access to the Riby Peerlending Platform<br/>
      Company Details:<br/>
      FULLNAME: {$fullname}<br/>
      EMAIL: {$email}<br/>
      PHONE NUMBER: {$phone}<br/>
      COMPANY NAME:{$company_name}<br/>
      HR EMAIL: {$hr_email}<br/>
      HR PHONE: {$hr_phone}<br/>
      Please Get in touch with this company as soon as possible.
        ";

      if ($invite->save()) {
        $this->sendEmail('mailtemplate', $this->getReplyEmail(),$message , 'Peerlending Company Request Access / Invite Notification', 'operations@riby.me', $email, 'Admin');
        return [
            'status' => true,
            'message' => "Invite Submitted",
        ];
      }
      else {
        return ApiUtility::errorResponse($msg);
      }
    }

    public function actionGetallnewrequest()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminUserApiKey();
      $contact = RequestInvite::find()->where(['status' => 0])->all();

      if (count($contact) > 0) {
        return [
            'status' => true,
            'message' => "Invite Fetched Successfully",
            'data' => $contact
        ];
      }
      else {
        return [
            'status' => true,
            'message' => "No Messages",
        ];
      }
    }

    public function actionGetallapprovedrequest()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminUserApiKey();
      $contact = RequestInvite::find()->where(['status' => 1])->all();


      if (count($contact) > 0) {
        return [
            'status' => true,
            'message' => "Invite Fetched Successfully",
            'data' => $contact
        ];
      }
      else {
        return [
            'status' => true,
            'message' => "No Messages",
        ];
      }
    }

    public function actionGetalldeclinedrequest()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminUserApiKey();
      $contact = RequestInvite::find()->where(['status' => 2])->all();
      if (count($contact) > 0) {
        return [
            'status' => true,
            'message' => "Invite Fetched Successfully",
            'data' => $contact
        ];
      }
      else {
        return [
            'status' => true,
            'message' => "No Messages",
        ];
      }
    }



    public function actionApproveinvite()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminUserApiKey();
      $request_invite = Yii::$app->request->post('request_invite');

      $save_invite = RequestInvite::updateAll(['status' => 1], ['request_invite' => $request_invite]);
      $invite = RequestInvite::findOne(['request_invite' => $request_invite]);
      // return $invite[0]->status;
      // $invite[0]['status'] = 1;
      $message = "Dear {$invite->fullname}, Congratulations, Your Invite to access the Riby Peerlending Platform has been approved.<br/>
      Thank you for your request.
        ";

      if ($save_invite) {
        $this->sendEmail('mailtemplate', $this->getReplyEmail(),$message , 'Congratulations, Invite Approved', 'operations@riby.me', $invite->email, 'Riby Peerlending');
        return [
            'status' => true,
            'message' => "Invite Approved",
        ];
      }
      else {
        $msg = 'Could not Update, Try Again';
        return ApiUtility::errorResponse($msg);
      }

    }


    public function actionDeclineinvite()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminUserApiKey();
      $request_invite = Yii::$app->request->post('request_invite');

      $save_invite = RequestInvite::updateAll(['status' => 2], ['request_invite' => $request_invite]);
      $invite = RequestInvite::findOne(['request_invite' => $request_invite]);
      $message = "Dear {$invite->fullname}, We are sorry, we could not proceed with the approval of your request. <br/>
      We hope that you do apply again. Thank you for your request
        ";

      if ($save_invite) {
        $this->sendEmail('mailtemplate', $this->getReplyEmail(),$message , 'Congratulations, Invite Approved', 'operations@riby.me', $invite->email, 'Riby Peerlending');
        return [
            'status' => true,
            'message' => "Invite Approved",
        ];
      }
      else {
        return ApiUtility::errorResponse($msg);
      }

    }





    public function actionGetallemail()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminUserApiKey();
      $contact = ContactUs::find()->all();

      if (count($contact) > 0) {
        return [
            'status' => true,
            'message' => "Message Fetched Successfully",
            'data' => $contact
        ];
      }
      else {
        return [
            'status' => true,
            'message' => "No Messages",
        ];
      }
    }
}
