<?php

namespace app\modules\api\modules\v1\controllers;


use app\modules\api\modules\v1\models\UserDetails;
use app\modules\api\modules\v1\models\UserLogin;
use app\modules\api\modules\v1\models\UserLoginApiKeyHelper;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;
use app\modules\api\modules\v1\models\Company;




class LoginController extends BaseController
{

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                                'login'    =>          ['POST'],

                            ],
                        ],
        ]);
    }

    public function actionIndex()
    {

        return json_encode(['kayode']);


    }

    public function actionTest(){
      $this->setHeader(200);
      return $_POST;
    }

    public function actionLogin()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsValuePair = [
            UserLoginApiKeyHelper::USER_EMAIL => ApiUtility::TYPE_STRING,
            UserLoginApiKeyHelper::USER_PASSWORD => ApiUtility::TYPE_STRING
        ];
        $this->paramCheck($paramsValuePair, $params);

        $email = $params[UserLoginApiKeyHelper::USER_EMAIL];
        $password = $this->hashpassword($params[UserLoginApiKeyHelper::USER_PASSWORD]);

        $user = UserLogin::findOne(['user_email'=>$email, 'user_password'=> $password]);

        if($user){
            if($user->status_id == 3){
                $errormsg = "Sorry, your account has not been activated, continue to check within the next 24 hours";

                return ApiUtility::errorResponse($errormsg);
            }

            else if($user->status_id == 2) {
                $access_token = $user->access_token;
                $user_type_id = $user->user_type_id;
                $username = $user->user_email;
                $userdetails = UserDetails::findOne(['user_login_id'=>$user->user_login_id]);
                $company_name = Company::findOne(['company_id'=>$userdetails->company_id]);
                if ($company_name->is_active == 0) {
                  return [
                      'status' => false,
                      'message' => "Invalid Username/Password",
                      'data' => [
                          'user_access_token' => $access_token,
                          'user_type_id'=> $user_type_id,
                          'user_email' => $username,
                          'company_name' => $company_name->company_name
                      ]
                  ];
                }
                else {
                  return [
                      'status' => true,
                      'message' => "Login was successful",
                      'data' => [
                          'user_access_token' => $access_token,
                          'user_type_id'=> $user_type_id,
                          'user_email' => $username,
                          'company_name' => $company_name->company_name,
                          'user_details' => $userdetails,
                      ]
                  ];
                }

            }
            else{
                $errormsg = "sorry, you do not have an account with us";
                return ApiUtility::errorResponse($errormsg);
            }
        }
        else
        {
            $msg = "Invalid username/password";
            return ApiUtility::errorResponse($msg);
        }

    }


}
