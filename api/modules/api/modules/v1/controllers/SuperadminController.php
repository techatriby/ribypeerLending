<?php

namespace app\modules\api\modules\v1\controllers;


use app\modules\api\modules\v1\models\BenchMark;
use app\modules\api\modules\v1\models\Borrow;
use app\modules\api\modules\v1\models\BorrowReason;
use app\modules\api\modules\v1\models\Company;
use app\modules\api\modules\v1\models\Lend;
use app\modules\api\modules\v1\models\PaymentType;
use app\modules\api\modules\v1\models\UserBankDetails;
use app\modules\api\modules\v1\models\UserDetails;
use app\modules\api\modules\v1\models\UserLogin;
use app\modules\api\modules\v1\models\RepaymentSchedule;
use app\modules\api\modules\v1\models\KegowLoader as KegowTransactions;

use app\modules\api\modules\v1\models\UserPayment;
use Exception;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;
use app\modules\api\modules\v1\models\PaymentTypeApiKeyHelper;
use app\modules\api\modules\v1\models\PaymentOptionApiKeyHelper;
use app\modules\api\modules\v1\models\BorrowerLenderApiKeyHelper;
use app\modules\api\modules\v1\models\BenchMarkApiKeyHelper;
use app\modules\api\modules\v1\models\CompanyApiKeyHelper;
use app\modules\api\modules\v1\models\BankApiKeyHelper;
use app\modules\api\modules\v1\models\UserDetailsApiKeyHelper;
use app\modules\api\modules\v1\models\UserLoginApiKeyHelper;
use app\modules\api\modules\v1\models\UserTypeApiKeyHelper;
use app\modules\api\modules\v1\models\TransactionRemark;
use app\modules\api\modules\v1\models\UserPaymentApiKeyHelper;
use Flutterwave\Bvn;
use Flutterwave\Flutterwave;
use app\modules\api\modules\v1\models\PaymentOptionType;
use app\modules\api\modules\v1\controllers\FPDF;
use app\modules\api\modules\v1\models\LoanNotes;


class SuperadminController extends BaseController
{

  public $kegow = '';
  public function behaviors()
  {
    return ArrayHelper::merge(parent::behaviors(), [

      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'login' => ['POST'],

        ],
      ],
    ]);
  }

  public function actionIndex()
  {
    return json_encode(['kayode']);
  }

  public function actionGetpaymentoptiontype()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $payment_option_id = Yii::$app->request->getQueryParam('payment_option_id');
    $banks = PaymentOptionType::find(['payment_option_id' => $payment_option_id])->all();
    return [
        'status' => true,
        'message' => 'Payment Options fetched successfully',
        'data' => $banks
    ];
  }

  public function actionGetcompanyadminrepaidloan()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    // $payment_option_id = Yii::$app->request->getQueryParam('payment_option_id');
    $command = Yii::$app->db->createCommand("SELECT * FROM repayment_schedule INNER JOIN company ON company.company_id = repayment_schedule.company_id
    INNER JOIN user_login ON user_login.access_token = repayment_schedule.repayer_key
    INNER JOIN user_details ON user_details.user_login_id = user_login.user_login_id WHERE repayment_schedule.status = 2");
    $repayment = $command->queryAll();
    return [
        'status' => true,
        'message' => 'Company admin Repayed Loan Datails Fetched Successfully',
        'data' => $repayment
    ];
  }

  public function actionGetexternaltransactionsdetails($value='')
  {
    // $transactions = KegowTransactions::find()->leftJoin('user_details', 'user_details.user_login_id=kegow_transaction.user_id')->leftJoin('company', 'user_details.company_id = company.company_id')->all();
    $command = Yii::$app->db->createCommand("
      SELECT kegow_transaction.*,user_details.firstname,user_details.lastname,user_details.company_id,company.company_name
      FROM kegow_transaction INNER JOIN user_details ON user_details.user_login_id = kegow_transaction.user_id INNER JOIN company ON
      user_details.company_id = company.company_id
    ");

    $transactions = $command->queryAll();
    return [
        'status' => true,
        'message' => 'External Transactions Fetched Successfully',
        'data' => $transactions
    ];
  }
  public function actionGetexternaltransactionsdetailsbytype($value='')
  {
    // $transactions = KegowTransactions::find()->leftJoin('user_details', 'user_details.user_login_id=kegow_transaction.user_id')->leftJoin('company', 'user_details.company_id = company.company_id')->all();
    $type = Yii::$app->request->post('transaction_type');
    $command = Yii::$app->db->createCommand("
      SELECT kegow_transaction.*,user_details.firstname,user_details.lastname,user_details.company_id,company.company_name
      FROM kegow_transaction INNER JOIN user_details ON user_details.user_login_id = kegow_transaction.user_id INNER JOIN company ON
      user_details.company_id = company.company_id WHERE kegow_transaction.transaction_type = '{$type}'
    ");

    $transactions = $command->queryAll();
    return [
        'status' => true,
        'message' => 'External Transactions Fetched Successfully',
        'data' => $transactions
    ];
  }
  public function actionDeclinehrrepayrequest()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $transaction_id = Yii::$app->request->getQueryParam('transaction_id');
    $month = Yii::$app->request->getQueryParam('month');
    $user_payment = UserPayment::find()->where(['transaction_id' => $transaction_id, 'month' => $month])->one();
    $borrow = Borrow::find()->where(['transaction_id' => $transaction_id])->one();
    if (count($user_payment)) {
      $connection = Yii::$app->db;
      $transaction = $connection->beginTransaction();
      $user_payment->paid = 0;
      $user_payment->repayment_reference_id = '';
      $user_payment->payment_description = '';
      // $user_payment->save();
      // return $user_payment->getErrors();
      if ($user_payment->save()) {
        $borrow->approve_flag = 3;
        if ($borrow->save()) {
          $transaction->commit();
          return [
            'status' => true,
            'message' => 'Repayment Declined Sucessfully',
          ];
        }
        else {
          $transaction->rollBack();
          return [
            'status' => false,
            'message' => 'Repayment Declined not Successful',
          ];
        }
      }
      else {
        $transaction->rollBack();
        return [
          'status' => false,
          'message' => 'Repayment Declined not Successful',
        ];
      }
    }
    else {
      return [
        'status' => false,
        'message' => 'Loan Repayment not Found',
      ];
    }
  }

  public function actionApprovehrrepayrequest()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $repayment_schedule_id = Yii::$app->request->getQueryParam('repayment_schedule_id');
    $repay = RepaymentSchedule::find()->where(['repayment_schedule_id' => $repayment_schedule_id])->one();
    $repay->status = 1;
    if ($repay->save()) {
      return [
        'status' => true,
        'message' => 'Repayment Completed Successfully'
      ];
    }
    else {
      return [
        'status' => false,
        'message' => 'Couldn\'t, Complete Repayment, Please Try Again'
      ];
    }

  }

  public function actionGetcompanyadminrepaidloandetails()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $transaction_id = Yii::$app->request->getQueryParam('transaction_id');
    $repayment = RepaymentSchedule::find()->where(['status' => 2])->asArray(true)->all();
    return [
        'status' => true,
        'message' => 'Company admin Repayed Loan Datails Fetched Successfully',
        'data' => $repayment
    ];
  }

  public function actionAddpaymentoptiontype()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $params = $_REQUEST;
    $paramsValuePair = [
      'payment_option_id' => ApiUtility::TYPE_INT,
      'name' => ApiUtility::TYPE_STRING,
    ];
    $this->paramCheck($paramsValuePair, $params);

    $payment_option_id = $params['payment_option_id'];
    $name = $params['name'];

    $create_type = new PaymentOptionType;
    $create_type->name = $name;
    $create_type->payment_option_id = $payment_option_id;
    if ($create_type->save()) {
      return [
        'status' => true,
        'message' => 'New Payment Option Type Insert Successful',
        'data' => null
      ];
    }
    else {
      return [
        'status' => false,
        'message' => 'Could not complete request, Please try again',
        'data' => null
      ];
    }
  }

  public function actionUpdatepaymentoptiontype()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $params = $_REQUEST;
    $paramsValuePair = [
      'payment_option_type_id' => ApiUtility::TYPE_INT,
      'payment_option_id' => ApiUtility::TYPE_INT,
      'name' => ApiUtility::TYPE_STRING,
    ];
    $this->paramCheck($paramsValuePair, $params);

    $payment_option_id = $params['payment_option_id'];
    $name = $params['name'];
    $payment_option_type_id = $params['payment_option_type_id'];

    $create_type = PaymentOptionType::findOne(['payment_option_type_id' => $payment_option_type_id]);
    $create_type->name = $name;
    $create_type->payment_option_id = $payment_option_id;
    if ($create_type->save()) {
      return [
        'status' => true,
        'message' => 'New Payment Option Type Update Successful',
        'data' => null
      ];
    }
    else {
      return [
        'status' => false,
        'message' => 'Could not complete request, Please try again',
        'data' => null
      ];
    }
  }

  public function actionVerifybvn()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $merchantKey = "tk_PIwcm06Nh3"; //can be found on flutterwave dev portal
    $apiKey = "tk_CYcyIq1g0h3tO9UHNXQf"; //can be found on flutterwave dev portal
    $env = "staging"; //this can be production when ready for deployment
    Flutterwave::setMerchantCredentials($merchantKey, $apiKey, $env);
    $bvn = Yii::$app->request->getQueryParam('bvn');
    if (!isset($bvn)) {
      return [
        'status' => false,
        'message' => 'BVN Missing from Request',
        'data' => null
      ];
    }
    // Wale
    // $bvn = "22143074076";
    //WIlliams
    // $bvn = "22370072647";
    $result = Bvn::verify($bvn, Flutterwave::SMS); //this will send otp to the telephone used for the bvn registration
    //methods like getResponseData(), getStatusCode(), getResponseCode(), isSuccessfulResponse()
    if ($result->isSuccessfulResponse()) {
      return [
        'status' => true,
        'message' => 'BVN was Successfully Confirmed',
        'data' => null
      ];
    } else {
      return [
        'status' => false,
        'message' => 'Incorrect BVN',
        'data' => null
      ];
    }
  }

  public function actionGetuserbankfromid()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();
    $user_id = Yii::$app->getRequest()->getQueryParam('user_id');
    $bankdetails = UserBankDetails::findOne(['user_id'=>$user_id]);
    if (count($bankdetails) > 0) {
      return [
        'status' => true,
        'message' => 'Bank Details fetched successfully',
        'data'  => $bankdetails
      ];
    }
    else {
      return [
        'status' => true,
        'message' => 'No bank Details',
        'data'  => null
      ];
    }
  }
  public function actionGettransactionremarks()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();

    $transactionremark = TransactionRemark::find()->select(['remark_id', 'remark_description'])->asArray()->all();

    if (!is_array($transactionremark)) {
      $message = "Error retrieving Transaction Remarks";
      return ApiUtility::errorResponse($message);
    } else {
      $this->setHeader(200);
      return [
        'status' => true,
        'message' => 'Transaction Remarks Fetched Successfully',
        'data' => $transactionremark
      ];
    }
  }

  public function actionGetsuperadminlevels()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();

    //Fetch All super admin levels from the DB
    $levels = (new \yii\db\Query())
    ->select(['*'])
    ->from('user_type')
    ->where(['and','user_type_name like "Super%"','user_type_name like "%Admin%"'])
    ->all();
    return $levels;
  }

  public function actionCreatecompany()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();

    $params = $_REQUEST;
    $access_token = ApiKeyHelper::ACCESS_TOKEN;

    $paramsValuePair = [
      CompanyApiKeyHelper::COMPANYNAME => ApiUtility::TYPE_STRING,
      CompanyApiKeyHelper::COMPANYADDRESS => ApiUtility::TYPE_STRING,
      CompanyApiKeyHelper::COMPANYINFO => ApiUtility::TYPE_STRING,
      UserLoginApiKeyHelper::USER_EMAIL => ApiUtility::TYPE_STRING
    ];
    $this->paramCheck($paramsValuePair, $params);

    $company_name = $params[CompanyApiKeyHelper::COMPANYNAME];
    $company_address = $params[CompanyApiKeyHelper::COMPANYADDRESS];
    $company_info = $params[CompanyApiKeyHelper::COMPANYINFO];
    $email = $params[UserLoginApiKeyHelper::USER_EMAIL];

    $fetch_mail = UserDetails::find()->where(['email' => $email])->asArray()->one();
    $fetch_names = UserLogin::find()->where(['user_email' => $email, 'status_id' => 3])->asArray()->one();

    if (ApiUtility::isValidEmail($email)) {

      if ($fetch_mail == null && $fetch_names == null) {


        //check if company name does not already exist
        $checkcompanyexist = Company::findOne(['company_name' => $company_name]);
        if (!$checkcompanyexist) {
          $comp = new Company();
          $comp->company_name = $company_name;
          $comp->company_address = $company_address;
          $comp->company_info = $company_info;

          $connection = Yii::$app->db;
          $transaction = $connection->beginTransaction();
          try {

            if ($comp->validate()) {
              if ($comp->save()) {

                $admin_user = self::createcompanyadmin($company_name, $email, $transaction);
                if ($admin_user['status'] == true) {
                  $admin_user = $admin_user['data'];
                  return [
                    'status' => true,
                    'message' => "New company added (" . $comp->company_name . ') with Admin (' . $admin_user['email'] . ')',
                    'data' => ['id' => $admin_user['company_id']]
                  ];
                } else {

                  $transaction->rollback();
                  $msg = 'There was an error setting up the company Admin. Kindly contact the Dev team.';
                  return ApiUtility::errorResponse($msg);
                }
              } else {
                $msg = "sorry new company could not be added, please ensure there is internet and also cross check the credentials";
                return ApiUtility::errorResponse($msg);
              }
            } else {
              $msg = $comp->getErrors();
              return ApiUtility::errorResponse($msg);
            }
          } catch (Exception $ex) {
            $transaction->rollBack();
            return ApiUtility::errorResponse('Network error creating Company. Please try again');
          }
        } else {
          $msgg = "Company already exist";
          return ApiUtility::errorResponse($msgg);
        }

      } else {
        $msgg = 'User already exists';
        return ApiUtility::errorResponse($msgg);
      }

    } else {
      $msgg = 'Invalid email address, Please check and try again';
      return ApiUtility::errorResponse($msgg);
    }


  }

  public function actionFetchbenchmark()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();

    $benchmark = BenchMark::find()->select(['bench_mark_id', 'bench_mark_max', 'bench_mark_min'])->asArray()->all();
    if (count($benchmark) > 0 && $benchmark != null) {
      return [
        'status' => true,
        'message' => 'benchmark fetched successfully',
        'data' => $benchmark
      ];
    } else {
      return ApiUtility::errorResponse("Bench mark could not be fetched");
    }

  }

  public function actionUpdatebenchmark()
  {

    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();

    $params = $_REQUEST;

    $access_token = ApiKeyHelper::ACCESS_TOKEN;

    $paramsValuePair = [
      BenchMarkApiKeyHelper::BENCH_MARK_MIN => ApiUtility::TYPE_STRING,
      BenchMarkApiKeyHelper::BENCH_MARK_MAX => ApiUtility::TYPE_STRING,
    ];

    $this->paramCheck($paramsValuePair, $params);

    $bench_mark_min = $params[BenchMarkApiKeyHelper::BENCH_MARK_MIN];
    $bench_mark_max = $params[BenchMarkApiKeyHelper::BENCH_MARK_MAX];

    $bench = BenchMark::findOne(['bench_mark_id' => 1]);
    if ($bench) {
      $bench->bench_mark_min = $bench_mark_min;
      $bench->bench_mark_max = $bench_mark_max;

      if ($bench->update()) {
        return
        [
          'status' => true,
          'message' => 'bench updated successfully',
          'data' => null
        ];
      } else {
        return ApiUtility::errorResponse('sorry, bench mark could not be updated');

      }
    }

  }

  public function actionKegowloadcard()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();

    $amount = Yii::$app->request->post('amount');
    // return $amount;
    if (!$amount || $amount = '') {
      return
      [
        'status' => false,
        'message' => 'Amount Cannot be Empty',
        'data' => null
      ];
    }
    $amount = Yii::$app->request->post('amount');
    $user_login_id = Yii::$app->request->post('user_id');
    $user_details = UserDetails::findOne(['user_login_id' => $user_login_id, 'status_id' => 2]);
    if (count($user_details) > 0) {
      /** Tries to Register User if User is not Registered **/
      Yii::$app->kegowloader->kegowRegisterUser($user_details->phone_number,$user_details->firstname);

      /** Confirm Payment to User Account **/

      $res = Yii::$app->kegowloader->loadUserAccount($user_details->phone_number,$amount,$user_login_id);
      // return $res;
    }
    else {
      return
      [
        'status' => false,
        'message' => 'User is unactive',
        'data' => null
      ];
    }

  }


  public function actionKegowloaduserbankaccount()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();


    $params = $_REQUEST;

    $paramsValuePair = [
      BankApiKeyHelper::AMOUNT => ApiUtility::TYPE_INT,
      BankApiKeyHelper::ACCOUNT_NUM => ApiUtility::TYPE_INT,
      BankApiKeyHelper::USER_ACCOUNT_NAME => ApiUtility::TYPE_STRING,
      BankApiKeyHelper::BANK_NAME => ApiUtility::TYPE_STRING,
      BankApiKeyHelper::BANK_SORT_CODE => ApiUtility::TYPE_INT,
      BankApiKeyHelper::BANK_ID => ApiUtility::TYPE_INT,
      BankApiKeyHelper::USER_ID => ApiUtility::TYPE_STRING
    ];
    $this->paramCheck($paramsValuePair, $params);

    $amount = $params[BankApiKeyHelper::AMOUNT];
    $user_acct_num = $params[BankApiKeyHelper::ACCOUNT_NUM];
    $user_account_name = $params[BankApiKeyHelper::USER_ACCOUNT_NAME];
    $user_id = $params[BankApiKeyHelper::USER_ID];
    $bank_name = $params[BankApiKeyHelper::BANK_NAME];
    $bank_sort_code = $params[BankApiKeyHelper::BANK_SORT_CODE];
    $bank_id = $params[BankApiKeyHelper::BANK_ID];

    // Get User Bank Details for Verification

    $user_bank_details = UserBankDetails::findOne(['user_id' => $user_id]);
    // return $user_bank_details;
    $user_details = UserDetails::findOne(['user_login_id' => $user_id, 'status_id' => 2]);
    // return $user_details;
    if (count($user_details) > 0) {
      /** Tries to Register User if User is not Registered **/
      // Yii::$app->kegowloader->kegowRegisterUser($user_details->phone_number,$user_details->firstname);

      /** Confirm Payment to User Account **/

      $res = Yii::$app->kegowloader->Transfertobank($user_bank_details->account_number, $bank_sort_code, $amount, $user_id);
      return $res;
    }
    else {
      return
      [
        'status' => false,
        'message' => 'User is unactive',
        'data' => null
      ];
    }

  }

  public function actionTestkegowreg()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();

    $phone = Yii::$app->request->post('phone');
    $name = Yii::$app->request->post('name');
    $address = Yii::$app->request->post('address');
    $city = Yii::$app->request->post('city');
    $res = Yii::$app->kegowloader->kegowRegisterUser($phone,$name,$address,$city);
    return $res;
  }

  public function actionGetusercardid()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();
    $user_id = Yii::$app->request->getQueryParam('user_id');
    $user_details = UserDetails::findOne(['user_login_id' => $user_id]);
    if (count($user_details) > 0) {
      /** Confirm Payment to User Account **/
      $getCard = Yii::$app->kegowloader->getUserCardID($user_details->phone_number, $user_id);
      return $getCard;

    }
    else {
      return
      [
        'status' => false,
        'message' => 'User Not Found',
        'data' => null
      ];
    }
  }

  public function actionVerifybank()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();
    $account = Yii::$app->request->getQueryParam('account');
    $bankCode = Yii::$app->request->getQueryParam('bankcode');
    // return [
    //   $account, $bankCode
    // ];
    $getCard = Yii::$app->kegowloader->VerifyBank($account, $bankCode);
    return $getCard;
  }

  public function actionSetborrowreason()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();
    $reason = Yii::$app->request->post('title');
    if (empty($reason)) {
      return
      [
        'status' => false,
        'message' => 'Reason cannot be empty',
        'data' => null
      ];
    }
    else {
      $newreason = new BorrowReason;
      $newreason->title = $reason;
      if ($newreason->save()) {
        return
        [
          'status' => true,
          'message' => 'Reason saved successfully',
          'data' => null
        ];
      }
    }
  }

  public function actionDeleteborrowreason()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();
    $reason_id = Yii::$app->request->post('reason_id');
    if (empty($reason_id)) {
      return
      [
        'status' => false,
        'message' => 'Reason ID cannot be empty',
        'data' => null
      ];
    }
    else {
      $reason = BorrowReason::findOne($reason_id);
      if ($reason->delete()) {
        return
        [
          'status' => true,
          'message' => 'Reason Deleted successfully',
          'data' => null
        ];
      }
      else {
        return
        [
          'status' => false,
          'message' => 'Error Deleting Reason',
          'data' => null
        ];
      }
    }
  }

  public function actionUpdateborrowreason()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();
    $reason_id = Yii::$app->request->post('id');
    $reason_body = Yii::$app->request->post('title');
    if (empty($reason_id) || empty($reason_body)) {
      return
      [
        'status' => false,
        'message' => 'Reason Fields cannot be empty',
        'data' => null
      ];
    }
    else {
      $reason = BorrowReason::findOne($reason_id);
      $reason->title = $reason_body;
      if ($reason->save()) {
        return
        [
          'status' => true,
          'message' => 'Reason Updated successfully',
          'data' => null
        ];
      }
      else {
        return
        [
          'status' => false,
          'message' => 'Error Deleting Reason',
          'data' => null
        ];
      }
    }
  }

  public function actionGetborrowreason()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();
    $reasons = BorrowReason::find()->asArray()->all();
    if (is_array($reasons)) {
      return
      [
        'status' => true,
        'message' => 'Reason fetched successfully',
        'data' => $reasons
      ];
    }
    else {
      return
      [
        'status' => false,
        'message' => 'Error Getting Reasons, Please try again',
        'data' => null
      ];
    }
  }

  public function actionGetwalletbalance()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();
    $user_id = Yii::$app->request->getQueryParam('user_id');
    $user_details = UserDetails::findOne(['user_login_id' => $user_id]);
    if (count($user_details) > 0) {
      /** Confirm Payment to User Account **/
      $getCard = Yii::$app->kegowloader->getUserCardID($user_details->phone_number, $user_id);
      if (isset($getCard['data'])) {
        return Yii::$app->kegowloader->getWalletBalance($getCard['data']['CardID']);
      }
      else {
        return $getCard;
      }

    }
    else {
      return
      [
        'status' => false,
        'message' => 'User Not Found',
        'data' => null
      ];
    }


  }

  public function inAppKegowLoadCard($amount,$user_login_id)
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();

    // $amount = Yii::$app->request->post('amount');
    if (!$amount || $amount = '') {
      return
      [
        'status' => false,
        'message' => 'Amount Cannot be Empty',
        'data' => null
      ];
    }
    // $amount = Yii::$app->request->post('amount');
    // $user_login_id = Yii::$app->request->post('user_id');
    $user_details = UserDetails::findOne(['user_login_id' => $user_login_id, 'status_id' => 2]);
    if (count($user_details) > 0) {
      /** Tries to Register User if User is not Registered **/
      Yii::$app->kegowloader->kegowRegisterUser($user_details->phone_number,$user_details->firstname);

      /** Confirm Payment to User Account **/

      $res = Yii::$app->kegowloader->loadUserAccount($user_details->phone_number,$amount,$user_login_id);
      return $res;
    }
    else {
      return
      [
        'status' => false,
        'message' => 'User is unactive',
        'data' => null
      ];
    }

  }
  public function actionKegowregisteruser()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();

    $user_id = Yii::$app->request->post('user_id');
    $user_login_id = Yii::$app->request->post('user_id');
    $user_details = UserDetails::findOne(['user_login_id' => $user_login_id, 'status_id' => 2]);
    if (count($user_details) > 0) {
      Yii::$app->kegowloader->kegowRegisterUser($user_details->phone_number,$user_details->firstname);
    }
    // return $res;
  }
  public function createcompanyadmin($company_name, $admin_email, $dbtransaction)
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminAdminApiKey();

    $params = $_REQUEST;
    $passwordLength = 7;
    //Set a Default password of 'Password' for all new Company Admins

    $access_token = ApiKeyHelper::ACCESS_TOKEN;

    $paramsValuePair = [
      UserDetailsApiKeyHelper::PHONE_NUMBER => ApiUtility::TYPE_STRING,
      UserDetailsApiKeyHelper::FIRSTNAME => ApiUtility::TYPE_STRING,
      UserDetailsApiKeyHelper::LASTNAME => ApiUtility::TYPE_STRING,
    ];

    $this->paramCheck($paramsValuePair, $params);

    $user_type = 2;
    $password = $this->getAppParams('companyAdminPassword');
    $companyname = $company_name;

    date_default_timezone_set('Africa/Lagos');
    $time = date('Y-m-d H:i:s');

    $phone_number = $params[UserDetailsApiKeyHelper::PHONE_NUMBER];
    $firstname = $params[UserDetailsApiKeyHelper::FIRSTNAME];
    $lastname = $params[UserDetailsApiKeyHelper::LASTNAME];

    $email = $admin_email;

    $companyid = Company::findOne(['company_name' => $companyname]);
    $get_access_token = ApiUtility::generateAccessToken();

    $transaction = $dbtransaction;

    $error_msg = '';

    try {

      if ($companyid != null) {
        if (!empty($password) && !empty($email)) {

          if (strlen($password) >= $passwordLength) {

            $userlogins = new UserLogin();

            $userlogins->user_email = $email;
            $userlogins->user_password = ApiUtility::generatePasswordHash($password);
            $userlogins->access_token = $get_access_token;
            $userlogins->user_type_id = $user_type;
            $userlogins->created_date = $time;
            $userlogins->status_id = 2;
            $userlogins->modified_date = $time;

            #TODO set default expiration date to Null
            $userlogins->expiration_date = date('Y-m-d H:i:s',122);

            if ($userlogins->validate()) {
              if ($userlogins->save()) {

                $userdetails = new UserDetails();

                $userdetails->email = $email;
                $userdetails->company_id = $companyid->company_id;
                $userdetails->user_type_id = $user_type;
                $userdetails->phone_number = $phone_number;
                $userdetails->firstname = $firstname;
                $userdetails->lastname = $lastname;
                $userdetails->user_login_id = $userlogins->user_login_id;
                $userdetails->created_date = $time;
                $userdetails->modified_date = $time;

                if ($userdetails->validate()) {
                  if ($userdetails->save()) {
                    $transaction->commit();

                    //send respnse email

                    $message = "Your account has been created on our platform.<br>
                    You are activated as the admin for $companyname.<br>
                    Your Login credentials:<br/>
                    Username: <var>Email address on which this email was received</var><br>
                    Password: $password<br><br>
                    Kindly Login to change your password and have access to your page<br>
                    Cheers,<br/><br/>
                    ";


                    $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Admin account creation', $userdetails->email, $this->getReplyEmail(), $email);


                    $this->setHeader(200);

                    return [
                      'status' => true,
                      'message' => 'New admin added for company (' . $companyname . '), An Email Has Been Sent To the owner.',
                      'data' => ['email' => $userdetails->email,
                      'company_id' => $companyid->company_id
                    ]
                  ];

                } else {
                  $error_msg = 'UserDetails not saved, Please try again';
                }
              } else {
                $error_msg = $userdetails->getErrors();
              }

            } else {
              $error_msg = 'UserLogin could not be created, Please try again';
            }
          } else {
            $error_msg = $userlogins->getErrors();
          }

        } else {
          $error_msg = 'Password should not be less than 7 characters';
        }
      } else {
        $error_msg = 'Fill in the required fields';
      }
    } else {
      $error_msg = 'Company not available';
    }
  } catch (Exception $ex) {
    $transaction->rollBack();
    return ApiUtility::errorResponse('Error creating Admin User, Company not Created');
  }

  return $error_msg;

}

public function actionRejectborrowrequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminAdminApiKey();
  $params = $_REQUEST;

  $access_token = ApiKeyHelper::ACCESS_TOKEN;

  $paramsValuePair = [
    BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION => ApiUtility::TYPE_STRING,
    BorrowerLenderApiKeyHelper::PROCESS_REMARKS => ApiUtility::TYPE_STRING
  ];

  $this->paramCheck($paramsValuePair,$params);

  $process_description = $params[BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION];
  $process_remarks = $params[BorrowerLenderApiKeyHelper::PROCESS_REMARKS];

  $borrower_id = Yii::$app->request->getQueryParam('id');

  $checkborrower = Borrow::find()->select(['borrow_id','loan_amount'])->where(['borrow_id'=> $borrower_id, 'approve_flag'=>0, 'status_id'=>2])->asArray()->one();

  //get user email
  $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
  $email = $getemail['email'];
  $company = Company::find()->innerJoin('user_details us','us.company_id =company.company_id')->innerJoin('borrow bo','bo.user_id=us.user_id')->where(['bo.borrow_id'=> $borrower_id])->asArray()->one();
  if($checkborrower['borrow_id'] != null and count($checkborrower) > 0)
  {
    $update =   Borrow::updateAll(['approve_flag'=> 5], ['borrow_id'=> $checkborrower['borrow_id']]);
    $update_description = Borrow::updateAll(['process_description' => $process_description], ['borrow_id' => $checkborrower['borrow_id']]);
    $update_remarks = Borrow::updateAll(['process_remarks' => $process_remarks], ['borrow_id' => $checkborrower['borrow_id']]);


    if($update && $update_remarks) {
      /*
      //Notification email to user don upon Confirmation by Super Admin;
      $message = "Unfortunately, Your loan request of ₦ {$checkborrower['loan_amount']} has been Declined.<br>
      If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
      <br/><br>
      ";


      $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan rejected', $email, $this->getReplyEmail(), $email, $company['company_name']);
      */
      return
      [
        'status' => true,
        'message' => 'user not allowed to borrow',
        'data' => null
      ];
    }
    else{
      $msg = "sorry, we could not perform the reject borrow request please try again";
      return ApiUtility::errorResponse($msg);
    }
  }



  else{
    $msg = "invalid transaction";
    return ApiUtility::errorResponse($msg);
  }


}

public function actionConfirmusers()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminAdminApiKey();
  $user_login_id = Yii::$app->request->getQueryParam('user_login_id');

  $checkifuseralreadyactivated = UserLogin::findOne(['user_login_id' => $user_login_id, 'status_id' => 2]);
  if ($checkifuseralreadyactivated) {
    $errormsg = "user account already activated";
    return ApiUtility::errorResponse($errormsg);
  }
  $checkuserid = UserLogin::findOne(['user_login_id' => $user_login_id, 'status_id' => 3]);
  if ($checkuserid) {

    $checkuserid->status_id = 2;
    if ($checkuserid->update()) {
      //send email to user that account has been activated;
      return [
        'status' => true,
        'message' => 'User account activated successfully',
        'data' => null
      ];
    } else {
      $errormsg = "User account could not be activated";
      return ApiUtility::errorResponse($errormsg);
    }
  }


}

public function actionFetchborrowerrequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  //get company_id of the current admin

  $apikey = Yii::$app->request->getQueryParam('key');
  $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

  $admin_company_id = UserDetails::find()->select(['company_id'])
  ->where(['user_login_id' => $admin_id])->asArray()->one();

  $comp_id = $admin_company_id['company_id'];

  $getborrowerrequest =
  Borrow::findBySql(
    "
    select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
    us.company_id, cm.company_name, br.loan_amount,br.date_of_request,br.date_to_pay,br.borrow_id, reason.title as reason FROM borrow br left join borrow_reason reason ON br.reason_id=reason.reason_id INNER JOIN company cm ON cm.company_id = br.company_id
    inner join user_details us ON us.user_id = br.user_id
    WHERE us.company_id = $comp_id AND br.approve_flag = 0
    AND br.status_id = 2
    "
    )->asArray()->all();


    return [
      'status' => true,
      'message' => 'list of borrowers fetched from company admin',
      'data' => $getborrowerrequest

    ];

  }


  public function actionFetchstaffincompany()
  {
    $this->setHeader(200);
    ApiAuthenticator::verifySuperAdminUserApiKey();

    //get company_id of the current admin

    $apikey = Yii::$app->request->getQueryParam('key');
    $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

    $admin_company_id = UserDetails::find()->select(['company_id'])
    ->where(['user_login_id' => $admin_id])->asArray()->one();

    $comp_id = $admin_company_id['company_id'];


    $getstaffincompany =
    UserLogin::findBySql(
      "
      select usl.user_email,us.firstname, us.lastname,us.email,
      us.company_id, cm.company_name,us.user_login_id,us.phone_number
      from user_login usl INNER JOIN user_details us ON us.user_login_id = usl.user_login_id
      INNER JOIN company cm ON cm.company_id = us.company_id
      WHERE us.company_id = $comp_id AND usl.status_id = 2

      "
      )->asArray()->all();


      return [
        'status' => true,
        'message' => 'List of company staffs fetched successfully',
        'data' => $getstaffincompany
      ];

    }


    public function actionFetchstaffdetails()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminUserApiKey();

      $user_login_id = Yii::$app->request->getQueryParam('staff_id');
      //get company_id of the current admin

      $staffdetails = UserLogin::findBySql(
        "
        select usl.user_email,us.firstname, us.lastname,us.email,
        us.company_id, cm.company_name,us.user_login_id,us.phone_number,
        us.gender, us.address, us.birthdate,us.middlename
        from user_login usl INNER JOIN user_details us ON us.user_login_id = usl.user_login_id
        INNER JOIN company cm ON cm.company_id = us.company_id
        WHERE us.user_login_id = $user_login_id AND usl.status_id = 2

        "
        )->asArray()->all();


        return [
          'status' => true,
          'message' => 'Staffs details fetched successfully',
          'data' => $staffdetails
        ];

      }


      public function actionFetchlenderrequest()
      {
        $this->setHeader(200);
        ApiAuthenticator::verifySuperAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
        ->where(['user_login_id' => $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];


        $getlendrequest =
        Lend::findBySql(
          "
          select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
          us.company_id, cm.company_name, br.date_lend,
          br.amount_to_receive,br.lend_amount,br.lend_id
          from lend br INNER JOIN company cm ON cm.company_id = br.company_id
          inner join user_details us ON us.user_id = br.user_id
          WHERE us.company_id = $comp_id AND br.approve_flag = 0
          AND br.status_id = 2
          "
          )->asArray()->all();


          return [
            'status' => true,
            'message' => 'List of company borrowers fetched successfully',
            'data' => $getlendrequest

          ];

        }

        public function actionDeletestaff()
        {
          $this->setHeader(200);
          ApiAuthenticator::verifySuperAdminApiKey();

          $user_id = Yii::$app->request->getQueryParam('user_id');

          $checkstaff = UserLogin::find()->select(['user_login_id', 'user_email'])->where(['user_login_id' => $user_id, 'status_id' => 2])->asArray()->one();
          $email = $checkstaff['user_email'];
          $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->where(['user_login_id' => $user_id])->asArray->one();

          if ($checkstaff['user_login_id'] != null and count($checkstaff) > 0) {
            $update = UserLogin::updateAll(['status_id' => 1], ['user_login_id' => $checkstaff['user_login_id']]);

            if ($update) {

              //Notification email to user ;
              $message = "Please be informed, that your account has been deactivated on the portal.<br>
              If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
              Thank you for using our portal<br/><br>
              ";


              $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Account Deactivated', $email, $this->getReplyEmail(), $email, $company['company_name']);

              return
              [
                'status' => true,
                'message' => 'User account deleted',
                'data' => null
              ];
            } else {
              $msg = "Sorry, the account could not be removed, please try again later";
              return ApiUtility::errorResponse($msg);
            }

          } else {
            $msg = "Sorry, the user does not exist";
            return ApiUtility::errorResponse($msg);
          }
        }

        public function actionApproveborrowrequest()
        {
          $this->setHeader(200);
          ApiAuthenticator::verifySuperAdminApiKey();

          $borrower_id = Yii::$app->request->getQueryParam('id');

          $checkborrower = Borrow::find()->select(['id', 'loan_amount', 'period'])->where(['id' => $borrower_id, 'approve_flag' => 0, 'status_id' => 2])->asArray()->one();

          $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us', 'us.user_id = borrow.user_id')->where(['borrow.borrow_id' => $borrower_id])->asArray()->one();
          $email = $getemail['email'];

          $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->innerJoin('borrow bo', 'bo.user_id=us.user_id')->where(['bo.borrow_id' => $borrower_id])->asArray->one();

          $time = date('Y-m-d H:i:s');

          if ($checkborrower['id'] != null and count($checkborrower) > 0) {
          $update = Borrow::updateAll(['approve_flag' => 2,/*'date_updated'=>$time*/], ['id' => $checkborrower['id']]);

          if ($update) {

            //approve email to user ;
            $message = "Congratulations!!! Your loan request of ₦ {$checkborrower['loan_amount']} has been Approved.<br>
            Your account will be credited within 48 hours. <br/>
            If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
            Thank you for using our portal<br/><br>
            ";


            $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan processing', $email, $this->getReplyEmail(), $email, $company['company_name']);


            return
            [
              'status' => true,
              'message' => 'user approved to borrow',
              'data' => null
            ];
          } else {
            $msg = "Sorry, we could not approve the loan request, please try again later";
            return ApiUtility::errorResponse($msg);
          }

        } else {
          $msg = "The user does not exist, Please refresh the page and try again";
          return ApiUtility::errorResponse($msg);
        }


      }


      public function actionApprovelendrequest()
      {
        $this->setHeader(200);
        ApiAuthenticator::verifySuperAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('id');

        $checkborrower = Lend::find()->select(['id'])->where(['id' => $borrower_id, 'approve_flag' => 0, 'status_id' => 2])->asArray()->one();


        if ($checkborrower['id'] != null and count($checkborrower) > 0) {
          $update = Lend::updateAll(['approve_flag' => 1], ['id' => $checkborrower['id']]);

          if ($update) {

            return
            [
              'status' => true,
              'message' => 'User loan approved',
              'data' => null
            ];
          } else {
            $msg = "Sorry we could not approve loan request, please try again later";
            return ApiUtility::errorResponse($msg);
          }

        } else {
          $msg = "Sorry, the user does not exist. Please refresh and try again";
          return ApiUtility::errorResponse($msg);
        }

      }

      public function actionConfirmborrowreject(){
        $this->setHeader(200);
        ApiAuthenticator::verifySuperAdminApiKey();
        $params = $_REQUEST;

        $borrower_id = Yii::$app->request->getQueryParam('borrow_id');

        $checkborrower = Borrow::find()->select(['borrow_id','loan_amount','process_remarks','process_description'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 5, 'status_id' => 2])->asArray()->one();

        $reason='';
        if($checkborrower['process_remarks']>0)$reason = TransactionRemark::find()->select('remark_description')->where(['remark_id'=>$checkborrower['process_remarks']])->one()->remark_description;
        $reason .= (strlen($reason)>1)?((strlen($checkborrower['process_description'])>1)?' : ':''):'';
        $reason .= $checkborrower['process_description'];

        //get user email
        $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
        $email = $getemail['email'];
        $company = Company::find()->innerJoin('user_details us','us.company_id =company.company_id')->innerJoin('borrow bo','bo.user_id=us.user_id')->where(['bo.borrow_id'=> $borrower_id])->asArray()->one();

        if ($checkborrower['borrow_id'] != null and count($checkborrower) > 0) {
          $update = Borrow::updateAll(['status_id' => 1,'approve_flag'=>0], ['borrow_id' => $checkborrower['borrow_id']]);

          if ($update) {

            //Notification email to user ;
            $message = "Unfortunately, Your loan request of ₦ {$checkborrower['loan_amount']} has been Declined by your company.<br><br>
            <b>Reason</b>:$reason<br><br>
            If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
            Thank you for using our portal";


            $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan processingDeclined', $email, $this->getReplyEmail(), $email);

            return
            [
              'status' => true,
              'message' => 'User loan decline confirmed',
              'data' => null
            ];
          } else {
            $msg = "Sorry, we could not confirm loan rejection at this time, Please try again";
            return ApiUtility::errorResponse($msg);
          }
        } else {
          $msg = "Sorry, the Loan request does not exist. Please refresh and try again";
          return ApiUtility::errorResponse($msg);
        }

      }

      /*
      public function actionRejectborrowrequest()
      {
      $this->setHeader(200);
      ApiAuthenticator::verifySuperAdminApiKey();
      $params = $_REQUEST;

      $access_token = ApiKeyHelper::ACCESS_TOKEN;

      $paramsValuePair = [
      BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION => ApiUtility::TYPE_STRING,
      BorrowerLenderApiKeyHelper::PROCESS_REMARKS => ApiUtility::TYPE_STRING
    ];

    $this->paramCheck($paramsValuePair,$params);

    $process_description = $params[BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION];
    $process_remarks = $params[BorrowerLenderApiKeyHelper::PROCESS_REMARKS];

    $borrower_id = Yii::$app->request->getQueryParam('id');

    $checkborrower = Borrow::find()->select('id')->where(['id' => $borrower_id, 'approve_flag' => 0, 'status_id' => 2])->asArray()->one();

    //get user email
    $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
    $email = $getemail['email'];
    $company = Company::find()->innerJoin('user_details us','us.company_id =company.company_id')->innerJoin('borrow bo','bo.user_id=us.user_id')->where(['bo.borrow_id'=> $borrower_id])->asArray()->one();

    if ($checkborrower['id'] != null and count($checkborrower) > 0) {
    $update = Borrow::updateAll(['status_id' => 1], ['id' => $checkborrower['id']]);
    $update_description = Borrow::updateAll(['process_description' => $process_description], ['id' => $checkborrower['id']]);
    $update_remarks = Borrow::updateAll(['process_remarks' => $process_remarks], ['id' => $checkborrower['id']]);


    if ($update && $update_remarks) {

    //Notification email to user ;
    $message = "Unfortunately, Your loan request of ₦ {$checkborrower['loan_amount']} has been Declined.<br>
    If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
    Thank you for using our portal";


    $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan processing', $email, $this->getReplyEmail(), $email);

    return
    [
    'status' => true,
    'message' => 'User loan declined',
    'data' => null
  ];
} else {
$msg = "Sorry, we could not reject the loan request at this time, Please try again";
return ApiUtility::errorResponse($msg);
}
} else {
$msg = "Sorry, the user does not exist. Please refresh and try again";
return ApiUtility::errorResponse($msg);
}


}*/

public function actionTotalusers()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $users = UserDetails::find()->where(['user_type_id' => 3])->asArray()->all();

  $numberofuser = count($users);


  return
  [
    'status' => true,
    'message' => 'Total Number of Users',
    'data' => $numberofuser
  ];

}

public function actionTotallends()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $lenders = Lend::find()->asArray()->all();

  $numberofuser = count($lenders);

  return
  [
    'status' => true,
    'message' => 'Total Number of Lends',
    'data' => $numberofuser
  ];
}




public function actionTotaldeclines()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $declines = Borrow::find()->select(['borrow_id','loan_amount'])->where(['status_id'=>1])->asArray()->all();
  $numberofuser = count($declines);
  $ForCount = 0;

  foreach ($declines as  $value) {
    $ForCount +=intval($value['loan_amount']);
  };

  // Total Amount Declined and number of declines loans for Super Admin
  return
  [
    'status' => true,
    'message' => 'Total Number of Declines',
    'data' => $numberofuser,
    'amount' => $ForCount
  ];
}


public function actionAmountdeclined()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();

  $AmountDeclined = Borrow::find()->select(['loan_amount'])->where(['status_id'=>1])->asArray()->all();
  $TotalDeclined = array_sum($AmountDeclined);
  // Total Amount Declined and number of declines loans for Super Admin
  return
  [
    'status' => true,
    'message' => 'Total amount Declined',
    'data' => $TotalDeclined
  ];
}

public function actionTotalamountborrowed(){
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $access_token = ApiKeyHelper::ACCESS_TOKEN;
  $apikey = Yii::$app->request->getQueryParam('key');

  $loanamount = Borrow::find()->select('loan_amount')->where(['status_id'=>2,'approve_flag'=>[3,4]])->asArray()->all();
  $amounts = 0;
  foreach($loanamount as $amount){
    $amounts += $amount["loan_amount"];
  }

  if (!is_array($loanamount)) {
    $message = "An Error Occured";
    return ApiUtility::errorResponse($message);
  }elseif(empty($loanamount)&& $amounts<=0){
    $this->setHeader(200);
    return [
      'status' => true,
      'message' => 'No Borrowed at the moment',
      'data' => 0
    ];
  } else {
    $this->setHeader(200);
    return [
      'status' => true,
      'message' => 'Total Amount Borrowed Fetched Successfully',
      'data' => $amounts
    ];
  }

}



public function actionTotalborrowers()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $borrowers = UserDetails::find()->distinct(true)->select(['user_details.user_id'])->innerJoin('borrow bo','user_details.user_id=bo.user_id')->where(['and','bo.status_id=2','bo.approve_flag>2'])->asArray()->all();

  $numberofuser = count($borrowers);

  return
  [
    'status' => true,
    'message' => 'Total Number of Borrowers',
    'data' => $numberofuser
  ];
}

public function actionTotallenders()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $lenders = UserDetails::find()->distinct(true)->select(['user_details.user_id'])->innerJoin('lend ld','user_details.user_id=ld.user_id')->asArray()->all();

  $numberofuser = count($lenders);

  return
  [
    'status' => true,
    'message' => 'Total Number of Lenders',
    'data' => $numberofuser
  ];
}


public function actionTotaluserspercompany()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $companies = $this->getCompaniesid();
  $usersbycompany = array();
  $us = array();
  foreach ($companies as $company) {
    $userspercompany = UserDetails::find()->where(['company_id' => $company["company_id"]])->asArray()->all();
    $numberofuser = count($userspercompany);
    $usersbycompany["company_name"] = $company["company_name"];
    $usersbycompany["number_of_user"] = $numberofuser;
    $usersbycompany["is_active"] = $company["is_active"];
    $usersbycompany["is_approved"] = $company["is_approved"];
    array_push($us, $usersbycompany);
  }

  return
  [
    'status' => true,
    'message' => 'Total User Per Company',
    'data' => $us
  ];
}

public function actionUpdatecompanyapproved()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  if (Yii::$app->request->isPost) {
    $company_id = Yii::$app->request->post('company_id');
    $company = Company::findOne(['company_id' => $company_id]);
    if (count($company) == 1) {
      $company->is_approved = 1;
      if ($company->save()) {
        return
        [
          'status' => true,
          'message' => 'Success',
          'data' => $company
        ];
      }
    }
  }
}

public function actionDeletecompany()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  if (Yii::$app->request->isPost) {
    $company_id = Yii::$app->request->post('company_id');
    $company = Company::findOne(['company_id' => $company_id]);
    if (count($company) == 1) {

      $lend = Lend::find()->where(['company_id' => $company->company_id, 'status_id' => 2])->all();
      $borrow = Borrow::find()->where(['company_id' => $company->company_id, 'status_id' => 2])->all();
      $lendCount = count($lend);
      $borrowCount = count($borrow);
      if ($lendCount > 0 || $borrowCount > 0) {
        return
        [
          'status' => false,
          'message' => 'Error - One or More Users in This Company as an Active Transaction!',
          'data' => null,
          'lenders' => $lendCount,
          'borrowers' => $borrowCount,
        ];
      }
      else {
        $admin = UserDetails::find()->where(['company_id' => $company->company_id, 'user_type_id' => 1])->all();
        $adminCount = count($admin);

        if ($adminCount > 0) {
          return
          [
            'status' => false,
            'message' => 'Cannot delete Super admin Company',
            'count' => $adminCount,
          ];
        }
        elseif ($adminCount <= 0) {
          $company->is_active = 0;
          if ($company->save()) {
            return
            [
              'status' => true,
              'message' => 'Company Successfully Deleted',
              'data' => $company
            ];
          }
        }
      }
    }
  }
}

public function actionTotalusersapprove()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $userapprove = Borrow::find()->where(['approve_flag' => 2])->asArray()->all();
  $numberofuserapprove = count($userapprove);

  return
  [
    'status' => true,
    'message' => 'Total Approved loan requests',
    'data' => $numberofuserapprove
  ];
}

public function actionTotalusersreject()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $userreject = Borrow::find()->where(['status_id' => 1])->asArray()->all();
  $numberofuserreject = count($userreject);

  return
  [
    'status' => true,
    'message' => 'Total rejected Loan requests',
    'data' => $numberofuserreject
  ];
}

public function actionLoanpermonth()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  $loanerbymonth = Borrow::findBySql("
  select YEAR(date_of_request) as year, MONTH(date_of_request) as month, COUNT(*) as number_of_user FROM borrow GROUP BY year, month
  ")->asArray()->all();

  if ($loanerbymonth != null) {
    $this->setHeader(200);
    return
    [
      'status' => true,
      'message' => 'Number of Loan Applicants Per Month',
      'data' => $loanerbymonth
    ];
  } else {
    $msg = "Loan Applicants list is empty";
    return ApiUtility::errorResponse($msg);
  }


}

public function actionLenderpermonth()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $lenderbymonth = Lend::findBySql("
  select YEAR(date_lend) as year, MONTH(date_lend) as month, COUNT(*) as number_of_user FROM lend GROUP BY year, month
  ")->asArray()->all();
  if ($lenderbymonth != null) {
    $this->setHeader(200);
    return
    [
      'status' => true,
      'message' => 'Number of Lenders Per Month',
      'data' => $lenderbymonth
    ];
  } else {
    $msg = "Lenders list is empty";
    return ApiUtility::errorResponse($msg);
  }


}

public function actionGetbankdetails()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();


  $user_bank_details = UserBankDetails::find()->select(['user_details.firstname', 'user_details.lastname',
  'user_bank_details.bank_name',
  'user_bank_details.account_number', 'user_bank_details.bvn'])->innerJoin('user_details', 'user_details.user_id = user_bank_details.user_id')
  ->where(['user_details.status_id' => 2])->asArray(true)->all();

  return
  [
    'status' => true,
    'message' => 'users bank details fetched successfully',
    'data' => $user_bank_details
  ];


}


public function actionGetdisbursementoptions()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $paymentoptions = self::get_payment_options('disbursement');

  return
  [
    'status' => true,
    'message' => 'Disbursement options fetched successfully',
    'data' => $paymentoptions
  ];

}

public function actionGetrepaymentoptions()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();


  $paymentoptions = self::get_payment_options('repayment');

  return
  [
    'status' => true,
    'message' => 'Repayment options fetched successfully',
    'data' => $paymentoptions
  ];

}

public function getCompaniesid()
{
  return Company::find()->select(['company_id', 'company_name','is_active', 'is_approved'])->asArray()->all();
}

public function actionGetalllenders()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $lenders = Lend::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'lend.lend_id', 'lend.lend_amount', 'lend.date_lend', 'lend.lend_tenure', 'lend.amount_to_receive', 'lend.status_id', 'lend.approve_flag',
  'lend.company_id', 'company.company_name',
  ])->innerJoin('user_details', 'user_details.user_id = lend.user_id')->innerJoin('company', 'company.company_id = lend.company_id')->asArray(true)->all();

  if ($lenders) {
    $this->setHeader(200);
    return
    [
      'status' => true,
      'message' => 'All lenders fetched successfully',
      'data' => $lenders
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Lender',
      'data' => null
    ];
  }
}

public function actionGetallrequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $borrowers = Borrow::find()->select(['user_details.firstname','user_details.user_login_id', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'borrow.borrow_id', 'borrow.loan_amount', 'borrow.amount_to_pay','borrow.transaction_id', 'borrow.period', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
  'borrow.company_id','borrow_reason.title as reason','borrow.other_reasons', 'company.company_name',
  ])->innerJoin('user_details', 'user_details.user_id = borrow.user_id')->innerJoin('company', 'company.company_id = borrow.company_id')->leftJoin('borrow_reason', 'borrow_reason.reason_id = borrow.reason_id')->asArray(true)->all();

  if ($borrowers) {
    $this->setHeader(200);
    return
    [
      'status' => true,
      'message' => 'All request fetched successfully',
      'data' => $borrowers
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }
}

public function actionOpenloans()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $openloans = Borrow::find()->select(['user_details.firstname','user_details.user_login_id', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'borrow.borrow_id', 'borrow.loan_amount', 'borrow.amount_to_pay','borrow.transaction_id', 'borrow.period', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
  'borrow.company_id','borrow_reason.title as reason','borrow.other_reasons', 'company.company_name',
  ])->innerJoin('user_details', 'user_details.user_id = borrow.user_id')->innerJoin('company', 'company.company_id = borrow.company_id')->leftJoin('borrow_reason', 'borrow_reason.reason_id = borrow.reason_id')->where(['borrow.approve_flag' => 3,'borrow.status_id' => 2])->asArray(true)->all();

  if ($openloans) {
    $this->setHeader(200);
    return
    [
      'status' => true,
      'message' => 'All request fetched successfully',
      'data' => $openloans
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }
}

public function actionCloseopenloans()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  $b_id = $_GET['b_id'];
  $closeloans = Yii::$app->db->createCommand('UPDATE borrow SET approve_flag = 4, status_id = 3 WHERE borrow_id = :b_id')
                       ->bindParam(':b_id', $b_id)->execute();
                              

  if ($closeloans > 0) {
    $this->setHeader(200);
    //Superadmin::actionOpenloans();
    return
    [
      'status' => true,
      'message' => 'Loan closed successfully'  
    ];
      
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }
}

public function actionGetallpendingrequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $borrowers = Borrow::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'borrow.borrow_id', 'borrow.loan_amount', 'borrow.amount_to_pay', 'borrow.date_of_request', 'borrow.period', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
  'borrow.process_remarks','borrow.process_description','borrow.company_id','borrow.other_reasons','borrow_reason.title as reason', 'company.company_name',
  ])->innerJoin('user_details', 'user_details.user_id = borrow.user_id')->innerJoin('company', 'company.company_id = borrow.company_id')->leftJoin('borrow_reason', 'borrow_reason.reason_id = borrow.reason_id')->where(['borrow.approve_flag'=>[1,5],'borrow.status_id'=>2])->asArray(true)->all();
  if ($borrowers) {
    $this->setHeader(200);
    return
    [
      'status' => true,
      'message' => 'All Pending request fetched successfully',
      'data' => $borrowers
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }
}

public function actionGetallinitiatedrequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $borrowers = Borrow::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'borrow.borrow_id', 'borrow.loan_amount', 'borrow.amount_to_pay', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
  'borrow.company_id', 'company.company_name',
  ])->innerJoin('user_details', 'user_details.user_id = borrow.user_id')->innerJoin('company', 'company.company_id = borrow.company_id')->where(['borrow.approve_flag' => 0, 'borrow.status_id' => 2])->asArray(true)->all();

  if ($borrowers) {
    $this->setHeader(200);
    return
    [
      'status' => true,
      'message' => 'All Initiated borrow request fetched successfully',
      'data' => $borrowers
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }
}

public function actionApproverequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();

  $borrower_id = Yii::$app->request->getQueryParam('borrow_id');

  $checkborrower = Borrow::find()->select(['borrow_id'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 1, 'status_id' => 2])->asArray()->one();

  $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->innerJoin('borrow bo', 'bo.user_id=us.user_id')->where(['bo.borrow_id' => $borrower_id])->asArray()->one();

  //get user email
  $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us', 'us.user_id = borrow.user_id')->where(['borrow.borrow_id' => $borrower_id])->asArray()->one();
  $email = $getemail['email'];
  if ($checkborrower['borrow_id'] != null and count($checkborrower) > 0) {
    $update = Borrow::updateAll(['approve_flag' => 2], ['borrow_id' => $checkborrower['borrow_id']]);

    if ($update) {
      //approve email to user ;
      $message = "We wish to inform you that your loan application has been approved, and your bank account account will be credited shortly.]<br/><br/>";

      $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Approved', $email, $this->getReplyEmail(), $email, $company['company_name']);


      return
      [
        'status' => true,
        'message' => 'user approved to borrow',
        'data' => null
      ];
    } else {
      $msg = "Sorry, we could not approve the loan request, please try again later";
      return ApiUtility::errorResponse($msg);
    }

  } else {
    $msg = "invalid transaction";
    return ApiUtility::errorResponse($msg);
  }
}

public function actionRejectrequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();
  $params = $_REQUEST;
  //$params[BorrowerLenderApiKeyHelper::PROCESS_REMARKS] = join(',',$params[BorrowerLenderApiKeyHelper::PROCESS_REMARKS]);
  if(isset($params['process_remarks']))$params['process_remarks'] = join(',',$params['process_remarks']);

  //Ensure atleast one of the reasons are set
  if(strlen($params['process_remarks']) < 1 && strlen($params["process_description"]) < 4)
  return ApiUtility::errorResponse("You have not set a valid reason");

  //Provide for empty value
  if(strlen($params['process_remarks']) < 1)$params['process_remarks']=0;
  if(strlen($params["process_description"]) < 1)$params["process_description"]="N/A";

  $access_token = ApiKeyHelper::ACCESS_TOKEN;

  $paramsValuePair = [
    BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION => ApiUtility::TYPE_STRING,
    BorrowerLenderApiKeyHelper::PROCESS_REMARKS => ApiUtility::TYPE_STRING
  ];

  $this->paramCheck($paramsValuePair,$params);

  $process_description = $params[BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION];
  $process_remarks = $params[BorrowerLenderApiKeyHelper::PROCESS_REMARKS];

  $borrower_id = Yii::$app->request->getQueryParam('borrow_id');
  $checkborrower = Borrow::find()->select(['borrow_id','loan_amount'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 1, 'status_id' => 2])->asArray()->one();

  /*
  //get user email
  $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
  $email = $getemail['email'];
  */

  if ($checkborrower['borrow_id'] != null and count($checkborrower) > 0) {
    $update = Borrow::updateAll(['approve_flag' => 6], ['borrow_id' => $checkborrower['borrow_id']]);
    $update_description = Borrow::updateAll(['process_description' => $process_description], ['borrow_id' => $checkborrower['borrow_id']]);
    $update_remarks = Borrow::updateAll(['process_remarks' => $process_remarks], ['borrow_id' => $checkborrower['borrow_id']]);

    if ($update && $update_description && $update_remarks) {

      /*
      //approve email to user ;
      $message = "We wish to inform you that your application to borrow money wasn't successful. Please Try Again<br/>
      <br/>
      Thanks for using RibyPeerLending<br/> Team RibyPeerLending";
      $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $email, $this->getReplyEmail(), $email);
      */

      return
      [
        'status' => true,
        'message' => 'user request rejected',
        'data' => null
      ];
    } else {
      $msg = "Oops, please try again later thanks";
      return ApiUtility::errorResponse($msg);
    }

  } else {
    $msg = "invalid transaction";
    return ApiUtility::errorResponse($msg);
  }
}

public function actionCheckborrowrepaymentstatus()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();
  $transaction_id = Yii::$app->request->getQueryParam('transaction_id');
  // return $transaction_id;
  $all_details = Borrow::find()->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.transaction_id'=> $transaction_id])->asArray()->one();
  if ($all_details <= 0) {
    return
    [
      'status' => false,
      'message' => 'Invalid Transaction ID',
      'data' => null
    ];
  }
  else {
    $total_disbursed = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
    where(['transaction_id'=>$transaction_id])->asArray()->one();
    // $payments = UserPayment::find()->where()->asArray()->all();
    $current_month = date('F');
    $dues = UserPayment::findBySql("
      SELECT * FROM user_payment WHERE transaction_id = '{$transaction_id}' AND month_to_pay = '{$current_month}' AND paid != 1
    ")->all();
    return
    [
      'status' => true,
      'message' => 'Data fetched Successfully',
      'data' => $dues,
      'total' => $total_disbursed
    ];
  }
}

public function Generateloannote($borrow_id, $transaction_id)
{


  $connection = Yii::$app->db;
  $transaction = $connection->beginTransaction();

  $new_loan_note = LoanNotes::findOne(['borrow_id' => $borrow_id]);
  // return $new_loan_note;
  $update_borrow = Borrow::findOne(['borrow_id' => $borrow_id]);

  $amount_to_pay = unserialize($new_loan_note->amount_to_pay);
  $period = unserialize($new_loan_note->period);
  $interest_rate = unserialize($new_loan_note->interest_rate);
  $fixed_charge = unserialize($new_loan_note->fixed_charge);
  $insurance_rate = unserialize($new_loan_note->insurance_rate);
  $priority = unserialize($new_loan_note->priority);
  $fine = unserialize($new_loan_note->fine);
  $loan_amount = $update_borrow->loan_amount;

  $user_id = $new_loan_note->user_id;
  $user_details = UserDetails::find()->where(['user_login_id' => $user_id])->one();
  $company_details = Company::find()->where(['company.company_id' => $user_details['company_id'], 'user_details.user_type_id' => 2])->leftJoin('user_details', 'user_details.company_id = company.company_id')->one();
  $get_super_admin = UserDetails::find()->where(['user_details.company_id' => $user_details['company_id'], 'user_details.user_type_id' => 2])->one();

  $principal_repayment_holder = unserialize($new_loan_note->principal_repayment_holder);
  $monthly_interest_holder = unserialize($new_loan_note->monthly_interest_holder);
  $insurance_charges_holder = unserialize($new_loan_note->insurance_charges_holder);
  $priority_charges_holder = unserialize($new_loan_note->priority_charges_holder);
  $fines_charges_holder = unserialize($new_loan_note->fines_charges_holder);
  $transaction_charges_holder = unserialize($new_loan_note->transaction_charges_holder);
  $monthly = unserialize($new_loan_note->monthly);
  $principal_charge = unserialize($new_loan_note->principal_charge);

  $payment_total_charges = unserialize($new_loan_note->payment_total_charges);
  $loan_repayment = unserialize($new_loan_note->loan_repayment);
  $total_repayment_rate = unserialize($new_loan_note->total_repayment_rate);
  $effrate = unserialize($new_loan_note->effrate);

  $new_loan_note->date_disbursed = date('d/m/Y');
  $new_loan_note->transaction_id = $transaction_id;

  if ($new_loan_note->save()) {
    // return $new_loan_note_id;
  $pdf = new FPDF();
  $pdf->AddPage();
  $name = strtoupper($user_details->firstname." ".$user_details->lastname);
  $date = date('l \, jS \of F Y');
  $pdf->SetTitle($name.' Loan Note - '.$date);
  $pdf->SetFont('Times','B');
  $pdf->SetFontSize(20);
  $pdf->Cell(40,10,$name);
  $pdf->Ln(10);
  $pdf->SetFont('Times','',14);
  $pdf->SetTextColor(0);
  $pdf->SetFontSize(12);
  $pdf->Cell(30,5,'Loan Note',0,1);
  $pdf->Ln(5);
  $pdf->SetFontSize(9);
  $pdf->SetTextColor(0);
  $pdf->Cell(75,5,"Date: ".$date,0,0);
  $eir = $effrate."%";
  $pdf->Cell(73,5,'Effective Interest Rate: '.$eir,1,0,'C');
  $pdf->Cell(42,5,'Total Repayment: N'.number_format($amount_to_pay, 2),1,1,'C');

  $pdf->Ln(5);
  $loan_type = ucfirst('Standard');
  if ($priority) {
    $loan_type = ucfirst('Priority');
  }

  $pdf->Cell(75,5,'Loan Type: '.$loan_type,0,0);
  $tr = number_format(89063);
  $pdf->Cell(73,5,'Total Repayment Rate Inclusive of All Charges: '.$total_repayment_rate.'%',1,0,'L');
  $pdf->Cell(42,5,'Loan Repayment: N'.number_format($loan_repayment,2),1,1);
  $pdf->SetTextColor(0);

  $pdf->Cell(75,5,'',0,0);
  $pdf->Cell(73,5,'Loan Tenure in Months: '.$period,1,0,'C');
  $pdf->Cell(42,5,'Total Charges: N'.number_format($payment_total_charges,2),1,1,'C');
  $pdf->Ln(5);

  $pdf->SetFontSize(20);
  $pdf->SetTextColor(32, 200, 91);


  $pdf->Cell(110,5,'TRANSACTION ID: '.$transaction_id,0,0);
  $pdf->Cell(70,5,'',0,0,'C');
  $pdf->Cell(20,5,'',0,1,'C');

  $pdf->SetFontSize(9);
  $pdf->SetTextColor(0);

  $pdf->Cell(190,5,'Loan Amount',0,1,'R');
  $pdf->SetFontSize(20);
  $loan_amount = number_format($loan_amount);
  $pdf->Cell(190,5,'N'.$loan_amount,0,1,'R');
  $pdf->SetFontSize(9);

  $pdf->Image(getcwd().'/logo.png',150,10,-100);
  $pdf->Ln(5);
  $pdf->SetFont('Times','',9);

  /** Loan Details Configurations **/

  $pdf->Cell(30,15,'Description',1,0,'C');
  $tr = number_format(95875);
  $pdf->Cell(30,15,'Amount / Rate',1,0,'C');
  $pdf->Cell(100,10,'Repayments',1,0,'C');
  $pdf->SetFillColor(230, 119, 22);
  $pdf->Cell(30,20,'Total',1,0,'C','***1***');
  $pdf->Ln(10);
  $pdf->Cell(30,5,'',0,0,'C');
  $pdf->Cell(30,5,'',0,0,'R');
  $i = 0;
  $len = count($monthly);
  $rW = 100/$len;
  foreach ($monthly as $key => $value) {
    if(++$i === $len) {
      $pdf->Cell($rW,5,($key+1),1,1,'C');
    }
    else {
      $pdf->Cell($rW,5,($key+1),1,0,'C');
    }
  }

  $pdf->SetFillColor(131, 146, 132);
  $pdf->Cell(30,5,'Principal',1,0,'C','***1***');
  $principal = $loan_amount;
  $pdf->Cell(30,5,'N'.$principal,1,0,'R','***1***');
  $i = 0;
  $len = count($principal_charge);
  $rW = 100/$len;
  foreach ($principal_charge as $key => $value) {
    if(++$i === $len) {
      $pdf->Cell($rW,5,'N'.number_format($value,2),1,1,'C','***1***');
    }
    else {
      $pdf->Cell($rW,5,'N'.number_format($value,2),1,0,'C','***1***');
    }
  }


  $pdf->SetFillColor(131, 146, 132);
  $pdf->Cell(30,10,'Principal Repayment',1,0,'C');
  $pdf->Cell(30,10,$period,1,0,'R');
  foreach ($principal_repayment_holder as $key => $value) {
      $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
  }
  $principal_total = number_format(array_sum($principal_repayment_holder), 2);
  $pdf->Cell(30,10,$principal_total,1,1,'R');

  $pdf->Cell(30,10,'Monthly Interest Rate',1,0,'C');
  $pdf->Cell(30,10,($interest_rate*100).'%',1,0,'R');
  foreach ($monthly_interest_holder as $key => $value) {
      $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
  }
  $monthly_int_total = number_format(array_sum($monthly_interest_holder), 2);
  $pdf->Cell(30,10,$monthly_int_total,1,1,'R');

  $pdf->Cell(30,10,'Priority Interest',1,0,'L');
  $pdf->Cell(30,10,($priority*100).'%',1,0,'R');
  $tr = number_format(89063);
  foreach ($priority_charges_holder as $key => $value) {
      $pdf->Cell($rW,10,number_format($value,2),1,0,'R');
  }
  $priority_total = number_format(array_sum($priority_charges_holder), 2);
  $pdf->Cell(30,10,$priority_total,1,1,'R');

  $pdf->Cell(30,10,'Fines',1,0,'L');
  $pdf->Cell(30,10,($fine*100).'%',1,0,'R');
  foreach ($fines_charges_holder as $key => $value) {
      $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
  }
  $fines_total = number_format(array_sum($fines_charges_holder), 2);
  $pdf->Cell(30,10,$fines_total,1,1,'R');

  $pdf->Cell(30,10,'Transaction Charges',1,0,'L');
  $pdf->Cell(30,10,'N'.$transaction_charges_holder[0],1,0,'R');
  foreach ($transaction_charges_holder as $key => $value) {
      $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
  }

  $transaction_charges_total = number_format(array_sum($transaction_charges_holder), 2);
  $pdf->Cell(30,10,'N'.$transaction_charges_total,1,1,'R');

  $pdf->Cell(30,10,'Assurance Charges',1,0,'L');
  $pdf->Cell(30,10,($insurance_rate*100).'%',1,0,'R');
  $tr = number_format(89063);
  foreach ($insurance_charges_holder as $key => $value) {
      $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
  }
  $assurance_charges_total = number_format(array_sum($insurance_charges_holder), 2);
  $pdf->Cell(30,10,'N'.$assurance_charges_total,1,1,'R');

  $pdf->Cell(60,10,'Monthly/Total Repayments',1,0,'L');
  $pdf->SetFillColor(131, 146, 132);
  foreach ($monthly as $key => $value) {
      $pdf->Cell($rW,10,'N'.number_format($value,2),1,0,'R');
  }
  $pdf->SetFillColor(228, 24, 9);
  $montly_repayment_total = number_format($amount_to_pay, 2);
  $pdf->Cell(30,10,'N'.$montly_repayment_total,1,1,'R','***1***');
  $pdf->Ln(5);
  $pdf->SetFillColor(255,255,255);
  $pdf->Cell(70,5,'Approvals, Repayment Mode, Ts&Cs:',0,0,'L');
  $pdf->Cell(90,5,'All loans / lending are based on an intra-company peer-to-peer marketplace where one',0,1);
  $pdf->Cell(70,5,'',0,0,'L');
  $pdf->Cell(90,5,'colleague lends to another. Loans are approved by the Human Resources and Accounts ',0,1);
  $pdf->Cell(70,5,'',0,0,'L');
  $pdf->Cell(90,5,'Department and Repayment are done through Salary Deductions. By accepting this loan,',0,1);
  $pdf->Cell(70,5,'',0,0,'L');
  $pdf->Cell(90,5,'you hereby also accept all applicable charges and rates stated above and the terms and ',0,1);
  $pdf->Cell(70,5,'',0,0,'L');
  $pdf->Cell(90,5,'conditions of service',0,1);
  $pdf->Cell(70,5,'',0,1,'L');
  $pdf->Cell(40,5,'For Service & Inquiries:',1,1,'C');
  $pdf->Cell(40,5,$company_details->company_name,1,0,'L');
  $pdf->Cell(40,5,'Operations',1,0,'L');
  $pdf->Cell(60,5,$get_super_admin->email,1,0,'L');
  $pdf->Cell(40,5,$get_super_admin->phone_number,1,1,'L');

  $pdf->Cell(40,5,$company_details->company_name,1,0,'L');
  $pdf->Cell(40,5,'Operations',1,0,'L');
  $pdf->Cell(60,5,'-',1,0,'L');
  $pdf->Cell(40,5,'-',1,1,'L');


  $pdf->Cell(40,5,'Platform Service by Riby',1,0,'L');
  $pdf->Cell(40,5,'Operations Manager',1,0,'L');
  $pdf->Cell(60,5,'peerlending@riby.me',1,0,'L');
  $pdf->Cell(40,5,'01.291.4247',1,0,'L');

  $getDate = date('jS \, F Y');
  $fileBuffer = 'Loan Note - '.$name.'-'.$date.'.pdf';
  $filename= getcwd().'/loannotes/'.$fileBuffer;
  $pdf->Output($filename, 'F');
  $update_borrow->loan_note_url = $fileBuffer;
  if ($update_borrow->save()) {
    $transaction->commit();
    return [$fileBuffer, $fileBuffer];
  }
  else {
    $transaction->rollBack();
    exit();
  }
}
else {
  $transaction->rollBack();
  exit();
}


}

public function actionLoannotetest()
{
  $gen = $this->Generateloannote(21, 78829901);
  return $gen;
}

public function actionGetrepaymentdues()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();
  $current_month = date('F');
  $dues = UserPayment::findBySql("
    SELECT * FROM user_payment WHERE month_to_pay = '{$current_month}' AND paid = 0
  ")->all();
  return [
    'status' => true,
    'message' => 'Dues Fetch',
    'data' => $dues
  ];
}
public function actionConfirmborrowdisburse(){

  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();
  $borrower_id = Yii::$app->request->getQueryParam('borrow_id');
  $access_token = ApiKeyHelper::ACCESS_TOKEN;


  $paramsValuePair = [
    PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID => ApiUtility::TYPE_INT,
    // UserPaymentApiKeyHelper::REFERENCE_ID => ApiUtility::TYPE_STRING,
    UserPaymentApiKeyHelper::PAYMENT_AMOUNT => ApiUtility::TYPE_STRING,
  ];

  $paymenttype = self::getdisbursementpaymenttype();

  //Ensure that payment of 'Disbursement' type exists
  if(count($paymenttype)<1)
  return ApiUtility::errorResponse("Payment of disbursement type does not exist");

  $payment_type_id = $paymenttype['payment_type_id'];
  $params = $_REQUEST;

  $this->paramCheck($paramsValuePair,$params);

  $payment_option = $params[PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID];
  // $reference_id = $params[UserPaymentApiKeyHelper::REFERENCE_ID];
  $payment_amount = $params[UserPaymentApiKeyHelper::PAYMENT_AMOUNT];

  $checkborrower = Borrow::find()->select(['borrow_id','loan_amount','period','transaction_id'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 2, 'status_id' => 2])->asArray()->one();
  $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->innerJoin('borrow bo', 'bo.user_id=us.user_id')->where(['bo.borrow_id' => $borrower_id])->asArray()->one();
  $user = Borrow::find()->select('us.email,us.phone_number,us.user_login_id,us.firstname,us.address,us.city')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
  $user_bank_details = UserBankDetails::findOne(['user_id' => $user['user_login_id']]);
  $getemail['email'] = $user['email'];
  // return $getemail;
  $monthly = Yii::$app->request->post('monthly');
  $months_to_pay = Yii::$app->request->post('months_to_pay');

  //kegow_transaction_type which is either 1 or 2. 1 => CARD TRANSFER, 2 => BANK TRANSFER
  $kegow_transaction_type = Yii::$app->request->post('kegow_transaction_type');
  // $payment_description = Yii::$app->request->post('payment_description');

  $email = $getemail['email'];
  $time = date('Y-m-d H:i:s');
  $date_to_pay = date('Y-m-d H:i:s', strtotime('+'.$checkborrower['period'].' months',strtotime($time)));

  $total_disbursed = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
  where(['transaction_id'=>$checkborrower['transaction_id'],'payment_type'=>$payment_type_id])->asArray()->one();
  //Check if loan has been fully disbursed
  // if((intval($total_disbursed['payment_amount'])) >= (intval($checkborrower['loan_amount'])))
  // return ApiUtility::errorResponse("Loan already disbursed.");

  //Ensure amount being disbursed is not greater than loan
  if(intval($payment_amount)>intval($checkborrower['loan_amount']))
  return ApiUtility::errorResponse("Disbursement amount exceeds requested loan.");

  //Ensure total amount disbursed is not greater than loan
  // if((intval($total_disbursed['payment_amount'])+intval($payment_amount))>intval($checkborrower['loan_amount']))
  //     return ApiUtility::errorResponse("Total Disbursement amount will exceed requested loan.");

  // return $payment_type_id;

  if ($checkborrower['borrow_id'] != null and count($checkborrower) > 0) {

    $savedCount = 0;

    $connection = Yii::$app->db;
    $transaction = $connection->beginTransaction();

    foreach (array_combine($monthly, $months_to_pay) as $key => $m_to_pay) {

      $userpayment = new UserPayment;
      if ($payment_option == 1) {
        $userpayment->transaction_id = $checkborrower['transaction_id'];
        $userpayment->payment_amount = $key;
        $userpayment->payment_option = $payment_option;
        $userpayment->payment_type = $payment_type_id;
        $userpayment->reference_id = 0000000;
        $userpayment->payment_date = date('Y-m-d H:i:s');
        $userpayment->status_id = 2;
        $userpayment->month = $savedCount+1;
        $userpayment->month_to_pay = $m_to_pay;
        //Fill remaining table slots
        $userpayment->payment_number = '';
        $userpayment->payment_description = '';

      }
      else {
        $reference_id = Yii::$app->request->post('reference_id');
        $userpayment->transaction_id = $checkborrower['transaction_id'];
        $userpayment->payment_amount = $key;
        $userpayment->payment_option = $payment_option;
        $userpayment->payment_type = $payment_type_id;
        $userpayment->reference_id = $reference_id;
        $userpayment->payment_date = date('Y-m-d H:i:s');
        $userpayment->status_id = 2;
        $userpayment->month = $savedCount+1;
        $userpayment->month_to_pay = $m_to_pay;

        //Fill remaining table slots
        $userpayment->payment_number = '';
        $userpayment->payment_description = '';

      }

      if($userpayment->save()) {
        $savedCount++;

      }
      else {
        $err= $userpayment->getErrors();
        $msg = "Disbursement could not be confirmed: ".json_encode($err);
        return ApiUtility::errorResponse($msg);
      }
    }
    // return $payment_option;
    $update = Borrow::updateAll(['approve_flag' => 3, 'date_updated' => $time, 'date_to_pay' => $date_to_pay], ['borrow_id' => $checkborrower['borrow_id']]);

    if ($update) {
      if ($payment_option == 1) {
        if ($kegow_transaction_type == 1) {
          Yii::$app->kegowloader->kegowRegisterUser($user['phone_number'],$user['firstname'],$user['address'],$user['city']);
          $this->kegow =  Yii::$app->kegowloader->loadUserAccount($user['phone_number'],$payment_amount,$user['user_login_id']);
          // return $this->kegow;
        }
        elseif ($kegow_transaction_type == 2) {
          $verifybank = Yii::$app->kegowloader->VerifyBank($user_bank_details->account_number, $user_bank_details->bank_code);
          if ($verifybank['status']) {
            if (strcasecmp(preg_replace('/\s+/', '', $user_bank_details->account_name), preg_replace('/\s+/', '', $verifybank['data']['accountname'])) == 0) {
              $this->kegow = Yii::$app->kegowloader->Transfertobank($user_bank_details->account_number, $user_bank_details->bank_code, $payment_amount, $user['user_login_id']);
            }
            else {
              $transaction->rollBack();
              return [
                'status' => false,
                'message' => 'Bank Details Mismatch. Please contact user to verify bank'
              ];
            }
          }
          else {
            $transaction->rollBack();
            return [
              'status' => false,
              'message' => 'Transaction not successful, User Bank Details could not be verified'
            ];
          }
        }
      if ($this->kegow['status']) {
        $transaction->commit();
      }
      else {
        $transaction->rollBack();
        return $this->kegow;
      }

    }
    else {
      $transaction->commit();
    }
    //approve email to user ;
    $loan_note = $this->Generateloannote($checkborrower['borrow_id'], $checkborrower['transaction_id']);
    $message = "Congratulations!!! Your loan request of ({$checkborrower['loan_amount']}) has been disbursed to your account.<br>
    For further inquiries, send an email to <a href='support@riby.me'>support@riby.me</a>
    <br/><br>
    ";

    $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Disbursed', $email, $this->getReplyEmail(), $email, $company['company_name'],$loan_note[1]);

    return
    [
      'status' => true,
      'message' => 'User loan disbursed successfully',
      'data' => null
    ];

  } else {

    $msg = "Sorry, we could not confirm loan disbursement, please try again later";
    return ApiUtility::errorResponse($msg);
  }
  return
  [
    'status' => true,
    'message' => 'Data tested Successfully',
    'data' => $savedCount.' '. "Records Saved"
  ];

} else {
  $msg = "The loan request does not exist, Please refresh the page and try again";
  return ApiUtility::errorResponse($msg);
}
}

public function actionGeneraterepayschedule()
{
  $staff_array = Yii::$app->request->post('staffs_array');
  $payment_option_id = Yii::$app->request->post('payment_option_id');
  // return $staff_array[0]['company_name'];
  $pdf = new FPDF();
  // Column headings
  // Data loading
  // $data = $pdf->LoadData('country.txt');
  $pdf->AddPage();
  $company_name = $staff_array[0]['company_name'];
  $name = strtoupper($company_name);
  $date = date('l \, jS \of F Y');
  $pdf->SetTitle('Staff Repayment Schedule - '.$date);
  $pdf->SetFont('Times','B');
  $pdf->SetFontSize(20);
  $pdf->Cell(40,10,$name);
  $pdf->Ln(10);
  $pdf->SetFont('Times','',14);
  $pdf->SetTextColor(0);
  $pdf->SetFontSize(12);
  $pdf->Cell(30,5,'Staff Repayment Schedule',0,1);
  $pdf->Ln(5);
  $pdf->SetFontSize(9);
  $pdf->SetTextColor(0);
  $pdf->Cell(75,5,"Date: ".$date,0,0);
  $eir = 5;
  $pdf->Cell(75,5,'Total Number of Staff: '.count($staff_array),0,0,'C');
  $pdf->Cell(40,5,'',0,1,'C');

  $pdf->Ln(5);
  $pdf->SetTextColor(0);

  $pay_arr = [];
  foreach ($staff_array as $each => $staff_variable) {
    $staffs =  $staff_variable;
    array_push($pay_arr, $staffs['payment_amount']);
  }

  $pdf->Cell(50,5,'Month',0,0,'R');
  $pdf->Cell(140,5,'Total Repayment',0,1,'R');
  $pdf->SetFontSize(20);
  $pdf->Cell(50,5,date('F'),0,0,'R');
  $pdf->Cell(140,5,'N'.array_sum($pay_arr),0,1,'R');
  $pdf->SetFontSize(9);

  $pdf->Image(getcwd().'/logo.png',150,10,-100);
  $pdf->Ln(5);
  $pdf->SetFont('Times','',9);

  /** Loan Details Configurations **/
  $pdf->Cell(45,10,'S/N',1,0,'C');
  $pdf->Cell(45,10,'Staff Name',1,0,'C');
  $pdf->Cell(45,10,'Amount',1,0,'C');
  $pdf->Cell(45,10,'Month',1,0,'C');
  $pdf->Ln(10);

  foreach ($staff_array as $each => $staff_variable) {
    $staffs =  $staff_variable;
    $i = 0;
    $pdf->Cell(45,5, $each+1,1,0,'C');
    $pdf->Cell(45,5,$staffs['firstname'].' '.$staffs['lastname'],1,0,'C');
    $pdf->Cell(45,5,$staffs['payment_amount'],1,0,'C');
    $pdf->Cell(45,5,$staffs['month'],1,1,'C');
  }

  $pdf->Ln(5);
  $pdf->SetFillColor(255,255,255);
  $pdf->Cell(40,5,'For Service & Inquiries:',1,1,'C');

  $pdf->Cell(40,5,'Platform Service by Riby',1,0,'L');
  $pdf->Cell(40,5,'Operations Manager',1,0,'L');
  $pdf->Cell(60,5,'peerlending@riby.me',1,0,'L');
  $pdf->Cell(40,5,'01.291.4247',1,0,'L');

  $getDate = date('jS \, F Y');
  $fileBuffer = 'Repayment Schedule - '.$name.'-'.$date.'.pdf';
  $filename= getcwd().'/loannotes/'.$fileBuffer;
  $pdf->Output($filename, 'F');

  $RepaymentSchedule = new RepaymentSchedule;
  // return $staff_array;
  $RepaymentSchedule->user_payment_details = base64_encode(serialize($staff_array));;
  $RepaymentSchedule->total_amount = array_sum($pay_arr);
  $RepaymentSchedule->date_submitted = $getDate;
  $RepaymentSchedule->month = date('F');
  $RepaymentSchedule->company_id = $staff_array[0]['company_id'];
  $RepaymentSchedule->repayment_schedule_url = $fileBuffer;
  $RepaymentSchedule->payment_type = $payment_option_id;
  if ($RepaymentSchedule->save()) {
    $processed_message = "Please find the attached Staffs Loan Repayment Schedule";
    $company_admin = UserDetails::findOne(['company_id' => $staff_array[0]['company_id'], 'user_type_id' => 2]);
    $this->sendEmail('mailtemplate', $this->getReplyEmail(), $processed_message, 'Peerlending Loan Repayment Schedule'.$date, $company_admin->email, $this->getReplyEmail(), $company_admin->email, $staff_array[0]['company_name'], $fileBuffer);
    return [
      'status' => true,
      'message' => 'Repayment Schedule Generated Successfully'
    ];
  }
  else {
    unlink($filename);
    return [
      'status' => false,
      'message' => 'Could not generate Repayment Schedule ',
      // 'error' => $RepaymentSchedule->getErrors(),
      // 'sum' => array_sum($pay_arr)
    ];
  }
}

public function actionTestwithdraw($value='')
{
  return Yii::$app->kegowloader->KegowWithdraw(Yii::$app->request->post('phone'), Yii::$app->request->post('amount'), Yii::$app->request->post('user_id'));
}
public function actionConfirmloanrepayment(){

  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();
  $borrower_id = Yii::$app->request->getQueryParam('borrow_id');
  $access_token = ApiKeyHelper::ACCESS_TOKEN;

  // return $this->Generaterepayschedule();

  $paramsValuePair = [
    PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID => ApiUtility::TYPE_INT,
    // UserPaymentApiKeyHelper::REFERENCE_ID => ApiUtility::TYPE_STRING,
    // UserPaymentApiKeyHelper::PAYMENT_AMOUNT => ApiUtility::TYPE_STRING,
    // UserPaymentApiKeyHelper::PAYMENT_NUMBER => ApiUtility::TYPE_STRING,
    UserPaymentApiKeyHelper::PAYMENT_DESCRIPTION => ApiUtility::TYPE_STRING,
  ];

  $paymenttype = self::getrepaymentpaymenttype();

  //Ensure that payment of 'Repayment' type exists
  if(count($paymenttype)<1)
  return ApiUtility::errorResponse("Payment of Repayment type does not exist");

  $payment_type_id = $paymenttype['payment_type_id'];
  $params = $_REQUEST;

  //Ensure Array passed as payment_number
  // if(!is_array($params['payment_number']) || count($params['payment_number'])<1)
  //     return ApiUtility::errorResponse("Payment batch not indicated");
  // else{
  //     $params['payment_number'] =  join(',',$params['payment_number']);
  // }

  $this->paramCheck($paramsValuePair,$params);

  $payment_option = $params[PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID];
  if ($payment_option != 6) {
    $reference_id = $params[UserPaymentApiKeyHelper::REFERENCE_ID];
  }
  else {
    $reference_id = "KEGOW".uniqid();
  }
  // $payment_amount = $params[UserPaymentApiKeyHelper::PAYMENT_AMOUNT];
  // $payment_number = $params[UserPaymentApiKeyHelper::PAYMENT_NUMBER];
  $payment_description = $params[UserPaymentApiKeyHelper::PAYMENT_DESCRIPTION];
  $month = Yii::$app->request->post('month');
  $repayment_amount = Yii::$app->request->post('repayment_amount');
  // $reference_id = Yii::$app->request->post('reference_id');

  if ($month == '') {
    return
    [
      'status' => false,
      'message' => 'Missing Month',
      'data' => null
    ];
  }

  $checkborrower = Borrow::find()->select(['borrow_id','loan_amount','amount_to_pay','period','transaction_id'])->where(['borrow_id' => $borrower_id, 'approve_flag' => [3,4], 'status_id' => 2])->asArray()->one();
  $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->innerJoin('borrow bo', 'bo.user_id=us.user_id')->where(['bo.borrow_id' => $borrower_id])->asArray()->one();

  $getemail = Borrow::find()->select('us.email, us.phone_number, us.user_login_id')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();

  $email = $getemail['email'];
  $time = date('Y-m-d H:i:s');
  //$date_to_pay = date('Y-m-d H:i:s', strtotime('+'.$checkborrower['period'].' months',strtotime($time)));

  if ($checkborrower != null && $checkborrower['borrow_id'] > 0) {



    $updateuserpayment = UserPayment::findOne(['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
    // return $updateuserpayment;
    // return $updateuserpayment->payment_amount;
    if ($repayment_amount != $updateuserpayment->payment_amount) {
      return
      [
        'status' => false,
        'message' => 'Invalid repayment Amount',
        'data' => null
      ];
    }
    else{

      // $updatePayment = UserPayment::updateAll(['paid' => 1, 'payment_description' => $payment_description, 'repayment_reference_id' => $reference_id], ['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
      if ($payment_option == 6) {
        $kegow_pay = Yii::$app->kegowloader->KegowWithdraw($getemail['phone_number'], $repayment_amount, $getemail['user_login_id']);
        if (!$kegow_pay['status']) {
          return $kegow_pay;
        }
      }
      $updatePayment = UserPayment::updateAll(['paid' => 2, 'payment_description' => $payment_description, 'repayment_reference_id' => $reference_id], ['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
      $total_to_pay = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
      where(['transaction_id' => $checkborrower['transaction_id']])->asArray()->one();

      $total_paid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
      where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 1])->asArray()->one();

      // if ($total_to_pay == $total_paid) {
      //   $update = Borrow::updateAll(['approve_flag' => 4], ['borrow_id' => $checkborrower['borrow_id']]);
      //   //
      //   if ($update) {
      //     $alert = 'User loan repayment confirmed';
      //
      //     if($update) {
      //       $alert .= '; Loan completely Paid Off';
      //       //Notify repayment completion
      //       $message = "Congratulations!!! You have successfully paid Off your Loan and you can now request a new Loan.<br>
      //       If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
      //       <br/><br>";
      //
      //       $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Cleared', $email, $this->getReplyEmail(), $email, $company['company_name']);
      //     }
      //
      //     //Notify for repayment
      //     $message = "Congratulations!!! You have successfully made a repayment on your Loan.<br>
      //     If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
      //     <br/><br>";
      //     $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Serviced', $email, $this->getReplyEmail(), $email, $company['company_name']);
      //
      //     return
      //     [
      //       'status' => true,
      //       'message' => $alert,
      //       'data' => null
      //     ];
      //
      //   }
      //   else {
      //     $err = $updateuserpayment->getErrors();
      //     $msg = "Sorry, we could not confirm loan repayment, please try again later";
      //     return ApiUtility::errorResponse($msg.': '.$err);
      //   }
      // }
      if ($updatePayment) {
        $total_unpaid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
        where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0])->asArray()->one();

        $unpaid = UserPayment::find(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0]);

        return
        [
          'status' => true,
          'message' => 'Repayment Confirmed and User has'.' '.$total_unpaid['payment_amount']. ' Left to pay',
          'data' => $total_unpaid
        ];
      }
      elseif (!$updatePayment) {
        return
        [
          'status' => false,
          'message' => 'An error Occured',
          'data' => null,
        ];
      }
    }
  }
  else {
    $msg = "The Loan could not be found";
    return ApiUtility::errorResponse($msg);
  }
}
// public function actionConfirmloanrepayment(){
//
//   $this->setHeader(200);
//   ApiAuthenticator::verifySuperAdminApiKey();
//   $borrower_id = Yii::$app->request->getQueryParam('borrow_id');
//   $access_token = ApiKeyHelper::ACCESS_TOKEN;
//
//   // return $this->Generaterepayschedule();
//
//   $paramsValuePair = [
//     PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID => ApiUtility::TYPE_INT,
//     UserPaymentApiKeyHelper::REFERENCE_ID => ApiUtility::TYPE_STRING,
//     // UserPaymentApiKeyHelper::PAYMENT_AMOUNT => ApiUtility::TYPE_STRING,
//     // UserPaymentApiKeyHelper::PAYMENT_NUMBER => ApiUtility::TYPE_STRING,
//     UserPaymentApiKeyHelper::PAYMENT_DESCRIPTION => ApiUtility::TYPE_STRING,
//   ];
//
//   $paymenttype = self::getrepaymentpaymenttype();
//
//   //Ensure that payment of 'Repayment' type exists
//   if(count($paymenttype)<1)
//   return ApiUtility::errorResponse("Payment of Repayment type does not exist");
//
//   $payment_type_id = $paymenttype['payment_type_id'];
//   $params = $_REQUEST;
//
//   //Ensure Array passed as payment_number
//   // if(!is_array($params['payment_number']) || count($params['payment_number'])<1)
//   //     return ApiUtility::errorResponse("Payment batch not indicated");
//   // else{
//   //     $params['payment_number'] =  join(',',$params['payment_number']);
//   // }
//
//   $this->paramCheck($paramsValuePair,$params);
//
//   $payment_option = $params[PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID];
//   $reference_id = $params[UserPaymentApiKeyHelper::REFERENCE_ID];
//   // $payment_amount = $params[UserPaymentApiKeyHelper::PAYMENT_AMOUNT];
//   // $payment_number = $params[UserPaymentApiKeyHelper::PAYMENT_NUMBER];
//   $payment_description = $params[UserPaymentApiKeyHelper::PAYMENT_DESCRIPTION];
//   $month = Yii::$app->request->post('month');
//   $repayment_amount = Yii::$app->request->post('repayment_amount');
//   // $reference_id = Yii::$app->request->post('reference_id');
//
//   if ($month == '') {
//     return
//     [
//       'status' => false,
//       'message' => 'Missing Month',
//       'data' => null
//     ];
//   }
//
//   $checkborrower = Borrow::find()->select(['borrow_id','loan_amount','amount_to_pay','period','transaction_id'])->where(['borrow_id' => $borrower_id, 'approve_flag' => [3,4], 'status_id' => 2])->asArray()->one();
//   $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->innerJoin('borrow bo', 'bo.user_id=us.user_id')->where(['bo.borrow_id' => $borrower_id])->asArray()->one();
//
//   $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
//
//   $email = $getemail['email'];
//   $time = date('Y-m-d H:i:s');
//   //$date_to_pay = date('Y-m-d H:i:s', strtotime('+'.$checkborrower['period'].' months',strtotime($time)));
//
//   if ($checkborrower != null && $checkborrower['borrow_id'] > 0) {
//
//
//
//     $updateuserpayment = UserPayment::findOne(['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
//     // return $updateuserpayment;
//     // return $updateuserpayment->payment_amount;
//     if ($repayment_amount != $updateuserpayment->payment_amount) {
//       return
//       [
//         'status' => false,
//         'message' => 'Invalid repayment Amount',
//         'data' => null
//       ];
//     }
//     else{
//
//       // $updatePayment = UserPayment::updateAll(['paid' => 1, 'payment_description' => $payment_description, 'repayment_reference_id' => $reference_id], ['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
//       $updatePayment = UserPayment::updateAll(['paid' => 2, 'payment_description' => $payment_description, 'repayment_reference_id' => $reference_id], ['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
//       $total_to_pay = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
//       where(['transaction_id' => $checkborrower['transaction_id']])->asArray()->one();
//
//       $total_paid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
//       where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 1])->asArray()->one();
//
//       // if ($total_to_pay == $total_paid) {
//       //   $update = Borrow::updateAll(['approve_flag' => 4], ['borrow_id' => $checkborrower['borrow_id']]);
//       //   //
//       //   if ($update) {
//       //     $alert = 'User loan repayment confirmed';
//       //
//       //     if($update) {
//       //       $alert .= '; Loan completely Paid Off';
//       //       //Notify repayment completion
//       //       $message = "Congratulations!!! You have successfully paid Off your Loan and you can now request a new Loan.<br>
//       //       If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
//       //       <br/><br>";
//       //
//       //       $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Cleared', $email, $this->getReplyEmail(), $email, $company['company_name']);
//       //     }
//       //
//       //     //Notify for repayment
//       //     $message = "Congratulations!!! You have successfully made a repayment on your Loan.<br>
//       //     If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
//       //     <br/><br>";
//       //     $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Serviced', $email, $this->getReplyEmail(), $email, $company['company_name']);
//       //
//       //     return
//       //     [
//       //       'status' => true,
//       //       'message' => $alert,
//       //       'data' => null
//       //     ];
//       //
//       //   }
//       //   else {
//       //     $err = $updateuserpayment->getErrors();
//       //     $msg = "Sorry, we could not confirm loan repayment, please try again later";
//       //     return ApiUtility::errorResponse($msg.': '.$err);
//       //   }
//       // }
//       if ($updatePayment) {
//         $total_unpaid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
//         where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0])->asArray()->one();
//
//         $unpaid = UserPayment::find(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0]);
//
//         return
//         [
//           'status' => true,
//           'message' => 'Repayment Confirmed and User has'.' '.$total_unpaid['payment_amount']. ' Left to pay',
//           'data' => $total_unpaid
//         ];
//       }
//       elseif (!$updatePayment) {
//         return
//         [
//           'status' => false,
//           'message' => 'An error Occured',
//           'data' => null,
//         ];
//       }
//     }
//   }
//   else {
//     $msg = "The Loan could not be found";
//     return ApiUtility::errorResponse($msg);
//   }
// }

public function actionGetcollectivetransactions()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  $values = array();

  for($i = 1 ; $i <=12 ; $i++)
  {
    $borrows = (new \yii\db\Query())
    ->select(['sum(loan_amount) as total'])
    ->from('borrow')
    ->where(['YEAR(date_of_request)' => date('Y'), 'MONTH(date_of_request)' => $i])
    ->all();
    $rem = array($i, $borrows);
    $values[] = $rem;
  }
  return
  [
    'status' => true,
    'message' => 'Result',
    'data' => $values,
    'year' => date('Y')
  ];
}
public function actionTotalcompanies()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $companies = Company::find()->asArray()->all();

  $numberofcompanies = count($companies);

  if ($numberofcompanies) {
    return
    [
      'status' => true,
      'message' => 'Total Number of Companies Fetched',
      'data' => $numberofcompanies
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'Total Number of Companies Fetched',
      'data' => null
    ];
  }

}

public function actionGetregisteredcompanies()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $companies = Company::find()->asArray()->all();


  for ($i = 0; $i < sizeof($companies); $i++) {

    $staff = UserDetails::find()->where(['company_id' => $companies[$i]['company_id']])->all();
    $companies[$i]["number_of_staff"] = count($staff);
  }

  if ($companies) {
    return
    [
      'status' => true,
      'message' => 'All Registered Companies Fetched',
      'data' => $companies
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'Oops, Error Fetching Registered Companies',
      'data' => null
    ];
  }

}

public function actionDeclinecompany()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  if (Yii::$app->request->isPost) {
    $company_id = Yii::$app->request->post('company_id');
    $decline = Yii::$app->request->post('decline');
    if ($decline == 1) {
      if ($company_id) {
        $company = Company::findOne($company_id);
        if (!$company) {
          return [
            'status' => true,
            'message' => 'Sorry, This company doesnt exist',
            'data' => null,
          ];
        }
        $company->is_approved = 2;
        if ($company->save()) {
          return [
            'status' => true,
            'message' => 'Company has been successfully declined',
            'data' => $company,
          ];
        }
        else {
          return [
            'status' => false,
            'message' => 'Oops, An error ocured while processing your request, Please try again',
            'data' => null
          ];
        }

      }
      else {
        return [
          'status' => false,
          'message' => 'Oops, Company ID is Missing from Request',
          'data' => null
        ];
      }

    }
  }
}

public function actionCompanydeclinestatus()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  if (Yii::$app->request->isPost) {
    $company_id = Yii::$app->request->post('company_id');
    if ($company_id) {
      $company = Company::findOne($company_id);
      if (!$company) {
        return [
          'status' => false,
          'message' => 'Sorry, This company doesnt exist',
          'data' => null,
        ];
      }
      if ($company->is_approved == 2) {
        return [
          'status' => true,
          'message' => 'Company has Been Declined',
          'data' => $company
        ];
      }
      elseif ($company->is_approved != 2) {
        return [
          'status' => true,
          'message' => 'Company has not Been Declined',
          'data' => $company
        ];
      }
    }
  }
  else {
    return [
      'status' => false,
      'message' => 'Oops, Company ID is Missing from Request',
      'data' => null
    ];
  }

}

public function actionGetcompanystaffs()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $company_id = Yii::$app->request->getQueryParam('company_id');
  $staff = UserDetails::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename',
  'user_details.user_id', 'user_details.phone_number', 'user_details.email', 'company.company_name', 'company.company_id', 'user_details.address'
  ])->innerJoin('company', 'company.company_id = user_details.company_id')->where(['user_details.company_id' => $company_id])->asArray(true)->all();
  if ($staff) {
    return
    [
      'status' => true,
      'message' => 'All Staff Fetched',
      'data' => $staff,
      'company' => $company_id
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'Staff Not Fetched',
      'data' => null
    ];
  }

}

public function actionGetcompaniesstaffdetails()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $user_id = Yii::$app->request->getQueryParam('user_id');

  $companiesstaff = UserDetails::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename',
  'user_details.user_id', 'user_details.phone_number', 'user_details.email', 'company.company_name'
  ])->innerJoin('company', 'company.company_id=user_details.company_id')->where(['user_details.user_id' => $user_id])->asArray()->all();

  return
  [
    'status' => true,
    'message' => 'Companies Staff Details Fetched',
    'data' => $companiesstaff
  ];
}

public function actionGetstafftransactions()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $user_id = Yii::$app->request->getQueryParam('user_id');

  $lend = Lend::find()->where(['user_id' => $user_id])->asArray()->all();
  $borrow = Borrow::find()->where(['user_id' => $user_id])->asArray()->all();
  $userinfo = UserDetails::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename',
  'user_details.user_id', 'user_details.phone_number', 'user_details.email', 'company.company_name'
  ])->innerJoin('company', 'company.company_id=user_details.company_id')->where(['user_details.user_id' => $user_id])->asArray()->all();

  $transaction = array();

  $transaction["lend_transact"] = $lend;
  $transaction["borrow_transact"] = $borrow;
  $transaction["user_info"] = $userinfo;

  if ($transaction) {
    return
    [
      'status' => true,
      'message' => 'Companies Staff Details Fetched',
      'data' => $transaction
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'Staff Transaction Not Available',
      'data' => $transaction
    ];
  }

}

public function actionGetloanhistory()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();


  $access_token = ApiKeyHelper::ACCESS_TOKEN;

  $companyloantransactionhistory = Borrow::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'borrow.borrow_id', 'borrow.loan_amount', 'borrow.period', 'borrow.status_id', 'borrow.amount_to_pay', 'borrow.date_of_request','borrow.other_reasons', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
  'borrow.company_id', 'company.company_name','borrow_reason.title as reason'])
  ->innerJoin('user_details', 'user_details.user_id=borrow.user_id')->innerJoin('company', 'company.company_id=borrow.company_id')->leftJoin('borrow_reason','borrow.reason_id=borrow_reason.reason_id')
  ->where(['user_details.status_id' => 2])->asArray()->all();

  if (!is_array($companyloantransactionhistory)) {
    $message = "Error retrieving borrow transaction";
    return ApiUtility::errorResponse($message);
  } elseif (empty($companyloantransactionhistory)) {
    $this->setHeader(200);
    return [
      'status' => true,
      'message' => 'No loan transaction',
      'data' => null
    ];
  } else {
    $this->setHeader(200);
    return [
      'status' => true,
      'message' => 'Loan transaction fetched',
      'data' => $companyloantransactionhistory
    ];
  }

}


public function actionPayment(){

  $rows = (new \yii\db\Query())
  ->select(['*'])
  ->from('user_payment')
  ->all();

  return [
    'status' => true,
    'message' => 'Query successfully',
    'data' => $rows
  ];
}

//get all lend history for each company
public function actionGetlendhistory()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $companylendtransactionhistory = Lend::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'lend.lend_id', 'lend.lend_amount', 'lend.date_lend', 'lend.amount_to_receive', 'lend.status_id', 'lend.approve_flag',
  'lend.company_id', 'lend.status_id', 'company.company_name'])
  ->innerJoin('user_details', 'user_details.user_id=lend.user_id')->innerJoin('company', 'company.company_id=lend.company_id')
  ->where(['user_details.status_id' => 2])->asArray()->all();
  if (!is_array($companylendtransactionhistory)) {
    $message = "Error retrieving lend transaction";
    return ApiUtility::errorResponse($message);
  } elseif (empty($companylendtransactionhistory)) {
    $this->setHeader(200);
    return [
      'status' => true,
      'message' => 'No lend transaction',
      'data' => null
    ];
  } else {
    $this->setHeader(200);
    return [
      'status' => true,
      'message' => 'Lend transaction fetched',
      'data' => $companylendtransactionhistory
    ];
  }

}

public function actionGetlatestrequest()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $borrow = Borrow::find()->where(['approve_flag' => 1, 'status_id' => 2])->orderBy(['borrow_id' => SORT_DESC])->asArray(true)->one();

  if ($borrow) {
    return
    [
      'status' => true,
      'message' => 'Latest Request',
      'data' => $borrow
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }

}

public function actionGetlatestrequestdetails()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  $borrow_id = Yii::$app->request->getQueryParam('borrow_id');

  //$borrow = Borrow::findOne(['borrow_id'=>$borrow_id]);
  //$user = UserDetails::findOne(['user_id'=>$borrow->user_id]);

  $requestdetails = Borrow::find()->select(['user_details.firstname','user_details.user_login_id', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'borrow.borrow_id', 'borrow.user_id', 'borrow.loan_amount', 'borrow.amount_to_pay', 'borrow.transaction_id', 'borrow.date_of_request', 'borrow.date_to_pay','borrow_reason.title as reason','borrow.other_reasons', 'borrow.period', 'borrow.status_id', 'borrow.approve_flag',
  'borrow.company_id', 'company.company_name'])->innerJoin('user_details', 'user_details.user_id=borrow.user_id')->innerJoin('company', 'company.company_id=borrow.company_id')->leftJoin('borrow_reason', 'borrow_reason.reason_id = borrow.reason_id')->where(['borrow.borrow_id' => $borrow_id])->asArray()->one();

  $disbusement_type = $this->getdisbursementpaymenttype();
  $repayment_type = $this->getrepaymentpaymenttype();

  if ($requestdetails) {

    for($i=0;$i<count($requestdetails);$i++){
      $requestdetails['bank'] =  UserBankDetails::find()->select("bvn,account_name,account_number,bank_name,bank_code")->where(['user_id'=>$requestdetails['user_login_id']])->one();
      $requestdetails['disbursement'] =  UserPayment::find()->where(['transaction_id'=>$requestdetails['transaction_id'],'payment_type'=>$disbusement_type['payment_type_id']])->one();
      $requestdetails['repayement'] =  UserPayment::find()->where(['transaction_id'=>$requestdetails['transaction_id'], 'payment_type'=>$repayment_type['payment_type_id']])->one();
    }

    return
    [
      'status' => true,
      'message' => 'Latest Request Details',
      'data' => $requestdetails
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }

}

public function actionGetlatestlend()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();

  $lend = Lend::find()->where(['approve_flag' => 1, 'status_id' => 2])->orderBy(['borrow_id' => SORT_DESC])->asArray(true)->one();

  if ($lend) {
    return
    [
      'status' => true,
      'message' => 'Latest Request',
      'data' => $lend
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Request',
      'data' => null
    ];
  }

}

public function actionGetlatestlenddetails()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminUserApiKey();
  $lend_id = Yii::$app->request->getQueryParam('lend_id');

  //$borrow = Borrow::findOne(['borrow_id'=>$borrow_id]);
  //$user = UserDetails::findOne(['user_id'=>$borrow->user_id]);

  $requestdetails = Lend::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
  'lend.lend_id', 'lend.user_id', 'lend.lend_amount', 'lend.date_lend', 'lend.amount_to_receive', 'lend.status_id', 'lend.approve_flag',
  'lend.company_id', 'company.company_name'])->innerJoin('user_details', 'user_details.user_id=lend.user_id')->innerJoin('company', 'company.company_id=lend.company_id')->where(['lend.lend_id' => $lend_id])->one();
  if ($requestdetails) {

    return
    [
      'status' => true,
      'message' => 'Latest Lend Details',
      'data' => $requestdetails
    ];
  } else {
    return
    [
      'status' => false,
      'message' => 'No Lend',
      'data' => null
    ];
  }

}

public function actionGetAllPayment(){

  $rows = (new \yii\db\Query())
  ->select(['*'])
  ->from('user_payment')
  ->limit(10)
  ->all();

  return [
    'status' => true,
    'message' => 'Query successfully',
    'data' => $rows
  ];
}

public function actionGetaudit(){

  $this->setHeader(200);
  ApiAuthenticator::verifyApiKey();

  $access_token = ApiKeyHelper::ACCESS_TOKEN;
  $get_access_token = $this->checkApiKey();

  $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
  $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
  $id = $user->user_id;

  $rows = (new \yii\db\Query())
  ->select(['*'])
  ->from('admin_log')
  ->all();

  return [
    'status' => true,
    'message' => 'Query successfully',
    'data' => $rows
  ];
}


public function actionGetaccesslevels()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();
  $apikey = Yii::$app->request->getQueryParam('key');
  $user_id = Yii::$app->request->getQueryParam('user_id');

  $rows = (new \yii\db\Query())
  ->select(['*'])
  ->from('user_type')
  ->limit(10)
  ->all();

  return [
    'status' => true,
    'message' => 'Access Levels Fetched Successfully',
    'data' => $rows
  ];


}

public function actionCreatesuperadmin()
{
  $this->setHeader(200);
  ApiAuthenticator::verifySuperAdminApiKey();
  $api_key = $this->checkApiKey();
  $admin_id  = ApiAuthenticator::getSuperAdminUserIdFromKey($api_key);
  $admin = UserDetails::find()->select(['company_id'])
  ->where(['user_login_id'=> $admin_id])->asArray()->one();
  $comp_id = $admin['company_id'];
  $params = $_REQUEST;

  $passwordLength = 7;

  $access_token = ApiKeyHelper::ACCESS_TOKEN;

  $paramsValuePair = [
    UserDetailsApiKeyHelper::FIRSTNAME => ApiUtility::TYPE_STRING,
    UserDetailsApiKeyHelper::LASTNAME => ApiUtility::TYPE_STRING,
    UserDetailsApiKeyHelper::PHONE_NUMBER => ApiUtility::TYPE_STRING,
    UserLoginApiKeyHelper::USER_EMAIL => ApiUtility::TYPE_STRING,
    UserTypeApiKeyHelper::USER_TYPE_ID => ApiUtility::TYPE_STRING
  ];

  $this->paramCheck($paramsValuePair, $params);

  $firstname = $params[UserDetailsApiKeyHelper::FIRSTNAME];
  $lastname = $params[UserDetailsApiKeyHelper::LASTNAME];
  $phone_number = $params[UserDetailsApiKeyHelper::PHONE_NUMBER];
  $password = $this->getAppParams('superAdminPassword');
  $user_type = $params[UserTypeApiKeyHelper::USER_TYPE_ID];
  $email = $params[UserLoginApiKeyHelper::USER_EMAIL];

  $get_access_token = ApiUtility::generateAccessToken();

  date_default_timezone_set('Africa/Lagos');
  $time = date('Y-m-d H:i:s');
  $expiration_date = date('Y-m-d H:i:s', strtotime("+7 day", strtotime($time)));

  $superadmin_types = $this->getAppParams('superadmin_user_types');

  if(!in_array($user_type,$superadmin_types)){
    $error_msg = "You should not create a user of this type";
    return ApiUtility::errorResponse($error_msg);
  }

  $connection = Yii::$app->db;
  $transaction = $connection->beginTransaction();

  $error_msg = '';


  $fetch_mail = UserDetails::find()->where(['email'=>$email])->asArray()->one();
  $fetch_names = UserLogin::find()->where(['user_email' => $email, 'status_id' => 2])->asArray()->one();

  if (!empty($password) && !empty($email)) {

    if(ApiUtility::isValidEmail($email))
    {
      if($fetch_mail == null && $fetch_names == null)
      {
        if (strlen($password) >= $passwordLength) {
          try {
            $userlogins = new UserLogin();

            $userlogins->user_email = $email;
            $userlogins->user_password = ApiUtility::generatePasswordHash($password);
            $userlogins->access_token = $get_access_token;
            $userlogins->user_type_id = $user_type;
            $userlogins->created_date = $time;
            $userlogins->modified_date = $time;
            $userlogins->expiration_date = $expiration_date;


            if ($userlogins->validate() && $userlogins->save()) {

              $userdetails = new UserDetails();

              $userdetails->email = $email;

              $userdetails->user_type_id = $user_type;
              $userdetails->company_id = $comp_id;
              $userdetails->phone_number = $phone_number;
              $userdetails->user_login_id = $userlogins->user_login_id;
              $userdetails->created_date = $time;
              $userdetails->modified_date = $time;


              if ($userdetails->validate() && $userdetails->save()) {
                $transaction->commit();

                //send respnse email

                $this->setHeader(200);
                $message = "Congratulations! Your SuperAdmin Account has been created on the platform. <br>
                Your Login credentials:<br/>
                Username: <var>Email address on which this email was received</var><br>
                Password: $password<br><br>
                Kindly Login to change your password and have access to your page<br>
                Thank you.<br/><br>
                ";

                $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'SuperAdmin Account Activation', $userdetails->email, $this->getReplyEmail(), $email, 'RIBY Finance');

                return [
                  'status' => true,
                  'message' => 'Registration Successful, An Email Has Been Sent To the account owner.',
                  'data' => null,
                ];

              } else {
                $err= $userdetails->getErrors();
                $error_msg = 'Something went wrong: '.json_encode($err);
              }
            } else {
              $err = $userlogins->getErrors();
              $error_msg = 'User could not be created: '.json_encode($err);
            }

          } catch (Exception $ex) {
            $transaction->rollBack();
            $error_msg = 'Error:'.$ex->getMessage().' L#'.$ex->getLine();
          }
        } else {
          $error_msg = 'Password should not be less than 7 characters';
        }
      }
      else
      {
        $error_msg = 'User already exists';
      }
    }
    else
    {
      $error_msg = 'Invalid email address. Kindly check and try again';
    }
  } else {
    $error_msg = 'Fill in the required fields';
  }

  return ApiUtility::errorResponse($error_msg);
}


public function updatePassword($access_token, $password)
{
  $update = UserLogin::findOne(['access_token' => $access_token]);

  $update->user_password = $password;
  if($update->update()){
    return true;
  }else{
    return false;
  }
}

public function actionTotallendandborrowbymonth()
{
  $this->setHeader(200);
  ApiAuthenticator::verifyApiKey();
  $borrow = Borrow::findBySql("
  SELECT MONTHNAME(date_of_request) as month, coalesce(SUM(loan_amount),0) as amount FROM borrow WHERE status_id = 2 AND (approve_flag = 3 OR approve_flag= 4) GROUP BY MONTH(date_of_request)
  ")->asArray()->all();

  $lend = Lend::findBySql("
  SELECT MONTHNAME(date_lend) as month, SUM(lend_amount) as amount FROM lend WHERE status_id = 2 AND (approve_flag = 3 OR approve_flag= 4) GROUP BY MONTH(date_lend)
  ")->asArray()->all();

  $borrow_x = [];
  $borrow_y = [];
  foreach ($borrow as $key) {
    array_push($borrow_x, $key['month']);
    array_push($borrow_y, $key['amount']);
  }

  $lend_x = [];
  $lend_y = [];
  foreach ($lend as $key) {
    array_push($lend_x, $key['month']);
    array_push($lend_y, $key['amount']);
  }

  return [
    'status' => true,
    'message' => 'Total Sum Generated',
    'data' => json_encode(['borrow_x' => $borrow_x, 'borrow_y' => $borrow_y, 'lend_x' => $lend_x, 'lend_y' => $lend_y]),
  ];
}
public function actionChangepassword()
{
  $this->setHeader(200);
  ApiAuthenticator::verifyApiKey();
  $params = $_REQUEST;
  $passwordlength = 7;
  $access_token = ApiKeyHelper::ACCESS_TOKEN;

  $paramsPairValue = [
    UserLoginApiKeyHelper::USER_PASSWORD  => ApiUtility::TYPE_STRING,
    UserLoginApiKeyHelper::CONFIRM_PASSWORD  => ApiUtility::TYPE_STRING,
  ];

  $this->paramCheck($paramsPairValue, $params);

  $get_access_token = $this->checkApiKey();
  $password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
  $cpassword = $params[UserLoginApiKeyHelper::CONFIRM_PASSWORD];

  if(strlen($password)>=$passwordlength){
    if($password == $cpassword){
      $password =  ApiUtility::generatePasswordHash($password);

      $upass = $this->updatePassword($get_access_token, $password);

      if($upass){
        $this->setHeader(200);
        return[
          'status' => true,
          'message' => 'Password Updated',
        ];
      }else{
        return[
          'status' => false,
          'message' => 'Password Not Updated',
        ];
      }

    }else{
      return[
        'status' => false,
        'message' => 'Passwords do not match',
      ];
    }
  }else{
    return[
      'status' => false,
      'message' => 'Password Length must be at least 7 characters',
    ];
  }
}

}
