<?php

namespace app\modules\api\modules\v1\controllers;


use app\modules\api\modules\v1\models\Borrow;
use app\modules\api\modules\v1\models\BorrowReason;
use app\modules\api\modules\v1\models\Company;
use app\modules\api\modules\v1\models\CompanyApiKeyHelper;
use app\modules\api\modules\v1\models\EmailSenderApiKeyHelper;
use app\modules\api\modules\v1\models\Lend;
use app\modules\api\modules\v1\models\AdminLog;
use app\modules\api\modules\v1\models\UserBankDetails;
use app\modules\api\modules\v1\models\UserDetails;
use app\modules\api\modules\v1\models\UserDetailsApiKeyHelper;
use app\modules\api\modules\v1\models\UserLogin;
use app\modules\api\modules\v1\models\PaymentOptionApiKeyHelper;
use app\modules\api\modules\v1\models\UserPaymentApiKeyHelper;
use app\modules\api\modules\v1\models\UserLoginApiKeyHelper;
use app\modules\api\modules\v1\models\UserType;
use app\modules\api\modules\v1\models\UserPayment;
use app\modules\api\modules\v1\models\RepaymentSchedule;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\components\Simplexlsx;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;
use app\modules\api\modules\v1\models\TransactionRemark;
use app\modules\api\modules\v1\models\BorrowerLenderApiKeyHelper;



class AdminController extends BaseController
{

  public $error = array();

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                                'login'    =>          ['POST'],
                                'deletestaff' => ['DELETE'],

                            ],
                        ],
        ]);
    }

    public function actionIndex()
    {

        return json_encode(['kayode']);


    }


    public function actionGetclientlevels(){
        $user_types = Yii::$app->params['company_user_types'];
        $admin_type = [Yii::$app->params['company_admin_user_type']];

        $allowed_types = array_merge_recursive($admin_type,$user_types);

        //Add General User
        $allowed_types[] = 3;

        $levels = (new \yii\db\Query())
            ->select(['*'])
            ->from('user_type')
            ->where(['user_type_id'=> $allowed_types])
            ->all();
        return $levels;
    }

    public function actionGetallrequest()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();

      $apikey = Yii::$app->request->getQueryParam('key');
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
      $admin = UserDetails::find()->select(['company_id'])
      ->where(['user_login_id'=> $admin_id])->asArray()->one();
      $comp_id = $admin['company_id'];

      $borrowers = Borrow::find()->select(['user_details.firstname','user_details.user_login_id', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
      'borrow.borrow_id', 'borrow.loan_amount', 'borrow.amount_to_pay','borrow.transaction_id', 'borrow.period', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
      'borrow.company_id','borrow_reason.title as reason','borrow.other_reasons', 'company.company_name',
      ])->innerJoin('user_details', 'user_details.user_id = borrow.user_id')->innerJoin('company', 'company.company_id = borrow.company_id')->leftJoin('borrow_reason', 'borrow_reason.reason_id = borrow.reason_id')->where(['borrow.approve_flag' => 3, 'user_details.company_id' => $comp_id])->asArray(true)->all();

      // return $borrowers;
      if ($borrowers) {
        $this->setHeader(200);
        return
        [
          'status' => true,
          'message' => 'All request fetched successfully',
          'data' => $borrowers
        ];
      } else {
        return
        [
          'status' => false,
          'message' => 'No Request',
          'data' => null
        ];
      }
    }

    public function actionCompleterepaymentviakegow()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();
      $apikey = Yii::$app->request->getQueryParam('key');
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
      $admin = UserDetails::find()->where(['user_login_id'=> $admin_id])->asArray()->one();
      $comp_id = $admin['company_id'];

      $amount = Yii::$app->request->post('amount');
      $repayment_schedule_id = Yii::$app->request->post('repayment_schedule_id');
      $payment_type = Yii::$app->request->post('payment_type');
      $company_details = Company::findOne(['company_id' => $comp_id]);
      $merchantId = 577;
      $privateKey = '2B612340211D6D79F4D0C354220FD00D';
      // $merchantId = 297;
      // $privateKey = "4F979B996A149779C50C399A686DEF38";
      $orderId = rand(100, 100000000);
      $real_amount = $amount * 100;
      $currency = 'NGN';
      $hash = Yii::$app->encryption->getEncryptionKeyFive([$merchantId,$real_amount,$currency,$orderId,$privateKey]);
      $riby_hash = Yii::$app->encryption->getEncryptionKeyFour([$merchantId,$currency,$orderId,$privateKey]);
      $session = Yii::$app->session;
      $session->set('ongoing_transaction', 1);
      $session->set('trasaction_hash', $riby_hash);
      $session->set('repayment_schedule_id', $repayment_schedule_id);
      // $session->has('ongoing_transaction');
      // Yii::$app->request->getQueryParam('borrow_id');
      $successUrl = 'http://localhost/api/web/api/v1/admin/successcompletestaffsrepayement/?key='.$apikey;
      $failureUrl = 'http://localhost/api/web/api/v1/admin/failcompletestaffsrepayement?key='.$apikey;
      if (count($amount) < 0) {
        $this->redirect($failureUrl,302);
        return [
          'status' => false,
          'message' => 'Amount Cannot Be empty'
        ];
      }

      if ($payment_type == 6) {
        // return $successUrl;
        // $this->redirect($successUrl, 302);
        return [
          'status' => true,
          'message' => 'Transaction Initiated',
          'data' => $successUrl
        ];
      }
      else {
        $payment_link = "https://kegow.com/paymentwindow/bancore.do?amount=".$real_amount."&currency=NGN&purpose=RepaymentScheduleFrom$company_details->company_name&accountId=".$merchantId."&orderId=".$orderId."&encKey=".$hash."&name=".$admin['firstname']."&phone=".$admin['phone_number']."&email=".$admin['email']."&successUrl=".$successUrl."&failureUrl=".$failureUrl;
        return [
          'status' => true,
          'message' => 'Transaction Initiated',
          'data' => $payment_link
        ];
      }

    }

    public function actionSuccesscompletestaffsrepayement()
    {
      $session = Yii::$app->session;
      // $session->remove('repayment_schedule_id');
      // $session->set('repayment_schedule_id', 17);

      if (!$session->has('repayment_schedule_id')) {
        return [
          'status' => false,
          'message' => 'Invalid Transaction ID'
        ];
      }
      $paramSplice = Yii::$app->request->getQueryParam('key');
      $setter = explode('?', $paramSplice);
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($setter[0]);
      $admin = UserDetails::find()->select(['company_id'])
      ->where(['user_login_id'=> $admin_id])->asArray()->one();
      $comp_id = $admin['company_id'];
      if (count($setter) > 1) {
        $cardReferenceExplode = explode('=', $setter[1]);
        $repayment_schedule_id = $session->get('repayment_schedule_id');
        $transaction_identifier = Yii::$app->request->getQueryParam('transaction');
        $enckey = Yii::$app->request->getQueryParam('enckey');
        $orderId = Yii::$app->request->getQueryParam('orderId');
        $cardReference = $cardReferenceExplode[1];

      }
      else {
        $cardReferenceExplode = "KEGOWREPAY".strtoupper(uniqid());
        $repayment_schedule_id = $session->get('repayment_schedule_id');
        $transaction_identifier = "KEGOWREPAY".strtoupper(uniqid());
        $enckey = "KEGOWREPAY".strtoupper(uniqid());
        $orderId = "KEGOWREPAY".strtoupper(uniqid());
        $cardReference = "KEGOWREPAY".strtoupper(uniqid());

      }


      $update_repayment = RepaymentSchedule::findOne(['repayment_schedule_id' => $repayment_schedule_id, 'company_id' => $comp_id, 'status' => 0]);
      $connection = Yii::$app->db;
      $transaction = $connection->beginTransaction();
      if (!count($update_repayment)) {
        return [
          'status' => false,
          'message' => 'Invalid Repayment'
        ];
      }
      $update_repayment->status = 2;
      $update_repayment->transaction_identifier = $transaction_identifier;
      $update_repayment->encKey = $enckey;
      $update_repayment->orderId = $orderId;
      $update_repayment->repayer_key = $setter[0];
      $update_repayment->cardReference = $cardReference;
      $update_repayment->date_paid = date('jS \, F Y');;
      if ($update_repayment->save()) {
      $staffs = unserialize(base64_decode($update_repayment->user_payment_details));
      $payment_state = 0;
      foreach ($staffs as $key ) {
        $borrower_id = $key['borrow_id'];
        $month = $key['month'];
        $repayment_amount = $key['payment_amount'];

        $checkborrower = Borrow::find()->select(['borrow_id','loan_amount','amount_to_pay','period','transaction_id'])->where(['borrow_id' => $borrower_id, 'approve_flag' => [3,4], 'status_id' => 2])->asArray()->one();
        $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->innerJoin('borrow bo', 'bo.user_id=us.user_id')->where(['bo.borrow_id' => $borrower_id])->asArray()->one();

        $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();

        $email = $getemail['email'];
        $time = date('Y-m-d H:i:s');
        //$date_to_pay = date('Y-m-d H:i:s', strtotime('+'.$checkborrower['period'].' months',strtotime($time)));

        if ($checkborrower != null && $checkborrower['borrow_id'] > 0) {
          $updateuserpayment = UserPayment::findOne(['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
          // return $updateuserpayment;
          // return $updateuserpayment->payment_amount;
          if ($repayment_amount != $updateuserpayment->payment_amount) {
            return
            [
              'status' => false,
              'message' => 'Invalid repayment Amount',
              'data' => null
            ];
          }
          else{
            $updatePayment = UserPayment::updateAll(['paid' => 1, 'payment_description' => 'Kegow Repayment', 'repayment_reference_id' => $transaction_identifier], ['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
            $total_to_pay = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
            where(['transaction_id' => $checkborrower['transaction_id']])->asArray()->one();

            $total_paid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
            where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 1])->asArray()->one();

            if ($total_to_pay == $total_paid) {
              $update = Borrow::updateAll(['approve_flag' => 4], ['borrow_id' => $checkborrower['borrow_id']]);
              //
              if ($update) {
                $alert = 'User loan repayment confirmed';

                if($update) {
                  $alert .= '; Loan completely Paid Off';
                  //Notify repayment completion
                  $message = "Congratulations!!! You have successfully paid Off your Loan and you can now request a new Loan.<br>
                  If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
                  <br/><br>";

                  $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Cleared', $email, $this->getReplyEmail(), $email, $company['company_name']);
                }

                //Notify for repayment
                $message = "Congratulations!!! You have successfully made a repayment on your Loan.<br>
                If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
                <br/><br>";
                $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Serviced', $email, $this->getReplyEmail(), $email, $company['company_name']);
                $payment_state++;
              }
              else {
                $transaction->rollBack();
                $err = $updateuserpayment->getErrors();
                $msg = "Sorry, we could not confirm loan repayment, please try again later";
                // return ApiUtility::errorResponse($msg.': '.$err);
              }
            }
            elseif ($updatePayment) {
              $total_unpaid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
              where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0])->asArray()->one();

              $unpaid = UserPayment::find(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0]);
              $payment_state++;
            }
            elseif (!$updatePayment) {
              return
              [
                'status' => false,
                'message' => 'An error Occured',
                'data' => null,
              ];
            }
          }
        }
        else {
          $msg = "The Loan could not be found";
          // return ApiUtility::errorResponse($msg);
        }
      }
      if ($payment_state) {
        $transaction->commit();
        $session->remove('repayment_schedule_id');
        $this->redirect('http://localhost/#/repayments?'.uniqid().'#1', 301)->send();
      }
      else {
        $session->remove('repayment_schedule_id');
        $transaction->rollBack();
        $this->redirect('http://localhost/#/repayments?'.uniqid().'#2', 301)->send();
      }
    }

    }
    public function actionFailcompletestaffsrepayement()
    {
      $paramSplice = Yii::$app->request->getQueryParam('key');
      $setter = explode('?', $paramSplice);
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($setter[0]);
      $this->redirect('http://localhost/#/repayments?'.uniqid().'#3', 301)->send();
    }

    public function actionDeclinerepaymentschedule()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();

      $apikey = Yii::$app->request->getQueryParam('key');
      $repayment_schedule_id = Yii::$app->request->post('repayment_schedule_id');
      $decline_reason = Yii::$app->request->post('decline_reason');
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
      $admin = UserDetails::find()->select(['company_id'])
      ->where(['user_login_id'=> $admin_id])->asArray()->one();
      $comp_id = $admin['company_id'];

      $repayment = RepaymentSchedule::find()->where(['repayment_schedule_id'=> $repayment_schedule_id, 'company_id' => $comp_id])->one();
      $repayment->status = 2;
      $repayment->decline_reason = $decline_reason;
      if ($repayment->save()) {
        return
          [
            'status' => true,
            'message' => 'Repayment Declined Successfully',
          ];
      }
      else {
        return
          [
            'status' => false,
            'message' => 'Repayment Declined not successful, Please try Again',
          ];
      }

    }
    public function actionGetallrepaymentschedule()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();

      $apikey = Yii::$app->request->getQueryParam('key');
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
      $admin = UserDetails::find()->select(['company_id'])
      ->where(['user_login_id'=> $admin_id])->asArray()->one();
      $comp_id = $admin['company_id'];

      $repayment = RepaymentSchedule::find()->where(['company_id' => $comp_id, 'status' => 0])->asArray(true)->all();
      // return $borrowers;
      if ($repayment) {
        $this->setHeader(200);
        return
        [
          'status' => true,
          'message' => 'All request fetched successfully',
          'data' => $repayment,
          // 'total_amount' => unserialize($repayment['total_amount']),
        ];
      } else {
        return
        [
          'status' => false,
          'message' => 'No Request',
          'data' => null
        ];
      }
    }

    // This function is meant to be removed;
    public function actionGetrepaymentoptions()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();


      $paymentoptions = self::get_payment_options('repayment');

      return
      [
        'status' => true,
        'message' => 'Repayment options fetched successfully',
        'data' => $paymentoptions
      ];

    }

    // To be removed when Repayment is Complete
    public function actionCheckborrowrepaymentstatus()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();
      $transaction_id = Yii::$app->request->getQueryParam('transaction_id');
      // return $transaction_id;
      $all_details = Borrow::find()->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.transaction_id'=> $transaction_id])->asArray()->one();
      if ($all_details <= 0) {
        return
        [
          'status' => false,
          'message' => 'Invalid Transaction ID',
          'data' => null
        ];
      }
      else {
        $total_disbursed = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
        where(['transaction_id'=>$transaction_id])->asArray()->one();
        $payments = UserPayment::find()->where(['transaction_id' => $transaction_id])->asArray()->all();
        return
        [
          'status' => true,
          'message' => 'Data fetched Successfully',
          'data' => $payments,
          'total' => $total_disbursed
        ];
      }
    }

    public function actionConfirmloanrepayment(){

      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();
      $borrower_id = Yii::$app->request->getQueryParam('borrow_id');
      $access_token = ApiKeyHelper::ACCESS_TOKEN;

      $paramsValuePair = [
        PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID => ApiUtility::TYPE_INT,
        UserPaymentApiKeyHelper::REFERENCE_ID => ApiUtility::TYPE_STRING,
        // UserPaymentApiKeyHelper::PAYMENT_AMOUNT => ApiUtility::TYPE_STRING,
        // UserPaymentApiKeyHelper::PAYMENT_NUMBER => ApiUtility::TYPE_STRING,
        UserPaymentApiKeyHelper::PAYMENT_DESCRIPTION => ApiUtility::TYPE_STRING,
      ];

      $paymenttype = self::getrepaymentpaymenttype();

      //Ensure that payment of 'Repayment' type exists
      if(count($paymenttype)<1)
      return ApiUtility::errorResponse("Payment of Repayment type does not exist");

      $payment_type_id = $paymenttype['payment_type_id'];
      $params = $_REQUEST;

      //Ensure Array passed as payment_number
      // if(!is_array($params['payment_number']) || count($params['payment_number'])<1)
      //     return ApiUtility::errorResponse("Payment batch not indicated");
      // else{
      //     $params['payment_number'] =  join(',',$params['payment_number']);
      // }

      $this->paramCheck($paramsValuePair,$params);

      $payment_option = $params[PaymentOptionApiKeyHelper::PAYMENT_OPTION_ID];
      $reference_id = $params[UserPaymentApiKeyHelper::REFERENCE_ID];
      // $payment_amount = $params[UserPaymentApiKeyHelper::PAYMENT_AMOUNT];
      // $payment_number = $params[UserPaymentApiKeyHelper::PAYMENT_NUMBER];
      $payment_description = $params[UserPaymentApiKeyHelper::PAYMENT_DESCRIPTION];
      $month = Yii::$app->request->post('month');
      $repayment_amount = Yii::$app->request->post('repayment_amount');
      // $reference_id = Yii::$app->request->post('reference_id');

      if ($month == '') {
        return
        [
          'status' => false,
          'message' => 'Missing Month',
          'data' => null
        ];
      }

      $checkborrower = Borrow::find()->select(['borrow_id','loan_amount','amount_to_pay','period','transaction_id'])->where(['borrow_id' => $borrower_id, 'approve_flag' => [3,4], 'status_id' => 2])->asArray()->one();
      $company = Company::find()->innerJoin('user_details us', 'us.company_id =company.company_id')->innerJoin('borrow bo', 'bo.user_id=us.user_id')->where(['bo.borrow_id' => $borrower_id])->asArray()->one();

      $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();

      $email = $getemail['email'];
      $time = date('Y-m-d H:i:s');
      //$date_to_pay = date('Y-m-d H:i:s', strtotime('+'.$checkborrower['period'].' months',strtotime($time)));

      if ($checkborrower != null && $checkborrower['borrow_id'] > 0) {



        $updateuserpayment = UserPayment::findOne(['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
        // return $updateuserpayment;
        // return $updateuserpayment->payment_amount;
        if ($repayment_amount != $updateuserpayment->payment_amount) {
          return
          [
            'status' => false,
            'message' => 'Invalid repayment Amount',
            'data' => null
          ];
        }
        else{

          $updatePayment = UserPayment::updateAll(['paid' => 2, 'payment_description' => $payment_description, 'repayment_reference_id' => $reference_id], ['transaction_id' => $checkborrower['transaction_id'], 'month' => $month]);
          $total_to_pay = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
          where(['transaction_id' => $checkborrower['transaction_id']])->asArray()->one();

          $total_paid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
          where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 2])->asArray()->one();

          if ($total_to_pay == $total_paid) {
            $update = Borrow::updateAll(['approve_flag' => 7], ['borrow_id' => $checkborrower['borrow_id']]);
            //
            if ($update) {
              $alert = 'User loan repayment confirmed';

              if($update) {
                $alert .= '; Loan completely Paid Off';
                //Notify repayment completion
                $message = "Congratulations!!! You have successfully paid Off your Loan and you can now request a new Loan.<br>
                If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
                <br/><br>";

                $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Cleared', $email, $this->getReplyEmail(), $email, $company['company_name']);
              }

              //Notify for repayment
              $message = "Congratulations!!! You have successfully made a repayment on your Loan.<br>
              If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
              <br/><br>";
              $this->sendEmail('mailtemplate', $this->getReplyEmail(), $message, 'Loan Serviced', $email, $this->getReplyEmail(), $email, $company['company_name']);

              return
              [
                'status' => true,
                'message' => $alert,
                'data' => null
              ];

            }
            else {
              $err = $updateuserpayment->getErrors();
              $msg = "Sorry, we could not confirm loan repayment, please try again later";
              return ApiUtility::errorResponse($msg.': '.$err);
            }
          }
          elseif ($updatePayment) {
            $total_unpaid = UserPayment::find()->select('sum(payment_amount) as payment_amount')->
            where(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0])->asArray()->one();

            $unpaid = UserPayment::find(['transaction_id' => $checkborrower['transaction_id'],'paid' => 0]);

            return
            [
              'status' => true,
              'message' => 'Repayment Confirmed and User has'.' '.$total_unpaid['payment_amount']. ' Left to pay',
              'data' => $total_unpaid
            ];
          }
          elseif (!$updatePayment) {
            return
            [
              'status' => false,
              'message' => 'An error Occured',
              'data' => null,
            ];
          }
        }
      }
      else {
        $msg = "The Loan could not be found";
        return ApiUtility::errorResponse($msg);
      }
    }


    public function actionGetaccesslevels()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminAdminApiKey();
        $apikey = Yii::$app->request->getQueryParam('key');
        $user_id = Yii::$app->request->getQueryParam('user_id');

        $rows = (new \yii\db\Query())
            ->select(['*'])
            ->from('user_type')
            ->limit(10)
            ->all();

        return [
            'status' => true,
            'message' => 'Access Levels Fetched Successfully!',
            'data' => $rows
        ];


    }

    public function actionMassuploadstaffs()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();
      set_time_limit(30000);
      $params = $_REQUEST;
      $access_token = ApiKeyHelper::ACCESS_TOKEN;

      //get company_id of the current admin
      $apikey = Yii::$app->request->getQueryParam('key');
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
      $admin = UserDetails::find()->select(['company_id'])
      ->where(['user_login_id'=> $admin_id])->asArray()->one();
      $comp_id = $admin['company_id'];
      // return $comp_id;
      $newusers = 0;
      $errors = [];
      if($comp_id != null){
        if ($_FILES['file']) {
          $filename = $_FILES['file']['tmp_name'];
          $file = fopen($filename, "r");
          $count = 0;
          $existArray = [];
          while (($details = fgetcsv($file, 10000, ",")) !== FALSE)
          {
            $count++;
            if($count>1){
              $details['firstname'] = $details[0];
              $details['lastname'] = $details[1];
              $details['user_email'] = $details[2];
              $details['phone_number'] = $details[3];
              $details['company_id'] = $comp_id;
              $details['user_type'] = 3;
              $created = $this->adminsignupusers($details);
              if($created['status'])
              $newusers++;
              else{
                $errors[$details['firstname']. ' ' . $details['lastname'] . ','. $details['user_email']] = $created['message'];
              }
            }
          }
          $message = ($newusers>0)?$newusers.' '.'Accounts were created successfully':'No account was created';
          if(count($errors)>0){
            $error_msg = ' Some Account Already Exist';
            $message = $newusers>0?$message.$error_msg:$message;
            return [
              'status'=>false,
              'message'=>$message,
              'data' => $errors,
            ];
          }
          else{
            return [
              'status'=>true,
              'message'=>$message,
              'data' => null,
            ];
          }
        }
      }
      else{
        $error_msg = 'Company not available';
        return ApiUtility::errorResponse($error_msg);
      }
    }


    public function actionGetalllenders()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminAdminApiKey();

      $params = $_REQUEST;
      $access_token = ApiKeyHelper::ACCESS_TOKEN;

      $paramsPairValue = [
          UserLoginApiKeyHelper::USERS_LIST => ApiUtility::TYPE_ARR,
      ];

      //get company_id of the current admin
      $apikey = Yii::$app->request->getQueryParam('key');
      $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
      $admin = UserDetails::find()->select(['company_id'])
          ->where(['user_login_id'=> $admin_id])->asArray()->one();
      $comp_id = $admin['company_id'];

        $lenders = Lend::find()->select(['user_details.firstname', 'user_details.lastname', 'user_details.middlename', 'user_details.phone_number', 'user_details.email',
            'lend.lend_id', 'lend.lend_amount', 'lend.date_lend', 'lend.amount_to_receive','lend.lend_tenure', 'lend.status_id', 'lend.approve_flag',
            'lend.company_id', 'company.company_name',
        ])->where(['lend.company_id'=> $comp_id])->innerJoin('user_details', 'user_details.user_id = lend.user_id')->innerJoin('company', 'company.company_id = lend.company_id')->asArray(true)->all();

        if ($lenders) {
            $this->setHeader(200);
            return
                [
                    'status' => true,
                    'message' => 'All lenders fetched successfully',
                    'data' => $lenders
                ];
        } else {
            return
                [
                    'status' => false,
                    'message' => 'No Lender',
                    'data' => null
                ];
        }
    }

    public function actionCreatestaffs(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminAdminApiKey();

        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsPairValue = [
            UserLoginApiKeyHelper::USERS_LIST => ApiUtility::TYPE_ARR,
        ];
        $this->paramCheck($paramsPairValue, $params);

        $new_staffs = $params[UserLoginApiKeyHelper::USERS_LIST];

        //get company_id of the current admin
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
        $admin = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();
        $comp_id = $admin['company_id'];


        $newusers = 0;
        $errors = [];

        if($comp_id != null){

            foreach ($new_staffs as $staff) {
                $staff['company_id'] = $comp_id;
                // Yii::$app->kegowloader->kegowRegisterUser($details[3],$details[0]);
                $created = $this->adminsignupusers($staff);
                return $created;
                if($created['status'])

                    $newusers++;

                else{
                    $errors[$staff[UserLoginApiKeyHelper::USER_EMAIL]] = $created['message'];
                }
            }
            // return $created['status'];
            if ($newusers > 0) {
              $message = "Accounts were created successfully";
            }
            else {
              $message = "No account was created";
            }
            // $message = ($newusers>0)?'Accounts were created successfully':'No account was created';
            if(count($errors)>0){
                $error_msg = ' but with some errors';
                $message = $newusers>0?$message.$error_msg:$message;
                return [
                    'status'=>false,
                    'message'=>$message,
                    'data' => $errors,
                ];
            }
            else{
                return [
                    'status'=>true,
                    'message'=>$message,
                    'data' => null,
                ];
            }
        }
        else{
            $error_msg = 'Company not available';
            return ApiUtility::errorResponse($error_msg);
        }



    }

    public function adminsignupusers($user){

        //Only allow creation of allowed roles
        $user_type =  $user['user_type'];
        $user_type = intval($user_type) > 0 ?
            UserType::find()->where(['user_type_id' => $user_type])->asArray()->one() :
            UserType::find()->where(['user_type_name' => $user_type])->asArray()->one();
        $user_type_id = is_array($user_type)?$user_type['user_type_id']:false;

        if(!in_array($user_type_id, $this->getadminadminusertypes()) && $user_type_id != 3){
            $error_msg = 'You can not create a user of type '.$user_type['user_type_name'];
            return ApiUtility::errorResponse($error_msg);
        }
        $user['user_password'] = $user_type_id == 3 ?$this->getAppParams('userPassword'):$this->getAppParams('companyAdminPassword');

        date_default_timezone_set('Africa/Lagos');
        $time = date('Y-m-d H:i:s');
        $passwordLength = 7;

        $firstname = $user[UserDetailsApiKeyHelper::FIRSTNAME];
        $lastname = $user[UserDetailsApiKeyHelper::LASTNAME];
        $email = $user[UserLoginApiKeyHelper::USER_EMAIL];
        $phone_number = $user[UserDetailsApiKeyHelper::PHONE_NUMBER];
        $password = $user[UserLoginApiKeyHelper::USER_PASSWORD];
        $company = $user[UserDetailsApiKeyHelper::COMPANY_ID];
        $expiration_date = date('Y-m-d H:i:s', strtotime("+7 day", strtotime($time)));
        $companyname = Company::findOne(['company_id'=>$company]);

        // return $password;
        $get_access_token = ApiUtility::generateAccessToken();

        $fetch_mail = UserDetails::find()->where(['email'=>$email])->asArray()->one();

        $fetch_names = UserLogin::find()->where(['user_email' => $email, 'status_id' => 2])->asArray()->one();

        if (!empty($password) && !empty($email)) {

            if(ApiUtility::isValidEmail($email))
            {
                if($fetch_mail == null && $fetch_names == null)
                {
                    if (strlen($password) >= $passwordLength) {


                        $connection = Yii::$app->db;
                        $transaction = $connection->beginTransaction();
                        // $this->error_msg = '';

                        try {
                            $values = [];
                            $userlogins = new UserLogin();

                            $userlogins->user_email = $email;
                            $userlogins->user_password = ApiUtility::generatePasswordHash($password);
                            $userlogins->access_token = $get_access_token;
                            $userlogins->user_type_id = $user_type_id;
                            $userlogins->created_date = $time;
                            $userlogins->modified_date = $time;


                            #TODO set default expiration date to Null
                            $userlogins->expiration_date = date('Y-m-d H:i:s',122);

                            if ($userlogins->save()) {

                                $userdetails = new UserDetails();

                                $userdetails->email = $email;
                                $userdetails->company_id = $company;
                                $userdetails->user_type_id = $user_type_id;
                                $userdetails->firstname = $firstname;
                                $userdetails->lastname = $lastname;
                                $userdetails->phone_number = $phone_number;
                                $userdetails->user_login_id = $userlogins->user_login_id;
                                $userdetails->created_date = $time;
                                $userdetails->modified_date = $time;
                                $userdetails->city = "Lagos";
                                $userdetails->country = "Nigeria";
                                $userdetails->zip_code = "23401";
                                if ($userdetails->save()) {

                                  /** Register Users at Point Of registration */
                                    // Yii::$app->kegowloader->kegowRegisterUser($phone_number,$firstname);

                                    $userCompany = Company::findOne($company);
                                    // return $userCompany->company_name;
                                    $this->setHeader(200);
                                    $message = "You have been registered on the $userCompany->company_name PeerLending Service.<br/>
                                                You can now borrow and lend to your colleagues. <br/>
                                                Borrow at fair interest (principal + interest deduction from Salary)<br/>
                                                and lend at attractive rates. All borrow transactions are approved by your company.
                                               Your Login credentials:<br/>
                                               Username: <var>Email address on which this email was received</var><br>
                                               Password: $password<br><br>
                                               Kindly Login to your account, change your password and start borrowing / lending. <br/>
                                               For enquiry, please call 01.291.4247 or 07000-CALL-RIBY or email peerlending@riby.me.<br/><br/>
                                               Welcome on board! ";
                                    // $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account created', $userdetails->email, $this->getReplyEmail(),$email,$companyname['company_name']);

                                      return [
                                          'status' => true,
                                          'message' => 'Registration Successful, An Email Has Been Sent To You.',
                                          'data' => null,
                                          'transaction_status' => $transaction->commit(),
                                      ];
                                }
                            }
                            else{
                                $transaction->rollBack();
                                $error_msg = 'Account could not be created. Please Try again';
                            }

                        } catch (Exception $ex) {
                            $transaction->rollBack();
                            $error_msg = 'Account could not be created. Please Try again';
                        }
                    } else {
                        $error_msg = 'Password should not be less than 7 characters';
                    }
                }
                else
                {
                    $error_msg = 'User already exists';
                }
            }
            else
            {
                $error_msg = 'Invalid email address';
            }
        } else {
          $error_msg = 'Fill in the required fields';
        }
          return ApiUtility::errorResponse($error_msg);
    }





    public function actionUploadstaffsheet()
    {
      // $this->setHeader(200);
      // ApiAuthenticator::verifyAdminApiKey();
      if (isset($_FILES['file'])) {
        $xlsx = new SimpleXLSX($_FILES['file']['tmp_name']);
        list($num_cols, $num_rows) = $xlsx->dimension();
        $f = 0;
        $data = array();
        foreach( $xlsx->rows() as $r )
        {
          if ($f == 0)
          {
            $f++;
            continue;
          }
          for( $i=0; $i < $num_cols; $i++ )
          {
            if ($i == 0)      $data['ac_no']      = $r[$i];
            else if ($i == 1) $data['surname']    = $r[$i];
            else if ($i == 2) $data['firstname']    = '';
            else if ($i == 3) $data['sex']      = $r[$i];
            else if ($i == 4) $data['dateofbirth']  = 'none';
            else if ($i == 5) $data['qualification']  = 'none';
            else if ($i == 6) $data['department']   = 1;
            else if ($i == 7) $data['yearjoined']   = 'none';
            else if ($i == 8) $data['email']      = 'none';
            else if ($i == 9) $data['basicsalary']  = 0;
          }
          if (empty($data)) {
            return[
              'status'=> false,
              'message' => 'Empty Sheet Uploaded',
              'data' => null
            ];
          }
          print_r($data);
        }
      }
    }

    public function actionGetunconfirmedusers()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminApiKey();

      $getallunconfirmedusers = UserLogin::findBySql("select usl.user_login_id, usd.user_id,usl.user_email,cm.company_name,usd.phone_number
      from user_login usl inner join user_details usd ON usl.user_login_id = usd.user_login_id
      INNER JOIN company cm ON usd.company_id = cm.company_id WHERE
      usl.status_id = 3")->asArray()->all();

      return[
        'status'=> true,
        'message' => 'Unconfirmed users fetched successfully',
        'data' => $getallunconfirmedusers
      ];

    }

    public function actionConfirmuser()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $user_login_id = Yii::$app->request->getQueryParam('user_login_id');

        $checkifuseralreadyactivated = UserLogin::findOne(['user_login_id'=>$user_login_id, 'status_id'=>2]);
        if($checkifuseralreadyactivated)
        {
            $errormsg = "User account already activated";
            return ApiUtility::errorResponse($errormsg);
        }


        $checkuserid = UserLogin::findOne(['user_login_id'=>$user_login_id, 'status_id'=>3]);
        if($checkuserid)
        {

            $checkuserid->status_id = 2;
            if($checkuserid->update()){
                //send email to user that account has been activated;
                $message = "Thanks for registering on our platform. We will like to inform you that your account has been activated. Welcome on board <br/>
                                                Thank you.<br/>";




                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $checkuserid->user_email, $this->getReplyEmail(), $checkuserid->user_email);


                return [
                    'status'=> true,
                    'message'=> 'User account activated successfully',
                    'data'=>null
                ];
            }
            else{
                $errormsg = "user account could not be activated";
                return ApiUtility::errorResponse($errormsg);
            }
        }


    }

    public function actionMailsender(){
        return ['message'=>'Message not sent'];
        ApiAuthenticator::verifyAdminApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsPairValue = [
            EmailSenderApiKeyHelper::SENDER_EMAIL => ApiUtility::TYPE_STRING,
            EmailSenderApiKeyHelper::RECEIVER_EMAIL => ApiUtility::TYPE_STRING,
            EmailSenderApiKeyHelper::SUBJECT => ApiUtility::TYPE_STRING,
            EmailSenderApiKeyHelper::MESSAGE => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsPairValue, $params);

        $get_access_token = $this->checkApiKey();
        $sender_email = $params[EmailSenderApiKeyHelper::SENDER_EMAIL];
        $receiver_email = $params[EmailSenderApiKeyHelper::RECEIVER_EMAIL];
        $message = $params[EmailSenderApiKeyHelper::MESSAGE];
        $subject = $params[EmailSenderApiKeyHelper::SUBJECT];

        if($this->sendEmail('mailtemplate',$sender_email,$message,$subject, $receiver_email, $sender_email, $receiver_email)){
            return[
                'status' => true,
                'message' => 'Mail Sent',
            ];
        }else{

            return[
                'status' => false,
                'message' => 'Oops Failed!, Please try Again',
            ];
        }
    }

    public function actionConfirmpassword()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminUserApiKey();
      $params = $_REQUEST;
      $access_token = ApiKeyHelper::ACCESS_TOKEN;

      $paramsPairValue = [
          UserLoginApiKeyHelper::USER_PASSWORD  => ApiUtility::TYPE_STRING,
      ];

      $this->paramCheck($paramsPairValue, $params);

      $get_access_token = $this->checkApiKey();
      $password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
      $password =  ApiUtility::generatePasswordHash($password);
      $get_password = UserLogin::findOne(['access_token' => $get_access_token, 'user_password' => $password]);
        if(count($get_password) > 0){
            return [
              'status' => true,
              'message' => 'Password Confirmed'
            ];
        }else{
          return [
            'status' => false,
            'message' => 'Incorrect Passsword'
          ];
        }
    }


    public function actionFetchborrowerrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
                        ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getborrowerrequest =
           Borrow::findBySql(
                    "
                        select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
                        us.company_id, cm.company_name, br.loan_amount,br.date_of_request,br.period,
                        br.date_to_pay,br.loan_note_url,br.borrow_id,br.process_remarks,br.process_description, br.other_reasons, borrow_reason.title as reason FROM borrow br left join borrow_reason ON br.reason_id=borrow_reason.reason_id INNER JOIN company cm ON cm.company_id = br.company_id
                        inner join user_details us ON us.user_id = br.user_id
                        WHERE us.company_id = $comp_id AND br.approve_flag IN (0,6) AND br.status_id = 2
                    "
                )->asArray()->all();

        return [
            'status' => true,
            'message' => 'List of company borrowers fetched',
            'data' => $getborrowerrequest

        ];

    }


    public function actionFetchborrowerqueue()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getborrowerrequest =
            Borrow::findBySql(
                "
                        select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
                        us.company_id, cm.company_name, br.loan_amount,br.date_of_request,br.date_to_pay, br.borrow_id
                        from borrow br INNER JOIN company cm ON cm.company_id = br.company_id
                        inner join user_details us ON us.user_id = br.user_id
                        WHERE us.company_id = $comp_id AND br.approve_flag = 1
                        AND br.status_id = 2 order by br.date_of_request desc
                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'Company borrowers queue fetched',
            'data' => $getborrowerrequest

        ];

    }


    public function actionFetchstaffincompany()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getstaffincompany =
            UserDetails::findBySql(
                "
                        select usl.user_email,us.firstname, us.lastname,us.email,
                        us.company_id, cm.company_name,us.user_login_id,us.phone_number,us.user_type_id
                        from user_login usl INNER JOIN user_details us ON us.user_login_id = usl.user_login_id
                        INNER JOIN company cm ON cm.company_id = us.company_id
                        WHERE us.company_id = $comp_id AND us.status_id = 2

                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'List of company staffs fetched successfully',
            'data' => $getstaffincompany
        ];

    }


    public function actionFetchstaffdetails()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $user_login_id = Yii::$app->request->getQueryParam('staff_id');
        //get company_id of the current admin

        $staffdetails =          UserLogin::findBySql(
                "
                        select usl.user_email,us.firstname, us.lastname,us.email,
                        us.company_id, cm.company_name,us.user_login_id,us.phone_number,
                        us.gender, us.address, us.birthdate,us.middlename
                        from user_login usl INNER JOIN user_details us ON us.user_login_id = usl.user_login_id
                        INNER JOIN company cm ON cm.company_id = us.company_id
                        WHERE us.user_login_id = $user_login_id AND usl.status_id = 2

                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'Staffs details fetched successfully',
            'data' => $staffdetails
        ];

    }

    public function actionFetchlenderrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getlendrequest =
            Lend::findBySql(
                "
                        select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
                        us.company_id, cm.company_name, br.date_lend,
                        br.amount_to_receive, br.lend_amount, br.lend_id
                        from lend br INNER JOIN company cm ON cm.company_id = br.company_id
                        inner join user_details us ON us.user_id = br.user_id
                        WHERE us.company_id = $comp_id AND br.approve_flag = 1
                        AND br.status_id = 2
                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'list of borrowers fetched from company admin',
            'data' => $getlendrequest

        ];

    }

    public function actionGetadminlevels()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $apikey = Yii::$app->request->getQueryParam('key');
        $user_id = Yii::$app->request->getQueryParam('user_id');

     $rows = (new \yii\db\Query())
    ->select(['*'])
    ->from('user_type')
    ->limit(10)
    ->all();

   return [
            'status' => true,
            'message' => 'Query successfully',
            'data' => $rows
        ];


    }


    public function actionEditstaffrole()
    {
      $this->setHeader(200);
      ApiAuthenticator::verifyAdminApiKey();
      if (Yii::$app->request->isPost) {
        $user_id = Yii::$app->request->post('user_id');
        $role = Yii::$app->request->post('role');
        $user = UserDetails::findOne(['user_id' => $user_id]);
        $user->user_type_id = $role;
        if ($user->save()) {
          return
              [
                  'status' => true,
                  'message' => 'Success',
                  'data' => $user
              ];
        }
      }
    }
     public function actionDeleteuser(){

        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $user_id = Yii::$app->request->getQueryParam('user_id');

         $apikey = Yii::$app->request->getQueryParam('key');
         $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
         $admin_company_id = UserDetails::find()->select(['company_id'])
             ->where(['user_login_id'=> $admin_id])->asArray()->one();
         $comp_id = $admin_company_id['company_id'];

        $to_delete = UserDetails::find()->where(['user_login_id'=>$user_id,'company_id'=>$comp_id])->one();
        $can_delete = $this->getadminadminusertypes();
        $can_delete = is_array($can_delete)?$can_delete:[$can_delete];
        $can_delete[] = 3; //Add Normal staff to can delete list

        if(!$to_delete){
            $msg = "Sorry! User does not exist, Kindly check and try again.";
            return ApiUtility::errorResponse($msg);
        }
         else if(!in_array($to_delete->user_type_id,$can_delete)){
            $msg = "Sorry! You do not have permission to delete this user.";
            return ApiUtility::errorResponse($msg);
        }

         $found = 0;
        //$active_loan = $this->getuseractiveloan($user_id);
        $pending_loan = $this->getUserLoanHistory($user_id);
        //$initiated_loan = $this->getUserLoanAwaitingApproval($user_id);
         foreach($pending_loan as $one){
             if($one['status_id'] == 2 && $one['approve_flag'] < 4)
                 $found++;
         }


        if ($found<1) {
            UserDetails::updateAll(['status_id' => 1], ['user_login_id' => $user_id]);


          return [
            'status' => true,
            'message' => 'User deleted successfully',
            'data' => $pending_loan
        ];
        }
        else{
            $msg = "Sorry! User cannot be deleted due to pending loan.";
                return ApiUtility::errorResponse($msg);

        }
     }

     public function actionDeletealluserwithoutloan(){

        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $comp_id = 6;
        $allUsers = UserDetails::find()->where(['company_id' => $comp_id])->all();
        foreach ($allUsers as $user) {
          // print_r($user);

        //      ->where(['user_login_id'=> $admin_id])->asArray()->one();
        $user_id = $user['user_login_id'];

         $apikey = Yii::$app->request->getQueryParam('key');
         $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
        //  $admin_company_id = UserDetails::find()->select(['company_id'])
        //      ->where(['user_login_id'=> $admin_id])->asArray()->one();
        //  $comp_id = Yii::$app->request->post('comp_id');

        // $to_delete = UserDetails::find()->where(['user_login_id'=>$user_id,'company_id'=>$comp_id])->one();
        // $can_delete = $this->getadminadminusertypes();
        // $can_delete = is_array($can_delete)?$can_delete:[$can_delete];
        // $can_delete[] = 3; //Add Normal staff to can delete list
        //
        // if(!$to_delete){
        //     $msg = "Sorry! User does not exist, Kindly check and try again.";
        //     return ApiUtility::errorResponse($msg);
        // }
        //  else if(!in_array($to_delete->user_type_id,$can_delete)){
        //     $msg = "Sorry! You do not have permission to delete this user.";
        //     return ApiUtility::errorResponse($msg);
        // }

         $found = 0;
        //$active_loan = $this->getuseractiveloan($user_id);
        $pending_loan = $this->getUserLoanHistory($user_id);
        //$initiated_loan = $this->getUserLoanAwaitingApproval($user_id);
         foreach($pending_loan as $one){
             if($one['status_id'] == 2 && $one['approve_flag'] < 4)
                 $found++;
         }


        if ($found<1) {


          $transaction = Yii::$app->db->beginTransaction();
            try{
                $sql = "DELETE *  FROM user_details  INNER JOIN user_login WHERE user_details.user_login_id= user_login.user_login_id and user_details.user_login_id = $user_id";
                Yii::$app->db->createCommand($sql)->execute();
                if($transaction->commit())
                {
                  return [
                    'status' => true,
                    'message' => 'Users deleted successfully',
                    'data' => $pending_loan
                ];
                }

            }
            catch(Exception $e){
              if($transaction->rollback())
              {
                return [
                  'status' => false,
                  'message' => 'Users not deleted successfully',
                  'data' => $e
              ];
              }
            }


        }
        else{
            $msg = "Sorry! User cannot be deleted due to pending loan.";
                return ApiUtility::errorResponse($msg);

        }
      }
     }

    public function actionApproveborrowrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('id');

        $checkborrower = Borrow::find()->select(['borrow_id','loan_amount'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 0, 'status_id' => 2])->asArray()->one();

        $superadmin_decline_status = Borrow::find()->select(['borrow_id','loan_amount','process_remarks'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 6, 'status_id' => 2])->asArray()->one();
        if (count($superadmin_decline_status) > 0) {
          $transactionremark = TransactionRemark::findOne(["remark_id"=>$superadmin_decline_status['process_remarks']]);
          return [
            'status' => false,
            'message' => "This loan has been declined by Super admin. Reason: {$transactionremark->remark_description}"
          ];
        }
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserDetails::find()->innerJoin('user_login','user_details.user_login_id=user_login.user_login_id')->where(['access_token'=>$apikey])->asArray()->one();
        $company = Company::findOne(['company_id'=>$admin['company_id']]);
        $time = date('Y-m-d H:i:s');

        //get user email
        $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
        $email = $getemail['email'];

        if (count($checkborrower) > 0 && $checkborrower['borrow_id'] != null) {
            $update = Borrow::updateAll(['approve_flag' => 1,'date_updated'=>$time], ['borrow_id' => $checkborrower['borrow_id']]);

            if ($update) {
                //approve email to user ;
                $message = "Your loan request of ₦ {$checkborrower['loan_amount']} is currently being processed.<br>
                            You will receive an email notification once the process is completed. <br/>
                            <br/><br>
                            ";
                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan processing', $email, $this->getReplyEmail(), $email, $company['company_name']);


                //approve email to superAdmin ;
                $message = "A new loan request has been sent for approval.<br><br>
                            Company: $company->company_name<br>
                            Amount: {$checkborrower['loan_amount']}<br>
                            Date Aproved: $time<br><br>
                            Kindly login to approve request<br/>
                            ";
                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan awaiting Approval', $this->getAdminEmail(), $this->getReplyEmail(), $this->getAdminEmail(),$company->company_name);





                return
                    [
                        'status' => true,
                        'message' => 'user approved to borrow',
                        'data' => null
                    ];
            } else {
                $msg = "sorry we couldnt approve user borrow request, please try again later thanks";
                return ApiUtility::errorResponse($msg);
            }

        }
        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }






    /// Aprrrove Lend request...            //////

    public function actionApprovelendrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('id');

        $checkborrower = Lend::find()->select(['lend_id'])->where(['lend_id' => $borrower_id, 'approve_flag' => 0, 'status_id' => 2])->asArray()->one();
        $company = Company::find()->innerJoin('user_details us','us.company_id =company.company_id')->innerJoin('lend le','le.user_id=us.user_id')->where(['lend.lend_id'=> $borrower_id])->asArray()->one();

        //get user email
        $getemail = Lend::find()->select('us.email')->innerJoin('user_details us','us.user_id = lend.user_id')->where(['lend.lend_id'=> $borrower_id])->asArray()->one();
        $email = $getemail['email'];
        if ($checkborrower['lend_id'] != null and count($checkborrower) > 0) {
            $update = Lend::updateAll(['approve_flag' => 1], ['lend_id' => $checkborrower['lend_id']]);

            if ($update) {

                $message = "We wish you to inform you that your application to Lend money has been accepted. <br/>You will soon be contacted on the details of how to go about disbursing <br/>
                the money and you will also get an invoice containing the details of when and how much you are going<br/>
                 to get back. Just stay in touch. <br/>
                         ";


                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $email, $this->getReplyEmail(), $email,$company['company_name']);


                return
                    [
                        'status' => true,
                        'message' => 'user approved to borrow',
                        'data' => null
                    ];
            } else {
                $msg = "sorry we could not approve user borrow request, please try again later thanks";
                return ApiUtility::errorResponse($msg);
            }

        }
        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }


    public function actionRejectborrowrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $params = $_REQUEST;

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsValuePair = [
            BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION => ApiUtility::TYPE_STRING,
            BorrowerLenderApiKeyHelper::PROCESS_REMARKS => ApiUtility::TYPE_STRING
        ];

        $this->paramCheck($paramsValuePair,$params);

        $process_description = $params[BorrowerLenderApiKeyHelper::PROCESS_DESCRIPTION];
        $process_remarks = $params[BorrowerLenderApiKeyHelper::PROCESS_REMARKS];

        $borrower_id = Yii::$app->request->getQueryParam('id');

        $checkborrower = Borrow::find()->select(['borrow_id','loan_amount'])->where(['borrow_id'=> $borrower_id, 'approve_flag'=>0, 'status_id'=>2])->asArray()->one();

        $superadmin_decline_status = Borrow::find()->select(['borrow_id','loan_amount','process_remarks'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 6, 'status_id' => 2])->asArray()->one();
        if (count($superadmin_decline_status) > 0) {
          $transactionremark = TransactionRemark::findOne(["remark_id"=>$superadmin_decline_status['process_remarks']]);
          return [
            'status' => false,
            'message' => "This loan has been declined by Super admin. Reason: {$transactionremark->remark_description}"
          ];
        }

        //get user email
        $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
        $email = $getemail['email'];
        $company = Company::find()->innerJoin('user_details us','us.company_id =company.company_id')->innerJoin('borrow bo','bo.user_id=us.user_id')->where(['bo.borrow_id'=> $borrower_id])->asArray()->one();
        if($checkborrower['borrow_id'] != null and count($checkborrower) > 0)
        {
            $update =   Borrow::updateAll(['approve_flag'=> 5], ['borrow_id'=> $checkborrower['borrow_id']]);
            $update_description = Borrow::updateAll(['process_description' => $process_description], ['borrow_id' => $checkborrower['borrow_id']]);
            $update_remarks = Borrow::updateAll(['process_remarks' => $process_remarks], ['borrow_id' => $checkborrower['borrow_id']]);


            if($update && $update_remarks) {
                /*
                //Notification email to user don upon Confirmation by Super Admin;
                $message = "Unfortunately, Your loan request of ₦ {$checkborrower['loan_amount']} has been Declined.<br>
                             If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
                            <br/><br>
                             ";


                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan rejected', $email, $this->getReplyEmail(), $email, $company['company_name']);
                */
                return
                    [
                        'status' => true,
                        'message' => 'user not allowed to borrow',
                        'data' => null
                    ];
            }
            else{
                $msg = "sorry, we could not perform the reject borrow request please try again";
                return ApiUtility::errorResponse($msg);
            }
        }



        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }


    }

    public function actionGetallborrower(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $borrowers = Borrow::find()->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if (!is_array($borrowers)) {
            $message = "Error retrieving borrowers";
            return ApiUtility::errorResponse($message);
        }elseif(empty($borrowers)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Borrowers Available',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Lenders Fetched Successfully',
                'data' => $borrowers
            ];
        }
    }

    public function actionGetalllender(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $lenders = Lend::find()->where(['company_id'=>$admindetails->company_id, 'status_id'=>2])->asArray()->all();

        if (!is_array($lenders)) {
            $message = "Error retrieving lenders";
            return ApiUtility::errorResponse($message);
        }elseif(empty($lenders)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Lenders Available',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Lenders Fetched Successfully',
                'data' => $lenders
            ];
        }
    }

    public function actionTotalstaff(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $users = UserDetails::find()->where(['user_type_id'=>3,'company_id'=>$admindetails->company_id, 'status_id'=>2])->asArray()->all();

        $numberofuser = count($users);

        if (!is_array($users)) {
            $message = "Error retrieving staff";
            return ApiUtility::errorResponse($message);
        }elseif(empty($users)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staffs Not Available',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staff Fetched Successfully',
                'data' => $numberofuser
            ];
        }
    }

    public function actionTotalamountlent(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $lenderamount = Lend::find()->select('lend_amount')->where(['company_id'=>$admindetails->company_id])->asArray()->all();
        $amounts = 0;
        foreach($lenderamount as $amount){
            $amounts += $amount["lend_amount"];
        }

        if (!is_array($lenderamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($lenderamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Lender at the moment',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Lent Fetched Successfully',
                'data' => $amounts
            ];
        }

    }

    public function actionTotalamountborrowed(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $loanamount = Borrow::find()->select('loan_amount')->where(['company_id'=>$admindetails->company_id, 'status_id'=>2,'approve_flag'=>[3,4]])->asArray()->all();
        $amounts = 0;
        foreach($loanamount as $amount){
            $amounts += $amount["loan_amount"];
        }

        if (!is_array($loanamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($loanamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Borrowed at the moment',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Borrowed Fetched Successfully',
                'data' => $amounts
            ];
        }

    }


    // Tottal amount Declined

     public function actionTotalamountdeclined(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $loanamount = Borrow::find()->select('loan_amount')->where(['company_id'=>$admindetails->company_id, 'status_id'=>1])->asArray()->all();
        $amounts = 0;
        foreach($loanamount as $amount){
            $amounts += $amount["loan_amount"];
        }

        if (!is_array($loanamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($loanamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'no amount Declined',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Declined Successfully',
                'data' => $amounts
            ];
        }

    }


    public function actionTotalamountrequested(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $loanamount = Borrow::find()->select('loan_amount')->where(['company_id'=>$admindetails->company_id])->asArray()->all();
        $amounts = 0;
        foreach($loanamount as $amount){
            $amounts += $amount["loan_amount"];
        }

        if (!is_array($loanamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($loanamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Request at the moment',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Requested Fetched Successfully',
                'data' => $amounts
            ];
        }
    }

    public function actionUserloanpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Borrow::findBySql("
            select CONCAT(YEAR(date_of_request),'/', MONTH(date_of_request)) as month, COUNT(*) as number_of_user FROM borrow GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Number of Loan Applicants Per Month',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Number of Loan Applicants Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionUserlendpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Lend::findBySql("
            select CONCAT(YEAR(date_lend),'/', MONTH(date_lend)) as month, COUNT(*) as number_of_user FROM lend GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Number of Lenders Per Month',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Number of Lenders Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionLendpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Lend::findBySql("
            select CONCAT(YEAR(date_lend),'/', MONTH(date_lend)) as month, SUM(lend_amount) as totalamount FROM lend GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Lent Per Mont',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Lent Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionLoanpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Borrow::findBySql("
            select CONCAT(YEAR(date_of_request),'/', MONTH(date_of_request)) as month, SUM(loan_amount) as totalamount FROM borrow GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id, 'approve_flag'=>2])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Borrowed Per Month',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Borrowed Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionStaffloanhistory(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $user_login_id = Yii::$app->request->getQueryParam('staff_id');
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $staff = UserDetails::findOne(['user_login_id'=>$user_login_id]);
        $history = Borrow::find()->select(['user_details.firstname','user_details.lastname','user_details.middlename','user_details.phone_number','user_details.email',
            'borrow.borrow_id', 'borrow.loan_amount', 'borrow.period', 'borrow.status_id', 'borrow.amount_to_pay', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
            'borrow.company_id', 'company.company_name'])->innerJoin('user_details','user_details.user_id=borrow.user_id')->innerJoin('company', 'company.company_id=borrow.company_id')->where(['borrow.user_id'=>$staff->user_id, 'borrow.company_id'=>$admindetails->company_id])->asArray()->all();

        if (!is_array($history)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($history)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No History',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staff Loan History Fetched Successfully',
                'data' => $history
            ];
        }
    }

    public function actionStafflendhistory(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $user_login_id = Yii::$app->request->getQueryParam('staff_id');
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $staff = UserDetails::findOne(['user_login_id'=>$user_login_id]);
        $history = Lend::find()->select(['lend.lend_id', 'lend.lend_amount', 'lend.date_lend', 'lend.amount_to_receive', 'lend.status_id', 'lend.approve_flag',
            'lend.company_id', 'lend.status_id', 'company.company_name'])->innerJoin('company', 'company.company_id=lend.company_id')->where(['lend.user_id'=>$staff->user_id, 'lend.company_id'=>$admindetails->company_id])->asArray()->all();

        if (!is_array($history)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($history)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No History',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staff Lend History Fetched Successfully',
                'data' => $history
            ];
        }
    }

    //get all loan transaction history for each company

    public function actionLoanhistory()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();


        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();
        $admin = UserLogin::find()->where([$access_token=>$get_access_token])->one();
        $admindetails = UserDetails::find()->where(['user_login_id'=>$admin->user_login_id])->one();


        $companyloantransactionhistory = Borrow::find()->select(['user_details.firstname','user_details.lastname','user_details.middlename','user_details.phone_number','user_details.email',
            'borrow.borrow_id', 'borrow.loan_amount', 'borrow.period', 'borrow.status_id', 'borrow.priority', 'borrow.amount_to_pay', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
            'borrow.company_id', 'company.company_name'])->innerJoin('user_details','user_details.user_id=borrow.user_id')->innerJoin('company', 'company.company_id=borrow.company_id')
            ->where(['borrow.company_id'=>$admindetails->company_id, 'user_details.status_id'=>2])->asArray()->all();

//        $lenders = Lend::find()->where(['company_id'=>$admindetails->company_id, 'status_id'=>2])->asArray()->all();

        if(!is_array($companyloantransactionhistory)){
            $message = "Error retrieving borrow transaction for company";
            return ApiUtility::errorResponse($message);
        }
        elseif(empty($companyloantransactionhistory)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No loan transaction for this company',
                'data' => null
            ];
        }
        else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Loan transaction fetched for company',
                'data' => $companyloantransactionhistory
            ];
        }

    }

    //get all lend history for each company
    public function actionLendhistory()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $companylendtransactionhistory = Lend::find()->select(['lend.lend_id', 'lend.lend_amount', 'lend.date_lend', 'lend.amount_to_receive', 'lend.status_id', 'lend.approve_flag',
            'lend.company_id', 'lend.status_id', 'company.company_name'])
            ->innerJoin('user_details','user_details.user_id=lend.user_id')->innerJoin('company', 'company.company_id=lend.company_id')
            ->where(['lend.company_id'=>$admindetails->company_id, 'user_details.status_id'=>2])->asArray()->all();
        if(!is_array($companylendtransactionhistory))
        {
            $message = "Error retrieving lend transaction for company";
            return ApiUtility::errorResponse($message);
        }
        elseif(empty($companylendtransactionhistory))
        {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No lend transaction for this company',
                'data' => null
            ];
        }
        else
        {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Lend transaction fetched for company',
                'data' => $companylendtransactionhistory
            ];
        }

    }

/*
    public function actionRegistermultipleuser(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $params = $_REQUEST;

        $passwordLength = 7;

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $userinfos = $params['data'];
        //$userinfos = Yii::$app->request->getQueryParam('users_list');
        /*$paramsValuePair = [
            UserDetailsApiKeyHelper::USER_INFO => ApiUtility::TYPE_ARR,
        ];

        $this->paramCheck($paramsValuePair, $params);*/

        //$userinfos = [['password'=>'jj111111j', 'phone_number'=>'0', 'user_email'=>'yiifgmail.com', 'company_name'=>'ESL'],['password'=>'jj111111j', 'phone_number'=>'0', 'user_email'=>'yii2@gmail.com', 'company_name'=>'ESL']];
        //$userinfos = $params[UserDetailsApiKeyHelper::USER_INFO];
        //$userinfos = unserialize($userinfos);

        //return $userinfos;
   /*     $error_msg = '';

        $count=0;
        $tag=0;
        $response = array();
            foreach($userinfos as $userinfo){
                $password = $userinfo["password"];
                $phone_number = $userinfo["phone_number"];
                $companyname = $userinfo["company_name"];

                $companyid = Company::findOne(['company_name'=>$companyname]);
                $user_type = 3;
                $email = $userinfo["user_email"];

                $get_access_token = ApiUtility::generateAccessToken();
                date_default_timezone_set('Africa/Lagos');
                $time = date('Y-m-d H:i:s');

                //$connection = Yii::$app->db;
                //$transaction = $connection->beginTransaction();

                $fetch_mail = UserDetails::find()->where(['email'=>$email])->asArray()->one();

                $fetch_names = UserLogin::find()->where(['user_email' => $email, 'status_id' => 2])->asArray()->one();

                if($companyid != null){
                    if (!empty($password) && !empty($email)) {

                        if(ApiUtility::isValidEmail($email))
                        {
                            if($fetch_mail == null && $fetch_names == null)
                            {
                                if (strlen($password) >= $passwordLength) {
                                    //try {
                                    $values = [];
                                    $userlogins = new UserLogin();

                                    $userlogins->user_email = $email;
                                    $userlogins->user_password = ApiUtility::generatePasswordHash($password);
                                    $userlogins->access_token = $get_access_token;
                                    $userlogins->user_type_id = $user_type;
                                    $userlogins->created_date = $time;
                                    $userlogins->modified_date = $time;


                                    if ($userlogins->save()) {

                                        array_push($values, $userlogins->user_login_id);
                                    }

                                    $userdetails = new UserDetails();

                                    $userdetails->email = $email;
                                    $userdetails->company_id = $companyid->company_id;
                                    $userdetails->user_type_id = $user_type;
                                    $userdetails->phone_number = $phone_number;
                                    $userdetails->user_login_id = $values[0];
                                    $userdetails->created_date = $time;
                                    $userdetails->modified_date = $time;


                                    if($userdetails->save()){
                                        //$transaction->commit();

                                        //send respnse email

                                        array_push($response, $email.' Registration Successful');
                                    }

                                    //} catch (Exception $ex) {
                                    //$transaction->rollBack();
                                    //}
                                } else {
                                    $tag = 3;
                                    array_push($response, $email.' Password is less than 7 characters');
                                    //$response["password_error"] = $email.' Password is less than 7 characters';
                                    $error_msg = 'Password should not be less than 7 characters';
                                }
                            }
                            else
                            {
                                $count++;

                                array_push($response, $email.' already exists');
                                //$response["password_error"] = $email.' Password is less than 7 characters';
                                $error_msg = 'User already exists';
                            }
                        }
                        else
                        {

                            array_push($response, $email.' Email Invalid');
                            $error_msg = 'Invalid email address';
                        }
                    } else {

                        array_push($response, $email.' Should Fill in the required fields');
                        $error_msg = 'Fill in the required fields';
                    }
                }else{

                    array_push($response, $email.' Company not available');
                    $error_msg = 'Company not available';
                }


            }

            $this->setHeader(200);
            /*$message = "Thanks for registering on our platform. Your account will be activated
                        within the next 24 hours. Please bear with us<br/>
                        Thanks,<br/> Team RibyPeerLending";


            $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $userdetails->email, $this->getReplyEmail(),$email);*/
/*
            return [
                'status' => true,
                'message' => $response,
                'data' => null,
            ];
    }
*/


    public function actionClearloan(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('borrow_id');

        $checkborrower = Borrow::find()->select('borrow_id')->where(['borrow_id'=> $borrower_id, 'approve_flag'=>2, 'status_id'=>2])->asArray()->one();

        if($checkborrower['borrow_id'] != null and count($checkborrower) > 0)
        {
            $update =   Borrow::updateAll(['status_id'=> 1], ['borrow_id'=> $checkborrower['borrow_id']]);

            if($update) {


                return
                    [
                        'status' => true,
                        'message' => 'Loan Cleared',
                        'data' => null
                    ];
            }
            else{
                $msg = "Oops, An Error Occurs";
                return ApiUtility::errorResponse($msg);
            }
        }



        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }

    public function actionClearlend(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $lend_id = Yii::$app->request->getQueryParam('lend_id');

        $checklender = Lend::find()->select('lend_id')->where(['lend_id'=> $lend_id, 'approve_flag'=>1, 'status_id'=>2])->asArray()->one();

        if($checklender['lend_id'] != null and count($checklender) > 0)
        {
            $update =   Lend::updateAll(['status_id'=> 1], ['lend_id'=> $checklender['len_id']]);

            if($update) {


                return
                    [
                        'status' => true,
                        'message' => 'Lend Cleared',
                        'data' => null
                    ];
            }
            else{
                $msg = "Oops, An Error Occurs";
                return ApiUtility::errorResponse($msg);
            }
        }



        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }



    public function actionGetaudit(){

          $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        $id = $user->user_id;

    $rows = (new \yii\db\Query())
    ->select(['*'])
    ->from('admin_log')
    ->where(['user_id' => $id])
    ->all();

   return [
            'status' => true,
            'message' => 'Query successfully',
            'data' => $rows
        ];
    }

    public function actionGettransactionremarks()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $remark_types = [self::getAppParams('companyonlydeclineremarks'),self::getAppParams('alltypesdeclineremarks')];

        $transactionremark = TransactionRemark::find()->select(['remark_id', 'remark_description'])->where(["remark_type"=>$remark_types])->asArray()->all();
        if (!is_array($transactionremark)) {
            $message = "Error retrieving Transaction Remarks";
            return ApiUtility::errorResponse($message);
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Transaction Remarks Fetched Successfully',
                'data' => $transactionremark
            ];
        }
    }

    public function updatePassword($access_token, $password)
    {
        $update = UserLogin::findOne(['access_token' => $access_token]);

        $update->user_password = $password;
        if($update->update()){
            return true;
        }else{
            return false;
        }
    }

    public function actionChangepassword()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;
        $passwordlength = 7;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsPairValue = [
            UserLoginApiKeyHelper::USER_PASSWORD  => ApiUtility::TYPE_STRING,
            UserLoginApiKeyHelper::CONFIRM_PASSWORD  => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsPairValue, $params);

        $get_access_token = $this->checkApiKey();
        $password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
        $cpassword = $params[UserLoginApiKeyHelper::CONFIRM_PASSWORD];

        if(strlen($password)>=$passwordlength){
            if($password == $cpassword){
                $password =  ApiUtility::generatePasswordHash($password);

                $upass = $this->updatePassword($get_access_token, $password);

                if($upass){
                    $this->setHeader(200);
                    return[
                        'status' => true,
                        'message' => 'Password Updated',
                    ];
                }else{
                    return[
                        'status' => false,
                        'message' => 'Password Not Updated',
                    ];
                }

            }else{
                return[
                    'status' => false,
                    'message' => 'Passwords do not match',
                ];
            }
        }else{
            return[
                'status' => false,
                'message' => 'Password Length must be at least 7 characters',
            ];
        }
    }

    public function getUserActiveLoan($user_id){
        return Borrow::find()->where(['user_id'=>$user_id, 'approve_flag'=>3, 'status_id'=>2])->orderBy(['date_of_request'=> SORT_DESC])->asArray()->one();
    }

    public function getUserLoanAwaitingApproval($user_id){
        return Borrow::find()->where(['user_id'=>$user_id, 'approve_flag'=>0, 'status_id'=>2])->orderBy(['date_of_request'=> SORT_DESC])->asArray()->one();
    }

    public function getUserLoanHistory($user_id){
        $hist = Borrow::findBySql("
            SELECT br.company_id, br.user_id, br.loan_amount, br.period, br.amount_to_pay, br.date_of_request, br.date_to_pay, br.status_id, br.approve_flag,
            us.firstname, us.middlename, us.lastname, us.phone_number, us.email, us.address, cm.company_name FROM borrow br
            inner join user_details us ON us.user_id=br.user_id inner join company cm
            ON cm.company_id=br.company_id WHERE us.user_id = $user_id
        ")->asArray()->all();

        return $hist;
        //return Borrow::find()->where(['user_id'=>$user_id])->asArray()->all();
    }
}
