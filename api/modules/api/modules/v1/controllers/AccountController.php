<?php

namespace app\modules\api\modules\v1\controllers;

use app\modules\api\modules\v1\models\Company;
use app\modules\api\modules\v1\models\CompanyApiKeyHelper;
use app\modules\api\modules\v1\models\UserDetails;
use app\modules\api\modules\v1\models\UserLogin;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\modules\v1\models\UserDetailsApiKeyHelper;
use app\modules\api\modules\v1\models\UserLoginApiKeyHelper;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;
use yii\base\Exception;

class AccountController extends BaseController
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create'  => ['POST'],

                ],
            ],
        ]);
    }
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSignup()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();
        $params = $_REQUEST;

        $passwordLength = 7;

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsValuePair = [
            UserDetailsApiKeyHelper::PHONE_NUMBER => ApiUtility::TYPE_STRING,
            UserLoginApiKeyHelper::USER_PASSWORD => ApiUtility::TYPE_STRING,
            UserLoginApiKeyHelper::USER_EMAIL => ApiUtility::TYPE_STRING,
            CompanyApiKeyHelper::COMPANYNAME => ApiUtility::TYPE_STRING
        ];

        $this->paramCheck($paramsValuePair, $params);

        $phone_number = $params[UserDetailsApiKeyHelper::PHONE_NUMBER];
        $password = $params[UserLoginApiKeyHelper::USER_PASSWORD];
        $user_type = 3;
        $companyname = $params[CompanyApiKeyHelper::COMPANYNAME];
        $companyid = Company::findOne(['company_name'=>$companyname]);
        $email = $params[UserLoginApiKeyHelper::USER_EMAIL];

        $get_access_token = ApiUtility::generateAccessToken();

        date_default_timezone_set('Africa/Lagos');
        $time = date('Y-m-d H:i:s');
        $expiration_date = date('Y-m-d H:i:s', strtotime("+7 day", strtotime($time)));

        $connection = Yii::$app->db;

        $transaction = $connection->beginTransaction();

        $error_msg = '';

        $fetch_mail = UserDetails::find()->where(['email'=>$email])->asArray()->one();

        $fetch_names = UserLogin::find()->where(['user_email' => $email, 'status_id' => 2])->asArray()->one();

        if($companyid != null){
            if (!empty($password) && !empty($email)) {

                if(ApiUtility::isValidEmail($email))
                {
                    if($fetch_mail == null && $fetch_names == null)
                    {
                        if (strlen($password) >= $passwordLength) {
                            try {
                                $values = [];
                                $userlogins = new UserLogin();

                                $userlogins->user_email = $email;
                                $userlogins->user_password = ApiUtility::generatePasswordHash($password);
                                $userlogins->access_token = $get_access_token;
                                $userlogins->user_type_id = $user_type;
                                $userlogins->created_date = $time;
                                $userlogins->modified_date = $time;
                                //$userlogins->expiration_date = $expiration_date;


                                if ($userlogins->save()) {

                                    array_push($values, $userlogins->user_login_id);
                                }

                                $userdetails = new UserDetails();

                                $userdetails->email = $email;
                                $userdetails->company_id = $companyid->company_id;
                                $userdetails->user_type_id = $user_type;
                                $userdetails->phone_number = $phone_number;
                                $userdetails->user_login_id = $values[0];
                                $userdetails->created_date = $time;
                                $userdetails->modified_date = $time;


                                if($userdetails->save()){
                                    $transaction->commit();

                                    //send respnse email

                                    $this->setHeader(200);
                                    $message = "Thank you for registering on our platform. <br>
                                                Your account will be activated within the next 24 hours. Please bear with us<br/>
                                                Thank you.<br/><br>
                                                 ";


                                    $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $userdetails->email, $this->getReplyEmail(),$email,$companyname);

                                    return [
                                        'status' => true,
                                        'message' => 'Registration Successful, An Email Has Been Sent To You.',
                                        'data' => null,
                                    ];

                                }

                            } catch (Exception $ex) {
                                $transaction->rollBack();
                            }
                        } else {
                            $error_msg = 'Password should not be less than 7 characters';
                        }
                    }
                    else
                    {
                        $error_msg = 'User already exists';
                    }
                }
                else
                {
                    $error_msg = 'Invalid email address. Kindly check and try again';
                }
            } else {
                $error_msg = 'Fill in the required fields';
            }
        }else{
            $error_msg = 'Company not available';
        }

        return ApiUtility::errorResponse($error_msg);

    }
    
    

}
