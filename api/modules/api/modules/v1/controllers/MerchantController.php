<?php

namespace app\modules\api\modules\v1\controllers;
use app\modules\api\modules\v1\models\MerchantAccess;

class MerchantController extends \yii\web\Controller
{

    private $_app_secret = '';

  
    private function generate_api_secret()
    {
          for ($i = 16; $i <= 16; $i++) {
          $bytes = openssl_random_pseudo_bytes($i, $cstrong);
          $this->_api_secret = bin2hex($bytes);
          return $this->_api_secret;
        }
    }


}
