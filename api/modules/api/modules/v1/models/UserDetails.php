<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "user_details".
 *
 * @property integer $user_id
 * @property string $title
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $phone_number
 * @property string $email
 * @property integer $company_id
 * @property string $address
 * @property string $gender
 * @property string $birthdate
 * @property integer $user_type_id
 * @property string $user_image_url
 * @property string $created_date
 * @property string $modified_date
 * @property integer $status_id
 * @property integer $employee_status_id
 * @property integer $user_login_id
 *
 * @property AdminLog[] $adminLogs
 * @property AdminLog[] $adminLogs0
 * @property UserType $userType
 * @property Status $status
 * @property EmployeeStatus $employeeStatus
 * @property Company $company
 * @property UserLogin $userLogin
 */
class UserDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone_number', 'email', 'company_id', 'user_type_id', 'created_date', 'modified_date', 'user_login_id'], 'required'],
            [['company_id', 'user_type_id', 'status_id', 'employee_status_id', 'user_login_id'], 'integer'],
            [['gender'], 'string'],
            [['birthdate', 'created_date', 'modified_date'], 'safe'],
            [['title'], 'string', 'max' => 10],
            [['firstname', 'middlename', 'lastname', 'user_image_url'], 'string', 'max' => 50],
            [['phone_number'], 'string', 'max' => 14],
            [['email'], 'string', 'max' => 80],
            [['address'], 'string', 'max' => 350],
            [['user_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserType::className(), 'targetAttribute' => ['user_type_id' => 'user_type_id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'status_id']],
            [['employee_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmployeeStatus::className(), 'targetAttribute' => ['employee_status_id' => 'employee_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'company_id']],
            [['user_login_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserLogin::className(), 'targetAttribute' => ['user_login_id' => 'user_login_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'title' => 'Title',
            'firstname' => 'Firstname',
            'middlename' => 'Middlename',
            'lastname' => 'Lastname',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'company_id' => 'Company ID',
            'address' => 'Address',
            'gender' => 'Gender',
            'birthdate' => 'Birthdate',
            'user_type_id' => 'User Type ID',
            'user_image_url' => 'User Image Url',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'status_id' => 'Status ID',
            'employee_status_id' => 'Employee Status ID',
            'user_login_id' => 'User Login ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminLogs()
    {
        return $this->hasMany(AdminLog::className(), ['admin_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminLogs0()
    {
        return $this->hasMany(AdminLog::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserType()
    {
        return $this->hasOne(UserType::className(), ['user_type_id' => 'user_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeStatus()
    {
        return $this->hasOne(EmployeeStatus::className(), ['employee_id' => 'employee_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogin()
    {
        return $this->hasOne(UserLogin::className(), ['user_login_id' => 'user_login_id']);
    }
}
