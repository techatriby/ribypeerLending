<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "lend".
 *
 * @property integer $lend_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $lend_amount
 * @property string $date_lend
 * @property integer $amount_to_receive
 * @property string $date_to_receive
 * @property integer $status_id
 * @property integer $approve_flag
 * @property string $modified_date
 *
 * @property Company $company
 * @property UserDetails $user
 * @property Status $status
 */
class Lend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'user_id', 'lend_amount', 'date_lend', 'amount_to_receive', 'status_id', 'approve_flag', 'modified_date'], 'required'],
            [['company_id', 'user_id', 'lend_amount', 'amount_to_receive', 'status_id', 'approve_flag'], 'integer'],
            [['date_lend', 'date_to_receive', 'modified_date'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'company_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'status_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lend_id' => 'Lend ID',
            'company_id' => 'Company ID',
            'user_id' => 'User ID',
            'lend_amount' => 'Lend Amount',
            'date_lend' => 'Date Lend',
            'amount_to_receive' => 'Amount To Receive',
            'date_to_receive' => 'Date To Receive',
            'status_id' => 'Status ID',
            'approve_flag' => 'Approve Flag',
            'modified_date' => 'Modified Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['status_id' => 'status_id']);
    }
}
