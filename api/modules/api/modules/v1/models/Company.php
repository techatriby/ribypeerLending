<?php

namespace app\modules\api\modules\v1\models;
use Yii;

/**
 * This is the model class for table "company".
 *
 * @property integer $company_id
 * @property string $company_name
 * @property string $company_address
 * @property string $company_info
 *
 * @property UserDetails[] $userDetails
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name','company_address'], 'required'],
            [['company_name'], 'string', 'max' => 150],
            [['company_address','company_info'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'company_name' => 'Company Name',
            'company_address' => 'Company Address',
            'company_info' => 'Company Info',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasMany(UserDetails::className(), ['company_id' => 'company_id']);
    }
}
