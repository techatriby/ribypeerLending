<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "user_bank_details".
 *
 * @property integer $user_bank_details_id
 * @property integer $user_id
 * @property string $bank_name
 * @property integer $account_number
 * @property integer $bvn
 * @property string $created_date
 * @property string $modified_date
 *
 * @property UserDetails $user
 */
class UserBankDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_bank_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'bank_name', 'account_number', 'bvn', 'created_date', 'modified_date'], 'required'],
            [['user_id', 'account_number', 'bvn'], 'integer'],
            [['created_date', 'modified_date'], 'safe'],
            [['bank_name'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['user_id' => 'user_login_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_bank_details_id' => 'User Bank Details ID',
            'user_id' => 'User ID',
            'bank_name' => 'Bank Name',
            'account_number' => 'Account Number',
            'bvn' => 'Bvn',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'user_id']);
    }
}
