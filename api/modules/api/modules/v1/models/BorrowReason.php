<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "borrow_reason".
 *
 * @property integer $id
 * @property string $title
 */
class BorrowReason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'borrow_reason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reason_id' => 'ID',
            'title' => 'Title',
        ];
    }
}
