<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "user_payment".
 *
 * #@property integer #$user_id
 * @property string $transaction_id
 * @property string $payment_amount
 * @property string $reference_id
 * @property string $payment_type
 * @property string $payment_option
 * @property string $payment_number
 * @property string $payment_description
 * @property string $payment_date
 * @property string $status_id
 *
 *
 * @property UserDetails[] $userDetails
 * @property UserLogin[] $userLogins
 */
class UserPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['transaction_id', /*'user_id',*/ 'payment_amount', /*'reference_id',*/ 'payment_type', 'payment_option', 'payment_date', 'status_id'], 'required'],
            //  [['transaction_id', /*'user_id',*/ 'payment_type', 'payment_option', 'status_id'], 'integer'],
            //  [['payment_number', 'payment_description','payment_amount'], 'string'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

            'transaction_id' => 'Trasaction ID',
            /*'user_id'        =>  'User ID',*/
            'payment_amount' =>  'Payment Amount',
            // 'reference_id'  =>   'Reference ID',
            'payment_type'  =>   'Payment Type',
            'payment_option'  =>   'Payment Option',
            'payment_number'  =>   'Payment Number',
            'payment_description'  =>   'Payment Description',
            'payment_date'  =>   'Payment Date',
            'status_id'      =>    'Status ID'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery

    public function getPaymentDetails()
    {
        return $this->hasMany(UserPayment::className(), ['user_id' => 'user_id']);
    }
     * */

}
