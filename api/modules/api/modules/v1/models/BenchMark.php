<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "bench_mark".
 *
 * @property integer $bench_mark_id
 * @property string $bench_mark_min
 * @property string $bench_mark_max
 * @property string $created_date
 * @property string $modified_date
 */
class BenchMark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bench_mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bench_mark_min', 'bench_mark_max', 'created_date', 'modified_date'], 'required'],
            [['created_date', 'modified_date'], 'safe'],
            [['bench_mark_min', 'bench_mark_max'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bench_mark_id' => 'Bench Mark ID',
            'bench_mark_min' => 'Bench Mark Min',
            'bench_mark_max' => 'Bench Mark Max',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
        ];
    }
}
