<?php


namespace app\modules\api\modules\v1\models;


class BankApiKeyHelper extends ApiKeyHelper
{
  const AMOUNT = "amount";
  const ACCOUNT_NUM = "account_num";
  const USER_ACCOUNT_NAME = "user_account_name";
  const BANK_NAME = "bank_name";
  const BANK_SORT_CODE = "bank_sort_code";
  const BANK_ID = "bank_id";
  const USER_ID = "user_id";
}
