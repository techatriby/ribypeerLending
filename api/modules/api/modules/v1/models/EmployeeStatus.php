<?php

namespace app\modules\api\modules\v1\models;
use Yii;

/**
 * This is the model class for table "employee_status".
 *
 * @property integer $employee_id
 * @property string $employee_status_name
 *
 * @property UserDetails[] $userDetails
 */
class EmployeeStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_status_name'], 'required'],
            [['employee_status_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'employee_status_name' => 'Employee Status Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasMany(UserDetails::className(), ['employee_status_id' => 'employee_id']);
    }
}
