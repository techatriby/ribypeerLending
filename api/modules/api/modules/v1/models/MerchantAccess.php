<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "merchant_access".
 *
 * @property integer $id
 * @property string $name
 * @property string $api_key
 * @property string $secret
 */
class MerchantAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'api_key', 'secret'], 'required'],
            [['name', 'api_key', 'secret'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'api_key' => 'Api Key',
            'secret' => 'Secret',
        ];
    }
}
