<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "borrow".
 *
 * @property integer $borrow_id
 * @property integer $company_id
 * @property integer $user_id
 * @property integer $loan_amount
 * @property double $amount_to_pay
 * @property string $date_of_request
 * @property integer $period
 * @property string $date_to_pay
 * @property string $date_updated
 * @property float $interest_rate
 * @property float $insurance_rate
 * @property double $fixed_charge
 * @property double $fine
 * @property integer $priority
 * @property integer $status_id
 * @property integer $approve_flag
 *
 * @property Company $company
 * @property UserDetails $user
 * @property Status $status
 */
class Borrow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'borrow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'user_id', 'loan_amount', 'amount_to_pay', 'date_of_request', 'period','date_to_pay',  'interest_rate', 'insurance_rate', 'fixed_charge', 'fine', 'priority', 'status_id','transaction_id','reason_id'], 'required'],
            [['company_id', 'user_id', 'loan_amount', 'status_id','approve_flag','transaction_id','reason_id'], 'integer'],
            [['date_of_request', 'date_to_pay'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'company_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['reason_id'], 'exist', 'skipOnError' => true, 'targetClass' => BorrowReason::className(), 'targetAttribute' => ['reason_id' => 'reason_id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'status_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'borrow_id' => 'Borrow ID',
            'company_id' => 'Company ID',
            'user_id' => 'User ID',
            'loan_amount' => 'Loan Amount',
            'amount_to_pay' => 'Amount To Pay',
            'date_of_request' => 'Date Of Request',
            'period' => 'Loan Period',
            'date_to_pay' => 'Date To Pay',
            'date_updated' => 'Date Last Updated',
            'interest_rate' => 'Interest Rate',
            'insurance_rate' => 'Insurance Rate',
            'fixed_charge' => 'Fixed Charge',
            'fine' => 'Fine',
            'priority' => 'Priotity',
            'status_id' => 'Status ID',
            'approve_flag' => 'Approve Flag',
            'transaction_id' => 'Transaction ID',
            'process_description' => 'Process Description',
            'process_remarks' => 'Process Remarks',
            'reason_id' => 'Reason ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserDetails::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['status_id' => 'status_id']);
    }
}
