<?php


namespace app\modules\api\modules\v1\models;


class ContactApiKeyHelper extends ApiKeyHelper
{
  const NAME = "name";
  const EMAIL = "email";
  const PHONE = "phone";
  const MESSAGE = "message";

  // REQUEST INVITE
  const FULLNAME = "fullname";
  const COMPANY_NAME = "company_name";
  const HR_EMAIL = "hr_email";
  const HR_PHONE = "hr_phone";

}
