<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "payment_option_type".
 *
 * @property integer $payment_option_type_id
 * @property string $name
 * @property integer $payment_option
 */
class PaymentOptionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_option_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'payment_option_id'], 'required'],
            [['payment_option_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_option_type_id' => 'Payment Option Type ID',
            'name' => 'Name',
            'payment_option_id' => 'Payment Option ID',
        ];
    }
}
