<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "user_type".
 *
 * @property integer $user_type_id
 * @property string $user_type_name
 *
 * @property UserDetails[] $userDetails
 * @property UserLogin[] $userLogins
 */
class UserType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_type_name'], 'required'],
            [['user_type_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_type_id' => 'User Type ID',
            'user_type_name' => 'User Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasMany(UserDetails::className(), ['user_type_id' => 'user_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogins()
    {
        return $this->hasMany(UserLogin::className(), ['user_type_id' => 'user_type_id']);
    }
}
