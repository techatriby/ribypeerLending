<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "repayment_schedule".
 *
 * @property integer $repayment_schedule_id
 * @property integer $user_payment_details
 * @property string $total_amount
 * @property string $date_submitted
 * @property string $date_paid
 * @property integer $status
 * @property string $repayment_schedule_url
 */
class RepaymentSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'repayment_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['user_payment_details', 'total_amount', 'repayment_schedule_url'], 'required'],
            // [['status'], 'integer'],
            // [['total_amount', 'date_submitted', 'date_paid'], 'string', 'max' => 40],
            // [['repayment_schedule_url'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'repayment_schedule_id' => 'Repayment Schedule ID',
            'user_payment_details' => 'User Payment Details',
            'total_amount' => 'Total Amount',
            'date_submitted' => 'Date Submitted',
            'date_paid' => 'Date Paid',
            'status' => 'Status',
            'repayment_schedule_url' => 'Repayment Schedule Url',
        ];
    }
}
