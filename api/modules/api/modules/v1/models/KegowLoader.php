<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "kegow_transaction".
 *
 * @property integer $transaction_id
 * @property string $result
 * @property string $transactID
 * @property string $checkSUM
 * @property string $orderID
 */
class KegowLoader extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kegow_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['result', 'transactID', 'checkSUM', 'orderID'], 'required'],
            [['result', 'transactID', 'checkSUM', 'orderID'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id' => 'Transaction ID',
            'result' => 'Result',
            'transactID' => 'Transact ID',
            'checkSUM' => 'Check Sum',
            'orderID' => 'Order ID',
        ];
    }
}
