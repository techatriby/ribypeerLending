<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "admin_log".
 *
 * @property integer $admin_log_id
 * @property string $admin_log_details
 * @property integer $admin_id
 * @property integer $user_id
 *
 * @property UserDetails $admin
 * @property UserDetails $user
 */
class AdminLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_log_details', 'company_id', 'user_id','admin_log_id','transaction_id'], 'required'],
            [['company_id', 'user_id','transaction_id'], 'integer'],
            [['admin_log_details'], 'string', 'max' => 500],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['company_id' => 'company_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserDetails::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'admin_log_id' => 'Admin Log ID',
            'admin_log_details' => 'Admin Log Details',
            'company_id' => 'Company ID',
            'user_id' => 'User ID',
            'transaction_id'=>'Transaction_ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(UserDetails::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(AdminLog::className(), ['transaction_id' => 'transaction_id']);
    }
}
