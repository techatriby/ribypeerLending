<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "request_invite".
 *
 * @property integer $reques_invite
 * @property string $email
 * @property string $phone
 * @property string $fullname
 * @property string $company_name
 * @property string $hr_email
 * @property string $hr_phone
 */
class RequestInvite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request_invite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'phone', 'fullname', 'company_name', 'hr_email', 'hr_phone'], 'required'],
            [['email', 'hr_email'], 'string', 'max' => 50],
            [['phone', 'hr_phone'], 'string', 'max' => 14],
            [['fullname', 'company_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'request_invite' => 'Request Invite',
            'email' => 'Email',
            'phone' => 'Phone',
            'fullname' => 'Fullname',
            'company_name' => 'Company Name',
            'hr_email' => 'Hr Email',
            'hr_phone' => 'Hr Phone',
        ];
    }
}
