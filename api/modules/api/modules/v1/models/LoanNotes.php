<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "loan_notes".
 *
 * @property integer $loan_note_id
 * @property integer $user_id
 * @property string $transaction_id
 * @property string $amount_to_pay
 * @property string $period
 * @property string $interest_rate
 * @property string $fixed_charge
 * @property string $insurance_rate
 * @property string $priority
 * @property string $fine
 * @property string $principal_repayment_holder
 * @property string $monthly_interest_holder
 * @property string $insurance_charges_holder
 * @property string $priority_charges_holder
 * @property string $fines_charges_holder
 * @property string $transaction_charges_holder
 * @property string $monthly
 * @property string $principal_charge
 * @property string $payment_total_charges
 * @property string $loan_repayment
 * @property string $total_repayment_rate
 * @property string $effrate
 * @property string $date_of_request
 * @property string $date_disbursed
 */
class LoanNotes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_notes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['user_id','amount_to_pay', 'period', 'interest_rate', 'fixed_charge', 'insurance_rate', 'priority', 'fine', 'principal_repayment_holder', 'monthly_interest_holder', 'insurance_charges_holder', 'priority_charges_holder', 'fines_charges_holder', 'transaction_charges_holder', 'monthly', 'principal_charge', 'payment_total_charges', 'loan_repayment', 'total_repayment_rate', 'effrate', 'date_of_request'], 'required'],
            // [['user_id'], 'integer'],
            // [['date_disbursed'], 'safe'],
            // [['transaction_id', 'amount_to_pay', 'period', 'interest_rate', 'loan_repayment', 'total_repayment_rate', 'effrate', 'date_of_request'], 'string', 'max' => 40],
            // [['fixed_charge', 'insurance_rate', 'priority', 'fine'], 'string', 'max' => 20],
            // [['principal_repayment_holder', 'monthly_interest_holder', 'insurance_charges_holder', 'priority_charges_holder', 'fines_charges_holder', 'transaction_charges_holder', 'monthly', 'principal_charge', 'payment_total_charges'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loan_note_id' => 'Loan Note ID',
            'user_id' => 'User ID',
            'transaction_id' => 'Transaction ID',
            'amount_to_pay' => 'Amount To Pay',
            'period' => 'Period',
            'interest_rate' => 'Interest Rate',
            'fixed_charge' => 'Fixed Charge',
            'insurance_rate' => 'Insurance Rate',
            'priority' => 'Priority',
            'fine' => 'Fine',
            'principal_repayment_holder' => 'Principal Repayment Holder',
            'monthly_interest_holder' => 'Monthly Interest Holder',
            'insurance_charges_holder' => 'Insurance Charges Holder',
            'priority_charges_holder' => 'Priority Charges Holder',
            'fines_charges_holder' => 'Fines Charges Holder',
            'transaction_charges_holder' => 'Transaction Charges Holder',
            'monthly' => 'Monthly',
            'principal_charge' => 'Principal Charge',
            'payment_total_charges' => 'Payment Total Charges',
            'loan_repayment' => 'Loan Repayment',
            'total_repayment_rate' => 'Total Repayment Rate',
            'effrate' => 'Effrate',
            'date_of_request' => 'Date Of Request',
            'date_disbursed' => 'Date Disbursed',
        ];
    }
}
