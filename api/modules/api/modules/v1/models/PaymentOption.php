<?php
/**
 * Copyright (c) 
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 11/2016
 * Licensed to: FinanceLife Tech
 * License subject to changes based on agreement between  Author and Licensee
 */

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "payment_option".
 *
 * @property integer $payment_option_id
 * @property string $payment_option_name
 * @property string $payment_option_type
 *
 * @property UserDetails[] $userLogin
 * @property UserDetails[] $userDetails
 * @property UserPayment[] $userPayment
 */
class PaymentOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_option_name','payment_option_type'], 'required'],
            [['payment_option_name'], 'string', 'max' => 70],
            [['payment_option_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_option_id' => 'Payment Option ID',
            'payment_option_name' => 'Payment Option Name',
            'payment_option_type' => 'Payment Option Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasMany(UserDetails::className(), ['payment_option_id' => 'payment_option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogins()
    {
        return $this->hasMany(UserLogin::className(), ['payment_option_id' => 'payment_option_id']);
    }
}
