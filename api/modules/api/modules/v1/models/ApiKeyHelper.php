<?php


namespace app\modules\api\modules\v1\models;

class ApiKeyHelper {
    CONST ACCESS_TOKEN = "access_token";
    CONST PAGE = "page";
    CONST FILTER = "filter";
    CONST LIMIT = "limit";
    CONST LAST_ID = "last_id";
}


class AdminLogApiKeyHelper extends ApiKeyHelper{

    const admin_log_ID = "admin_log_id";
    const admin_log_DETAILS = "admin_log_details";
    const ADMIN_ID = "admin_id";
}

class BorrowerLenderApiKeyHelper extends ApiKeyHelper
{
    const BORROWER_ID = "id";
    const TRANSACTION_ID = "transaction_id";
    const LOAN_AMOUNT = "loan_amount";
    const LEND_AMOUNT = "lend_amount";
    const AMOUNTTORECEIVE = "amount_to_receive";
    const AMOUNTTOPAY = "amount_to_pay";
    const INTEREST_RATE = "interest_rate";
    const INSURANCE_RATE = "insurance_rate";
    const FIXED_CHARGE = "fixed_charge";
    const FINE = "fine";
    const PRIORITY = "priority";
    const DATEOFREQUEST = "date_of_request";
    const PERIOD = "period";
    const DATETOPAY = "date_to_pay";
    const DATE_LEND = "date_lend";
    const DATE_TO_RECEIVE = "date_to_receive";
    const APPROVEFLAG = "approve_flag";
    const PROCESS_DESCRIPTION = "process_description";
    const PROCESS_REMARKS = "process_remarks";
    const REASON_ID = "reason_id";
}


class CompanyApiKeyHelper extends ApiKeyHelper
{
    const COMPANYID = "company_id";
    const COMPANYNAME = "company_name";
    const COMPANYADDRESS = "company_address";
    const COMPANYINFO = "company_info";

}

class EmployeeStatusApiKeyHelper extends ApiKeyHelper
{
    const EMPLOYEEID = "employee_id";
    const EMPLOYEESTATUSNAME = "employee_status_name";
}


class StatusApiKeyHelper extends ApiKeyHelper
{
    const STATUS_ID= "status_id";
    const STATUS_NAME= "status_name";
}

class UserDetailsApiKeyHelper extends ApiKeyHelper
{
    const TITLE = "title";
    const FIRSTNAME = "firstname";
    const MIDDLENAME = "middlename";
    const LASTNAME = "lastname";
    const PHONE_NUMBER = "phone_number";
    const EMAIL = "email";
    const COMPANY_ID = "company_id";
    const ADDRESS = "address";
    const GENDER= "gender";
    const BIRTHDATE = "birthdate";
    const EMPLOYEE_STATUS_ID = "employee_status_id";
    const USER_INFO = "user_info";
}

class UserLoginApiKeyHelper extends ApiKeyHelper
{
    const USER_LOGIN_ID = "user_login_id";
    const USER_EMAIL = "user_email";
    const USER_PASSWORD = "user_password";
    const CONFIRM_PASSWORD = "confirm_password";
    const USERS_LIST = "users_list";

}

class UserTypeApiKeyHelper  extends ApiKeyHelper
{
    const USER_TYPE_ID = "user_type_id";
    const USER_TYPE_NAME = "user_type_name";
}


class BenchMarkApiKeyHelper  extends ApiKeyHelper
{
    const BENCH_MARK_ID = "bench_mark_id";
    const BENCH_MARK_MIN = "bench_mark_min";
    const BENCH_MARK_MAX = "bench_mark_max";
}

class EmailSenderApiKeyHelper extends  ApiKeyHelper{
    const SENDER_EMAIL = "sender_email";
    const RECEIVER_EMAIL = "receiver_email";
    const MESSAGE = "message";
    const SUBJECT = "subject";
}

class UserBankDetailsApiKeyHelper extends  ApiKeyHelper{
    const BANK_NAME = "bank_name";
    const ACCOUNT_NUMBER = "account_number";
    const ACCOUNT_NAME = "account_name";
    const BANK_CODE = "bank_code";
    const BVN = "bvn";
}

class UserPaymentApiKeyHelper extends  ApiKeyHelper{

    const PAYMENT_AMOUNT = "payment_amount";
    const REFERENCE_ID = "reference_id";
    const PAYMENT_DATE = "payment_date";
    const PAYMENT_NUMBER = "payment_number";
    const PAYMENT_DESCRIPTION = "payment_description";
}

class PaymentOptionApiKeyHelper extends  ApiKeyHelper{

    const PAYMENT_OPTION_ID = "payment_option_id";
    const PAYMENT_OPTION_NAME = "payment_option_name";
    const PAYMENT_OPTION_TYPE = "payment_option_type";
}

class PaymentTypeApiKeyHelper extends  ApiKeyHelper{

    const PAYMENT_TYPE_ID = "payment_type_id";
    const PAYMENT_TYPE_NAME = "payment_type_name";
}

class TransactionRemarkApiKeyHelper extends ApiKeyHelper
{
    const REMARKID = "remark_id";
    const REMARKDESCRIPTION = "remark_description";
}
