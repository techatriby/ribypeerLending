<?php

namespace app\modules\api\modules\v1\models;


use Yii;

/**
 * This is the model class for table "user_login".
 *
 * @property integer $user_login_id
 * @property string $user_email
 * @property string $user_password
 * @property integer $user_type_id
 * @property integer $status_id
 * @property string $created_date
 * @property string $modified_date
 * @property string $access_token
 * @property string $reset_password_key
 * @property string $expiration_date
 *
 * @property UserDetails[] $userDetails
 * @property UserType $userType
 * @property Status $status
 */
class UserLogin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_login';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_email', 'user_password', 'user_type_id', 'created_date', 'modified_date', 'expiration_date', 'access_token'], 'required'],
            [['user_type_id', 'status_id'], 'integer'],
            [['created_date', 'expiration_date','modified_date'], 'safe'],
            [['user_email'], 'string', 'max' => 50],
            //[['reset_password_key'], 'string', 'max' => 200],
            [['user_password'], 'string', 'max' => 60],
            [['access_token'], 'string', 'max' => 150],
            [['user_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserType::className(), 'targetAttribute' => ['user_type_id' => 'user_type_id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'status_id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_login_id' => 'User Login ID',
            'user_email' => 'User Email',
            'user_password' => 'User Password',
            'user_type_id' => 'User Type ID',
            'status_id' => 'Status ID',
            'created_date' => 'Created Date',
            'modified_date' => 'Modified Date',
            'access_token' => 'Access Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasMany(UserDetails::className(), ['user_login_id' => 'user_login_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserType()
    {
        return $this->hasOne(UserType::className(), ['user_type_id' => 'user_type_id']);
    }

    /*
    public function sendForgotMail($activation_key,$uniqid,$expire,$to){

        $check =  Yii::$app->mailer->compose('forgot', ['activation_key'=> $activation_key,
            'uniqid'=> $uniqid, 'expire'=>$expire])

            ->setFrom('support@ribypeer')
            ->setTo($to)
            ->setReplyTo('support@ribypeer')
            ->setSubject("Forgot Password")
            ->send();
        return $check;
    }
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['status_id' => 'status_id']);
    }
}
