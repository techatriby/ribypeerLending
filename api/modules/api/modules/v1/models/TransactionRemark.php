<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "transaction_remark".
 *
 * @property integer $remark_id
 * @property string $remark_description
 * @property string $remark_type
 */
class TransactionRemark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction_remark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['remark_description','remark_type'], 'required'],
            [['remark_type'],'int'],
            [['remark_description'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'remark_id' => 'Remark ID',
            'remark_description' => 'Remark Description',
            'remark_type' => 'Remark Type',
        ];
    }
}
