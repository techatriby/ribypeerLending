<?php

namespace app\modules\api\modules\v1\models;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property string $user_id
 * @property integer $user_type_id
 * @property string $access_token
 * @property string $username
 * @property string $email
 * @property string $user_image_url
 * @property string $firstname
 * @property string $lastname
 * @property string $company_name
 * @property string $address
 * @property string $additional_address_info
 * @property integer $country_id
 * @property integer $state_id
 * @property integer $local_government_id
 * @property string $phone_num
 * @property string $password
 * @property string $subscription_type
 * @property string $my_referral_code
 * @property string $user_verification
 * @property string $password_reset_key
 * @property string $referral
 * @property string $created_at
 * @property string $modified_at
 * @property string $expiration_date
 * @property integer $active_status
 *
 * @property UserType $userType
 */
class User extends ActiveRecord
{

    const ACTIVE_STATUS = 1;
    const INACTIVE_STATUS = 0;
    const UNVERIFIED_STATUS = 2;
    private $_user = false;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_type_id', 'access_token', 'username', 'email', 'my_referral_code', 'user_verification', 'created_at', 'modified_at'], 'required'],
            [['user_type_id','country_id','state_id','local_government_id', 'active_status'], 'integer'],
            [['created_at', 'modified_at', 'expiration_date'], 'safe'],
            [['access_token'], 'string', 'max' => 400],
            [['username', 'company_name', 'my_referral_code', 'referral'], 'string', 'max' => 100],
            [['email', 'user_image_url', 'firstname', 'lastname', 'password','address','additional_address_info', 'password_reset_key'], 'string', 'max' => 200],
            [['phone_num'], 'string', 'max' => 30],
            [['subscription_type'], 'string', 'max' => 10],
            [['user_verification'], 'string', 'max' => 16],
            [['access_token'], 'unique'],
            [['email'], 'unique'],
            [['my_referral_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'user_type_id' => 'User Type ID',
            'access_token' => 'Access Token',
            'username' => 'Username',
            'email' => 'Email',
            'user_image_url' => 'User Image Url',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'company_name' => 'Company name',
            'address' => 'Address',
            'additional_address_info'=> 'Additional Address Info',
            'country_id' => 'Country ID',
            'state_id' => 'State ID',
            'local_government_id' => 'Local Government ID',
            'phone_num' => 'Phone Num',
            'password' => 'Password',
            'subscription_type' => 'Subscription Type',
            'my_referral_code' => 'My Referral Code',
            'user_verification' => 'User Verification',
            'password_reset_key' => 'Password Reset Key',
            'referral' => 'Referral',
            'created_at' => 'Created At',
            'modified_at' => 'Modified At',
            'expiration_date' => 'Expiration Date',
            'active_status' => 'Active Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserType()
    {
        return $this->hasOne(UserType::className(), ['user_type_id' => 'user_type_id']);
    }

    public function checkmailexist($email)
    {
        return User::findOne(['email'=>$email]);
    }


    public function resendmail($email)
    {
        $user = User::findOne(['email'=>$email]);

        return $user;
    }

    public function checkusernameexist($username)
    {
        return User::findOne(['username'=>$username]);
    }

    public function sendVerificationMail($verification_key, $username, $to){

        $check =  Yii::$app->mailer->compose('verification', ['verification_key'=> $verification_key, 'username'=>$username])

            ->setFrom('support@guyman.com.ng')
            ->setTo($to)
            ->setReplyTo('support@guyman.com.ng')
            ->setSubject("Email Verification")
            ->send();
        return $check;
    }

    public function sendForgotMail($activation_key,$uniqid,$expire,$to){

        $check =  Yii::$app->mailer->compose('forgot', ['activation_key'=> $activation_key,
                                                        'uniqid'=> $uniqid, 'expire'=>$expire])

            ->setFrom('support@guyman.com.ng')
            ->setTo($to)
            ->setReplyTo('support@guyman.com.ng')
            ->setSubject("Forgot Password")
            ->send();
        return $check;
    }


    public function checkverificationkey($userverification)
    {
        $message = [];
        $verification = User::findOne(['user_verification'=>$userverification, 'active_status'=>2]);
        $checkverificationdone = User::findOne(['user_verification'=>$userverification, 'active_status' => 1]);
        if(!$checkverificationdone){
            if($verification){
                    $activate = User::findOne(['user_verification'=>$userverification]);
                    $activate->active_status = 1;
                    if($activate->update()){
                        array_push($message, 1);
                        $msg= "Your account has been activated. You can now login";
                        array_push($message, $msg);
                    }
                else{
                    array_push($message, 0);
                    $msg ="Account could not activated, Try again later. If error persist, please contact the site admin";
                    array_push($message, $msg);
                }

            }
            else{
                if($checkverificationdone){
                    array_push($message, 0);
                    $msg = "Your account has already been activated";
                    array_push($message, $msg);
                }
                else{
                    array_push($message, 0);
                    $msg = "Invalid activation key";
                    array_push($message, $msg);
                }

            }
        }
        else{
            array_push($message, 0);
            $msg = "Your account has already been activated";
            array_push($message, $msg);
        }

        return $message;
    }


    public function fetchuserbillingdetails($user_id)
    {
        $user = User::find()->select(['firstname','lastname','company_name','email',
                                    'phone_num','address','additional_address_info','CONCAT("http://guyman.com.ng/api/web/",user_image_url) AS user_image',
                                    'username','state_id','local_government_id','country_id'])
                            ->where(['user_id' => $user_id, 'active_status'=>1])
                            ->asArray()->one();
        ;

        return $user;
    }


    public function fetchuserimage($user_id)
    {
        $user = User::find()->select(['CONCAT("http://guyman.com.ng/api/web/",user_image_url) AS user_image'
            ])
            ->where(['user_id' => $user_id, 'active_status'=>1])
            ->asArray()->one();
        ;

        return $user;
    }





}




