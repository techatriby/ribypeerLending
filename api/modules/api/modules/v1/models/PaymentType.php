<?php
/**
 * Copyright (c) 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 11/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "payment_type".
 *
 * @property integer $payment_type_id
 * @property string $payment_type_name
 *
 * @property UserDetails[] $userLogin
 * @property UserDetails[] $userDetails
 * @property UserPayment[] $userPaymnet
 */
class PaymentType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_type_name'], 'required'],
            [['payment_type_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_type_id' => 'Payment Type ID',
            'payment_type_name' => 'Payment Type Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDetails()
    {
        return $this->hasMany(UserDetails::className(), ['payment_type_id' => 'payment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLogins()
    {
        return $this->hasMany(UserLogin::className(), ['payment_type_id' => 'payment_type_id']);
    }
}
