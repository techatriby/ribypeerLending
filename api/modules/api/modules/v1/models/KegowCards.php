<?php

namespace app\modules\api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "kegow_cards".
 *
 * @property integer $kegow_cards_id
 * @property string $result
 * @property integer $user_id
 * @property string $cardID
 * @property string $checkSUM
 */
class KegowCards extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kegow_cards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['result', 'user_id', 'cardID', 'checkSUM'], 'required'],
            [['user_id'], 'integer'],
            [['result', 'cardID', 'checkSUM'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kegow_cards_id' => 'Kegow Cards ID',
            'result' => 'Result',
            'user_id' => 'User ID',
            'cardID' => 'Card ID',
            'checkSUM' => 'Check Sum',
        ];
    }
}
