<?php
/**
 * Created by PhpStorm.
 * User: Judekayode
 * Date: 4/16/16
 * Time: 4:17 PM
 */

namespace app\modules\api;

use Yii;

class Module extends \yii\base\Module{

    public function init(){
        parent::init();

        Yii::configure($this, require(__DIR__ . '/config.php'));
    }
}