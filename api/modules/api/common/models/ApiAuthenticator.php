<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/26/15
 * Time: 10:50 AM
 */

namespace app\modules\api\common\models;

use app\modules\api\modules\v1\controllers\BaseController;
use app\modules\api\modules\v1\models\User;
use app\modules\api\modules\v1\models\UserLogin;

use Yii;

class ApiAuthenticator {

    const STANDARD_API_KEY = "92215a06d3349532a5669db8dacb1b06";

    const STANDARD_ADMIN_API_KEY = "92215a06d3349532a5669dc8dacb1d06";

    const STANDARD_ADMIN_ADMIN_API_KEY = "1a06c521bc28b85093e9184af16ac39303be3d7d";

    const STANDARD_ADMIN_USER_API_KEY = "c9f028df95ae4cf92f3ea7b8d273a74b7faa7d05";

    const STANDARD_SUPER_ADMIN_API_KEY = "99912347866d0999a3239890232";
    
    const STANDARD_SUPER_ADMIN_ADMIN_API_KEY = "99998j2124d347866d0999a3239890232";
    
    const STANDARD_SUPER_ADMIN_USER_API_KEY = "999190fc23p47866d0999a3239890232";

    public function generateApiKey(){
        $found = 1;$key='';$exists='';
        while($found>0) {
            $key = md5(uniqid(rand(), true));
            $exists = UserLogin::find()->select('count(access_token) as count')->where(['access_token'=>$key])->asArray()->one();
            if(intval($exists['count'])<1)$found=0;
        }
        return $key;
    }

    public static function getUserIdFromKey($apiKey){
        $model = UserLogin::find()->where(['access_token'=>$apiKey,'status_id'=>2])->asArray()->one();

        if($model != null && count($model) > 0){
             return $model["user_login_id"];
        }
        else
            return false;
    }

    public static function getAdminUserIdFromKey($apiKey){

        $admin_user_types = Yii::$app->params['company_user_types'];
        $admin_type = [Yii::$app->params['company_admin_user_type']];

        $allowed_types = array_merge_recursive($admin_type,$admin_user_types);

        $model = UserLogin::find()->where(['access_token'=>$apiKey,'status_id'=>2])
        ->andWhere(['in',"user_type_id",$allowed_types])
        ->asArray()->one();

        if ($model != null && count($model) > 0){
            return $model["user_login_id"];
        }
        else
            return false;
    }


    public static function getAdminUserTypeFromKey($apiKey){

        $user_types = Yii::$app->params['company_user_types'];
        $admin_type = [Yii::$app->params['company_admin_user_type']];

        $allowed_types = array_merge_recursive($admin_type,$user_types);

        $model = UserLogin::find()->select('user_type_id')->where(['access_token'=>$apiKey,'status_id'=>2])

        ->andWhere(['in',"user_type_id",$allowed_types])
                        ->asArray()->one();

        if ($model != null && count($model) > 0){

            return $model["user_type_id"];
        }
        else
            return false;
    }



    public static function getsuperadminlevels()
    {
        //Fetch All super admin levels from the DB
        $levels = (new \yii\db\Query())
            ->select(['user_type_id'])
            ->from('user_type')
            ->where(['and','user_type_name like "Super%"','user_type_name like "%Admin%"'])
            ->all();
        $adminlevels = [];
        foreach($levels as $level){
            $adminlevels[] = $level['user_type_id'];
        }
        return $adminlevels;
    }


    public static function getSuperAdminUserTypeFromKey($apiKey){

        $superadminlevels = self::getsuperadminlevels();

        $model = UserLogin::find()->select('user_type_id')->where(['access_token'=>$apiKey,'status_id'=>2])

        ->andWhere(['in',"user_type_id",$superadminlevels])
                        ->asArray()->one();

        if ($model != null && count($model) > 0){

            return $model["user_type_id"];
        }
        else
            return false;
    }


    public static function getSuperAdminUserIdFromKey($apiKey){

        $superadminlevels = self::getsuperadminlevels();

        $model = UserLogin::find()->where(['access_token'=>$apiKey,'status_id'=>2])->andWhere(['in', 'user_type_id',$superadminlevels])
            ->asArray()->one();


        if ($model != null && count($model) > 0){

            return $model["user_login_id"];
        }
        else
            return false;
    }


    public static function verifyApiKey(){

        header('Content-Type: application/json');
        $apiKey = $_GET['key'];
        $isValid = false;
        if (ApiAuthenticator::getUserIdFromKey($apiKey) == false){
            if ($apiKey == ApiAuthenticator::STANDARD_API_KEY ){
                $isValid = true;
            }
            else {
                echo json_encode(['status'=> false, 'message'=> "invalid api key"], JSON_PRETTY_PRINT);
            exit;
            }
        }
        else {
            $isValid = true;
        }
        return $isValid;
    }

    public static function verifyAdminApiKey(){

        header('Content-Type: application/json');
        $apiKey = $_GET['key'];
        $admin_type = Yii::$app->params['company_admin_user_type'];
        $isValid = false;
        if (ApiAuthenticator::getAdminUserIdFromKey($apiKey) == false || ApiAuthenticator::getAdminUserTypeFromKey($apiKey) != $admin_type){
            if ($apiKey == ApiAuthenticator::STANDARD_ADMIN_API_KEY ){
                $isValid = true;
            }
            else {
                echo json_encode(['status'=> false, 'message'=> "invalid api key"], JSON_PRETTY_PRINT);
                exit;
            }
        }
        else {
            $isValid = true;
        }
        return $isValid;
    }

    public static function verifyAdminAdminApiKey(){

        header('Content-Type: application/json');
        $apiKey = $_GET['key'];
        $admin_type = Yii::$app->params['company_admin_user_type'];
        $isValid = false;
        if ((ApiAuthenticator::getAdminUserIdFromKey($apiKey) == false || ApiAuthenticator::getAdminUserTypeFromKey($apiKey) != 6) && ApiAuthenticator::getAdminUserTypeFromKey($apiKey) != $admin_type){
            if ($apiKey == ApiAuthenticator::STANDARD_ADMIN_ADMIN_API_KEY){
                $isValid = true;
            }
            else {
                echo json_encode(['status'=> false, 'message'=> "invalid api key"], JSON_PRETTY_PRINT);
                exit;
            }
        }
        else {
            $isValid = true;
        }
        return $isValid;
    }

    public static function verifyAdminUserApiKey(){

        header('Content-Type: application/json');
        $apiKey = $_GET['key'];
        $admin_type = Yii::$app->params['company_admin_user_type'];
        $isValid = false;
        if ((ApiAuthenticator::getAdminUserIdFromKey($apiKey) == false || (ApiAuthenticator::getAdminUserTypeFromKey($apiKey) != 6 && ApiAuthenticator::getAdminUserTypeFromKey($apiKey) != 7 )) && ApiAuthenticator::getAdminUserTypeFromKey($apiKey) != $admin_type){
            if ($apiKey == ApiAuthenticator::STANDARD_ADMIN_USER_API_KEY){
                $isValid = true;
            }
            else {
                echo json_encode(['status'=> false, 'message'=> "invalid api key"], JSON_PRETTY_PRINT);
                exit;
            }
        }
        else {
            $isValid = true;
        }
        return $isValid;
    }

    public static function verifySuperAdminApiKey(){

        header('Content-Type: application/json');
        $apiKey = $_GET['key'];
        $isValid = false;
        if (ApiAuthenticator::getSuperAdminUserIdFromKey($apiKey) == false || ApiAuthenticator::getSuperAdminUserTypeFromKey($apiKey) != 1){
            if ($apiKey == ApiAuthenticator::STANDARD_SUPER_ADMIN_API_KEY ){
                $isValid = true;
            }
            else {
                echo json_encode(['status'=> false, 'message'=> "invalid api key"], JSON_PRETTY_PRINT);
                exit;
            }
        }
        else {
            $isValid = true;
        }
        return $isValid;
    }

    public static function verifySuperAdminUserApiKey(){

        header('Content-Type: application/json');
        $apiKey = $_GET['key'];
        $isValid = false;
        if ((ApiAuthenticator::getSuperAdminUserIdFromKey($apiKey) == false || (ApiAuthenticator::getSuperAdminUserTypeFromKey($apiKey) != 4 && ApiAuthenticator::getSuperAdminUserTypeFromKey($apiKey) != 5)) && ApiAuthenticator::getSuperAdminUserTypeFromKey($apiKey) != 1){
            if ($apiKey == ApiAuthenticator::STANDARD_SUPER_ADMIN_USER_API_KEY ){
                $isValid = true;
            }
            else {
                echo json_encode(['status'=> false, 'message'=> "invalid api key"], JSON_PRETTY_PRINT);
                exit;
            }
        }
        else {
            $isValid = true;
        }
        return $isValid;
    }


    public static function verifySuperAdminAdminApiKey(){

        header('Content-Type: application/json');
        $apiKey = $_GET['key'];
        $isValid = false;
        if ((ApiAuthenticator::getSuperAdminUserIdFromKey($apiKey) == false || ApiAuthenticator::getSuperAdminUserTypeFromKey($apiKey) != 5) && ApiAuthenticator::getSuperAdminUserTypeFromKey($apiKey) != 1){
            if ($apiKey == ApiAuthenticator::STANDARD_SUPER_ADMIN_ADMIN_API_KEY ){
                $isValid = true;
            }
            else {
                echo json_encode(['status'=> false, 'message'=> "invalid api key"], JSON_PRETTY_PRINT);
                exit;
            }
        }
        else {
            $isValid = true;
        }
        return $isValid;
    }
}

