<?php
/**
 * Created by PhpStorm.
 * User: Judekayode
 * Date: 4/20/2016
 * Time: 7:54 AM
 */

namespace app\modules\api\common\models;


class ResponseCode {


    // file upload codes
    const FILE_UPLOAD_FAILED = "File could not be uploaded";
    const NO_FILE_UPLOADED = "No file uploaded";
    const FILE_TOO_LARGE = "File size too large";
    const FILE_UPLOAD_SUCCESSFUL = "File successfully uploaded";
    const UNKNOWN_ERROR = "Unknown error";
    const FILE_TYPE_NOT_ALLOWED = "This file type is not allowed";
    const FILE_EXISTS = "The file already exist";
    const DESTINATION_PERM_PROB = "Destination Directory Permission Problem";
    const FILE_DELETE_FAILED = "File not found! Could not delete";
    const FILE_SIZE_OK = "File size alright";
}
