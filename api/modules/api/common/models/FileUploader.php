<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 3/26/15
 * Time: 10:43 AM
 */

namespace app\modules\api\common\models;

use app\modules\api\common\models\ResponseCode;

class FileType {
    const JPEG = "image/jpeg";
    const JPG = "image/jpg";
    const GIF = "image/gif";
    const PNG = "image/png";
    const PDF = "pdf";
    const DOC = "doc";
}

class FileExtension {
    const JPEG = "jpeg";
    const JPG = "jpg";
    const GIF = "gif";
    const PNG = "png";
    const PDF = "pdf";
    const DOC = "doc";
}

class FileUploader {

    const MAX_FILE_SIZE = 2097152;
    public $upload_dir = "";
    public $mimeTypeWhiteList = array(FileType::JPEG, FileType::JPG, FileType::GIF, FileType::PNG, FileType::PDF, FileType::DOC);
    public $extensionWhiteList = array(FileExtension::JPEG, FileExtension::JPG, FileExtension::GIF, FileExtension::PNG, FileType::PDF, FileType::DOC);
    public $file_upload_url = null;

    public function __construct($dir){
        $this->upload_dir .= $dir.'/';
    }

    public function upload($name, $preferred_name = null,  $replace = true){

        if (isset($_FILES[$name])){
            $file_name = $_FILES[$name]['name'];
            $file_type = $_FILES[$name]['type'];
            $file_size = $_FILES[$name]['size'];
            $file_tmp_name = $_FILES[$name]['tmp_name'];
            $file_error = $_FILES[$name]['error'];

            // check the error code and ensure safety
            switch ($file_error){
                case UPLOAD_ERR_NO_FILE:
                    return ResponseCode::NO_FILE_UPLOADED;
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    return ResponseCode::FILE_TOO_LARGE;
                case UPLOAD_ERR_OK:
                    if (!$this->verifyFileType($file_type)){
                        return ResponseCode::FILE_TYPE_NOT_ALLOWED;
                    }
                    if ($this->verifyFileSize($file_size)){
                        $name_arr = explode('.', $file_name);
                        $extension = strtolower(end($name_arr));

                        $file_upload_url = $this->upload_dir . strtolower(str_replace(array(' ', "'", '"'), '_', $preferred_name)) .  '.' . $extension;


                        if ($this->checkFileExistence($file_upload_url)){
                            $this->setFileUploadUrl($file_upload_url);
                            if (!$replace)
                                return ResponseCode::FILE_EXISTS;
                        }
                        if (!move_uploaded_file($file_tmp_name,   $file_upload_url)){
                            return ResponseCode::FILE_UPLOAD_FAILED;
                        }
                        $this->setFileUploadUrl(basename($file_upload_url));
                    }
                    else{
                        return ResponseCode::FILE_TOO_LARGE;
                    }

                    return ResponseCode::FILE_UPLOAD_SUCCESSFUL;
                default:
                    return ResponseCode::UNKNOWN_ERROR;
            }

        }
        return ResponseCode::NO_FILE_UPLOADED;
    }

    public function verifyIsset($filename)
    {
        if(isset($_FILES[$filename]))
        {
            return true;
        }
        else{
            return false;
        }
    }

    public function verifyFileSize($size){
        if($size <= $this::MAX_FILE_SIZE)
        {
            return $size <= $this::MAX_FILE_SIZE;
        }
        else
        {
            return ResponseCode::FILE_TOO_LARGE;
        }
    }

    public function verifyFileSizeIndependent($name)
    {
        if (isset($_FILES[$name])) {
            $file_size = $_FILES[$name]['size'];
            if ($file_size <= $this::MAX_FILE_SIZE) {
                return ResponseCode::FILE_SIZE_OK;
            } else {
                return ResponseCode::FILE_TOO_LARGE;
            }
        }
        else{
            return ResponseCode::NO_FILE_UPLOADED;
        }

    }

    public function verifyFileType($file_type){
        if (empty($this->mimeTypeWhiteList) || in_array($file_type, $this->mimeTypeWhiteList)){
            return true;
        }
        return false;
    }

    public function verifyFileExtension($file_name){
        $extension = substr($file_name, strpos($file_name, '.') + 1);
        $extension = strtolower($extension);

        if ( in_array($extension, $this->extensionWhiteList)){

            return true;
        }
        return false;
    }


    public function checkFileExistence($file_name){
        return file_exists($file_name);
    }

    /**
     * @param array $mimeTypeWhiteList
     */
    public function setMimeTypeWhiteList($mimeTypeWhiteList)
    {
        $this->mimeTypeWhiteList = $mimeTypeWhiteList;
    }

    /**
     * @return array
     */
    public function getMimeTypeWhiteList()
    {
        return $this->mimeTypeWhiteList;
    }

    /**
     * @param array $extensionWhiteList
     */
    public function setExtensionWhiteList($extensionWhiteList)
    {
        $this->extensionWhiteList = $extensionWhiteList;
    }

    /**
     * @return array
     */
    public function getExtensionWhiteList()
    {
        return $this->extensionWhiteList;
    }

    /**
     * @param null $file_upload_url
     */
    public function setFileUploadUrl($file_upload_url)
    {
        $this->file_upload_url = $file_upload_url;
    }

    /**
     * @return null
     */
    public function getFileUploadUrl()
    {
        return $this->file_upload_url;
    }


} 