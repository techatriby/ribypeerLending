<?php

return [
    'site_url' => 'localhost/ribypeerlending',
    //'site_url' => 'http://staging.peerlending.riby.me',
    //'site_url' => 'http://peerlending.riby.me',
    'adminPhone' => '012914247',
    'adminEmail' => 'support@riby.me',
    'lendEmail' => 'lend@riby.me',
    'noreplyEmail' => 'no-reply@peerlending.riby.me',
    'superAdminPassword' => '^%TFVB+Ut^gyuinp8@_',
    'companyAdminPassword' => 'adminpassword',
    'userPassword' => 'password',
    'company_user_types' => [6,7],
    'company_admin_user_type' => 2,
    'superadmin_user_types' => [4,5],
    'main_superadmin_user_type' => 1,
    'alltypesdeclineremarks' => 0,
    'adminonlydeclineremarks' => 1,
    'companyonlydeclineremarks' => 2,
    'allpaymenttypes' => 0,
    'disbursementonly' => 1,
    'repaymentonly' => 2,
];
