<?php
/**
 * Created by PhpStorm.
 * User: Judekayode
 * Date: 4/16/16
 * Time: 4:17 PM
 */
return [

//    'agent/confirm/<transaction_id:\d+>' => 'agent/confirm',
    'api/v1/admin/confirmuser/<user_login_id:\d+>' => 'api/v1/admin/confirmuser',
    'api/v1/admin/approveborrowrequest/<id:\d+>' => 'api/v1/admin/approveborrowrequest',
    'api/v1/admin/approvelendrequest/<id:\d+>' => 'api/v1/admin/approvelendrequest',
    'api/v1/admin/rejectborrowrequest/<id:\d+>' => 'api/v1/admin/rejectborrowrequest',
    'api/v1/admin/deleteuser/<user_id:\d+>' => 'api/v1/admin/deleteuser',
    'api/v1/admin/fetchstaffdetails/<staff_id:\d+>' => 'api/v1/admin/fetchstaffdetails',
    'api/v1/admin/staffloanhistory/<staff_id:\d+>' => 'api/v1/admin/staffloanhistory',
    'api/v1/admin/stafflendhistory/<staff_id:\d+>' => 'api/v1/admin/stafflendhistory',
    'api/v1/admin/checkborrowrepaymentstatus/<transaction_id:\d+>' => 'api/v1/admin/checkborrowrepaymentstatus',
    'api/v1/admin/confirmloanrepayment/<borrow_id:\d+>' => 'api/v1/admin/confirmloanrepayment',


    //'api/v1/user/resetoldpassword/<activation_key:\w+>/<uniqid:\d+>' => 'api/v1/user/resetoldpassword',
    //'api/v1/user/resetoldpassword/<activation_key:\w+>/<uniqid:\w+>/<expire:\w+>' => 'api/v1/user/resetoldpassword',
    'api/v1/superadmin/getwalletbalance/<user_id:\d+>' => 'api/v1/superadmin/getwalletbalance',
    'api/v1/superadmin/verifybank/<account:\d+>/<bankcode:\d+>' => 'api/v1/superadmin/verifybank',

    'api/v1/superadmin/getusercardid/<user_id:\d+>' => 'api/v1/superadmin/getusercardid',
    'api/v1/superadmin/approverequest/<borrow_id:\d+>' => 'api/v1/superadmin/approverequest',
    'api/v1/superadmin/rejectrequest/<borrow_id:\d+>' => 'api/v1/superadmin/rejectrequest',
    'api/v1/superadmin/confirmborrowreject/<borrow_id:\d+>' => 'api/v1/superadmin/confirmborrowreject',
    'api/v1/superadmin/confirmborrowdisburse/<borrow_id:\d+>' => 'api/v1/superadmin/confirmborrowdisburse',
    'api/v1/superadmin/checkborrowrepaymentstatus/<transaction_id:\d+>' => 'api/v1/superadmin/checkborrowrepaymentstatus',
    'api/v1/superadmin/confirmloanrepayment/<borrow_id:\d+>' => 'api/v1/superadmin/confirmloanrepayment',
    'api/v1/superadmin/getstafftransactions/<user_id:\d+>' => 'api/v1/superadmin/getstafftransactions',
    'api/v1/superadmin/getlatestrequestdetails/<borrow_id:\d+>' => 'api/v1/superadmin/getlatestrequestdetails',
   
    'api/v1/superadmin/getlatestlenddetails/<lend_id:\d+>' => 'api/v1/superadmin/getlatestlenddetails',
    'api/v1/superadmin/getcompaniesstaffdetails/<user_id:\d+>' => 'api/v1/superadmin/getcompaniesstaffdetails',
    'api/v1/superadmin/getcompanystaffs/<company_id:\d+>' => 'api/v1/superadmin/getcompanystaffs',
    'api/v1/superadmin/verifybvn/<bvn:\d+>' => 'api/v1/superadmin/verifybvn',
    'api/v1/superadmin/getpaymentoptiontype/<payment_option_id:\d+>' => 'api/v1/superadmin/getpaymentoptiontype',
    'api/v1/superadmin/getuserbankfromid/<user_id:\d+>' => 'api/v1/superadmin/getuserbankfromid',
    'api/v1/superadmin/getcompanyadminrepaidloandetails/<transaction_id:\d+>' => 'api/v1/superadmin/getcompanyadminrepaidloandetails',
    'api/v1/superadmin/approvehrrepayrequest/<repayment_schedule_id:\d+>/<month:\d+>' => 'api/v1/superadmin/approvehrrepayrequest',
    'api/v1/superadmin/declinehrrepayrequest/<transaction_id:\d+>/<month:\d+>' => 'api/v1/superadmin/declinehrrepayrequest',


    'api/v1/user/getusercardid/<user_id:\d+>' => 'api/v1/user/getusercardid',
    'api/v1/user/getwalletbalance/<user_id:\d+>' => 'api/v1/user/getwalletbalance',
    'api/v1/user/verifybank/<account:\d+>/<bankcode:\d+>' => 'api/v1/user/verifybank',
    'api/v1/user/verifybvn/<bvn:\d+>' => 'api/v1/user/verifybvn',
    'api/v1/user/getpaymentoptiontype/<payment_option_id:\d+>' => 'api/v1/user/getpaymentoptiontype',


];
