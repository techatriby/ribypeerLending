<?php
/**
 * Created by PhpStorm.
 * User: Kayode
 * Date: 7/23/2016
 * Time: 7:14 PM
 */

namespace app\modules\api\modules\v1\controllers;

use app\modules\api\modules\v1\models\ApiKeyHelper;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\UserDetails;
use yii\web\Controller;
use Yii;

class AccountController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }



    public function actionLogin()
    {
        $this->layout = "";
        if(UtilsController::checkIsLogin())
        {
            //redirect to user dashboard based on user type id
        }
    }

    public function actionLogout()
    {
        Yii::$app->session->removeAll();
        Yii::$app->session->destroy();
    }





}