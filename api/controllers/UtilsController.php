<?php
/**
 * Created by PhpStorm.
 * User: Kayode
 * Date: 7/23/2016
 * Time: 7:19 PM
 */

namespace app\controllers;


use yii\rest\Controller;
use Yii;

class UtilsController extends Controller
{

    //check if user is already logged in
    public static function checkIsLogin()
    {
        if(Yii::$app->session->hasSessionId)
        {
            return true;
        }
        else{
            return false;
        }
    }

    //get the id of the current user
    public static function getUserSessionId()
    {
        $usersession_id = Yii::$app->session->get('user_id');
        return $usersession_id;
    }

    //get the user type id of logged in user
    public static function getUserTypeofUser($usersessionid)
    {
        //get usertypeid of user from db
    }

    public static function redirectUserbasedOnUserType($user_id)
    {
        if($user_id == 1)
        {
            //redirect to superadmin dashboard

        }
        else if($user_id == 2)
        {
            //redirect to company admin dashboard

        }

        else if($user_id == 3)
        {
            //redirect to user dashboard
        }
    }


    //set error message
    public static function setErrorMessage($error_type, $error_msg)
    {
        if($error_type == 0)
        {
            Yii::$app->session->setFlash('error', $error_msg);
        }
        else if($error_msg == 1)
        {
            Yii::$app->session->setFlash('success', $error_msg);
        }
    }


    //simple template to send email
    public static function sendEmail($mail_template_name, $reply_to = null, $subject, $email_to, $email_from, $username)
    {
        $check =  Yii::$app->mailer->compose('verification', ['username'=>$username])

            ->setFrom($email_from)
            ->setTo($email_to)
            ->setReplyTo($reply_to)
            ->setSubject($subject)
            ->send();
        return $check;

    }

    //show error messages on the page
    public static function showErrorMessage($error_type)
    {
        if (Yii::$app->session->getFlash('success')){
            echo
                "<div class='alert alert-success alert-collapsible'>".
                Yii::$app->session->getFlash('success').
                "</div>";

         }
        else if(Yii::$app->session->getFlash('error')){
            echo
                "<div class='alert alert-error alert-collapsible'>".
                Yii::$app->session->getFlash('error').
                "</div>";
        }

    }

    //check if email is valid
    public static function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    //check the password lenght
    public static function isPasswordLenCorrect($password, $length)
    {
        if (strlen($password) < $length) {
            return false;
        }
        return true;

    }

    //check if the date of birth is reasonable
    public static function isReasonableDOB($day, $month, $year)
    {
        return (intval($year) <= date('Y'));
    }


    //generate hash of password
    public static function generatePasswordHash($password)
    {
        return sha1($password);
    }

    //verify if the phone number is reasonable
    public static function isPhoneNumber($phone_number)
    {
        $pattern = '(\d{11})/';
        preg_match($pattern, $phone_number, $phoneNumber);
        if(empty($phoneNumber) || count($phoneNumber) <= 0){
            return false;
        }
        else{
            return true;
        }
    }

    //get age of user from date of birth
    public static function getAgeFromDOB($date_of_birth)
    {
        $today = date_create();
        $dob = new \DateTime($date_of_birth);
        return round(date_diff($today, $dob)->days / 365);

    }

    //needed for forgot password
    public static function generatepass_reset_key()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $res = "";
        $between_value = rand(60, 80);
        for ($i = 0; $i < $between_value; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $res;
    }


    //encrypt id for forgot password
    public static function encryptUserId($user_id)
    {
        return base64_encode($user_id);
    }

    //decrypt id for forgot password
    public static function decryptUserId($encrypted_id)
    {
        return base64_decode($encrypted_id);
    }

    //get the current datetime
    public static function getCurrentdatetime()
    {
        return date("Y-m-d h:m:s");
    }

    //get time ago for notifications
    public function timeagoString($giventime)
    {
        $timediff = strtotime(date("Y-m-d h:i:s")) - $giventime;

        if ($timediff < 1)
        {
            return '0 seconds';
        }
        else {

            $period = [
                365 * 24 * 60 * 60 => 'year',
                30 * 24 * 60 * 60 => 'month',
                24 * 60 * 60 => 'day',
                60 * 60 => 'hour',
                60 => 'minute',
                1 => 'second'
            ];
            $periods = array(
                'year' => 'years',
                'month' => 'months',
                'day' => 'days',
                'hour' => 'hours',
                'minute' => 'minutes',
                'second' => 'seconds'
            );

            foreach ($period as $secs => $str) {
                if($secs > $timediff){
                    continue;
                }

                $d = $timediff / $secs;


                if ($d >= 1) {
                    $r = round($d);
                    return $r . ' ' . ($r > 1 ? $periods[$str] : $str) . ' ago';
                } else {
                    return '0 seconds';
                }
            }
        }
    }












}