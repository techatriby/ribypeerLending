<?php

namespace app\modules\api\modules\v1\controllers;


use app\modules\api\modules\v1\models\Borrow;
use app\modules\api\modules\v1\models\Company;
use app\modules\api\modules\v1\models\CompanyApiKeyHelper;
use app\modules\api\modules\v1\models\EmailSenderApiKeyHelper;
use app\modules\api\modules\v1\models\Lend;
use app\modules\api\modules\v1\models\AdminLog;
use app\modules\api\modules\v1\models\UserBankDetails;
use app\modules\api\modules\v1\models\UserDetails;
use app\modules\api\modules\v1\models\UserDetailsApiKeyHelper;
use app\modules\api\modules\v1\models\UserLogin;
use app\modules\api\modules\v1\models\UserLoginApiKeyHelper;
use app\modules\api\modules\v1\models\UserType;
use Yii;
use yii\base\Exception;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\modules\api\common\models\ApiAuthenticator;
use app\modules\api\common\models\ApiUtility;
use app\modules\api\modules\v1\models\ApiKeyHelper;




class AdminController extends BaseController
{

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                                'login'    =>          ['POST'],
                                'deletestaff' => ['DELETE'],

                            ],
                        ],
        ]);
    }

    public function actionIndex()
    {

        return json_encode(['kayode']);


    }


    public function actionGetclientlevels(){
        $user_types = Yii::$app->params['company_user_types'];
        $admin_type = [Yii::$app->params['company_admin_user_type']];

        $allowed_types = array_merge_recursive($admin_type,$user_types);

        //Add General User
        $allowed_types[] = 3;

        $levels = (new \yii\db\Query())
            ->select(['*'])
            ->from('user_type')
            ->where(['user_type_id'=> $allowed_types])
            ->all();
        return $levels;
    }


    public function actionCreatestaffs(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminAdminApiKey();
        
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsPairValue = [
            UserLoginApiKeyHelper::USERS_LIST => ApiUtility::TYPE_ARR,
        ];
        $this->paramCheck($paramsPairValue, $params);
        
        $new_staffs = $params[UserLoginApiKeyHelper::USERS_LIST];
        
        //get company_id of the current admin
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);
        $admin = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();
        $comp_id = $admin['company_id'];
      

        $newusers = 0;
        $errors = [];

        if($comp_id != null){

            foreach ($new_staffs as $staff) {
            
                $staff['company_id'] = $comp_id;
                $created = $this->adminsignupusers($staff);
                if($created['status'])

                    $newusers++;

                else{
                    $errors[$staff[UserLoginApiKeyHelper::USER_EMAIL]] = $created['message'];
                }
            }

            $message = ($newusers>0)?'Accounts were created successfully':'No account was created';

          

            if(count($errors)>0){
                $error_msg = ' but with some errors';
                $message = $newusers>0?$message.$error_msg:$message;
                return [
                    'status'=>false,
                    'message'=>$message,
                    'data' => $errors,
                ];
            }
            else{
                return [
                    'status'=>true,
                    'message'=>$message,
                    'data' => null,
                ];
            }
        }
        else{
            $error_msg = 'Company not available';
            return ApiUtility::errorResponse($error_msg);
        }


        
    }

    public function adminsignupusers($user){

        //Only allow creation of allowed roles
        $user_type =  $user['user_type'];
        $user_type = intval($user_type) > 0 ?
            UserType::find()->where(['user_type_id' => $user_type])->asArray()->one() :
            UserType::find()->where(['user_type_name' => $user_type])->asArray()->one();
        $user_type_id = is_array($user_type)?$user_type['user_type_id']:false;

        if(!in_array($user_type_id, $this->getadminadminusertypes()) && $user_type_id != 3){
            $error_msg = 'You can not create a user of type '.$user_type['user_type_name'];
            return ApiUtility::errorResponse($error_msg);
        }

        $user['user_password'] = $user_type_id == 3 ?$this->getAppParams('userPassword'):$this->getAppParams('companyAdminPassword');
        
        date_default_timezone_set('Africa/Lagos');
        $time = date('Y-m-d H:i:s');
        $passwordLength = 7;

        $firstname = $user[UserDetailsApiKeyHelper::FIRSTNAME];
        $lastname = $user[UserDetailsApiKeyHelper::LASTNAME];
        $email = $user[UserLoginApiKeyHelper::USER_EMAIL];
        $phone_number = $user[UserDetailsApiKeyHelper::PHONE_NUMBER];
        $password = $user[UserLoginApiKeyHelper::USER_PASSWORD];
        $company = $user[UserDetailsApiKeyHelper::COMPANY_ID];
        $expiration_date = date('Y-m-d H:i:s', strtotime("+7 day", strtotime($time)));
        $companyname = Company::findOne(['company_id'=>$company]);

        $get_access_token = ApiUtility::generateAccessToken();

        $fetch_mail = UserDetails::find()->where(['email'=>$email])->asArray()->one();
        $fetch_names = UserLogin::find()->where(['user_email' => $email, 'status_id' => 2])->asArray()->one();

        if (!empty($password) && !empty($email)) {

            if(ApiUtility::isValidEmail($email))
            {
                if($fetch_mail == null && $fetch_names == null)
                {
                    if (strlen($password) >= $passwordLength) {


                        $connection = Yii::$app->db;
                        $transaction = $connection->beginTransaction();
                        $error_msg = '';

                        try {
                            $values = [];
                            $userlogins = new UserLogin();

                            $userlogins->user_email = $email;
                            $userlogins->user_password = ApiUtility::generatePasswordHash($password);
                            $userlogins->access_token = $get_access_token;
                            $userlogins->user_type_id = $user_type_id;
                            $userlogins->created_date = $time;
                            $userlogins->modified_date = $time;
                            //$userlogins->expiration_date = $expiration_date;

                           
                            if ($userlogins->save()) {

                                $userdetails = new UserDetails();

                                $userdetails->email = $email;
                                $userdetails->company_id = $company;
                                $userdetails->user_type_id = $user_type_id;
                                $userdetails->firstname = $firstname;
                                $userdetails->lastname = $lastname;
                                $userdetails->phone_number = $phone_number;
                                $userdetails->user_login_id = $userlogins->user_login_id;
                                $userdetails->created_date = $time;
                                $userdetails->modified_date = $time;


                                if ($userdetails->save()) {
                                    $transaction->commit();

                                    //send response email

                                    $this->setHeader(200);
                                    $message = "You have been registered on our platform by your company Admin.<br><br>
                                                     Your Login credentials:<br/>
                                                     Username: <var>Email address on which this email was received</var><br>
                                                     Password: $password<br><br>
                                                     Kindly Login to change your password and have access to your page<br>
                                                    Cheers,<br/><br/>
                                                     ";
                            

                                    $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account created', $userdetails->email, $this->getReplyEmail(),$email,$companyname['company_name']);

                                   
                                     
                                

                                    return [
                                        'status' => true,
                                        'message' => 'Registration Successful, An Email Has Been Sent To You.',
                                        'data' => null,
                                    ];

                                }
                            }
                            else{
                                $transaction->rollBack();
                                $error_msg = 'Account could not be created. Please Try again';
                            }

                        } catch (Exception $ex) {
                            $transaction->rollBack();
                            $error_msg = 'Account could not be created. Please Try again';
                        }
                    } else {
                        $error_msg = 'Password should not be less than 7 characters';
                    }
                }
                else
                {
                    $error_msg = 'User already exists';
                }
            }
            else
            {
                $error_msg = 'Invalid email address';
            }
        } else {
            $error_msg = 'Fill in the required fields';
        }

        return ApiUtility::errorResponse($error_msg);
    }

    





    public function actionGetunconfirmedusers()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $getallunconfirmedusers = UserLogin::findBySql("select usl.user_login_id, usd.user_id,usl.user_email,cm.company_name,usd.phone_number
                                    from user_login usl inner join user_details usd ON usl.user_login_id = usd.user_login_id
                                     INNER JOIN company cm ON usd.company_id = cm.company_id WHERE
                                     usl.status_id = 3")->asArray()->all();

        return[
            'status'=> true,
            'message' => 'Unconfirmed users fetched successfully',
            'data' => $getallunconfirmedusers
        ];

    }

    public function actionConfirmuser()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $user_login_id = Yii::$app->request->getQueryParam('user_login_id');

        $checkifuseralreadyactivated = UserLogin::findOne(['user_login_id'=>$user_login_id, 'status_id'=>2]);
        if($checkifuseralreadyactivated)
        {
            $errormsg = "User account already activated";
            return ApiUtility::errorResponse($errormsg);
        }


        $checkuserid = UserLogin::findOne(['user_login_id'=>$user_login_id, 'status_id'=>3]);
        if($checkuserid)
        {

            $checkuserid->status_id = 2;
            if($checkuserid->update()){
                //send email to user that account has been activated;
                $message = "Thanks for registering on our platform. We will like to inform you that your account has been activated. Welcome on board <br/>
                                                Thank you.<br/>";

                                


                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $checkuserid->user_email, $this->getReplyEmail(), $checkuserid->user_email);


                return [
                    'status'=> true,
                    'message'=> 'User account activated successfully',
                    'data'=>null
                ];
            }
            else{
                $errormsg = "user account could not be activated";
                return ApiUtility::errorResponse($errormsg);
            }
        }


    }

    public function actionMailsender(){
        return ['message'=>'Message not sent'];
        ApiAuthenticator::verifyAdminApiKey();
        $params = $_REQUEST;
        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $paramsPairValue = [
            EmailSenderApiKeyHelper::SENDER_EMAIL => ApiUtility::TYPE_STRING,
            EmailSenderApiKeyHelper::RECEIVER_EMAIL => ApiUtility::TYPE_STRING,
            EmailSenderApiKeyHelper::SUBJECT => ApiUtility::TYPE_STRING,
            EmailSenderApiKeyHelper::MESSAGE => ApiUtility::TYPE_STRING,
        ];

        $this->paramCheck($paramsPairValue, $params);

        $get_access_token = $this->checkApiKey();
        $sender_email = $params[EmailSenderApiKeyHelper::SENDER_EMAIL];
        $receiver_email = $params[EmailSenderApiKeyHelper::RECEIVER_EMAIL];
        $message = $params[EmailSenderApiKeyHelper::MESSAGE];
        $subject = $params[EmailSenderApiKeyHelper::SUBJECT];

        if($this->sendEmail('mailtemplate',$sender_email,$message,$subject, $receiver_email, $sender_email, $receiver_email)){
            return[
                'status' => true,
                'message' => 'Mail Sent',
            ];
        }else{

            return[
                'status' => false,
                'message' => 'Oops Failed!, Please try Again',
            ];
        }
    }

    public function actionFetchborrowerrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
                        ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getborrowerrequest =
                Borrow::findBySql(
                    "
                        select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
                        us.company_id, cm.company_name, br.loan_amount,br.date_of_request,br.date_to_pay,br.borrow_id
                        from borrow br INNER JOIN company cm ON cm.company_id = br.company_id
                        inner join user_details us ON us.user_id = br.user_id
                        WHERE us.company_id = $comp_id AND br.approve_flag = 0
                        AND br.status_id = 2
                    "
                )->asArray()->all();


        return [
            'status' => true,
            'message' => 'List of company borrowers fetched',
            'data' => $getborrowerrequest

        ];

    }


    public function actionFetchborrowerqueue()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getborrowerrequest =
            Borrow::findBySql(
                "
                        select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
                        us.company_id, cm.company_name, br.loan_amount,br.date_of_request,br.date_to_pay, br.borrow_id
                        from borrow br INNER JOIN company cm ON cm.company_id = br.company_id
                        inner join user_details us ON us.user_id = br.user_id
                        WHERE us.company_id = $comp_id AND br.approve_flag = 1
                        AND br.status_id = 2 order by br.date_of_request desc
                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'Company borrowers queue fetched',
            'data' => $getborrowerrequest

        ];

    }


    public function actionFetchstaffincompany()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getstaffincompany =
            UserLogin::findBySql(
                "
                        select usl.user_email,us.firstname, us.lastname,us.email,
                        us.company_id, cm.company_name,us.user_login_id,us.phone_number
                        from user_login usl INNER JOIN user_details us ON us.user_login_id = usl.user_login_id
                        INNER JOIN company cm ON cm.company_id = us.company_id
                        WHERE us.company_id = $comp_id AND usl.status_id = 2

                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'List of company staffs fetched successfully',
            'data' => $getstaffincompany
        ];

    }


    public function actionFetchstaffdetails()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $user_login_id = Yii::$app->request->getQueryParam('staff_id');
        //get company_id of the current admin

        $staffdetails =          UserLogin::findBySql(
                "
                        select usl.user_email,us.firstname, us.lastname,us.email,
                        us.company_id, cm.company_name,us.user_login_id,us.phone_number,
                        us.gender, us.address, us.birthdate,us.middlename
                        from user_login usl INNER JOIN user_details us ON us.user_login_id = usl.user_login_id
                        INNER JOIN company cm ON cm.company_id = us.company_id
                        WHERE us.user_login_id = $user_login_id AND usl.status_id = 2

                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'Staffs details fetched successfully',
            'data' => $staffdetails
        ];

    }

    public function actionFetchlenderrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        //get company_id of the current admin

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin_id = ApiAuthenticator::getAdminUserIdFromKey($apikey);

        $admin_company_id = UserDetails::find()->select(['company_id'])
            ->where(['user_login_id'=> $admin_id])->asArray()->one();

        $comp_id = $admin_company_id['company_id'];

        $getlendrequest =
            Lend::findBySql(
                "
                        select br.user_id,us.firstname, us.lastname, us.middlename, us.email,
                        us.company_id, cm.company_name, br.date_lend,
                        br.amount_to_receive, br.lend_amount, br.lend_id
                        from lend br INNER JOIN company cm ON cm.company_id = br.company_id
                        inner join user_details us ON us.user_id = br.user_id
                        WHERE us.company_id = $comp_id AND br.approve_flag = 1
                        AND br.status_id = 2
                    "
            )->asArray()->all();


        return [
            'status' => true,
            'message' => 'list of borrowers fetched from company admin',
            'data' => $getlendrequest

        ];

    }

    public function actionGetadminlevels()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $apikey = Yii::$app->request->getQueryParam('key');
        $user_id = Yii::$app->request->getQueryParam('user_id');
       
     $rows = (new \yii\db\Query())
    ->select(['*'])
    ->from('user_type')
    ->limit(10)
    ->all();

   return [
            'status' => true,
            'message' => 'Query successfully',
            'data' => $rows
        ];


    }
 

     public function actionDeleteuser(){

        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $apikey = Yii::$app->request->getQueryParam('key');
        $user_id = Yii::$app->request->getQueryParam('user_id');
        $update = UserDetails::updateAll(['status_id' => 1], ['user_id' => $user_id]);
        
          return [
            'status' => true,
            'message' => 'User deleted successfully',
            'data' => null
        ];

     }

    public function actionApproveborrowrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('id');

        $checkborrower = Borrow::find()->select(['borrow_id','loan_amount'])->where(['borrow_id' => $borrower_id, 'approve_flag' => 0, 'status_id' => 2])->asArray()->one();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserDetails::find()->innerJoin('user_login','user_details.user_login_id=user_login.user_login_id')->where(['access_token'=>$apikey])->asArray()->one();
        $company = Company::findOne(['company_id'=>$admin['company_id']]);
        $time = date('Y-m-d H:i:s');

        //get user email
        $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
        $email = $getemail['email'];

        if ($checkborrower['borrow_id'] != null and count($checkborrower) > 0) {
            $update = Borrow::updateAll(['approve_flag' => 1,'date_updated'=>$time], ['borrow_id' => $checkborrower['borrow_id']]);

            if ($update) {
                //approve email to user ;
                $message = "Your loan request of ₦ {$checkborrower['loan_amount']} is currently being processed.<br>
                            You will receive an email notification once the process is completed. <br/>
                            <br/><br>
                            ";
                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan processing', $email, $this->getReplyEmail(), $email, $company['company_name']);


                //approve email to superAdmin ;
                $message = "A new loan request has been sent for approval.<br><br>
                            Company: $company->company_name<br>
                            Amount: {$checkborrower['loan_amount']}<br>
                            Date Aproved: $time<br><br>
                            Kindly login to approve request<br/>
                            ";
                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan awaiting Approval', $this->getAdminEmail(), $this->getReplyEmail(), $this->getAdminEmail(),$company->company_name);

                



                return
                    [
                        'status' => true,
                        'message' => 'user approved to borrow',
                        'data' => null
                    ];
            } else {
                $msg = "sorry we couldnt approve user borrow request, please try again later thanks";
                return ApiUtility::errorResponse($msg);
            }

        }
        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }






    /// Aprrrove Lend request...            //////
    
    public function actionApprovelendrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('id');

        $checkborrower = Lend::find()->select(['lend_id'])->where(['lend_id' => $borrower_id, 'approve_flag' => 0, 'status_id' => 2])->asArray()->one();
        $company = Company::find()->innerJoin('user_details us','us.company_id =company.company_id')->innerJoin('lend le','le.user_id=us.user_id')->where(['lend.lend_id'=> $borrower_id])->asArray()->one();

        //get user email
        $getemail = Lend::find()->select('us.email')->innerJoin('user_details us','us.user_id = lend.user_id')->where(['lend.lend_id'=> $borrower_id])->asArray()->one();
        $email = $getemail['email'];
        if ($checkborrower['lend_id'] != null and count($checkborrower) > 0) {
            $update = Lend::updateAll(['approve_flag' => 1], ['lend_id' => $checkborrower['lend_id']]);

            if ($update) {

                $message = "We wish you to inform you that your application to Lend money has been accepted. <br/>You will soon be contacted on the details of how to go about disbursing <br/>
                the money and you will also get an invoice containing the details of when and how much you are going<br/>
                 to get back. Just stay in touch. <br/>
                         ";
            

                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $email, $this->getReplyEmail(), $email,$company['company_name']);


                return
                    [
                        'status' => true,
                        'message' => 'user approved to borrow',
                        'data' => null
                    ];
            } else {
                $msg = "sorry we could not approve user borrow request, please try again later thanks";
                return ApiUtility::errorResponse($msg);
            }

        }
        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }


    public function actionRejectborrowrequest()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('id');

        $checkborrower = Borrow::find()->select(['borrow_id','loan_amount'])->where(['borrow_id'=> $borrower_id, 'approve_flag'=>0, 'status_id'=>2])->asArray()->one();

        //get user email
        $getemail = Borrow::find()->select('us.email')->innerJoin('user_details us','us.user_id = borrow.user_id')->where(['borrow.borrow_id'=> $borrower_id])->asArray()->one();
        $email = $getemail['email'];
        $company = Company::find()->innerJoin('user_details us','us.company_id =company.company_id')->innerJoin('borrow bo','bo.user_id=us.user_id')->where(['bo.borrow_id'=> $borrower_id])->asArray()->one();
        if($checkborrower['borrow_id'] != null and count($checkborrower) > 0)
        {
          $update =   Borrow::updateAll(['status_id'=> 1], ['borrow_id'=> $checkborrower['borrow_id']]);

            if($update) {

                //Notification email to user ;
                $message = "Unfortunately, Your loan request of ₦ {$checkborrower['loan_amount']} has been Declined.<br>
                             If you have any issues or inquires, send an email to <a href='support@riby.me'>support@riby.me</a>
                            <br/><br>
                             ";
             

                $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Loan rejected', $email, $this->getReplyEmail(), $email, $company['company_name']);

                return
                    [
                        'status' => true,
                        'message' => 'user not allowed to borrow',
                        'data' => null
                    ];
            }
            else{
                $msg = "sorry, we could not perform the reject borrow request please try again";
                return ApiUtility::errorResponse($msg);
            }
        }



        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }


    }

    public function actionGetallborrower(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $borrowers = Borrow::find()->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if (!is_array($borrowers)) {
            $message = "Error retrieving borrowers";
            return ApiUtility::errorResponse($message);
        }elseif(empty($borrowers)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Borrowers Available',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Lenders Fetched Successfully',
                'data' => $borrowers
            ];
        }
    }

    public function actionGetalllender(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $lenders = Lend::find()->where(['company_id'=>$admindetails->company_id, 'status_id'=>2])->asArray()->all();

        if (!is_array($lenders)) {
            $message = "Error retrieving lenders";
            return ApiUtility::errorResponse($message);
        }elseif(empty($lenders)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Lenders Available',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Lenders Fetched Successfully',
                'data' => $lenders
            ];
        }
    }

    public function actionTotalstaff(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $users = UserDetails::find()->where(['user_type_id'=>3,'company_id'=>$admindetails->company_id, 'status_id'=>2])->asArray()->all();

        $numberofuser = count($users);

        if (!is_array($users)) {
            $message = "Error retrieving staff";
            return ApiUtility::errorResponse($message);
        }elseif(empty($users)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staffs Not Available',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staff Fetched Successfully',
                'data' => $numberofuser
            ];
        }
    }

    public function actionTotalamountlent(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $lenderamount = Lend::find()->select('lend_amount')->where(['company_id'=>$admindetails->company_id])->asArray()->all();
        $amounts = 0;
        foreach($lenderamount as $amount){
            $amounts += $amount["lend_amount"];
        }

        if (!is_array($lenderamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($lenderamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Lender at the moment',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Lent Fetched Successfully',
                'data' => $amounts
            ];
        }

    }

    public function actionTotalamountborrowed(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $loanamount = Borrow::find()->select('loan_amount')->where(['company_id'=>$admindetails->company_id, 'approve_flag'=>2])->asArray()->all();
        $amounts = 0;
        foreach($loanamount as $amount){
            $amounts += $amount["loan_amount"];
        }

        if (!is_array($loanamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($loanamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Borrower at the moment',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Borrowed Fetched Successfully',
                'data' => $amounts
            ];
        }

    }


    // Tottal amount Declined

     public function actionTotalamountdeclined(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $loanamount = Borrow::find()->select('loan_amount')->where(['company_id'=>$admindetails->company_id, 'status_id'=>1])->asArray()->all();
        $amounts = 0;
        foreach($loanamount as $amount){
            $amounts += $amount["loan_amount"];
        }

        if (!is_array($loanamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($loanamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'no amount Declined',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Declined Successfully',
                'data' => $amounts
            ];
        }

    }


    public function actionTotalamountrequested(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);

        $loanamount = Borrow::find()->select('loan_amount')->where(['company_id'=>$admindetails->company_id])->asArray()->all();
        $amounts = 0;
        foreach($loanamount as $amount){
            $amounts += $amount["loan_amount"];
        }

        if (!is_array($loanamount)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($loanamount)&& $amounts<=0){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No Request at the moment',
                'data' => 0
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Total Amount Requested Fetched Successfully',
                'data' => $amounts
            ];
        }
    }

    public function actionUserloanpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Borrow::findBySql("
            select CONCAT(YEAR(date_of_request),'/', MONTH(date_of_request)) as month, COUNT(*) as number_of_user FROM borrow GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Number of Loan Applicants Per Month',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Number of Loan Applicants Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionUserlendpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Lend::findBySql("
            select CONCAT(YEAR(date_lend),'/', MONTH(date_lend)) as month, COUNT(*) as number_of_user FROM lend GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Number of Lenders Per Month',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Number of Lenders Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionLendpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Lend::findBySql("
            select CONCAT(YEAR(date_lend),'/', MONTH(date_lend)) as month, SUM(lend_amount) as totalamount FROM lend GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Lent Per Mont',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Lent Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionLoanpermonth(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $loanerbymonth = Borrow::findBySql("
            select CONCAT(YEAR(date_of_request),'/', MONTH(date_of_request)) as month, SUM(loan_amount) as totalamount FROM borrow GROUP BY month
        ")->where(['company_id'=>$admindetails->company_id, 'approve_flag'=>2])->asArray()->all();

        if($loanerbymonth != null){
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Borrowed Per Month',
                    'data' => $loanerbymonth
                ];
        }else{
            return
                [
                    'status' => true,
                    'message' => 'Total Amount Borrowed Per Month',
                    'data' => $loanerbymonth
                ];
        }


    }

    public function actionStaffloanhistory(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $user_login_id = Yii::$app->request->getQueryParam('staff_id');
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $staff = UserDetails::findOne(['user_login_id'=>$user_login_id]);
        $history = Borrow::find()->select(['user_details.firstname','user_details.lastname','user_details.middlename','user_details.phone_number','user_details.email',
            'borrow.borrow_id', 'borrow.loan_amount', 'borrow.period', 'borrow.status_id', 'borrow.amount_to_pay', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
            'borrow.company_id', 'company.company_name'])->innerJoin('user_details','user_details.user_id=borrow.user_id')->innerJoin('company', 'company.company_id=borrow.company_id')->where(['borrow.user_id'=>$staff->user_id, 'borrow.company_id'=>$admindetails->company_id])->asArray()->all();

        if (!is_array($history)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($history)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No History',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staff Loan History Fetched Successfully',
                'data' => $history
            ];
        }
    }

    public function actionStafflendhistory(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();

        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $user_login_id = Yii::$app->request->getQueryParam('staff_id');
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $staff = UserDetails::findOne(['user_login_id'=>$user_login_id]);
        $history = Lend::find()->select(['lend.lend_id', 'lend.lend_amount', 'lend.date_lend', 'lend.amount_to_receive', 'lend.status_id', 'lend.approve_flag',
            'lend.company_id', 'lend.status_id', 'company.company_name'])->innerJoin('company', 'company.company_id=lend.company_id')->where(['lend.user_id'=>$staff->user_id, 'lend.company_id'=>$admindetails->company_id])->asArray()->all();

        if (!is_array($history)) {
            $message = "An Error Occured";
            return ApiUtility::errorResponse($message);
        }elseif(empty($history)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No History',
            ];
        } else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Staff Lend History Fetched Successfully',
                'data' => $history
            ];
        }
    }

    //get all loan transaction history for each company

    public function actionLoanhistory()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();


        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);


        $companyloantransactionhistory = Borrow::find()->select(['user_details.firstname','user_details.lastname','user_details.middlename','user_details.phone_number','user_details.email',
            'borrow.borrow_id', 'borrow.loan_amount', 'borrow.period', 'borrow.status_id', 'borrow.amount_to_pay', 'borrow.date_of_request', 'borrow.date_to_pay', 'borrow.status_id', 'borrow.approve_flag',
            'borrow.company_id', 'company.company_name'])->innerJoin('user_details','user_details.user_id=borrow.user_id')->innerJoin('company', 'company.company_id=borrow.company_id')
            ->where(['borrow.company_id'=>$admindetails->company_id, 'user_details.status_id'=>2])->asArray()->all();

//        $lenders = Lend::find()->where(['company_id'=>$admindetails->company_id, 'status_id'=>2])->asArray()->all();

        if(!is_array($companyloantransactionhistory)){
            $message = "Error retrieving borrow transaction for company";
            return ApiUtility::errorResponse($message);
        }
        elseif(empty($companyloantransactionhistory)){
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No loan transaction for this company',
                'data' => null
            ];
        }
        else {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Loan transaction fetched for company',
                'data' => $companyloantransactionhistory
            ];
        }

    }

    //get all lend history for each company
    public function actionLendhistory()
    {
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminUserApiKey();
        $apikey = Yii::$app->request->getQueryParam('key');
        $admin = UserLogin::findOne(['access_token'=>$apikey]);
        $admindetails = UserDetails::findOne(['user_login_id'=>$admin->user_login_id]);
        $companylendtransactionhistory = Lend::find()->select(['lend.lend_id', 'lend.lend_amount', 'lend.date_lend', 'lend.amount_to_receive', 'lend.status_id', 'lend.approve_flag',
            'lend.company_id', 'lend.status_id', 'company.company_name'])
            ->innerJoin('user_details','user_details.user_id=lend.user_id')->innerJoin('company', 'company.company_id=lend.company_id')
            ->where(['lend.company_id'=>$admindetails->company_id, 'user_details.status_id'=>2])->asArray()->all();
        if(!is_array($companylendtransactionhistory))
        {
            $message = "Error retrieving lend transaction for company";
            return ApiUtility::errorResponse($message);
        }
        elseif(empty($companylendtransactionhistory))
        {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'No lend transaction for this company',
                'data' => null
            ];
        }
        else
        {
            $this->setHeader(200);
            return [
                'status' => true,
                'message' => 'Lend transaction fetched for company',
                'data' => $companylendtransactionhistory
            ];
        }

    }

/*
    public function actionRegistermultipleuser(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();
        $params = $_REQUEST;

        $passwordLength = 7;

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $userinfos = $params['data'];
        //$userinfos = Yii::$app->request->getQueryParam('users_list');
        /*$paramsValuePair = [
            UserDetailsApiKeyHelper::USER_INFO => ApiUtility::TYPE_ARR,
        ];

        $this->paramCheck($paramsValuePair, $params);*/

        //$userinfos = [['password'=>'jj111111j', 'phone_number'=>'0', 'user_email'=>'yiifgmail.com', 'company_name'=>'ESL'],['password'=>'jj111111j', 'phone_number'=>'0', 'user_email'=>'yii2@gmail.com', 'company_name'=>'ESL']];
        //$userinfos = $params[UserDetailsApiKeyHelper::USER_INFO];
        //$userinfos = unserialize($userinfos);

        //return $userinfos;
   /*     $error_msg = '';

        $count=0;
        $tag=0;
        $response = array();
            foreach($userinfos as $userinfo){
                $password = $userinfo["password"];
                $phone_number = $userinfo["phone_number"];
                $companyname = $userinfo["company_name"];

                $companyid = Company::findOne(['company_name'=>$companyname]);
                $user_type = 3;
                $email = $userinfo["user_email"];

                $get_access_token = ApiUtility::generateAccessToken();
                date_default_timezone_set('Africa/Lagos');
                $time = date('Y-m-d H:i:s');

                //$connection = Yii::$app->db;
                //$transaction = $connection->beginTransaction();

                $fetch_mail = UserDetails::find()->where(['email'=>$email])->asArray()->one();

                $fetch_names = UserLogin::find()->where(['user_email' => $email, 'status_id' => 2])->asArray()->one();

                if($companyid != null){
                    if (!empty($password) && !empty($email)) {

                        if(ApiUtility::isValidEmail($email))
                        {
                            if($fetch_mail == null && $fetch_names == null)
                            {
                                if (strlen($password) >= $passwordLength) {
                                    //try {
                                    $values = [];
                                    $userlogins = new UserLogin();

                                    $userlogins->user_email = $email;
                                    $userlogins->user_password = ApiUtility::generatePasswordHash($password);
                                    $userlogins->access_token = $get_access_token;
                                    $userlogins->user_type_id = $user_type;
                                    $userlogins->created_date = $time;
                                    $userlogins->modified_date = $time;


                                    if ($userlogins->save()) {

                                        array_push($values, $userlogins->user_login_id);
                                    }

                                    $userdetails = new UserDetails();

                                    $userdetails->email = $email;
                                    $userdetails->company_id = $companyid->company_id;
                                    $userdetails->user_type_id = $user_type;
                                    $userdetails->phone_number = $phone_number;
                                    $userdetails->user_login_id = $values[0];
                                    $userdetails->created_date = $time;
                                    $userdetails->modified_date = $time;


                                    if($userdetails->save()){
                                        //$transaction->commit();

                                        //send respnse email

                                        array_push($response, $email.' Registration Successful');
                                    }

                                    //} catch (Exception $ex) {
                                    //$transaction->rollBack();
                                    //}
                                } else {
                                    $tag = 3;
                                    array_push($response, $email.' Password is less than 7 characters');
                                    //$response["password_error"] = $email.' Password is less than 7 characters';
                                    $error_msg = 'Password should not be less than 7 characters';
                                }
                            }
                            else
                            {
                                $count++;

                                array_push($response, $email.' already exists');
                                //$response["password_error"] = $email.' Password is less than 7 characters';
                                $error_msg = 'User already exists';
                            }
                        }
                        else
                        {

                            array_push($response, $email.' Email Invalid');
                            $error_msg = 'Invalid email address';
                        }
                    } else {

                        array_push($response, $email.' Should Fill in the required fields');
                        $error_msg = 'Fill in the required fields';
                    }
                }else{

                    array_push($response, $email.' Company not available');
                    $error_msg = 'Company not available';
                }


            }

            $this->setHeader(200);
            /*$message = "Thanks for registering on our platform. Your account will be activated
                        within the next 24 hours. Please bear with us<br/>
                        Thanks,<br/> Team RibyPeerLending";


            $this->sendEmail('mailtemplate',$this->getReplyEmail(),$message,'Account Activation', $userdetails->email, $this->getReplyEmail(),$email);*/
/*
            return [
                'status' => true,
                'message' => $response,
                'data' => null,
            ];
    }
*/


    public function actionClearloan(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $borrower_id = Yii::$app->request->getQueryParam('borrow_id');

        $checkborrower = Borrow::find()->select('borrow_id')->where(['borrow_id'=> $borrower_id, 'approve_flag'=>2, 'status_id'=>2])->asArray()->one();

        if($checkborrower['borrow_id'] != null and count($checkborrower) > 0)
        {
            $update =   Borrow::updateAll(['status_id'=> 1], ['borrow_id'=> $checkborrower['borrow_id']]);

            if($update) {


                return
                    [
                        'status' => true,
                        'message' => 'Loan Cleared',
                        'data' => null
                    ];
            }
            else{
                $msg = "Oops, An Error Occurs";
                return ApiUtility::errorResponse($msg);
            }
        }



        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }

    public function actionClearlend(){
        $this->setHeader(200);
        ApiAuthenticator::verifyAdminApiKey();

        $lend_id = Yii::$app->request->getQueryParam('lend_id');

        $checklender = Lend::find()->select('lend_id')->where(['lend_id'=> $lend_id, 'approve_flag'=>1, 'status_id'=>2])->asArray()->one();

        if($checklender['lend_id'] != null and count($checklender) > 0)
        {
            $update =   Lend::updateAll(['status_id'=> 1], ['lend_id'=> $checklender['len_id']]);

            if($update) {


                return
                    [
                        'status' => true,
                        'message' => 'Lend Cleared',
                        'data' => null
                    ];
            }
            else{
                $msg = "Oops, An Error Occurs";
                return ApiUtility::errorResponse($msg);
            }
        }



        else{
            $msg = "invalid transaction";
            return ApiUtility::errorResponse($msg);
        }

    }



    public function actionGetAudit(){

          $this->setHeader(200);
        ApiAuthenticator::verifyApiKey();

        $access_token = ApiKeyHelper::ACCESS_TOKEN;
        $get_access_token = $this->checkApiKey();

        $userlog = UserLogin::findOne(['access_token'=> $get_access_token]);
        $user = UserDetails::findOne(['user_login_id'=>$userlog->user_login_id]);
        $id = $user->user_id;

    $rows = (new \yii\db\Query())
    ->select(['*'])
    ->from('admin_log')
    ->where(['user_id' => $id])
    ->all();

   return [
            'status' => true,
            'message' => 'Query successfully',
            'data' => $rows
        ];
    }
}
