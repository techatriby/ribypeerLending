<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\GeneralAsset;
$baseUrlz = Yii::$app->request->baseUrl;
GeneralAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="page" class="page">
    <header class="nav-header9 nav-app1" id="nav-header9">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <a class="brand navbar-left" href="#"><img src="<?php echo $baseUrlz?>/images/factor-logo-1.png" alt="logo"></a>
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <a class="brand navbar-left" href="#"><img src="images/factor-logo-1.png" alt="logo"></a>
                    <ul class="nav navbar-nav navbar-right">
                        <li class=""><a class="scroll" href="#page">home</a></li>
                        <li><a class="scroll" href="#feature-4">features</a></li>
                        <li><a class="scroll" href="#content-area-5">services</a></li>
                        <li><a class="scroll" href="#pricing-table-2">packages</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

        <?= $content ?>

    <footer class="item footer  footer-4" id="footer-4" style="padding:50px 15px;">
        <div class="row">
            <div class="container">
                <div class="footer-social-2 tx-center">
                    <ul>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </div>
                <div class="copyright-text-1 tx-center">
                    <p>© Riby Peer Lending 2016, All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
