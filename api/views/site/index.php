<?php
/**
 * Created by PhpStorm.
 * User: Kayode
 * Date: 7/23/2016
 * Time: 8:26 PM
 */

$this->title = 'Home | Riby Peer Lending';
$this->params['breadcrumbs'][] = $this->title;
$baseUrlz = Yii::$app->request->baseUrl;
?>
<div class="item header margin-top-0 padding-bottom-30 header15" id="header15">
    <div class="container padding-top-30 padding-bottom-30">
        <div class="row margin-top-30 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
            <div class="col-md-7 col-sm-5 col-xs-12 padding-bottom-50">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="header-big-text-2 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
                            <h1 class="color-white tx-left">Our App Is <strong>Ready</strong> For You! Take A Look &amp; Enjoy.</h1>
                        </div>
                    </div>
                    <div class="item">
                        <div class="header-big-text-2 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
                            <h1 class="color-white tx-left">Our App Is <strong>Ready</strong> For You! Take A Look &amp; Enjoy.</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 col-sm-7 col-xs-12">
                <div class="banner-form animated fadeInUp" style="visibility: visible;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#calculator" aria-controls="calculator" role="tab" data-toggle="tab">Loan Calculator</a></li>
                        <li role="presentation"><a href="#signup" aria-controls="signup" role="tab" data-toggle="tab">Sign Up</a></li>
                        <li role="presentation"><a href="#signin" aria-controls="signin" role="tab" data-toggle="tab">Sign In</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="calculator">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form id="scredit-form" class="ng-valid ng-dirty ng-valid-parse">
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset>
                                                        <label for="amount"> I need <span style="font-size: 20px;" class="ng-binding">&#8358;<output></output> </span>worth of loan</label>
                                                        <input type="range" min="50000" max="1000000" step="10000" value="200000" data-rangeslider>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <fieldset>
                                                        <label for="amount"> I will pay for it over <span style="font-size: 20px;" class="ng-binding"> <output></output> </span> month(s)</label>
                                                        <input type="range" min="1" max="4" step="1" value="4" data-rangeslider>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <label style="">total payment:</label>
                                                        <label><span style="font-size:20px" class="ng-binding">₦460,800.00</span></label>
                                                    </fieldset>
                                                </div>
                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <label style="">monthly payment:</label>
                                                        <label><span style="font-size:20px" class="ng-binding">₦153,600.00</span></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <fieldset>
                                                        <button class="submit-btn btn-blue">APPLY NOW</button>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="signup">
                            <form id="signupform">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <label>Email</label>
                                            <input type="text" placeholder="Enter Email Address" name="email" required>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <label>Phone Number</label>
                                            <input type="number" placeholder="Enter Phone Number" name="phone" required>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <label>Company Name</label>
                                            <select>
                                                <option>-- Select Company --</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <button class="submit-btn btn-blue" name="signup">Sign Up</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="signin">
                            <form id="signinform">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <label>Email</label>
                                            <input type="text" placeholder="Enter Email Address" name="email" required>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <label>Password</label>
                                            <input type="password" placeholder="Enter Password"  name="password" required>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <button class="submit-btn btn-blue" name="signin">Sign In</button>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class=" item padding-top-60 padding-bottom-20" id="feature-4">

    <div class="wrapper">

    <div class="container" >

        <div class="row">
            <div class="section-title-1 tx-center margin-bottom-30 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
                <h5 class="color-white">What it can do for you</h5>
                <div class="sec-title-div-2"></div>
                <p class="color-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4 col-sm-4 col-xs-12 text-center os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0s">
                <div class="feature-4">
                    <div class="feature-figure-1">
                        <img src="images/app/feature-icon-1.png" alt="feature">
                    </div>
                    <div class="feature-title-3">
                        <h6>GOOD DESIGN</h6>
                    </div>
                    <div class="feature-disc-2">
                        <p>Sed bibendum, arcu ac rhoncus adipiscing, nulla lectus dictum mauris, non volutpat massa ipsum in nibh.</p>
                    </div>

                </div>

            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.5s">
                <div class="feature-4">
                    <div class="feature-figure-1">
                        <img src="images/app/feature-icon-2.png" alt="feature">
                    </div>
                    <div class="feature-title-3">
                        <h6>RESPONSIVE</h6>
                    </div>
                    <div class="feature-disc-2">
                        <p>Sed bibendum, arcu ac rhoncus adipiscing, nulla lectus dictum mauris, non volutpat massa ipsum in nibh.</p>
                    </div>

                </div>

            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 text-center os-animation" data-os-animation="fadeInRight" data-os-animation-delay="1s">
                <div class="feature-4">
                    <div class="feature-figure-1">
                        <img src="images/app/feature-icon-3.png" alt="feature">
                    </div>
                    <div class="feature-title-3">
                        <h6>FREE SUPPORT</h6>
                    </div>
                    <div class="feature-disc-2">
                        <p>Sed bibendum, arcu ac rhoncus adipiscing, nulla lectus dictum mauris, non volutpat massa ipsum in nibh.</p>
                    </div>

                </div>

            </div>
        </div>

    </div>

    </div>

</div>-->
<!--<div class=" item padding-top-60 padding-bottom-50" id="content-area-5">

    <div class="wrapper">

    <div class="container">

        <div class="row">
            <div class="section-title-1 tx-center margin-bottom-30 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
                <h5 class="">What it can do for you</h5>
                <div class="sec-title-div-1"></div>
                <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0s">
                <div class="content-area-5 tx-left">
                    <div class="feature-figure-1">
                        <img src="images/app/content-area-1.png" alt="content area">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 clearfix">
                <div class="content-area-5 clearfix margin-top-100 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
                    <div class="col-md-4 col-sm-4 col-xs-12 tx-left">
                        <img src="images/app/content-area-2.png" alt="content area">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 tx-left">
                        <h5>GOOD DESIGN</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud.</p>
                    </div>

                </div>
                <div class="content-area-5 clearfix margin-top-30 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                    <div class="col-md-4 col-sm-4 col-xs-12 tx-left">
                        <img src="images/app/content-area-3.png" alt="content area">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 tx-left">
                        <h5>GOOD DESIGN</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud.</p>
                    </div>

                </div>
                <div class="content-area-5 clearfix margin-top-30 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">
                    <div class="col-md-4 col-sm-4 col-xs-12 tx-left">
                        <img src="images/app/content-area-4.png" alt="content area">
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 tx-left">
                        <h5>GOOD DESIGN</h5>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud.</p>
                    </div>

                </div>

            </div>

        </div>

    </div>

    </div>

</div>-->
<!--<div class=" item padding-top-60 padding-bottom-50" id="content-area-6">
    <div class="row">

        <div class="container">

        <div class="row">
            <div class="section-title-1 tx-center margin-bottom-50 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
                <h5 class="color-white">about Our app</h5>
                <div class="sec-title-div-2"></div>
                <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                <div class="team-6-wrap padding-top-100">
                    <div class="content-area-title-1 tx-left os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0s">
                        <h5 class="color-white">All you have to know <br>about Our app</h5>
                    </div>

                    <div class="team-disc-1 tx-left os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0.2s">
                        <p class="">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Ut wisi enim ad minim veniam, quis nostrud Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br><br>

It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                <div class="content-area-6 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                    <div class="team-figure-1 tx-right">
                        <img src="images/app/content-area-5.png" alt="team">
                    </div>

                </div>
            </div>

        </div>
        </div>
    </div>
</div>-->
<!--<div class="item pricing-table-2 padding-top-50 padding-bottom-20" id="pricing-table-2">
    <div class="container">
        <div class="col-md-4 col-sm-4 col-xs-12 os-animation" data-os-animation="fadeInLeft" data-os-animation-delay="0s">
            <div class="p-table-2">
                <div class="ptable-title-1">
                    <h6>Basic plan</h6>
                </div>
                <div class="ptable-price-2">
                    <div class="ptable-currency-1">$</div>
                    <div class="ptable-amount-1">23</div>
                </div>
                <div class="ptable-plan-2">
                    <ul>
                        <li>10GB Space</li>
                        <li>Website Builder</li>
                        <li>100GB Bandwidth</li>
                        <li>Unlimited Subdomains</li>
                        <li>5 Domains (.com, .org, .eu)</li>
                    </ul>
                </div>
                <div class="ptable-btn-2">
                    <a href="#">select plan</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
            <div class="p-table-2 ptalbe-2-shadow">
                <div class="ptable-title-1">
                    <h6>gold plan</h6>
                </div>
                <div class="ptable-price-2">
                    <div class="ptable-currency-1">$</div>
                    <div class="ptable-amount-1">23</div>
                </div>
                <div class="ptable-plan-2">
                    <ul>
                        <li>200GB Space</li>
                        <li>Website Builder</li>
                        <li>600GB Bandwidth</li>
                        <li>Unlimited Subdomains</li>
                        <li>50 Domains (.com, .org, .eu)</li>
                    </ul>
                </div>
                <div class="ptable-btn-2">
                    <a href="#">select plan</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.4s">
            <div class="p-table-2">
                <div class="ptable-title-1">
                    <h6>silver PLAN</h6>
                </div>
                <div class="ptable-price-2">
                    <div class="ptable-currency-1">$</div>
                    <div class="ptable-amount-1">23</div>
                </div>
                <div class="ptable-plan-2">
                    <ul>
                        <li>100GB Space</li>
                        <li>Website Builder</li>
                        <li>200GB Bandwidth</li>
                        <li>Unlimited Subdomains</li>
                        <li>30 Domains (.com, .org, .eu)</li>
                    </ul>
                </div>
                <div class="ptable-btn-2">
                    <a href="#">select plan</a>
                </div>
            </div>
        </div>
    </div>
</div>-->
<!--<div class="item testimonial-4 padding-top-50 padding-bottom-50" id="testimonial-4">
    <div class="container">
        <div class="row os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
            <div class="section-title-1 tx-center">
                <h5>What Our Users Think About Us</h5>
            </div>
            <div class="sec-title-div-4"></div>
        </div>
        <div class="col-md-12 os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
            <div id="owl-testimonial-1" class="testimonials-ct">
                <div class="item">
                    <div class="thumbnail">
                        <img src="images/app/testi-1.png" alt="testimonial">
                    </div>
                    <div class="testi-cap-1">
                        <h6>Sarah Schmidt</h6>
                    </div>
                    <div class="testi-content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                </div>
                <div class="item">
                    <div class="thumbnail">
                        <img src="images/app/testi-1.png" alt="testimonial">
                    </div>
                    <div class="testi-cap-1">
                        <h6>Sarah Schmidt</h6>
                    </div>
                    <div class="testi-content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                </div>
                <div class="item">
                    <div class="thumbnail">
                        <img src="images/app/testi-1.png" alt="testimonial">
                    </div>
                    <div class="testi-cap-1">
                        <h6>Sarah Schmidt</h6>
                    </div>
                    <div class="testi-content">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</div>
                </div>
            </div>
        </div>
    </div>
</div>-->
<!--<div class="item callout-7" id="callout-7">
    <div class="row">
        <div class="container">
            <div class="callout-title-2 tx-center os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0s">
                <h3>Looking for ANY ASISSTANCE</h3>
            </div>
            <div class="callout-btn-6 tx-center os-animation" data-os-animation="fadeInUp" data-os-animation-delay="0.2s">
                <a href="#">CALL TO ACTION</a>
            </div>
        </div>
    </div>
</div>-->
