-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2016 at 10:44 AM
-- Server version: 5.6.30
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ribypeer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_log`
--

CREATE TABLE IF NOT EXISTS `admin_log` (
  `transaction_id` int(20) NOT NULL,
  `admin_log_id` int(11) NOT NULL,
  `admin_log_details` varchar(500) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_log`
--

-- --------------------------------------------------------

--
-- Table structure for table `bench_mark`
--

CREATE TABLE IF NOT EXISTS `bench_mark` (
  `bench_mark_id` int(11) NOT NULL,
  `bench_mark_min` varchar(40) NOT NULL,
  `bench_mark_max` varchar(40) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE IF NOT EXISTS `borrow` (
  `borrow_id` int(11) NOT NULL,
  `process_remarks` varchar(20) NOT NULL DEFAULT ' ',
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `loan_amount` int(11) NOT NULL,
  `amount_to_pay` int(11) NOT NULL,
  `date_of_request` datetime NOT NULL,
  `period` int(11) NOT NULL,
  `date_to_pay` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `interest_rate` float(10,3) NOT NULL,
  `insurance_rate` float(10,3) NOT NULL,
  `fixed_charge` double(20,2) NOT NULL,
  `fine` double(20,2) NOT NULL,
  `priority` float(10,3) NOT NULL,
  `status_id` int(11) NOT NULL,
  `approve_flag` int(11) NOT NULL DEFAULT '0',
  `transaction_id` int(20) NOT NULL,
  `process_description` varchar(500) NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_address` text NOT NULL,
  `company_info` varchar(500) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_approved` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_address`, `company_info`, `is_active`, `is_approved`) VALUES
(1, 'RIBY Finance', '7 Continental Way, CMD Road, Kosofe, Lagos', 'FinTech Company', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `employee_status`
--

CREATE TABLE IF NOT EXISTS `employee_status` (
  `employee_id` int(11) NOT NULL,
  `employee_status_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_status`
--

INSERT INTO `employee_status` (`employee_id`, `employee_status_name`) VALUES
(1, 'contract'),
(2, 'part-time'),
(3, 'full-time');

-- --------------------------------------------------------

--
-- Table structure for table `kegow_transaction`
--

CREATE TABLE IF NOT EXISTS `kegow_transaction` (
  `transaction_id` int(11) NOT NULL,
  `result` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `transactID` varchar(255) NOT NULL,
  `checkSUM` varchar(255) NOT NULL,
  `orderID` varchar(255) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lend`
--

CREATE TABLE IF NOT EXISTS `lend` (
  `lend_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lend_amount` int(11) NOT NULL,
  `lend_tenure` int(11) NOT NULL,
  `date_lend` datetime NOT NULL,
  `amount_to_receive` int(11) NOT NULL,
  `date_to_receive` datetime DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `approve_flag` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE IF NOT EXISTS `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(50) NOT NULL,
  `payment_option_type` int(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Payment options available to the system';

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_type`) VALUES
(1, 'Keegow', 1),
(2, 'Bank Deposit', 0),
(3, 'HR disbursement', 2),
(4, 'Bank Transfer', 0),
(5, 'Mobile money', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE IF NOT EXISTS `payment_type` (
  `payment_type_id` int(11) NOT NULL,
  `payment_type_name` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='All different payment types on the System';

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`payment_type_id`, `payment_type_name`) VALUES
(1, 'disbursement'),
(2, 'repayment');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_name`) VALUES
(1, 'unactive'),
(2, 'active'),
(3, 'unconfirm');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_remark`
--

CREATE TABLE IF NOT EXISTS `transaction_remark` (
  `remark_id` int(11) NOT NULL,
  `remark_description` varchar(500) NOT NULL,
  `remark_type` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_remark`
--

INSERT INTO `transaction_remark` (`remark_id`, `remark_description`, `remark_type`) VALUES
(1, 'Staff Leaving within the loan period', 2),
(2, 'Staff currently has a loan outside the platform', 2),
(3, 'Not Credit Worthy', 2),
(4, 'There are not enough lenders in the pool', 1),
(5, 'Incomplete Profile', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_bank_details`
--

CREATE TABLE IF NOT EXISTS `user_bank_details` (
  `user_id` int(100) NOT NULL,
  `bvn` varchar(20) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(20) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) NOT NULL,
  `title` varchar(10) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `middlename` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `phone_number` varchar(14) NOT NULL,
  `email` varchar(80) NOT NULL,
  `company_id` int(11) NOT NULL,
  `address` varchar(350) DEFAULT NULL,
  `gender` text,
  `birthdate` date DEFAULT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_image_url` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '2',
  `employee_status_id` int(11) DEFAULT NULL,
  `user_login_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `title`, `firstname`, `middlename`, `lastname`, `phone_number`, `email`, `company_id`, `address`, `gender`, `birthdate`, `user_type_id`, `user_image_url`, `created_date`, `modified_date`, `status_id`, `employee_status_id`, `user_login_id`) VALUES
(1, NULL, NULL, NULL, NULL, '012914247', 'admin@riby.me', 1, NULL, NULL, NULL, 1, NULL, '2016-09-05 00:00:00', '0000-00-00 00:00:00', 2, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `user_login_id` int(11) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `reset_password_key` varchar(200) DEFAULT NULL,
  `expiration_date` varchar(50) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '2',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `access_token` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_login_id`, `user_email`, `user_password`, `user_type_id`, `reset_password_key`, `expiration_date`, `status_id`, `created_date`, `modified_date`, `access_token`) VALUES
(1, 'admin@riby.me', '4f7e4ae8486f849cf8cf03180ff483d1539eb8f9', 1, NULL, '', 2, '2016-09-05 00:00:00', '0000-00-00 00:00:00', 'e93be6c8d7332255089b1b659b0c00c22c4eb9420aa4253ee51304e4e6815c90');

-- --------------------------------------------------------

--
-- Table structure for table `user_payment`
--

CREATE TABLE IF NOT EXISTS `user_payment` (
  `user_payment_id` int(11) NOT NULL,
  `transaction_id` int(20) NOT NULL,
  `payment_amount` float NOT NULL,
  `reference_id` varchar(50) NOT NULL,
  `repayment_reference_id` varchar(255) NOT NULL,
  `payment_number` text NOT NULL,
  `payment_description` text NOT NULL,
  `payment_type` int(1) NOT NULL,
  `payment_option` int(1) NOT NULL,
  `status_id` int(11) NOT NULL,
  `paid` int(11) NOT NULL DEFAULT '0',
  `month` int(11) NOT NULL,
  `payment_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `user_type_id` int(11) NOT NULL,
  `user_type_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_name`) VALUES
(1, 'Super Admin'),
(2, 'Company Admin'),
(3, 'Borrower/Lender'),
(4, 'Creator'),
(5, 'Viewer'),
(6, 'Staff Admin'),
(7, 'Staff Viewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`admin_log_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `admin_id` (`company_id`);

--
-- Indexes for table `bench_mark`
--
ALTER TABLE `bench_mark`
  ADD PRIMARY KEY (`bench_mark_id`),
  ADD KEY `bench_mark_id` (`bench_mark_id`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`borrow_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `company_id_2` (`company_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `employee_status`
--
ALTER TABLE `employee_status`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `kegow_transaction`
--
ALTER TABLE `kegow_transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `lend`
--
ALTER TABLE `lend`
  ADD PRIMARY KEY (`lend_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`payment_type_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `transaction_remark`
--
ALTER TABLE `transaction_remark`
  ADD PRIMARY KEY (`remark_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `user_type_id_2` (`user_type_id`),
  ADD KEY `employee_status_id` (`employee_status_id`),
  ADD KEY `user_login_id` (`user_login_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`user_login_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `user_payment`
--
ALTER TABLE `user_payment`
  ADD PRIMARY KEY (`user_payment_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `admin_log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `bench_mark`
--
ALTER TABLE `bench_mark`
  MODIFY `bench_mark_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `borrow_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee_status`
--
ALTER TABLE `employee_status`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kegow_transaction`
--
ALTER TABLE `kegow_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `lend`
--
ALTER TABLE `lend`
  MODIFY `lend_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `payment_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction_remark`
--
ALTER TABLE `transaction_remark`
  MODIFY `remark_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `user_login_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `user_payment`
--
ALTER TABLE `user_payment`
  MODIFY `user_payment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `borrow`
--
ALTER TABLE `borrow`
  ADD CONSTRAINT `borrow_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `borrow_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`),
  ADD CONSTRAINT `borrow_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`);

--
-- Constraints for table `lend`
--
ALTER TABLE `lend`
  ADD CONSTRAINT `lend_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `lend_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`),
  ADD CONSTRAINT `lend_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`);

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`user_type_id`),
  ADD CONSTRAINT `user_details_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`),
  ADD CONSTRAINT `user_details_ibfk_3` FOREIGN KEY (`employee_status_id`) REFERENCES `employee_status` (`employee_id`),
  ADD CONSTRAINT `user_details_ibfk_4` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `user_details_ibfk_5` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`user_login_id`);

--
-- Constraints for table `user_login`
--
ALTER TABLE `user_login`
  ADD CONSTRAINT `user_login_ibfk_2` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`user_type_id`),
  ADD CONSTRAINT `user_login_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
