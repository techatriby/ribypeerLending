-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 14, 2017 at 01:22 PM
-- Server version: 5.6.30
-- PHP Version: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ribypeer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_log`
--

CREATE TABLE IF NOT EXISTS `admin_log` (
  `transaction_id` int(20) NOT NULL,
  `admin_log_id` int(11) NOT NULL,
  `admin_log_details` varchar(500) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_log`
--

INSERT INTO `admin_log` (`transaction_id`, `admin_log_id`, `admin_log_details`, `company_id`, `user_id`) VALUES
(2147483647, 1, 'New Loan Requested', 7, 397),
(2147483647, 2, 'New Loan Requested', 7, 397),
(739735186, 3, 'New Loan Requested', 7, 397),
(2147483647, 4, 'New Loan Requested', 7, 397),
(2147483647, 5, 'New Loan Requested', 7, 397),
(73978819, 6, 'New Loan Requested', 7, 397),
(2147483647, 7, 'New Loan Requested', 7, 397),
(2147483647, 8, 'New Loan Requested', 7, 397),
(2147483647, 9, 'New Loan Requested', 7, 398),
(2147483647, 10, 'New Loan Requested', 7, 398),
(2147483647, 11, 'New Loan Requested', 7, 398),
(2147483647, 12, 'New Loan Requested', 7, 398),
(2147483647, 13, 'New Loan Requested', 7, 397),
(2147483647, 14, 'New Loan Requested', 8, 400),
(2147483647, 15, 'New Loan Requested', 8, 400),
(2147483647, 16, 'New Loan Requested', 8, 401),
(2147483647, 17, 'New Loan Requested', 8, 401),
(2147483647, 18, 'New Loan Requested', 8, 401),
(2147483647, 19, 'New Loan Requested', 8, 401),
(2147483647, 20, 'New Loan Requested', 8, 401),
(2147483647, 21, 'New Loan Requested', 8, 401),
(2147483647, 22, 'New Loan Requested', 8, 401),
(840119897, 23, 'New Loan Requested', 8, 401),
(2147483647, 24, 'New Loan Requested', 8, 401),
(2147483647, 25, 'New Loan Requested', 8, 401),
(2147483647, 26, 'New Loan Requested', 8, 401),
(2147483647, 27, 'New Loan Requested', 8, 401),
(2147483647, 28, 'New Loan Requested', 8, 401),
(2147483647, 29, 'New Loan Requested', 8, 401),
(2147483647, 30, 'New Loan Requested', 8, 401),
(2147483647, 31, 'New Loan Requested', 8, 401),
(2147483647, 32, 'New Loan Requested', 8, 401),
(840185174, 33, 'New Loan Requested', 8, 401),
(2147483647, 34, 'New Loan Requested', 8, 401),
(2147483647, 35, 'New Loan Requested', 8, 401),
(2147483647, 36, 'New Loan Requested', 8, 401),
(2147483647, 37, 'New Loan Requested', 8, 401),
(84018667, 38, 'New Loan Requested', 8, 401),
(2147483647, 39, 'New Loan Requested', 8, 401),
(2147483647, 40, 'New Loan Requested', 8, 401),
(2147483647, 41, 'New Loan Requested', 8, 401),
(2147483647, 42, 'New Loan Requested', 8, 401),
(2147483647, 43, 'New Loan Requested', 8, 401),
(2147483647, 44, 'New Loan Requested', 8, 401),
(2147483647, 45, 'New Loan Requested', 8, 401),
(2147483647, 46, 'New Loan Requested', 8, 401),
(2147483647, 47, 'New Loan Requested', 8, 401),
(2147483647, 48, 'New Loan Requested', 8, 401),
(2147483647, 49, 'New Loan Requested', 8, 401),
(2147483647, 50, 'New Loan Requested', 8, 401),
(2147483647, 51, 'New Loan Requested', 8, 401),
(2147483647, 52, 'New Loan Requested', 8, 401),
(2147483647, 53, 'New Loan Requested', 8, 401),
(2147483647, 54, 'New Loan Requested', 8, 401),
(2147483647, 55, 'New Loan Requested', 8, 401),
(2147483647, 56, 'New Loan Requested', 8, 401),
(2147483647, 57, 'New Loan Requested', 8, 401),
(2147483647, 58, 'New Loan Requested', 8, 401),
(2147483647, 59, 'New Loan Requested', 8, 401),
(2147483647, 60, 'New Loan Requested', 8, 401),
(2147483647, 61, 'New Loan Requested', 8, 401),
(2147483647, 62, 'New Loan Requested', 8, 401),
(2147483647, 63, 'New Loan Requested', 8, 401),
(2147483647, 64, 'New Loan Requested', 8, 401),
(2147483647, 65, 'New Loan Requested', 8, 401),
(2147483647, 66, 'New Loan Requested', 8, 401),
(2147483647, 67, 'New Loan Requested', 8, 401),
(2147483647, 68, 'New Loan Requested', 8, 401),
(2147483647, 69, 'New Loan Requested', 8, 401),
(2147483647, 70, 'New Loan Requested', 8, 401),
(2147483647, 71, 'New Loan Requested', 8, 401),
(2147483647, 72, 'New Loan Requested', 8, 401),
(2147483647, 73, 'New Loan Requested', 8, 401),
(2147483647, 74, 'New Loan Requested', 8, 401),
(2147483647, 75, 'New Loan Requested', 8, 401),
(2147483647, 76, 'New Loan Requested', 8, 401),
(840183496, 77, 'New Loan Requested', 8, 401),
(2147483647, 78, 'New Loan Requested', 8, 401),
(2147483647, 79, 'New Loan Requested', 8, 401),
(2147483647, 80, 'New Loan Requested', 8, 401),
(2147483647, 81, 'New Loan Requested', 8, 401),
(2147483647, 82, 'New Loan Requested', 8, 401),
(2147483647, 83, 'New Loan Requested', 8, 401),
(2147483647, 84, 'New Loan Requested', 8, 401),
(2147483647, 85, 'New Loan Requested', 8, 401),
(2147483647, 86, 'New Loan Requested', 8, 401),
(2147483647, 87, 'New Loan Requested', 8, 401),
(2147483647, 88, 'New Loan Requested', 8, 401),
(2147483647, 89, 'New Loan Requested', 8, 401),
(2147483647, 90, 'New Loan Requested', 8, 401),
(2147483647, 91, 'New Loan Requested', 8, 401),
(2147483647, 92, 'New Loan Requested', 8, 401),
(2147483647, 93, 'New Loan Requested', 8, 401),
(2147483647, 94, 'New Loan Requested', 8, 401),
(2147483647, 95, 'New Loan Requested', 8, 401),
(2147483647, 96, 'New Loan Requested', 8, 401),
(2147483647, 97, 'New Loan Requested', 8, 401),
(2147483647, 98, 'New Loan Requested', 8, 401),
(2147483647, 99, 'New Loan Requested', 7, 397),
(2147483647, 100, 'New Loan Requested', 7, 397),
(2147483647, 101, 'New Loan Requested', 7, 397);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE IF NOT EXISTS `bank` (
  `bank_id` int(11) NOT NULL,
  `bank_code` varchar(9) DEFAULT NULL,
  `bank_name` varchar(24) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`bank_id`, `bank_code`, `bank_name`) VALUES
(2, '000001', 'Sterling Bank'),
(3, '000002', 'Keystone Bank'),
(4, '000003', 'First City Monument Bank'),
(5, '000004', 'United Bank for Africa'),
(6, '000005', 'Diamond Bank'),
(7, '000006', 'JAIZ Bank'),
(8, '000007', 'Fidelity Bank'),
(9, '000008', 'Skye Bank'),
(10, '000009', 'Citi Bank'),
(11, '000010', 'Ecobank Bank'),
(12, '000011', 'Unity Bank'),
(13, '000012', 'StanbicIBTC Bank'),
(14, '000013', 'GTBank Plc'),
(15, '000014', 'Access Bank'),
(16, '000015', 'Zenith Bank'),
(17, '000016', 'First Bank of Nigeria'),
(18, '000017', 'Wema Bank'),
(19, '000018', 'Union Bank'),
(20, '000019', 'Enterprise Bank'),
(21, '000020', 'Heritage'),
(22, '000021', 'StandardChartered'),
(23, '000022', 'SUNTRUST BANK'),
(24, '060001', 'Coronation'),
(25, '070001', 'NPF MicroFinance Bank'),
(26, '070002', 'Fortis Microfinance Bank'),
(27, '070006', 'Covenant'),
(28, '090001', 'ASOSavings'),
(29, '090003', 'JubileeLife'),
(30, '090004', 'Parralex'),
(31, '090005', 'Trustbond'),
(32, '090006', 'SafeTrust'),
(33, '100001', 'FET'),
(34, '100002', 'Paga'),
(35, '100003', 'Parkway-ReadyCash'),
(36, '100004', 'Paycom'),
(37, '100005', 'Cellulant'),
(38, '100006', 'eTranzact'),
(39, '100007', 'StanbicMobileMoney'),
(40, '100008', 'EcoMobile'),
(41, '100009', 'GTMobile'),
(42, '100010', 'TeasyMobile'),
(43, '100011', 'Mkudi'),
(44, '100012', 'VTNetworks'),
(45, '100013', 'AccessMobile'),
(46, '100014', 'FBNMobile'),
(47, '100015', 'ChamsMobile'),
(48, '100016', 'FortisMobile'),
(49, '100017', 'Hedonmark'),
(50, '100018', 'ZenithMobile'),
(51, '100019', 'Fidelity Mobile'),
(52, '100020', 'MoneyBox'),
(53, '100021', 'Eartholeum'),
(54, '100022', 'Sterling Mobile'),
(55, '100023', 'TagPay'),
(56, '110001', 'UPSL'),
(57, '400001', 'FSDH'),
(58, '999999', 'NIP Virtual Bank');

-- --------------------------------------------------------

--
-- Table structure for table `bench_mark`
--

CREATE TABLE IF NOT EXISTS `bench_mark` (
  `bench_mark_id` int(11) NOT NULL,
  `bench_mark_min` varchar(40) NOT NULL,
  `bench_mark_max` varchar(40) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE IF NOT EXISTS `borrow` (
  `borrow_id` int(11) NOT NULL,
  `process_remarks` varchar(20) NOT NULL DEFAULT ' ',
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `loan_amount` int(11) NOT NULL,
  `amount_to_pay` int(11) NOT NULL,
  `date_of_request` datetime NOT NULL,
  `period` int(11) NOT NULL,
  `date_to_pay` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `interest_rate` float(10,3) NOT NULL,
  `insurance_rate` float(10,3) NOT NULL,
  `fixed_charge` double(20,2) NOT NULL,
  `fine` double(20,2) NOT NULL,
  `priority` float(10,3) NOT NULL,
  `status_id` int(11) NOT NULL,
  `approve_flag` int(11) NOT NULL DEFAULT '0',
  `transaction_id` varchar(20) NOT NULL,
  `process_description` varchar(500) NOT NULL DEFAULT '',
  `reason_id` int(11) DEFAULT NULL,
  `other_reasons` varchar(255) DEFAULT NULL,
  `loan_note_url` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borrow`
--

INSERT INTO `borrow` (`borrow_id`, `process_remarks`, `company_id`, `user_id`, `loan_amount`, `amount_to_pay`, `date_of_request`, `period`, `date_to_pay`, `date_updated`, `interest_rate`, `insurance_rate`, `fixed_charge`, `fine`, `priority`, `status_id`, `approve_flag`, `transaction_id`, `process_description`, `reason_id`, `other_reasons`, `loan_note_url`) VALUES
(25, ' ', 7, 397, 100, 96812, '2017-02-12 16:40:28', 4, '2017-06-12 17:31:35', '2017-02-12 17:31:35', 0.075, 0.020, 3000.00, 0.00, 0.000, 2, 3, '7397794403', '', 5, NULL, 'Loan Note - WILLIAMS ISAAC-Sunday , 12th of February 2017.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `borrow_reason`
--

CREATE TABLE IF NOT EXISTS `borrow_reason` (
  `reason_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borrow_reason`
--

INSERT INTO `borrow_reason` (`reason_id`, `title`) VALUES
(1, 'House Rent Support'),
(2, 'For a Friend'),
(3, 'Home Expenses/Improvement'),
(5, 'Quick Cash'),
(13, 'Quick Cash'),
(14, 'Funeral Funds'),
(15, 'others');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_address` text NOT NULL,
  `company_info` varchar(500) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_approved` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_address`, `company_info`, `is_active`, `is_approved`) VALUES
(1, 'RIBY Finance', '7 Continental Way, CMD Road, Kosofe, Lagos', 'FinTech Company', 1, 3),
(7, 'Paylater', 'Lagos, Nigeria', 'Just all about payment', 1, 1),
(8, 'Facebook Inc', 'PalAlto California CA', 'Social Media', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_status`
--

CREATE TABLE IF NOT EXISTS `employee_status` (
  `employee_id` int(11) NOT NULL,
  `employee_status_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_status`
--

INSERT INTO `employee_status` (`employee_id`, `employee_status_name`) VALUES
(1, 'contract'),
(2, 'part-time'),
(3, 'full-time');

-- --------------------------------------------------------

--
-- Table structure for table `kegow_cards`
--

CREATE TABLE IF NOT EXISTS `kegow_cards` (
  `kegow_cards_id` int(11) NOT NULL,
  `result` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cardID` varchar(255) NOT NULL,
  `checkSUM` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegow_cards`
--

INSERT INTO `kegow_cards` (`kegow_cards_id`, `result`, `user_id`, `cardID`, `checkSUM`) VALUES
(1, '30', 399, '4587572', 'FC49EFBA8888DEC414CBB955CB7F3FB92F4C0984EA7CC6B5174AF931FC505BFF');

-- --------------------------------------------------------

--
-- Table structure for table `kegow_transaction`
--

CREATE TABLE IF NOT EXISTS `kegow_transaction` (
  `transaction_id` int(11) NOT NULL,
  `result` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `transaction_key` varchar(255) NOT NULL,
  `transaction_type` varchar(1000) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `transactID` varchar(255) NOT NULL,
  `checkSUM` varchar(255) NOT NULL,
  `orderID` varchar(255) NOT NULL,
  `cardID` varchar(255) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kegow_transaction`
--

INSERT INTO `kegow_transaction` (`transaction_id`, `result`, `user_id`, `transaction_key`, `transaction_type`, `amount`, `transactID`, `checkSUM`, `orderID`, `cardID`, `date`) VALUES
(10, '30', 399, '0072907170', 'BANK TRANSFER', '1', '434744', 'E9D3B85A3362805D26083C72902EA8094D0BF2B2D94EDED274C3DBC872454D63', '58a093b0e4aea', NULL, '2017-02-12 17:56:22'),
(13, '30', 399, '2349055340932', 'CARD TRANSFER', '100', '434762', 'C29A3121218ACBB0B12D9208E76CD7EA961C24214EECFA39FEAE29B9FE56BD2A', '58a09bf87c508', NULL, '2017-02-12 18:31:42'),
(14, '30', 399, '2349055340932', 'CARD WITHDRAW', '100', '435198', 'B3BB0DF01C1A1FE6A4C4C3754430B9FD8160D4284503FF3E6E087AF56C73944A', '58a1b681a54d6', '4587572', '2017-02-13 14:37:15'),
(15, '30', 399, '2349055340932', 'CARD WITHDRAW', '1', '435204', 'EEF4F6B3D05639E5F933AB98EBEE817D3B4565201AF47BBB2546490FC8A5A148', '58a1b74cb5d6d', '4587572', '2017-02-13 14:40:37'),
(16, '30', 399, '2349055340932', 'CARD WITHDRAW', '1', '435206', '1FE99096E0B35399241DB8E274F344C567972EFE90A10002906356EBE23D87AD', '58a1b918cf81a', '4587572', '2017-02-13 14:48:21'),
(17, '30', 399, '2349055340932', 'CARD WITHDRAW', '1', '435208', 'FEC94BF693D7B27D6049A0AC08E961AA904B83896717A3D98B103E06D39E4157', '58a1b955a859b', '4587572', '2017-02-13 14:49:24'),
(18, '30', 399, '2349055340932', 'CARD WITHDRAW', '1', '435210', '2C4285EED5FE1B60A5E57C24979E739BB419E753477DF9CAC373B462117B71FE', '58a1b981158d7', '4587572', '2017-02-13 14:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `lend`
--

CREATE TABLE IF NOT EXISTS `lend` (
  `lend_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lend_amount` int(11) NOT NULL,
  `lend_tenure` int(11) NOT NULL,
  `date_lend` datetime NOT NULL,
  `amount_to_receive` int(11) NOT NULL,
  `date_to_receive` datetime DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `approve_flag` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `loan_notes`
--

CREATE TABLE IF NOT EXISTS `loan_notes` (
  `loan_note_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `transaction_id` varchar(40) DEFAULT NULL,
  `borrow_id` int(40) DEFAULT NULL,
  `amount_to_pay` varchar(40) NOT NULL,
  `period` varchar(40) NOT NULL,
  `interest_rate` varchar(40) NOT NULL,
  `fixed_charge` varchar(20) NOT NULL,
  `insurance_rate` varchar(20) NOT NULL,
  `priority` varchar(20) NOT NULL,
  `fine` varchar(20) NOT NULL,
  `principal_repayment_holder` varchar(200) NOT NULL,
  `monthly_interest_holder` varchar(200) NOT NULL,
  `insurance_charges_holder` varchar(200) NOT NULL,
  `priority_charges_holder` varchar(200) NOT NULL,
  `fines_charges_holder` varchar(200) NOT NULL,
  `transaction_charges_holder` varchar(200) NOT NULL,
  `monthly` varchar(200) NOT NULL,
  `principal_charge` varchar(200) NOT NULL,
  `payment_total_charges` varchar(200) NOT NULL,
  `loan_repayment` varchar(40) NOT NULL,
  `total_repayment_rate` varchar(40) NOT NULL,
  `effrate` varchar(40) NOT NULL,
  `date_of_request` varchar(40) NOT NULL,
  `date_disbursed` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan_notes`
--

INSERT INTO `loan_notes` (`loan_note_id`, `user_id`, `transaction_id`, `borrow_id`, `amount_to_pay`, `period`, `interest_rate`, `fixed_charge`, `insurance_rate`, `priority`, `fine`, `principal_repayment_holder`, `monthly_interest_holder`, `insurance_charges_holder`, `priority_charges_holder`, `fines_charges_holder`, `transaction_charges_holder`, `monthly`, `principal_charge`, `payment_total_charges`, `loan_repayment`, `total_repayment_rate`, `effrate`, `date_of_request`, `date_disbursed`) VALUES
(10, 399, '7397794403', 25, 's:7:"96812.5";', 's:1:"4";', 's:5:"0.075";', 's:4:"3000";', 's:4:"0.02";', 's:4:"0.00";', 's:1:"0";', 'a:4:{i:0;s:8:"18750.00";i:1;s:8:"18750.00";i:2;s:8:"18750.00";i:3;s:8:"18750.00";}', 'a:4:{i:0;s:7:"5625.00";i:1;s:7:"4218.75";i:2;s:7:"2812.50";i:3;s:7:"1406.25";}', 'a:4:{i:0;s:7:"1500.00";i:1;s:7:"1125.00";i:2;s:6:"750.00";i:3;s:6:"375.00";}', 'a:4:{i:0;s:4:"0.00";i:1;s:4:"0.00";i:2;s:4:"0.00";i:3;s:4:"0.00";}', 'a:4:{i:0;s:4:"0.00";i:1;s:4:"0.00";i:2;s:4:"0.00";i:3;s:4:"0.00";}', 'a:4:{i:0;s:4:"4000";i:1;s:4:"0.00";i:2;s:4:"0.00";i:3;s:4:"0.00";}', 'a:4:{i:0;s:8:"29500.00";i:1;s:8:"23812.50";i:2;s:8:"22125.00";i:3;s:8:"20437.50";}', 'a:4:{i:0;s:8:"75000.00";i:1;s:8:"56250.00";i:2;s:8:"37500.00";i:3;s:8:"18750.00";}', 's:4:"7750";', 's:7:"89062.5";', 's:4:"7.27";', 's:4:"4.69";', '12/02/2017', '12/02/2017');

-- --------------------------------------------------------

--
-- Table structure for table `merchant_access`
--

CREATE TABLE IF NOT EXISTS `merchant_access` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `secret` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE IF NOT EXISTS `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(50) NOT NULL,
  `payment_option_type` int(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Payment options available to the system';

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_type`) VALUES
(1, 'Kegow', 1),
(2, 'Bank Deposit', 0),
(3, 'HR disbursement', 2),
(4, 'Bank Transfer', 0),
(5, 'Mobile money', 1),
(6, 'Kegow Pay', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment_option_type`
--

CREATE TABLE IF NOT EXISTS `payment_option_type` (
  `payment_option_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `payment_option_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option_type`
--

INSERT INTO `payment_option_type` (`payment_option_type_id`, `name`, `payment_option_id`) VALUES
(1, 'KEGOW CARD TRANSFER', 1),
(2, 'KEGOW BANK TRANSFER', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE IF NOT EXISTS `payment_type` (
  `payment_type_id` int(11) NOT NULL,
  `payment_type_name` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='All different payment types on the System';

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`payment_type_id`, `payment_type_name`) VALUES
(1, 'disbursement'),
(2, 'repayment');

-- --------------------------------------------------------

--
-- Table structure for table `repayment_schedule`
--

CREATE TABLE IF NOT EXISTS `repayment_schedule` (
  `repayment_schedule_id` int(11) NOT NULL,
  `user_payment_details` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `month` varchar(40) NOT NULL,
  `payment_type` varchar(20) NOT NULL,
  `total_amount` varchar(40) NOT NULL,
  `date_submitted` varchar(40) DEFAULT NULL,
  `date_paid` varchar(40) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `transaction_identifier` varchar(200) DEFAULT NULL,
  `encKey` varchar(200) DEFAULT NULL,
  `orderId` varchar(200) DEFAULT NULL,
  `repayer_key` varchar(200) DEFAULT NULL,
  `cardReference` varchar(40) NOT NULL,
  `decline_reason` varchar(200) DEFAULT NULL,
  `repayment_schedule_url` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `repayment_schedule`
--

INSERT INTO `repayment_schedule` (`repayment_schedule_id`, `user_payment_details`, `company_id`, `month`, `payment_type`, `total_amount`, `date_submitted`, `date_paid`, `status`, `transaction_identifier`, `encKey`, `orderId`, `repayer_key`, `cardReference`, `decline_reason`, `repayment_schedule_url`) VALUES
(17, 'YToyOntpOjA7YTo5OntzOjk6ImJvcnJvd19pZCI7czoxOiI5IjtzOjE0OiJ0cmFuc2FjdGlvbl9pZCI7czoxMDoiODQwMDE1MjIyMSI7czo1OiJtb250aCI7czoxOiIxIjtzOjE0OiJwYXltZW50X2Ftb3VudCI7czo1OiIyOTUwMCI7czo5OiJmaXJzdG5hbWUiO3M6NzoiQWRleWVtaSI7czo4OiJsYXN0bmFtZSI7czo4OiJPZHVuYWlrZSI7czoxMDoiY29tcGFueV9pZCI7czoxOiI4IjtzOjEyOiJjb21wYW55X25hbWUiO3M6MTI6IkZhY2Vib29rIEluYyI7czo5OiIkJGhhc2hLZXkiO3M6OToib2JqZWN0OjQyIjt9aToxO2E6OTp7czo5OiJib3Jyb3dfaWQiO3M6MjoiMjEiO3M6MTQ6InRyYW5zYWN0aW9uX2lkIjtzOjEwOiI4NDAxMTc1OTk1IjtzOjU6Im1vbnRoIjtzOjE6IjEiO3M6MTQ6InBheW1lbnRfYW1vdW50IjtzOjU6IjQ2MTAwIjtzOjk6ImZpcnN0bmFtZSI7czo5OiJBZGVqdW1vYmkiO3M6ODoibGFzdG5hbWUiO3M6NzoiTWljaGVhbCI7czoxMDoiY29tcGFueV9pZCI7czoxOiI4IjtzOjEyOiJjb21wYW55X25hbWUiO3M6MTI6IkZhY2Vib29rIEluYyI7czo5OiIkJGhhc2hLZXkiO3M6OToib2JqZWN0OjQ0Ijt9fQ==', 8, 'January', '', '1', '20th , January 2017', '21st , January 2017', 1, '420836', '4DB760890147D4F0031D40FBBD40AF01F153506C2625D84633A259E036054560', 'ORDER6972', 'b8f9d0e5959404f6251279f079119f4e58a75ff73f4e78d5ac4d2de5e8681748', '1322456', NULL, 'Repayment Schedule - FACEBOOK INC-Friday , 20th of January 2017.pdf'),
(18, 'YToyOntpOjA7YTo5OntzOjk6ImJvcnJvd19pZCI7czoxOiI5IjtzOjE0OiJ0cmFuc2FjdGlvbl9pZCI7czoxMDoiODQwMDE1MjIyMSI7czo1OiJtb250aCI7czoxOiIyIjtzOjE0OiJwYXltZW50X2Ftb3VudCI7czo3OiIyMzgxMi41IjtzOjk6ImZpcnN0bmFtZSI7czo3OiJBZGV5ZW1pIjtzOjg6Imxhc3RuYW1lIjtzOjg6Ik9kdW5haWtlIjtzOjEwOiJjb21wYW55X2lkIjtzOjE6IjgiO3M6MTI6ImNvbXBhbnlfbmFtZSI7czoxMjoiRmFjZWJvb2sgSW5jIjtzOjk6IiQkaGFzaEtleSI7czo5OiJvYmplY3Q6NDMiO31pOjE7YTo5OntzOjk6ImJvcnJvd19pZCI7czoyOiIyMSI7czoxNDoidHJhbnNhY3Rpb25faWQiO3M6MTA6Ijg0MDExNzU5OTUiO3M6NToibW9udGgiO3M6MToiMiI7czoxNDoicGF5bWVudF9hbW91bnQiO3M6NzoiMzY1MTIuMyI7czo5OiJmaXJzdG5hbWUiO3M6OToiQWRlanVtb2JpIjtzOjg6Imxhc3RuYW1lIjtzOjc6Ik1pY2hlYWwiO3M6MTA6ImNvbXBhbnlfaWQiO3M6MToiOCI7czoxMjoiY29tcGFueV9uYW1lIjtzOjEyOiJGYWNlYm9vayBJbmMiO3M6OToiJCRoYXNoS2V5IjtzOjk6Im9iamVjdDo0NSI7fX0=', 8, 'January', '', '1', '20th , January 2017', NULL, 0, NULL, NULL, NULL, NULL, '', NULL, 'Repayment Schedule - FACEBOOK INC-Friday , 20th of January 2017.pdf'),
(19, 'YToxOntpOjA7YTo5OntzOjk6ImJvcnJvd19pZCI7czoxOiI3IjtzOjE0OiJ0cmFuc2FjdGlvbl9pZCI7czoxMDoiNzM5NzM0Nzc0OCI7czo1OiJtb250aCI7czoxOiI0IjtzOjE0OiJwYXltZW50X2Ftb3VudCI7czo1OiIyNy4yNSI7czo5OiJmaXJzdG5hbWUiO3M6ODoiV2lsbGlhbXMiO3M6ODoibGFzdG5hbWUiO3M6NToiSXNhYWMiO3M6MTA6ImNvbXBhbnlfaWQiO3M6MToiNyI7czoxMjoiY29tcGFueV9uYW1lIjtzOjg6IlBheWxhdGVyIjtzOjk6IiQkaGFzaEtleSI7czo5OiJvYmplY3Q6ODYiO319', 7, 'January', '', '1', '24th , January 2017', '24th , January 2017', 1, '422687', 'BBB086734433794578813E1FF041D64FBDA686D50A5C50442121A2A75F0A0082', 'ORDER7002', '45adb4c812fddeeb05bec52ab3daeae3bd9bc2c9bb1da15479c43b3dfe136272', '1322456', NULL, 'Repayment Schedule - PAYLATER-Tuesday , 24th of January 2017.pdf'),
(20, 'YToxOntpOjA7YTo5OntzOjk6ImJvcnJvd19pZCI7czoyOiIyNSI7czoxNDoidHJhbnNhY3Rpb25faWQiO3M6MTA6IjczOTc3OTQ0MDMiO3M6NToibW9udGgiO3M6MToiMSI7czoxNDoicGF5bWVudF9hbW91bnQiO3M6MToiMSI7czo5OiJmaXJzdG5hbWUiO3M6ODoiV2lsbGlhbXMiO3M6ODoibGFzdG5hbWUiO3M6NToiSXNhYWMiO3M6MTA6ImNvbXBhbnlfaWQiO3M6MToiNyI7czoxMjoiY29tcGFueV9uYW1lIjtzOjg6IlBheWxhdGVyIjtzOjk6IiQkaGFzaEtleSI7czo5OiJvYmplY3Q6MTYiO319', 7, 'February', '6', '1', '13th , February 2017', '13th , February 2017', 1, 'KEGOWREPAY58A1E7E52E2F0', 'KEGOWREPAY58A1E7E52E2F3', 'KEGOWREPAY58A1E7E52E2F6', '45adb4c812fddeeb05bec52ab3daeae3bd9bc2c9bb1da15479c43b3dfe136272', 'KEGOWREPAY58A1E7E52E2F9', NULL, 'Repayment Schedule - PAYLATER-Monday , 13th of February 2017.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `request_invite`
--

CREATE TABLE IF NOT EXISTS `request_invite` (
  `request_invite` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(14) NOT NULL,
  `fullname` varchar(40) NOT NULL,
  `company_name` varchar(40) NOT NULL,
  `hr_email` varchar(50) NOT NULL,
  `hr_phone` varchar(14) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_invite`
--

INSERT INTO `request_invite` (`request_invite`, `email`, `phone`, `fullname`, `company_name`, `hr_email`, `hr_phone`, `status`) VALUES
(1, 'williamscalg@gmail.com', '08146877423', 'Williams', 'Facebook', 'williamscalg@gmail.com', '08146877423', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_name`) VALUES
(1, 'unactive'),
(2, 'active'),
(3, 'unconfirm');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_remark`
--

CREATE TABLE IF NOT EXISTS `transaction_remark` (
  `remark_id` int(11) NOT NULL,
  `remark_description` varchar(500) NOT NULL,
  `remark_type` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_remark`
--

INSERT INTO `transaction_remark` (`remark_id`, `remark_description`, `remark_type`) VALUES
(1, 'Staff Leaving within the loan period', 2),
(2, 'Staff currently has a loan outside the platform', 2),
(3, 'Not Credit Worthy', 2),
(4, 'There are not enough lenders in the pool', 1),
(5, 'Incomplete Profile', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_bank_details`
--

CREATE TABLE IF NOT EXISTS `user_bank_details` (
  `user_bank_details_id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `bvn` varchar(20) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `account_number` varchar(20) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `bank_code` varchar(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_bank_details`
--

INSERT INTO `user_bank_details` (`user_bank_details_id`, `user_id`, `bvn`, `account_name`, `account_number`, `bank_name`, `bank_code`, `created_date`, `modified_date`) VALUES
(5, 399, '0009292928822', 'ISAAC  WILLIAMS', '0072907170', 'Diamond Bank', '000005', '2016-12-20 18:22:24', '2016-12-20 18:22:24'),
(6, 400, '99200293773783', 'ISAAC  WILLIAMS', '0072907170', 'Diamond Bank', '000005', '2017-01-09 11:28:11', '2017-01-09 11:28:11'),
(7, 402, '929282038809383', 'ISAAC  WILLIAMS', '0072907170', 'Diamond Bank', '000005', '2017-01-10 23:26:35', '2017-01-10 23:26:35'),
(8, 403, '9383739083098', 'ISAAC  WILLIAMS', '0072907170', 'Diamond Bank', '000005', '2017-01-16 10:44:03', '2017-01-16 10:44:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) NOT NULL,
  `title` varchar(10) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `middlename` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `phone_number` varchar(14) NOT NULL,
  `email` varchar(80) NOT NULL,
  `company_id` int(11) NOT NULL,
  `address` varchar(350) DEFAULT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `gender` text,
  `birthdate` date DEFAULT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_image_url` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '2',
  `employee_status_id` int(11) DEFAULT NULL,
  `user_login_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `title`, `firstname`, `middlename`, `lastname`, `phone_number`, `email`, `company_id`, `address`, `city`, `country`, `zip_code`, `gender`, `birthdate`, `user_type_id`, `user_image_url`, `created_date`, `modified_date`, `status_id`, `employee_status_id`, `user_login_id`) VALUES
(1, 'Mr', 'Riby', '', 'Finance', '012914247', 'admin@riby.me', 1, '', '', '', '', 'male', NULL, 1, NULL, '2016-09-05 00:00:00', '2016-12-01 09:46:02', 2, 3, 1),
(396, NULL, 'Williams', NULL, 'Esther', '2348060738273', 'williamsesther019@gmail.com', 7, NULL, '', '', '', NULL, NULL, 2, NULL, '2016-12-16 23:23:48', '2016-12-16 23:23:48', 2, NULL, 398),
(397, NULL, 'Williams', NULL, 'Isaac', '2349055340932', 'williamscalg@gmail.com', 7, NULL, '', '', '', NULL, NULL, 3, NULL, '2016-12-16 23:36:45', '2016-12-16 23:36:45', 2, NULL, 399),
(398, NULL, 'Adewoyin', NULL, 'Pelumi', '2348060738273', 'pel.adewoyin@gmail.com', 7, NULL, '', '', '', NULL, NULL, 3, NULL, '2017-01-09 11:25:50', '2017-01-09 11:25:50', 2, NULL, 400),
(399, NULL, 'Olamide', NULL, 'Micheal', '2348146877423', 'williamscalg@gmail.com', 8, NULL, '', '', '', NULL, NULL, 2, NULL, '2017-01-10 23:17:56', '2017-01-10 23:17:56', 2, NULL, 401),
(400, NULL, 'Adeyemi', NULL, 'Odunaike', '2348146877429', 'adeyemi@gmail.com', 8, NULL, '', '', '', NULL, NULL, 3, NULL, '2017-01-10 23:22:27', '2017-01-10 23:22:27', 2, NULL, 402),
(401, NULL, 'Adejumobi', NULL, 'Micheal', '234817788377', 'micheal@gmail.com', 8, NULL, '', '', '', NULL, NULL, 3, NULL, '2017-01-12 18:40:23', '2017-01-12 18:40:23', 2, NULL, 403);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `user_login_id` int(11) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `reset_password_key` varchar(200) DEFAULT NULL,
  `expiration_date` varchar(50) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '2',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `access_token` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=404 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_login_id`, `user_email`, `user_password`, `user_type_id`, `reset_password_key`, `expiration_date`, `status_id`, `created_date`, `modified_date`, `access_token`) VALUES
(1, 'admin@riby.me', '4f7e4ae8486f849cf8cf03180ff483d1539eb8f9', 1, NULL, '', 2, '2016-09-05 00:00:00', '2016-12-01 09:46:02', 'e93be6c8d7332255089b1b659b0c00c22c4eb9420aa4253ee51304e4e6815c90'),
(398, 'williamsesther019@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 2, NULL, '1970-01-01 01:02:02', 2, '2016-12-16 23:23:48', '2016-12-16 23:23:48', '45adb4c812fddeeb05bec52ab3daeae3bd9bc2c9bb1da15479c43b3dfe136272'),
(399, 'williamscalg@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 3, NULL, '1970-01-01 01:02:02', 2, '2016-12-16 23:36:45', '2016-12-16 23:36:45', '30d0b2db72f26437aa62ea05f81cf5793c1415e4164066c5d152a49cdeabe113'),
(400, 'pel.adewoyin@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 3, NULL, '1970-01-01 01:02:02', 2, '2017-01-09 11:25:50', '2017-01-09 11:25:50', '564b4aaff2f9aafaf75558070baaa6bbe5e2258ed926b1ea333ff55e9fdf7b4a'),
(401, 'olamide@gmail.com', 'efacc4001e857f7eba4ae781c2932dedf843865e', 2, NULL, '1970-01-01 01:02:02', 2, '2017-01-10 23:17:56', '2017-01-10 23:17:56', 'b8f9d0e5959404f6251279f079119f4e58a75ff73f4e78d5ac4d2de5e8681748'),
(402, 'adeyemi@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 3, NULL, '1970-01-01 01:02:02', 2, '2017-01-10 23:22:27', '2017-01-10 23:22:27', '15993c8e27fe8b90e8ae6338f47f53fada7998b129d74ea9bd82f4e56239834a'),
(403, 'micheal@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 3, NULL, '1970-01-01 01:02:02', 2, '2017-01-12 18:40:23', '2017-01-12 18:40:23', 'f35a85e263da53e5bf1c2657bfbce1027c01e1ea9f46dd8713ae92d3f3042476');

-- --------------------------------------------------------

--
-- Table structure for table `user_payment`
--

CREATE TABLE IF NOT EXISTS `user_payment` (
  `user_payment_id` int(11) NOT NULL,
  `transaction_id` varchar(20) NOT NULL,
  `payment_amount` float NOT NULL,
  `reference_id` varchar(50) NOT NULL,
  `repayment_reference_id` varchar(255) NOT NULL,
  `payment_number` text NOT NULL,
  `payment_description` text NOT NULL,
  `payment_type` int(1) NOT NULL,
  `payment_option` int(1) NOT NULL,
  `status_id` int(11) NOT NULL,
  `paid` int(11) NOT NULL DEFAULT '0',
  `month` int(11) NOT NULL,
  `month_to_pay` varchar(40) NOT NULL,
  `payment_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_payment`
--

INSERT INTO `user_payment` (`user_payment_id`, `transaction_id`, `payment_amount`, `reference_id`, `repayment_reference_id`, `payment_number`, `payment_description`, `payment_type`, `payment_option`, `status_id`, `paid`, `month`, `month_to_pay`, `payment_date`) VALUES
(149, '7397794403', 1, '0', 'KEGOWREPAY58A1E7E52E2F0', '', 'Kegow Repayment', 1, 1, 2, 1, 1, 'February', '2017-02-12 17:31:35'),
(150, '7397794403', 31.75, '0', '', '', '', 1, 1, 2, 0, 2, 'April', '2017-02-12 17:31:35'),
(151, '7397794403', 29.5, '0', '', '', '', 1, 1, 2, 0, 3, 'May', '2017-02-12 17:31:35'),
(152, '7397794403', 27.25, '0', '', '', '', 1, 1, 2, 0, 4, 'June', '2017-02-12 17:31:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `user_type_id` int(11) NOT NULL,
  `user_type_name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_name`) VALUES
(1, 'Super Admin'),
(2, 'Company Admin'),
(3, 'Borrower/Lender'),
(4, 'Creator'),
(5, 'Viewer'),
(6, 'Staff Admin'),
(7, 'Staff Viewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`admin_log_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `admin_id` (`company_id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `bench_mark`
--
ALTER TABLE `bench_mark`
  ADD PRIMARY KEY (`bench_mark_id`),
  ADD KEY `bench_mark_id` (`bench_mark_id`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`borrow_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `company_id_2` (`company_id`);

--
-- Indexes for table `borrow_reason`
--
ALTER TABLE `borrow_reason`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_status`
--
ALTER TABLE `employee_status`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `kegow_cards`
--
ALTER TABLE `kegow_cards`
  ADD PRIMARY KEY (`kegow_cards_id`);

--
-- Indexes for table `kegow_transaction`
--
ALTER TABLE `kegow_transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `lend`
--
ALTER TABLE `lend`
  ADD PRIMARY KEY (`lend_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `loan_notes`
--
ALTER TABLE `loan_notes`
  ADD PRIMARY KEY (`loan_note_id`);

--
-- Indexes for table `merchant_access`
--
ALTER TABLE `merchant_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `payment_option_type`
--
ALTER TABLE `payment_option_type`
  ADD PRIMARY KEY (`payment_option_type_id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`payment_type_id`);

--
-- Indexes for table `repayment_schedule`
--
ALTER TABLE `repayment_schedule`
  ADD PRIMARY KEY (`repayment_schedule_id`);

--
-- Indexes for table `request_invite`
--
ALTER TABLE `request_invite`
  ADD PRIMARY KEY (`request_invite`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `transaction_remark`
--
ALTER TABLE `transaction_remark`
  ADD PRIMARY KEY (`remark_id`);

--
-- Indexes for table `user_bank_details`
--
ALTER TABLE `user_bank_details`
  ADD PRIMARY KEY (`user_bank_details_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `user_type_id_2` (`user_type_id`),
  ADD KEY `employee_status_id` (`employee_status_id`),
  ADD KEY `user_login_id` (`user_login_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`user_login_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `user_payment`
--
ALTER TABLE `user_payment`
  ADD PRIMARY KEY (`user_payment_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `admin_log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `bench_mark`
--
ALTER TABLE `bench_mark`
  MODIFY `bench_mark_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `borrow_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `borrow_reason`
--
ALTER TABLE `borrow_reason`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_status`
--
ALTER TABLE `employee_status`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kegow_cards`
--
ALTER TABLE `kegow_cards`
  MODIFY `kegow_cards_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kegow_transaction`
--
ALTER TABLE `kegow_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `lend`
--
ALTER TABLE `lend`
  MODIFY `lend_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loan_notes`
--
ALTER TABLE `loan_notes`
  MODIFY `loan_note_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `merchant_access`
--
ALTER TABLE `merchant_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `payment_option_type`
--
ALTER TABLE `payment_option_type`
  MODIFY `payment_option_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `payment_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `repayment_schedule`
--
ALTER TABLE `repayment_schedule`
  MODIFY `repayment_schedule_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `request_invite`
--
ALTER TABLE `request_invite`
  MODIFY `request_invite` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `transaction_remark`
--
ALTER TABLE `transaction_remark`
  MODIFY `remark_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_bank_details`
--
ALTER TABLE `user_bank_details`
  MODIFY `user_bank_details_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=402;
--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `user_login_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=404;
--
-- AUTO_INCREMENT for table `user_payment`
--
ALTER TABLE `user_payment`
  MODIFY `user_payment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=153;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
