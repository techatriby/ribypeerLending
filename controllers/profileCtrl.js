/**
 * Created by Ukemeabasi on 24/07/2016.
 */

'use strict';

app.controller('profileCtrl', ['$scope', '$http', 'api', function($scope, $http, api){
    $("#owl-demo").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        navigation:false,
        autoPlay: true
    });

    //gets all registered companies
    $http({method: 'GET', url:api+'/company/company?key=92215a06d3349532a5669db8dacb1b06'})
        .success( function( data )
        {
            if(data.status == true){
                $scope.list_companies = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });


    $scope.signup_text = "Sign Up";
    $scope.signUp = function (user) {
        $(".spinner").show();
        $scope.signup_text = "";
        $http({
            method : 'POST',
            url : api+'/user/authenticate',
            data :user
        })
            .success( function( data )
            {
                if(data.status == 1){
                    $(".spinner").hide();
                    $scope.signup_text = "Sign Up";
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.msg,"Logged in Successfully!");
                    }, 0);
                    window.location.href = "#/home";
                }
                else{
                    $(".spinner").hide();
                    $scope.signup_text = "Sign Up";
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"Error Occurred!");
                    }, 0);
                }
            })
            .error( function( data )
            {
                $(".spinner").hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"Error Occurred!");
                }, 0);
            });
    }

}]);