'use strict';

app.controller('updateProfileCtrl', ['$scope', '$http', 'api', function($scope, $http, api) {
    $scope.matchPattern = new RegExp("^[234]");
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    //$scope.stafflevels = [{id:3,role:"Staff"},{id:6,role:"Admin Admin"},{id:7,role:"Admin User"}];
    //$scope.newstaff = {};
    $('#submit-spinner').hide();
    //{id:3,role:"Staff"}
    //$scope.staff.user_type= $scope.stafflevels[0];
    //gets all registered companies
    $http({ method: 'GET', url: api + "/company/company?key=" + $scope.user.user_access_token })
        .success(function(data) {
            if (data.status == true) {
                $scope.list_companies = data.data;
            } else {
                console.log(data)
            }
        })
        .error(function(data) { console.log(data) });
    $http({ method: 'GET', url: api + "/admin/getaccesslevels?key=" + $scope.user.user_access_token })
        .success(function(data) {
            //console.log("data is ",data);
            $scope.stafflevels = data.data;
            //console.log("Successful Data is ",data);
            //console.log("The user type id is ",$scope.user.user_type_id);
            switch ($scope.user.user_type_id) {
                case 2:
                    {
                        $scope.stafflevels = data.data.filter(function(item, index, array) {
                            return item.user_type_id == 3 || item.user_type_id == 6 || item.user_type_id == 7
                        })
                        break;
                    }
                case 6:
                    {
                        $scope.stafflevels = data.data.filter(function(item, index, array) {
                            return item.user_type_id == 3;

                        });
                        break;
                    }
                    /*case 7:{
                       $scope.stafflevels = data.filter(function(item,index,array){
                            return item.user_type_id == 3 || item.user_type_id == 7;

                        });
                       break;
                    }*/

            }
        }).error(function(data) {
            console.log("Error company Admin Level Data is ", data);
        })
    $("#spinner-signup").hide();
    $("#signup-text").show();
    $scope.uploadcsv = function() {
        $('#upload-text').hide();
        $('#upload-spinner').show();
        var fd = new FormData();
        var file = $scope.file;
        fd.append('file', file);
        $http.post(api + '/admin/massuploadstaffs?key=' + $scope.user.user_access_token, fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
            .success(function(data, status) {
                if (data.status == true) {
                    $('#upload-text').show();
                    $('#upload-spinner').hide();
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.success(data.message);
                } else {
                    //console.log('In the error brace ',data.status);
                    $('#upload-text').show();
                    $('#upload-spinner').hide();
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 10000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message);
                    var object = data.data;
                    for (var key in object) {
                        if (object.hasOwnProperty(key)) {
                            var element = object[key];
                            toastr.info(key + ": " + element);
                        }
                    }
                }
            })
            .error(function(data, status) {
                $('#upload-text').show();
                $('#upload-spinner').hide();
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message, "An Error Occured!");
                }, 1300);
            });
    }
    $scope.staffs = [];
    // $scope.staff= {};
    $scope.addstaff = function(staff) {
        if (!staff) {
            return;
        }
        //console.log(staff)
        staff.edit = false;
        if (!staff.lastname) staff.lastname = '';
        if (!staff.firstname) staff.firstname = '';

        $scope.staffs.push(staff);
        console.log("staffs are ", $scope.staffs);
        $scope.newstaff = {};
    };
    $scope.edit = function(staff) {
        staff.edit = true;
    }
    $scope.deletestaff = function(staff) {
        //console.log('Staff to delete is ',staff);
        // console.log("to delete are ",$scope.staffs[staff]);
        var index = $scope.staffs.indexOf(staff);
        // $scope.staffs = $scope.staffs.splice(staff,1);
        // console.log($scope.staffs,staff);
        // $scope.staffs = $scope.staffs.indexOf(staff)
        if (index === 0) {
            $scope.staffs.shift();
        } else {
            $scope.staffs.splice(index, 1);
            //console.log("Staffs left are ",$scope.staffs);
        }
    };
    $scope.signUpStaffs = function() {
        $scope.user_errors = [];
        //console.log("The check is ",angular.equals({},$scope.newstaff));
        //$('.btn-success').text('');
        $('span.submit').hide();
        $('.spinner').show();
        //return;
        if (angular.equals({}, $scope.newstaff)) {
            $.ajax({
                method: "POST",
                url: api + "/admin/createstaffs?key=" + $scope.user.user_access_token,
                data: { 'users_list': $scope.staffs },
                contentType: "application/x-www-form-urlencoded",
                success: function(data, status) {
                    if (data.status == true) {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message);
                        $('span.submit').show();
                        $('.spinner').hide();
                        $scope.staffs.length = 0;
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    } else {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message, "An Error Occured!");
                        $('span.submit').show();
                        $('.spinner').hide();
                        if (data.data == null) return;
                        var newdata = {};
                        if (typeof(data.data) == 'object') {
                            var dkey = Object.keys(data.data)[0];
                            newdata[dkey] = data.data[dkey];
                        } else data.data.forEach(function(d) {
                            var dkey = Object.keys(data.data)[0];
                            newdata[dkey] = d[dkey];
                        });
                        data.data = newdata;
                        $scope.staffs.forEach(function(s) {
                            console.log(data.data, data.data[s.user_email])
                            if (typeof(data.data[s.user_email]) == 'undefined') $scope.deletestaff(s);
                        });
                        $scope.user_errors = data.data;
                        if (!$scope.$$phase)
                            $scope.$apply();
                    }
                },
                error: function(data) {
                    $("#spinner-signup").hide();
                    $("#signup-text").show();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message, "An Error Occured!");
                    }, 1300);
                }
            });
        } else {
            if (confirm("You have some data not added to the list \n Proceed anyway?")) {
                $.ajax({
                    method: "POST",
                    url: api + "/admin/createstaffs?key=" + $scope.user.user_access_token,
                    data: { 'users_list': $scope.staffs },
                    contentType: "application/x-www-form-urlencoded",
                    success: function(data, status) {
                        if (data.status == true) {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.success(data.message);
                            $scope.staffs = [];
                        } else {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.error(data.message, "An Error Occured!");

                            if (data.data == null) return;

                            var newdata = {};
                            if (typeof(data.data) == 'object') {
                                var dkey = Object.keys(data.data)[0];
                                newdata[dkey] = data.data[dkey];
                            } else data.data.forEach(function(d) {
                                var dkey = Object.keys(data.data)[0];
                                newdata[dkey] = d[dkey];
                            });
                            data.data = newdata;
                            $scope.staffs.forEach(function(s) {
                                console.log(data.data, data.data[s.user_email])
                                if (typeof(data.data[s.user_email]) == 'undefined') $scope.deletestaff(s);
                            });
                            $scope.user_errors = data.data;
                            if (!$scope.$$phase)
                                $scope.$apply();
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        }
                    },
                    error: function(data) {
                        $("#spinner-signup").hide();
                        $("#signup-text").show();
                        setTimeout(function() {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.error(data.message, "An Error Occured!");
                        }, 1300);
                    }
                });
            } else {
                return;
            }
        }
    }
    $scope.signUp = function(user) {
        var user = user;
        $("#spinner-signup").show();
        $("#signup-text").hide();
        $.ajax({
            method: "POST",
            url: api + "/account/signup?key=" + $scope.user.user_access_token,
            data: user,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status) {
                if (data.status == true) {
                    console.log("Successful");
                    $("#signup-text").show();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message, user.user_email + " was Successful!");
                    }, 1300);
                    $.ajax({
                        method: "POST",
                        url: api + "/admin/mailsender?key=" + $scope.user.user_access_token,
                        data: {
                            sender_email: "noreply@ribypeerlending.me",
                            receiver_email: user.user_email,
                            subject: user.company_name + ": Riby Peer Lending Sign Up",
                            message: "Email: " + user.user_email + ", Password: " + user.user_password + ", Company: " + user.company_name
                        },
                        contentType: "application/x-www-form-urlencoded",
                        success: function(data, status) {
                            if (data.status == true) {
                                $("#spinner-signup").hide();
                                setTimeout(function() {
                                    toastr.options = {
                                        closeButton: true,
                                        debug: false,
                                        positionClass: "toast-top-right",
                                        onclick: null,
                                        showDuration: 1000,
                                        hideDuration: 1000,
                                        timeOut: 5000,
                                        extendedTimeOut: 1000,
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut"
                                    };
                                    toastr.success(data.message, "Email sent Successfully!");

                                }, 1300);
                            } else {
                                $("#spinner-signup").hide();
                                $("#signup-text").show();
                                console.log(data.message + " An Error Occured!")
                            }
                        },
                        error: function(data) {
                            $("#spinner-signup").hide();
                            $("#signup-text").show();
                            console.log(data.message + " An Error Occured!")
                        }
                    });
                } else {
                    $("#spinner-signup").hide();
                    $("#signup-text").show();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message, "An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-signup").hide();
                $("#signup-text").show();
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message, "An Error Occured!");

                }, 1300);
            }
        });
    }
}]);