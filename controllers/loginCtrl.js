/**
 * Created by Ukemeabasi on 02/08/2016.
 */

'use strict';

app.controller('loginCtrl', ['$scope', '$http', 'api', function($scope, $http, api){
    $("#owl-demo").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        navigation:false,
        autoPlay: true
    });

    $("#spinner-login").hide();
    $("#login-text").show();
    $scope.login = function (client) {
        $("#spinner-login").show();
        $("#login-text").hide();
        $.ajax({
            method: "POST",
            url: api+"/login/login?key=92215a06d3349532a5669db8dacb1b06",
            data: client,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    var user = data.data;
                    $.ajax({
                        method: "GET",
                        url: api+"/user/getuserdetail?key="+data.data.user_access_token,
                        contentType: "application/x-www-form-urlencoded",
                        success: function(data, status){
                            if (data.status == true) {
                                $("#spinner-login").hide();
                                $("#login-text").show();
                                var loginDetails = {"user":user, "profile":data.data};
                                window.localStorage.setItem("loginDetails",JSON.stringify(loginDetails));
                                setTimeout(function () {
                                    toastr.options = {
                                        closeButton: true,
                                        debug: false,
                                        positionClass: "toast-top-right",
                                        onclick: null,
                                        showDuration: 1000,
                                        hideDuration: 1000,
                                        timeOut: 5000,
                                        extendedTimeOut: 1000,
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut"
                                    };
                                    toastr.success(data.message,"Login was Successful!");

                                }, 1300);
                                switch (user.user_type_id){
                                    case 1:{
                                        window.location.href = "#/admin/dashboard"; break;
                                    }
                                    case 2:{
                                        window.location.href = "#/transaction"; break;
                                    }
                                    case 3:{
                                        window.location.href = "#/dashboard"; break;
                                    }
                                }
                            }
                            else{
                                $("#spinner-login").hide();
                                $("#login-text").show();
                                setTimeout(function () {
                                    toastr.options = {
                                        closeButton: true,
                                        debug: false,
                                        positionClass: "toast-top-right",
                                        onclick: null,
                                        showDuration: 1000,
                                        hideDuration: 1000,
                                        timeOut: 5000,
                                        extendedTimeOut: 1000,
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut"
                                    };
                                    toastr.error(data.message,"An Error Occured!");

                                }, 1300);
                            }
                        },
                        error: function(data) {
                            $("#spinner-login").hide();
                            $("#login-text").show();
                            setTimeout(function () {
                                toastr.options = {
                                    closeButton: true,
                                    debug: false,
                                    positionClass: "toast-top-right",
                                    onclick: null,
                                    showDuration: 1000,
                                    hideDuration: 1000,
                                    timeOut: 5000,
                                    extendedTimeOut: 1000,
                                    showEasing: "swing",
                                    hideEasing: "linear",
                                    showMethod: "fadeIn",
                                    hideMethod: "fadeOut"
                                };
                                toastr.error(data.message,"An Error Occured!");

                            }, 1300);
                        }
                    });
                }
                else{
                    $("#spinner-login").hide();
                    $("#login-text").show();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-login").hide();
                $("#login-text").show();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        });
    }
}]);