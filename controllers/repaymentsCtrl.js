'use strict';

app.controller('repaymentsCtrl', ['$scope', '$rootScope', '$http','$location', 'api', function($scope, $rootScope, $http,$location, api){
  $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
  $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

  $http({method: 'GET', url:api+'/admin/getallrepaymentschedule?key='+$scope.user.user_access_token})
      .success( function( data )
      {
          if(data.status == true){
              $scope.company_admin_repayments = data.data;
          }
          else{
             // console.log(data)
          }
      })
      .error( function( data )
      { console.log(data) });

  $scope.declineRepaymentModalClose = function(){
    $('#decline-repayment-modal').modal('hide');
    $('.action').show();
    $('#action-spinner').hide();
  }
  var base_url_encode = api.split('api');
  $scope.base_url = base_url_encode[0];

  $scope.previewRepaymentSchedule = function(loan_note_file_path){
    var url = 'https://docs.google.com/viewer?url='+$scope.base_url+'api/web/loannotes/'+loan_note_file_path;
    var win = window.open(url, '_blank');
    win.focus();
  }

  $scope.decline = {};
  $scope.selected_repayment_id;

  $scope.confirmKegowPay = function(repayment_schedule_id, index, type, amount, payment_type){
    $scope.selected_repayment_id = repayment_schedule_id;
    if (type == 'approve') {
      $('.action').hide();
      $('#action-spinner').show();
      var approve_data = {};
      approve_data.repayment_schedule_id = repayment_schedule_id;
      approve_data.amount = amount;
      approve_data.payment_type = payment_type;
      $.ajax({
          method: "POST",
          url: api+"/admin/completerepaymentviakegow?key="+$scope.user.user_access_token,
          data: approve_data,
          contentType: "application/x-www-form-urlencoded",
          success: function(data, status){
              if (data.status == true) {
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.success(data.message,"Payment Queued, You will be redirected Shortly");

                }, 1300);
                // $location.path().replace();
                window.location.replace(data.data);

              }
              else {
                $('.action').show();
                $('#action-spinner').hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"Am error Occured!");

                }, 1300);
              }
            }
          })
    }
  }
  $scope.authenticate = function(repayment_schedule_id, index, type, amount){
    $scope.selected_repayment_id = repayment_schedule_id;
    if (type == 'approve') {
      $('.action').hide();
      $('#action-spinner').show();
      var approve_data = {};
      approve_data.repayment_schedule_id = repayment_schedule_id;
      approve_data.amount = amount;
      $.ajax({
          method: "POST",
          url: api+"/admin/completerepaymentviakegow?key="+$scope.user.user_access_token,
          data: approve_data,
          contentType: "application/x-www-form-urlencoded",
          success: function(data, status){
              if (data.status == true) {
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.success(data.message,"Payment Queued, You will be redirected Shortly");

                }, 1300);
                // $location.path().replace();
                window.location.replace(data.data);

              }
              else {
                $('.action').show();
                $('#action-spinner').hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"Am error Occured!");

                }, 1300);
              }
            }
          })
    }
    if (type == 'reject') {
      $('#decline-repayment-modal').modal('show');
      $('.action').hide();
      $('#action-spinner').show();

    }
  }

  // $scope.transaction_state = $scope.getCookie('repayment_transaction_transfer_state');
  // console.log('Cookie is', $scope.transaction_state);
  // $.session.set('repayment_transaction_transfer_state', 'Williams')
    if ($location.hash() == 1) {
      setTimeout(function () {
          toastr.options = {
              closeButton: true,
              debug: false,
              positionClass: "toast-top-right",
              onclick: null,
              showDuration: 1000,
              hideDuration: 1000,
              timeOut: 5000,
              extendedTimeOut: 1000,
              showEasing: "swing",
              hideEasing: "linear",
              showMethod: "fadeIn",
              hideMethod: "fadeOut"
          };
          toastr.success("Your Payment Was Successful!");

      }, 1300);
      // window.open('#/repayments');
    }

    if ($location.hash() == 2) {
      setTimeout(function () {
          toastr.options = {
              closeButton: true,
              debug: false,
              positionClass: "toast-top-right",
              onclick: null,
              showDuration: 1000,
              hideDuration: 1000,
              timeOut: 5000,
              extendedTimeOut: 1000,
              showEasing: "swing",
              hideEasing: "linear",
              showMethod: "fadeIn",
              hideMethod: "fadeOut"
          };
          toastr.info("An error Occured, Please Contact Admin!");

      }, 1500);
      // window.open('#/repayments');
    }

    if ($location.hash() == 3) {
      setTimeout(function () {
          toastr.options = {
              closeButton: true,
              debug: false,
              positionClass: "toast-top-right",
              onclick: null,
              showDuration: 1000,
              hideDuration: 1000,
              timeOut: 5000,
              extendedTimeOut: 1000,
              showEasing: "swing",
              hideEasing: "linear",
              showMethod: "fadeIn",
              hideMethod: "fadeOut"
          };
          toastr.error("Payment Was not Successful, Please Try Again!");

      }, 1500);
      // window.open('#/repayments');
    }
  $scope.completeDecline = function (decline){
    decline.repayment_schedule_id = $scope.selected_repayment_id;
    $.ajax({
        method: "POST",
        url: api+"/admin/declinerepaymentschedule?key="+$scope.user.user_access_token,
        data: decline,
        contentType: "application/x-www-form-urlencoded",
        success: function(data, status){
            if (data.status == true) {
                $("#spinner-decline-detail").hide();
                $("#decline-repayment-modal").modal("hide");
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $scope.$apply();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.success(data.message,"Decline Successful!");

                }, 1300);
                window.reload();
            }
            else{
                $("#spinner-decline-detail").hide();
                $("#decline-button").show();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"Decline Unsuccessful!");

                }, 1300);
            }
        },
        error: function(data) {
            $("#spinner-decline-detail").hide();
            $("#decline-button").show();
            setTimeout(function () {
                toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass: "toast-top-right",
                    onclick: null,
                    showDuration: 1000,
                    hideDuration: 1000,
                    timeOut: 5000,
                    extendedTimeOut: 1000,
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut"
                };
                toastr.error(data.message);

            }, 1300);
        }
    });
}
}]);
