/**
 * Created by Ukemeabasi and Sunmonu-Adedeji Olawale on 08/08/2016.
 */
'use strict';

app.controller('staffCtrl', ['$scope', '$http', 'api', function($scope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $(".table-data").hide();
    $(".loader").show();
    //gets all registered staffs
    $http({method: 'GET', url:api+'/admin/fetchstaffincompany?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.staffs = data.data.filter(function(item,index,array){
                    return item.user_type_id == 3;
                })
                //console.log("Te staffs are",$scope.staffs);
                $scope.adminstrators = data.data.filter(function(item,index,array){
                    return item.user_type_id == 2 || item.user_type_id == 6 || item.user_type_id == 7;
                })
                $(".table-data").show();
                $(".loader").hide();
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });
    

    $scope.deleteStaff = function (staff_id,index) {
       //console.log("I dey here");
       //eturn;
        $(".staff-actions-"+index).hide();
        $(".staff-spinner-"+index).show();
        $.ajax({
            method: "DELETE",
            url: api+"/admin/deleteuser/"+staff_id+"?key="+$scope.user.user_access_token,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Staff Successfully Deleted!");
                        // console.log("i'm here");
                    }, 1300);
                    $(".staff-actions-"+index).hide();
                    $(".staff-spinner-"+index).hide();
                    $scope.staffs.splice(index,1);
                    $scope.$apply();
                }
                else{
                    $("#spinner-signup").hide();
                    $("#signup-text").show();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");
                        console.log(data);

                    }, 1300);
                    $(".staff-actions-"+index).show();
                    $(".staff-spinner-"+index).hide();
                }
            },
            error: function(data) {
                $("#spinner-signup").hide();
                $("#signup-text").show();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
                $(".staff-actions-"+index).show();
                $(".staff-spinner-"+index).hide();
            }
        });
    }

}]);

app.controller('staffDetailsCtrl', ['$scope', '$http', 'api', '$stateParams', function($scope, $http, api, $stateParams){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $(".table-data").hide();
    $(".loader").show();
    //gets staff's details
    $http({method: 'GET', url:api+'/admin/fetchstaffdetails/'+$stateParams.id+'?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.staff = data.data[0];
                //console.log("The staff is hee",$scope.staff);
                $(".table-data").show();
                $(".loader").hide();
            }
            else{
                console.log('Error fetching staff details ',data)
            }
        })
        .error( function( data )
        { console.log(data) });


    $(".table-data1").hide();
    $(".loader1").show();
    //gets list of all borrowed requests
    $http({method: 'GET', url:api+'/admin/staffloanhistory/'+$stateParams.id+'?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.borrows = data.data;
                $(".table-data1").show();
                $(".loader1").hide();
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $(".table-data2").hide();
    $(".loader2").show();
    //gets list of all staff lend requests
    $http({method: 'GET', url:api+'/admin/stafflendhistory/'+$stateParams.id+'?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.lends = data.data;
                $(".table-data2").show();
                $(".loader2").hide();
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

}]);