/**
 * Created by Ukemeabasi on 04/09/2016.
 */
'use strict';

app.controller('request-inviteCtrl', ['$scope', '$http', 'api', function($scope, $http, api){

  $scope.request_invite = function(){
    $('.btn-request-access').hide();
    $('.btn-spinner').show();
		var data = {};
    data.email = $scope.email;
		data.phone = $scope.phone;
    data.fullname = $scope.fullname;
		data.company_name = $scope.company_name;
    data.hr_email = $scope.hr_email;
		data.hr_phone = $scope.hr_phone;

		$.ajax({
            method: "POST",
            url: api+"/contact/requestinvite",
            data: data,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status) {
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Your Request Invite has been sent!");

                    }, 1300);
                    var base_url = api.split('/api');
                    window.location.replace(base_url[0]);
                }
                else{
       				setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An error has occured!");

                    }, 1300);
                    $('.btn-request-access').show();
                    $('.btn-spinner').hide();

                }
            },
            error: function(data) {
               setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An error has occured!");

                    }, 1300);
                    $('.btn-request-access').show();
                    $('.btn-spinner').hide();


            }
        });
	}

}]);
