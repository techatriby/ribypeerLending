/**
 * Created by Ukemeabasi on 04/09/2016.
 */
'use strict';

app.controller('external_transactionsCtrl', ['$scope', '$http', 'api','$window', function($scope, $http, api,$window){
  $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
  $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

  $scope.transaction = {};
  $scope.selectTransactionType = function(){
    if ($scope.transaction.type == 1) {
      // disbursements
      $scope.selectRepayType = function(){

        if ($scope.transaction.repaymentType == 1) {
          var id = 'CARD TRANSFER'
          var dataObject = {};
          dataObject.transaction_type = id;
          $.ajax({
            method:"POST",
            url:api+"/superadmin/getexternaltransactionsdetailsbytype?key="+$scope.user.user_access_token,
            data:dataObject,
            contentType:"application/x-www-form-urlencoded",
            success:function(data,status){
              if(data.status == true){
                $scope.external_transactions = data.data
                if(!$scope.$$phase) {
                  $scope.$apply();
                }
              }
            }
          })           // $scope.$apply();
        }
        else if ($scope.transaction.repaymentType == 2) {
          var id = 'BANK TRANSFER'
          var dataObject = {};
          dataObject.transaction_type = id;
          $.ajax({
            method:"POST",
            url:api+"/superadmin/getexternaltransactionsdetailsbytype?key="+$scope.user.user_access_token,
            data:dataObject,
            contentType:"application/x-www-form-urlencoded",
            success:function(data,status){
              if(data.status == true){
                $scope.external_transactions = data.data
                if(!$scope.$$phase) {
                  $scope.$apply();
                }
              }
            }
          })           // $scope.$apply();
        }
      }
    }
    else if ($scope.transaction.type == 2) {
      // repayments
      var id = 'CARD WITHDRAW'
      // $scope.external_transactions = $.grep($scope.external_transactions, function(e) { return e.transaction_type == id });
      var dataObject = {};
      dataObject.transaction_type = id;
      $.ajax({
        method:"POST",
        url:api+"/superadmin/getexternaltransactionsdetailsbytype?key="+$scope.user.user_access_token,
        data:dataObject,
        contentType:"application/x-www-form-urlencoded",
        success:function(data,status){
          if(data.status == true){
            $scope.external_transactions = data.data
            if(!$scope.$$phase) {
              $scope.$apply();
            }
          }
        }
      })      // $scope.$apply();
    }
  }

  $http({method: 'GET', url:api+'/superadmin/getexternaltransactionsdetails?key='+$scope.user.user_access_token})
  .success( function(data)
  {
    if(data.status == true){
      $scope.external_transactions = data.data
    }
  })

  $scope.$watch('external_transactions', function(){
    $scope.external_transactions = $scope.external_transactions;
  })

  // $scope.$digest();
}]);
