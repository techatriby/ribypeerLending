/**
 * Created by Ukemeabasi on 25/08/2016.
 */

'use strict';

app.controller('dashboardCtrl', ['$scope', '$http','api','$window', function($scope, $http, api,$window){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;
    $scope.total_notifications=$scope.total_borrowers=$scope.total_lenders=$scope.total_staffs =
    $scope.total_lent=$scope.total_borrowed=$scope.lent_count=$scope.borrowed_count=0;

    $("#spinner-company-reg").show()
    //gets all registered companies
    $http({method: 'GET', url:api+'/superadmin/totaluserspercompany?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $("#spinner-company-reg").hide();
                $scope.total_companies = (data.data).length;
                //console.log("The Company data are ",data.data);
                $scope.company_lists = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $http({method: 'GET', url:api+'/superadmin/totallendandborrowbymonth?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
              var x = JSON.parse(data.data);
              console.log(x.borrow_x);
              $scope.borrow_x = x.borrow_x;
              $scope.borrow_y = x.borrow_y;
              $scope.lend_x = x.lend_x;
              $scope.lend_y = x.borrow_y;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });


    //*gets total amount lent
    $http({method: 'GET', url:api+'/superadmin/getalllenders?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.lend_histories = data.data;
                $scope.lent_count = (data.data).length;
                //console.log("The ")
                $scope.total_lent = 0;
                for (var i=0; i< ($scope.lend_histories).length; i++){
                    $scope.total_lent = $scope.total_lent + parseInt($scope.lend_histories[i].lend_amount);
                }
            }
        })
        .error( function( data )
        { console.log(data) });

    //Total Declined
    $http({method:'GET',url:api+'/superadmin/totaldeclines?key='+$scope.user.user_access_token})
    	.success(function(data){
            //console.log("the data for declines are ",data);
    		if(data.status == true){
    			//console.log("Total declines are ",data);
    			//console.log("Total declines are ",data.amount);
    			$scope.total_declined_number = data.data;
   				$scope.total_declined_amount = data.amount;

    			//console.log("the data for declines are ",data);
                //console.log("the data for declines amount are ",data.Amount);
    			//$scope.total_declined = data.data;
                //$scope.total_declined_amount = data.Amount;

    		}
    	})
    	.error(function(data){
    		console.log("Error from declines ",data);
    	});
    //total Lenders
    $http({method:'GET',url:api+'/superadmin/totallenders?key='+$scope.user.user_access_token})
    	.success(function(data){
    		if(data.status == true){
    			//console.log("Total Lends are ",data);
    			$scope.totallends = data.data;
    		}
    	})
    //*gets total amount borrowed
    /*$http({method: 'GET', url:api+'/superadmin/getallrequest?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.borrowed_histories = $.grep(data.data, function (e) {return e.approve_flag == 2});
                $scope.borrowed_count = ($scope.borrowed_histories).length;
                $scope.total_borrowed = 0;
                for (var i=0; i< ($scope.borrowed_histories).length; i++){
                    $scope.total_borrowed = $scope.total_borrowed + parseInt($scope.borrowed_histories[i].loan_amount);
                }
            }
        })
        .error( function( data )
        { console.log(data) });*/

    // gets total borrowed api/vi/superadmin/totalamountborrowed?key =
    $http({method: 'GET', url:api+'/superadmin/totalamountborrowed?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_borrowed = data.data;
                //console.log('Total borrowed is ',$scope.total_borrowed);
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    // //gets total requested
    // $http({method: 'GET', url:api+'/admin/totalamountrequested?key='+$scope.user.user_access_token})
    //     .success( function( data )
    //     {
    //         if(data.status == true){
    //             $scope.total_requested = data.data;
    //         }
    //         else{
    //             console.log(data)
    //         }
    //     })
    //     .error( function( data )
    //     { console.log(data) });

    //gets total staffs
    $http({method: 'GET', url:api+'/superadmin/totalusers?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_staffs = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //gets total lenders
    $http({method: 'GET', url:api+'/superadmin/totallenders?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_lenders = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //gets total borrowers
    $http({method: 'GET', url:api+'/superadmin/totalborrowers?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
            	//console.log("Total borrowers are ",data.data);
                $scope.total_borrowers = data.data;
            }
            else{
                console.log("Error from feching borrowers ",data);
            }
        })
        .error( function( data )
        { console.log(data)
        	console.log("Error from borrowers is ",data);
        });

    $("#spinner-company-users").show();
    //gets total users per company
    $http({method: 'GET', url:api+'/superadmin/totaluserspercompany?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $("#spinner-company-users").hide();
                $scope.company_users = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //gets all latest notifications
    $http({method: 'GET', url:api+'/superadmin/getallpendingrequest?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_notifications = (data.data).length;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.graphLoan = function () {
        $("#graph-spinner").show();
        $("#graph").hide();
        // generates chart report for total agent transactions per month
        var url = api+'/superadmin/loanpermonth?key='+$scope.user.user_access_token;
        $http({method: 'GET', url:url})
            .success( function( data )
            {
                if(data.status == 1){
                    var graph_loan = data.data;

                    $("#graph-spinner").hide();
                    $("#graph").show();
                }
                else{
                    console.log(data)
                }
            })
            .error( function( data )
            {
                console.log(data)
            });
    }
    $scope.graphLoan();
}]);
