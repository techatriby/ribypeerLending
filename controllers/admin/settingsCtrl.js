/**
 * Created by Ukemeabasi and ssg315ass@yahoo.com on 25/08/2016.
 */

'use strict';

app.controller('settingsCtrl', ['$scope', '$http', 'api', function($scope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    //console.log('the user object is ',$scope.user);
    $scope.superadminlevels = [{id:4,level:"SuperAdminCreator"},{id:5,level:"SuperAdminViewer"}];
    $(".fa-spinner").hide();
    //console.log("The api is ",api);
    $scope.addUser = function(adminUser){
        //console.log('The user form is ',adminUser);
        $.ajax({
            method: "POST",
            url: api+"/superadmin/createsuperadmin?key="+$scope.user.user_access_token,
            data: adminUser,
            contentType: "application/x-www-form-urlencoded",
            success:function(data,status){
                if(data.status == true){
                    $scope.adminUser = '';
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"New Admin User created Successfully!");
                    }, 1300);
                }else{
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");
                    }, 1300);
                }
            },error: function(data) {
                //$("#spinner-login").hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        })
    }
    $scope.addPurpose = function(purpose){
        //console.log('Purpose to be added is ',purpose);
        $.ajax({
            method:'POST',
            url:api+'/superadmin/setborrowreason?key='+$scope.user.user_access_token,
            data:purpose,
            contentType:'application/x-www-form-urlencoded',
            success:function(data,status){
                if(data.status){
                    $scope.purpose = '';
                    //Update scope if function run outside of scope
                        if(!$scope.$$phase) {
                            //console.log('Calling apply');
                            $scope.$apply();
                        }
                       setTimeout(function () {

                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Purpose added Successfully!");
                        if(!$scope.$$phase) {
                            //console.log('Calling apply');
                            $scope.$apply();
                        }

                    }, 1300);
                }else{
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");
                    }, 1300);
                }
            },error:function(data){
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        })
        
    }
    //gets all reasons 
    $http({method:'GET',url:api+'/superadmin/getborrowreason?key='+$scope.user.user_access_token})
        .success(function(data){
            //console.log('Data for success is ',data.data);
            $scope.reasons = data.data;
        })
        .error(function(data){
            console.log("Error from fetching reasons are ",data);
        })
    //gets all access levels for superadmin
    /*$http({method: 'GET', url:api+'/superadmin/getaccesslevels?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            console.log("Data for access levels are ",data);
        })
        .error( function( data )
        { console.log("Error for access levels are ",data) });*/

}]);