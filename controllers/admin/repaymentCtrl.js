/**
* Created by Ukemeabasi and Sunmonu-Adedeji Olawale on 09/09/2016.
*/

'use strict';

app.controller('repaymentCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
  $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
  $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

  $(".loader").show();
  //gets all latest repayments
  $http({method: 'GET', url:api+'/superadmin/getallrequest?key='+$scope.user.user_access_token})
  .success( function( data )
  {
    if(data.status == true){
      $(".loader").hide();
      $scope.repayments = data.data;
      console.log("repayments are ",$scope.repayments);
      //$scope.repayments = [];
      for (var i=0; i<$scope.repayments.length; i++){
        //$(".loader").hide();
        var repayperiod = Number($scope.repayments[i].period);
        $scope.repayments[i].repaymonths = [];
        console.log("Period is ",repayperiod);
        for(var j=1;j<=repayperiod;j++){
          $scope.repayments[i].repaymonths.push(j);
        }
        //var tenure = $scope.tenure(repayments[i].date_of_request,repayments[i].date_to_pay);
        /*var months = [];
        for (var j=1; j<=tenure; j++){
        months.push({id:j})
      }
      $scope.repayments.push({data:repayments[i], months:months});*/
    }
    $scope.repayments.forEach(function(item,index,array){
      //console.log('Transaction id is ',item.transaction_id);

      $http({method:'GET',url:api+'/superadmin/checkborrowrepaymentstatus/'+item.transaction_id+'?key='+$scope.user.user_access_token})
      .success(function(data){
        if(data.status == true){
          item.repaymentdetails = data.data;
          console.log("The data gotten from the transaction id is ",data.data);
          //$scope.repaymentOptions = data.data;
        }
      })
      .error(function(data){
        console.log("Error is ",data);
      })
      console.log('item is ',item);
    })
  }
  else{
    console.log(data)
  }
})
.error( function( data )
{ console.log(data) });


$http({method:'GET',url:api+'/superadmin/getrepaymentoptions?key='+$scope.user.user_access_token})
.success(function(data){
  if(data.status == true){
    console.log("The data retreived is ",data.data);
    $scope.repaymentOptions = data.data;
  }
})
.error(function(data){
  console.log("Error is ",data);
})

$scope.sort = function(keyname){
  $scope.sortKey = keyname;   //set the sortKey to the param passed
  $scope.reverse = !$scope.reverse; //if true make it false and vice versa
}
$scope.pageSize = 10;

// set sidebar closed and body solid layout mode
// $rootScope.settings.layout.pageBodySolid = true;
// $rootScope.settings.layout.pageSidebarClosed = false;

$scope.disbursed  = function (data) {
  //console.log("Filter ",data);
  return data.approve_flag == 3;
}

Array.prototype.remove = function() {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
    what = a[--L];
    while ((ax = this.indexOf(what)) !== -1) {
      this.splice(ax, 1);
    }
  }
  return this;
};

$scope.repaymentModalClose = function(){
  $("#pay-action").show();
  $("#pay-spinner").hide();
}

var containsObject = function (obj, list) {
  var i;
  for (i = 0; i < list.length; i++) {
    if (list[i] === obj) {
      // return callback(true);
      return true;
    }
  }

  // return callback(false);
  return false;
}
$scope.items = [];
var companyValueCompare = function (value){
  if ($scope.items.length) {
    if($scope.items[0].company_id == value){
      return true;
    }
    else{
      return false
    }
  }
  else {
    return true;
  }
}
$scope.checkBox = function(borrow_id, repayment_amount, index, firstname, lastname, company_id, company_name, transaction_id){
  if (companyValueCompare(company_id)) {
    var repayment_month = index+1;
    var newItems = {borrow_id: borrow_id,transaction_id: transaction_id, month: repayment_month, payment_amount: repayment_amount, firstname: firstname, lastname: lastname,company_id: company_id, company_name: company_name};
    var check = ($.grep($scope.items, function(element) {
      return element.borrow_id == newItems.borrow_id;
    }))[0];
    function arrayObjectIndexOf(myArray, searchTerm, property) {
      for(var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) return i;
      }
      return -1;
    }
    console.log("First Check:", check);
    if (check == undefined) {
      $scope.items.push(newItems);
    }
    else {
      var index = arrayObjectIndexOf($scope.items, newItems.borrow_id, borrow_id);
      $scope.items.splice(index,1);
    }
    // $scope.items = ItrrItems;
    // console.log(ItrrItems)
    console.log('Items Scope is', $scope.items);
    console.log("Second Check:", check);
  }
  else {
    var elemClass = ".checkbox-"+borrow_id+'-'+index;
    $(elemClass).prop( "checked", false );
    setTimeout(function() {
      toastr.options = {
        closeButton: true,
        debug: false,
        positionClass: "toast-top-right",
        onclick: null,
        showDuration: 1000,
        hideDuration: 1000,
        timeOut: 5000,
        extendedTimeOut: 1000,
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
      };
      toastr.error("You cannot select staffs form multiple companies");
    }, 1300);
  }

}
$scope.payNow = function (Objitems) {
  $("#repaymodal").modal("show");

  for (var i = 0; i < Objitems.length; i++){
    console.log(Objitems[i]);
  }
}


$scope.repayd = function(Objitems){
  $("#repaymodal").modal("show");
  $("#pay-action").hide();
  $("#pay-spinner").show();
  function sumArray(a, b) {
    return a + b;
  }

  $scope.totalPay = function(Objitems){
    var payStack = [];
    for (var i = 0; i < Objitems.length; i++){
      payStack.push(parseFloat(Objitems[i].payment_amount));
    }
    var totalSum = payStack.reduce(sumArray, 0);
    return totalSum;
  }

  $scope.confirmrepay = function(payment_option){
    var dataObject = {};
    dataObject.staffs_array = $scope.items;
    $.ajax({
      method:"POST",
      url:api+"/superadmin/generaterepayschedule?key="+$scope.user.user_access_token,
      data:dataObject,
      contentType:"application/x-www-form-urlencoded",
      success:function(data,status){
        if(data.status == true){
          for (var i = 0; i < Objitems.length; i++){
            console.log('The amount repaid is ',Objitems[i].payment_amount);
            console.log("The payment object is ",payment_option,"and refernce_id ",$scope.referenceid);
            var postObject = {};
            let firstname = Objitems[i].firstname;
            let lastname = Objitems[i].lastname;
            postObject.repayment_amount = Objitems[i].payment_amount;
            postObject.payment_option_id = payment_option.payment_option_id;
            postObject.reference_id = $scope.referenceid;
            postObject.month = Objitems[i].month;
            postObject.payment_description = payment_option.payment_option_name;
            postObject.staffs_array = $scope.items;
            $.ajax({
              method:"POST",
              url:api+"/superadmin/confirmloanrepayment/"+Objitems[i].borrow_id+"?key="+$scope.user.user_access_token,
              data:postObject,
              contentType:"application/x-www-form-urlencoded",
              success:function(data,status){
                if(data.status == true){
                  /*$(".staff-actions-"+index).hide();
                  $(".staff-spinner-"+index).hide();
                  $scope.disbursements.splice(index,1);*/
                  $("#repaymodal").hide();
                  $scope.$apply();
                  setTimeout(function() {
                    toastr.options = {
                      closeButton: true,
                      debug: false,
                      positionClass: "toast-top-right",
                      onclick: null,
                      showDuration: 1000,
                      hideDuration: 1000,
                      timeOut: 5000,
                      extendedTimeOut: 1000,
                      showEasing: "swing",
                      hideEasing: "linear",
                      showMethod: "fadeIn",
                      hideMethod: "fadeOut"
                    };
                    toastr.success(data.message,firstname+" "+lastname+" Loan Repayment Confirmed");
                  }, 1300);
                  setTimeout(function(){
                    window.location.reload(true);
                  }, 5000);
                }else{
                  $("#repaymodal").modal("show");
                  $("#pay-action").show();
                  $("#pay-spinner").hide();
                  setTimeout(function () {
                    toastr.options = {
                      closeButton: true,
                      debug: false,
                      positionClass: "toast-top-right",
                      onclick: null,
                      showDuration: 1000,
                      hideDuration: 1000,
                      timeOut: 5000,
                      extendedTimeOut: 1000,
                      showEasing: "swing",
                      hideEasing: "linear",
                      showMethod: "fadeIn",
                      hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");
                  }, 1300);
                }
              },error:function(data){
                $("#repaymodal").modal("show");
                $("#pay-action").show();
                $("#pay-spinner").hide();
                setTimeout(function () {
                  toastr.options = {
                    closeButton: true,
                    debug: false,
                    positionClass: "toast-top-right",
                    onclick: null,
                    showDuration: 1000,
                    hideDuration: 1000,
                    timeOut: 5000,
                    extendedTimeOut: 1000,
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut"
                  };
                  toastr.error(data.message,"An Error Occured!");
                }, 1300);
              }
            })

          }
        }
        // else {
        //   $("#repaymodal").modal("show");
        //   $("#pay-action").show();
        //   $("#pay-spinner").hide();
        //   setTimeout(function () {
        //     toastr.options = {
        //       closeButton: true,
        //       debug: false,
        //       positionClass: "toast-top-right",
        //       onclick: null,
        //       showDuration: 1000,
        //       hideDuration: 1000,
        //       timeOut: 5000,
        //       extendedTimeOut: 1000,
        //       showEasing: "swing",
        //       hideEasing: "linear",
        //       showMethod: "fadeIn",
        //       hideMethod: "fadeOut"
        //     };
        //     toastr.error(data.message,"An Error Occured!");
        //   }, 1300);
        // }
      },error:function(data){
        $("#repaymodal").modal("show");
        $("#pay-action").show();
        $("#pay-spinner").hide();
        setTimeout(function () {
          toastr.options = {
            closeButton: true,
            debug: false,
            positionClass: "toast-top-right",
            onclick: null,
            showDuration: 1000,
            hideDuration: 1000,
            timeOut: 5000,
            extendedTimeOut: 1000,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"
          };
          toastr.error(data.message,"An Error Occured!");
        }, 1300);
      }
    })
  }
}

  $scope.repay = function (borrow_id,index,type) {
    $(".staff-actions-"+index).hide();
    $(".staff-spinner-"+index).show();
    $.ajax({
      method: "GET",
      url: api+"/superadmin/"+type+"request/"+borrow_id+"?key="+$scope.user.user_access_token,
      contentType: "application/x-www-form-urlencoded",
      success: function(data, status){
        if (data.status == true) {
          var loan_details = ($scope.repayments)[index];
          $(".staff-actions-"+index).hide();
          $(".staff-spinner-"+index).hide();
          $scope.repayments.splice(index,1);
          $scope.$apply();
          setTimeout(function () {
            toastr.options = {
              closeButton: true,
              debug: false,
              positionClass: "toast-top-right",
              onclick: null,
              showDuration: 1000,
              hideDuration: 1000,
              timeOut: 5000,
              extendedTimeOut: 1000,
              showEasing: "swing",
              hideEasing: "linear",
              showMethod: "fadeIn",
              hideMethod: "fadeOut"
            };
            toastr.info(borrow_data.message,type+" was Successful!");

          }, 1300);
        }
        else{
          $(".staff-actions-"+index).show();
          $(".staff-spinner-"+index).hide();
          setTimeout(function () {
            toastr.options = {
              closeButton: true,
              debug: false,
              positionClass: "toast-top-right",
              onclick: null,
              showDuration: 1000,
              hideDuration: 1000,
              timeOut: 5000,
              extendedTimeOut: 1000,
              showEasing: "swing",
              hideEasing: "linear",
              showMethod: "fadeIn",
              hideMethod: "fadeOut"
            };
            toastr.error(data.message,"An Error Occured!");

          }, 1300);
        }
      },
      error: function(data) {
        $(".staff-actions-"+index).show();
        $(".staff-spinner-"+index).hide();
        setTimeout(function () {
          toastr.options = {
            closeButton: true,
            debug: false,
            positionClass: "toast-top-right",
            onclick: null,
            showDuration: 1000,
            hideDuration: 1000,
            timeOut: 5000,
            extendedTimeOut: 1000,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"
          };
          toastr.error(data.message,"An Error Occured!");

        }, 1300);
      }
    });
  }

  $scope.tenure = function (start,end){
    var startMonth = parseInt(start.substring(5,7));
    var endMonth = parseInt(end.substring(5,7));
    var startYear = parseInt(start.substring(0,4));
    var endYear = parseInt(end.substring(0,4));
    if (startYear != endYear){
      var result = (12-startMonth)+endMonth;
      return result;
    }
    else {
      var result = endMonth-startMonth;
      return result;
    }
  }

  $scope.generatefromtable = function() {
    var data = [], height = 0, doc;
    doc = new jsPDF('p', 'pt', 'a4', true);
    doc.setFont("times", "normal");
    doc.setFontSize(20);
    doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
    doc.setFontSize(12);
    data = [];
    data = doc.tableToJson('editable');
    height = doc.drawTable(data, {
      xstart : 10,
      ystart : 10,
      tablestart : 60,
      marginleft : 10,
      xOffset : 10,
      yOffset : 15
    });
    doc.save("List of "+$scope.issuer.name+" Cards.pdf");
  }
}]);
