/**
 * Created by Ukemeabasi on 25/08/2016.
 */

'use strict';

app.controller('staffCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $(".loader").show();
    //gets all registered companies
    $http({method: 'GET', url:api+'/superadmin/getbankdetails?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $(".loader").hide();
                $scope.staffs = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);