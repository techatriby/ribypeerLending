/**
 * Created by Ukemeabasi on 25/08/2016.
 */

'use strict';

app.controller('lend_historyCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;
    console.log("the user here is ",$scope.user);
    $(".loader").show();

    

    //gets all lend history
    $http({method: 'GET', url:api+'/superadmin/getalllenders?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            $(".loader").hide();
            if(data.status == true){
                $scope.lend_histories = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);


app.controller('lendDetailsCtrl', ['$scope', '$rootScope', '$http', 'api', '$stateParams', function($scope, $rootScope, $http, api, $stateParams){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $(".loader").show();
    //gets all registered companies
    $http({method: 'GET', url:api+'/superadmin/getlatestlenddetails/'+$stateParams.id+'?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $(".loader").hide();
                $scope.loan = data.data;
                debugger;
                $("#spinner_company").show();
                $("#company_info").hide();
                //gets company details
                $http({method: 'GET', url:api+'/company/companieswithid?key='+$scope.user.user_access_token})
                    .success( function( data )
                    {
                        if(data.status == true){
                            $("#spinner_company").hide();
                            $("#company_info").show();
                            $scope.list_companies = data.data;
                            $scope.company = ($.grep($scope.list_companies, function (e) { return e.company_id == $scope.loan.company_id}))[0];
                            //console.log("Company Name is ",$scope.company);
                        }
                        else{
                            console.log(data)
                        }
                    })
                    .error( function( data )
                    { console.log(data) });

                $("#spinner_request").show(); $("#spinner_profile").show();
                $("#request_info").hide(); $("#profile_info").hide();
                //gets staff details
                $http({method: 'GET', url:api+'/superadmin/getstafftransactions/'+$scope.loan.user_id+'?key='+$scope.user.user_access_token})
                    .success( function( data )
                    {
                        if(data.status == true){
                            $("#spinner_request").hide(); $("#spinner_profile").hide();
                            $("#request_info").show(); $("#profile_info").show();
                            $scope.profile = (data.data.user_info)[0];
                            $scope.borrow_histories = data.data.borrow_transact;
                        }
                        else{
                            console.log(data)
                        }
                    })
                    .error( function( data )
                    { console.log(data) });
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);


app.controller('borrow_historyCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $(".loader").show();
    //gets all borrow history
    $http({method: 'GET', url:api+'/superadmin/getallrequest?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            $(".loader").hide();
            if(data.status == true){
                $scope.borrow_histories = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);

app.controller('openLoansCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $(".loader").show();
    //gets all borrow history
    $http({method: 'GET', url:api+'/superadmin/openloans?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            $(".loader").hide();
            if(data.status == true){
                $scope.openloans = data.data;
                $rootScope.openl = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.closeOpenLoan = function(index, id){
        console.log('Clicked')
        $http({method: 'GET', url:api+'/superadmin/closeopenloans?b_id='+id+'&key=' +$scope.user.user_access_token})
        .success( function( data )
        {
            $(".loader").hide();
            if(data.status == true){
               $scope.openloans.splice(index, 1);
               if(!$scope.$$phase){
                   $scope.$digest();
               }
                setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success("Loan Closing was Successful!");

                    }, 1300);
            }
            else{
                console.log(data)
                setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error("Loan Closing was not Successful, Please Try Again!");

                    }, 1300);
            }
        })
        .error( function( data )
        { console.log(data) });
    }

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);


app.controller('borrowDetailsCtrl', ['$scope', '$rootScope', '$http', 'api', '$stateParams', function($scope, $rootScope, $http, api, $stateParams){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $("#spinner_loan").show();
    $("#loan_info").hide();
    //gets loan details
    $http({method: 'GET', url:api+'/superadmin/getlatestrequestdetails/'+$stateParams.id+'?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $("#spinner_loan").hide();
                $("#loan_info").show();
                debugger;
                $scope.loan = data.data;
                $scope.bankDetails = data.data.bank;
                $("#spinner_company").show();
                $("#company_info").hide();
                //gets company details
                $http({method: 'GET', url:api+'/company/companieswithid?key='+$scope.user.user_access_token})
                    .success( function( data )
                    {
                        if(data.status == true){
                            $("#spinner_company").hide();
                            $("#company_info").show();
                            $scope.list_companies = data.data;
                            $scope.company = ($.grep($scope.list_companies, function (e) { return e.company_id == $scope.loan.company_id}))[0];
                        }
                        else{
                            console.log(data)
                        }
                    })
                    .error( function( data )
                    { console.log(data) });

                $("#spinner_request").show(); $("#spinner_profile").show();
                $("#request_info").hide(); $("#profile_info").hide();
                //gets staff details
                $http({method: 'GET', url:api+'/superadmin/getstafftransactions/'+$scope.loan.user_id+'?key='+$scope.user.user_access_token})
                    .success( function( data )
                    {
                        if(data.status == true){
                            $("#spinner_request").hide(); $("#spinner_profile").hide();
                            $("#request_info").show(); $("#profile_info").show();
                            $scope.profile = (data.data.user_info)[0];
                            $scope.borrow_histories = data.data.borrow_transact;
                        }
                        else{
                            console.log(data)
                        }
                    })
                    .error( function( data )
                    { console.log(data) });
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.authenticate = function (borrow_id,type) {
        $(".staff-actions").hide();
        $(".staff-spinner").show();
        $.ajax({
            method: "GET",
            url: api+"/superadmin/"+type+"request/"+borrow_id+"?key="+$scope.user.user_access_token,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    $(".staff-actions").hide();
                    $(".staff-spinner").hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.info(data.message,type+" was Successful!");

                    }, 1300);
                    window.location.href = "#/admin/borrow_history";
                }
                else{
                    $(".staff-actions").show();
                    $(".staff-spinner").hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $(".staff-actions").show();
                $(".staff-spinner").hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        });
    }

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        //doc.setFontSize(10);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(10);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 10
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);
