/**
 * Created by Ukemeabasi and Sunmonu-Adedeji Olawale on 25/08/2016.
 */

'use strict';

app.controller('companyCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;
    $scope.companies;
    $(".loader").show();
    //console.log("The user is ",$scope.user);
    //gets all registered companies
    $http({method: 'GET', url:api+'/company/companieswithid?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $(".loader").hide();
                $scope.companies = data.data;
                //console.log('The companies are ',$scope.companies);
                $scope.waiting_companies = data.data.filter(function(item,index,array){
                	return (item.is_approved == 0);
                })
                //console.log('Companies from db are with those details ',$scope.waiting_companies);
                //get company count 
                $http({method: 'GET', url:api+'/superadmin/totaluserspercompany?key='+$scope.user.user_access_token})
                    .success( function( data )
                    {
                        //console.log("Data from company na ",data);
                        if(data.status == true){
                        	//$scope.companies_check = data.data;
                            //$("#spinner-company-users").hide();
                            //$scope.company_users = data.data;
                            //console.log("the array are ",data.data);
                            //$scope.companies.push(data.data);
                            //console.log("Comapny users are from company ",$scope.companies);
                            for(var i=0;i<data.data.length;i++){
                                $scope.companies[i].company_count = data.data[i].number_of_user;
                            }
                            //console.log("The company user is ",$scope.companies);
                        }
                        else{
                            console.log('I am trying to fetch and error is ',data);
                        }
                    })
                    .error( function( data )
                    { console.log(data) });
            }
            else{
                console.log(data);
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.delete = function(company,index){
    	//console.log('The company about to be deleted is ',company,'and the index is ',index);
    	var data = {};
    	data.company_id = company;
    	$.ajax({
    		method:"POST",
    		url:api+"/superadmin/deletecompany?key="+$scope.user.user_access_token,
    		data:data,
    		contentType:'application/x-www-form-urlencoded',
    		success:function(data,status){
    			if(data.status == true){
    				$scope.companies.splice(index,1);
    				$scope.$apply();
    				setTimeout(function () {
	                    toastr.options = {
	                        closeButton: true,
	                        debug: false,
	                        positionClass: "toast-top-right",
	                        onclick: null,
	                        showDuration: 1000,
	                        hideDuration: 1000,
	                        timeOut: 5000,
	                        extendedTimeOut: 1000,
	                        showEasing: "swing",
	                        hideEasing: "linear",
	                        showMethod: "fadeIn",
	                        hideMethod: "fadeOut"
	                    };
	                    //toastr.info(data.message,type+" was Successful!");
	                    toastr.success("Company has been Deleted");
	                }, 1300);
    			}else{
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        //toastr.info(data.message,type+" was Successful!");
                        toastr.error(data.message ,"Error deleting a company");
                    }, 1300); 
                }
    		},
            error: function(data) {
                console.log('Error data is ',data);
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    //toastr.info(data.message,type+" was Successful!");
                    toastr.error("Error deleting a company");
                }, 1300); 
            }
    	})
    }
    $scope.approve = function(company,index){
    	//console.log('The company about to be approved is ',company,'and the index is ',index);
    	
    	var data = {};
    	data.company_id = company;
    	$.ajax({
    		method:"POST",
    		url:api+"/superadmin/updatecompanyapproved?key="+$scope.user.user_access_token,
    		data:data,
    		contentType:'application/x-www-form-urlencoded',
    		success:function(data,status){
    			if(data.status == true){

    				$scope.waiting_companies.splice(index,1);
	    			$scope.$apply();
	    			setTimeout(function () {
	                    toastr.options = {
	                        closeButton: true,
	                        debug: false,
	                        positionClass: "toast-top-right",
	                        onclick: null,
	                        showDuration: 1000,
	                        hideDuration: 1000,
	                        timeOut: 5000,
	                        extendedTimeOut: 1000,
	                        showEasing: "swing",
	                        hideEasing: "linear",
	                        showMethod: "fadeIn",
	                        hideMethod: "fadeOut"
	                    };
	                    //toastr.info(data.message,type+" was Successful!");
	                    toastr.success("Company has been Approved");
	                }, 1300);
    			}
	    			
    		},
            error: function(data) {
                console.log('Error data is ',data);
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    //toastr.info(data.message,type+" was Successful!");
                    toastr.error("Error approving a company");
                }, 1300); 
            }
    	})
    }

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $("#spinner-company").hide();
    //$("#error-company").hide();
    $scope.addCompany = function (company) {
        $("#spinner-company").show();
        $.ajax({
            method: "POST",
            url: api+"/superadmin/createcompany?key="+$scope.user.user_access_token,
            data: company,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    $("#spinner-company").hide();
                    $("#add_company").modal("hide");
                    $('body').removeClass('modal-open');
                    //console.log('Company data is ',data);
                    company.company_id = data.data.id;
                    $scope.companies.push(company);
                    $scope.$apply();
                    $('.modal-backdrop').remove();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Company created Successfully!");
                    }, 1300);
                }
                else{
                    $("#spinner-company").hide();
                    $scope.company_error = data.message;
                    $scope.$apply();
                    $("#error-company").show();
                    
                }
            },
            error: function(data) {
                //console.log('Error data is ',data);
                $("#spinner-company").hide();
                $scope.company_error = "Please Ensure you fill in all the fields";
                $scope.$apply();
                //$("#spinner-company").hide();
                $("#error-company").show();   
            }
        });
    }

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);


app.controller('companyStaffsCtrl', ['$scope', '$rootScope', '$http', 'api', '$stateParams', function($scope, $rootScope, $http, api, $stateParams){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $(".loader").show();

    

    //gets all registered companies
    $http({method: 'GET', url:api+'/superadmin/getcompanystaffs/'+$stateParams.id+'?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $(".loader").hide();
                $scope.staffs = data.data;
                //console.log("Staffs are ",$scope.staffs);
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //get company count 
    $http({method: 'GET', url:api+'/superadmin/totaluserspercompany?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            console.log("Data from company na ",data);
            if(data.status == true){
                //$("#spinner-company-users").hide();
                //console.log("Comapny users are from company ",data.data);
                $scope.company_users = data.data;
            }
            else{
                console.log('I am trying to fetch ',data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);