/**
 * Created by Ukemeabasi on 04/09/2016.
 */
'use strict';

app.controller('request-inviteCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

  $http({method: 'GET', url:api+'/contact/getallnewrequest?key='+$scope.user.user_access_token})
      .success( function( data )
      {
          if(data.status == true){
              $scope.new_invites = data.data;
          }
          else{
             // console.log(data)
          }
      })
      .error( function( data )
      { console.log(data) });

// console.log($scope.new_invites);
  $scope.auth = function(request_invite, index, state){
    $('.btn-request-access-'+index).hide();
    $('.btn-spinner').show();

		var data = {};
    data.request_invite = request_invite;

    if (state == 'approve') {
      $.ajax({
              method: "POST",
              url: api+"/contact/approveinvite?key="+$scope.user.user_access_token,
              data: data,
              contentType: "application/x-www-form-urlencoded",
              success: function(data, status){
                  if (data.status) {
                    $scope.$apply();
                      setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.success(data.message,"Approve Successful!");

                      }, 1300);
                      // var base_url = api.split('/api');
                      window.location.reload();
                  }
                  else{
                setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.error(data.message,"An error has occured!");

                      }, 1300);
                      $('.btn-request-access'+index).hide();

                      $('.btn-spinner').hide();

                  }
              },
              error: function(data) {
                 setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.error(data.message,"An error has occured!");

                      }, 1300);
                      $('.btn-request-access'+index).hide();

                      $('.btn-spinner').hide();


              }
          });
    }

    else if (state == 'reject') {
      $.ajax({
              method: "POST",
              url: api+"/contact/declineinvite?key="+$scope.user.user_access_token,
              data: data,
              contentType: "application/x-www-form-urlencoded",
              success: function(data, status){
                  if (data.status) {
                      setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.success(data.message,"Approve Successful!");

                      }, 1300);
                      var base_url = api.split('/api');
                      window.location.replace(base_url[0]+"/#/admin/request-invite");
                  }
                  else{
                setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.error(data.message,"An error has occured!");

                      }, 1300);
                      $('.btn-request-access'+index).hide();

                      $('.btn-spinner').hide();

                  }
              },
              error: function(data) {
                 setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.error(data.message,"An error has occured!");

                      }, 1300);
                      $('.btn-request-access'+index).hide();

                      $('.btn-spinner').hide();


              }
          });
    }

    }

}]);
