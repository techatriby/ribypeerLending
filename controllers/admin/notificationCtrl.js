/**
 * Created by Ukemeabasi and ssg315ass@yahoo.com (Sunmonu-Adedeji Olawale) on 25/08/2016.
 */

'use strict';

app.controller('notificationCtrl', ['$scope', '$rootScope', '$http', 'api', function($scope, $rootScope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;
    //console.log('Api is ',api);
    $(".loader").show();

    $http({method:'GET',url:api+'/superadmin/getcompanyadminrepaidloan?key='+$scope.user.user_access_token})
    	.success(function(data){
        console.log('Company Admin Repayed Loans Are:');
        console.log(data);
        $scope.repayments = data.data;
        console.log("the repayments are ",$scope.repayments);
        $scope.$apply();
      })
      .error(function(){
        console.log('An error Occured');
      })
    $scope.transactionRemarks;
    $scope.superAdminReasons;
    $scope.declineReasonschosenn =[];
    //get remarks
    $http({method:'GET',url:api+'/superadmin/gettransactionremarks?key='+$scope.user.user_access_token})
    	.success(function(data){
    		if(data.status == true){
    			//console.log("The transaction remarks are ",data.data);
    			$scope.transactionRemarks = data.data;
    			console.log("remarks are ",$scope.transactionRemarks);
    			$scope.superAdminReasons = $scope.transactionRemarks.filter(function(item,index,array){
    				return (item.remark_id =='4' || item.remark_id == '5');
    			});
    			//console.log("here are the ",$scope.superAdminReasons);

    			//gets all latest notifications
			    $http({method: 'GET', url:api+'/superadmin/getallpendingrequest?key='+$scope.user.user_access_token})
			        .success( function( data )
			        {
			            $(".loader").hide();
			            if(data.status == true){
			            	$scope.notifications = data.data;
			            	console.log("the notifications are ",$scope.notifications);
			            	//$scope.declineArray=[];
			            	$scope.notficationsRefix = $scope.notifications.map(function(item,index,array){
			            		item.declineArray= [];
			            		if(item.process_description){

			            			item.declineArray.push(item.process_description);
			            		}
			            		if(item.process_remarks !== " "){
			            			var gottenReasons = item.process_remarks.split(",");
			            			console.log("The gottenReasons are ",gottenReasons);
			            			console.log("The transaction remarks are ",$scope.transactionRemarks);
			            			$scope.transactionRemarks.filter(function(ite,index,array){
			            				if(gottenReasons.indexOf(ite.remark_id)!=-1){
			            					item.declineArray.push(ite.remark_description);
			            					console.log('decline array just got is ',item.declineArray);
			            				}
			            			})
			            		}
			            		console.log("Item is ",item);
			            	})


			            	/*console.log($scope.notifications[0].process_description);
			            	var description = $scope.notifications[0].process_description;
			            	if($scope.notifications[0].process_description !== " "){
			            		$scope.declineArray.push($scope.notifications[0].process_description);
			            		console.log("I deu here");
			            	}
			                $scope.gottenReasons = data.data[0].process_remarks.split(",");
			                console.log("the reasons are ",$scope.gottenReasons);
			                console.log("In here ",$scope.transactionRemarks);
			                $scope.declineReasons=$scope.transactionRemarks.filter(function(item,index,array){
			                	if($scope.gottenReasons.indexOf(item.remark_id)!=-1){
			                		//console.log(item.remark_description);
			                		$scope.declineArray.push(item.remark_description);
			                		return item.remark_description;
			                	}
			                });
			                //console.log("The declined reasons ",$scope.declineReasons);
			                console.log("The declined reasons ",$scope.declineArray);*/
			            }
			            else{
			                console.log(data)
			            }
			        })
			        .error( function( data )
			        { console.log(data) });
    		}
    	})

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;

    // set sidebar closed and body solid layout mode
    // $rootScope.settings.layout.pageBodySolid = true;
    // $rootScope.settings.layout.pageSidebarClosed = false;
    $scope.show = function(notification){
    	//console.log("Hello ",notification);
    	$scope.notificationArray = notification.declineArray;
    	$("#reasonmodal").modal("show");
    }

    $scope.confirmborrowreject = function(borrow_id,index){
    	$(".staff-actions-"+index).hide();
        $(".staff-spinner-"+index).show();
        console.log("The borrow id is ",borrow_id);
        $.ajax({
        	method:"GET",
        	url:api+"/superadmin/confirmborrowreject/"+borrow_id+"?key="+$scope.user.user_access_token,
        	contentType: "application/x-www-form-urlencoded",
        	success:function(data,status){
        		if(data.status == true){
        			$(".staff-actions-"+index).hide();
                    $(".staff-spinner-"+index).hide();
                    $scope.notifications.splice(index,1);
                    $scope.total_notifications--;
                    $scope.$apply();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message);
                    }, 1300);
        		}else{
        			$(".staff-actions-"+index).show();
                    $(".staff-spinner-"+index).hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
        		}
        	},error: function(data) {
                $(".staff-actions-"+index).show();
                $(".staff-spinner-"+index).hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        })
    }

    $scope.HrRepay = function (repayment_schedule_id,company_id,index) {
      $(".staff-actions-"+index).hide();
      $(".staff-spinner-"+index).show();
        $.ajax({
            method: "GET",
            url: api+"/superadmin/approvehrrepayrequest/"+repayment_schedule_id+"/"+company_id+"?key="+$scope.user.user_access_token,
            contentType: "application/x-www-form-urlencoded",
            success: function(data){
                    if (data.status == true) {
                      $(".staff-spinner-"+index).hide();
                      $scope.total_notifications--;
                      $scope.$apply();
                      setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.success(data.message,"Approved Successfully");
                      }, 1300);
                  }else{
                    $(".staff-spinner-"+index).hide();
                    $(".staff-actions-"+index).show();
                      setTimeout(function () {
                          toastr.options = {
                              closeButton: true,
                              debug: false,
                              positionClass: "toast-top-right",
                              onclick: null,
                              showDuration: 1000,
                              hideDuration: 1000,
                              timeOut: 5000,
                              extendedTimeOut: 1000,
                              showEasing: "swing",
                              hideEasing: "linear",
                              showMethod: "fadeIn",
                              hideMethod: "fadeOut"
                          };
                          toastr.error(data.message,"An Error Occured!");
                      }, 1300);
                  }
              },error: function(data) {
                $(".staff-spinner-"+index).hide();
                  $(".staff-actions-"+index).show();
                  setTimeout(function () {
                      toastr.options = {
                          closeButton: true,
                          debug: false,
                          positionClass: "toast-top-right",
                          onclick: null,
                          showDuration: 1000,
                          hideDuration: 1000,
                          timeOut: 5000,
                          extendedTimeOut: 1000,
                          showEasing: "swing",
                          hideEasing: "linear",
                          showMethod: "fadeIn",
                          hideMethod: "fadeOut"
                      };
                      toastr.error(data.message,"An Error Occured!");

                  }, 1300);
              }
            });
    }

    $scope.authenticate = function (borrow_id,index,type) {
        $(".staff-actions-"+index).hide();
        $(".staff-spinner-"+index).show();
        console.log("Type of request is ",type);
        if(type == "reject"){
        	$("#declinemodal").modal("show");
        	//return;
            $scope.declinereason = function(){
                $scope.others = "";
                console.log('Here are the reasons ',$scope.declineReasonschosenn,'and others are ',$scope.others);
                var datachosen = {};
                datachosen.process_remarks = $scope.declineReasonschosenn;
                datachosen.process_description = $scope.others;
                //return;
                $.ajax({
                    method:"POST",
                    //api/superadmin/rejectrequest/id?key
                    url:api+"/superadmin/rejectrequest/"+borrow_id+"?key="+$scope.user.user_access_token,
                    data:datachosen,
                    contentType: "application/x-www-form-urlencoded",
                    success:function(data,status){
                        if(data.status == true){
                            $("#declinemodal").hide();
                            $(".staff-actions-"+index).hide();
                            $(".staff-spinner-"+index).hide();
                            $scope.notifications.splice(index,1);
                            $scope.total_notifications--;
                            $scope.$apply();
                            setTimeout(function () {
                                toastr.options = {
                                    closeButton: true,
                                    debug: false,
                                    positionClass: "toast-top-right",
                                    onclick: null,
                                    showDuration: 1000,
                                    hideDuration: 1000,
                                    timeOut: 5000,
                                    extendedTimeOut: 1000,
                                    showEasing: "swing",
                                    hideEasing: "linear",
                                    showMethod: "fadeIn",
                                    hideMethod: "fadeOut"
                                };
                                toastr.success(data.message,"Loan Declined Successfully");
                            }, 1300);
                        }else{
                            setTimeout(function () {
                                toastr.options = {
                                    closeButton: true,
                                    debug: false,
                                    positionClass: "toast-top-right",
                                    onclick: null,
                                    showDuration: 1000,
                                    hideDuration: 1000,
                                    timeOut: 5000,
                                    extendedTimeOut: 1000,
                                    showEasing: "swing",
                                    hideEasing: "linear",
                                    showMethod: "fadeIn",
                                    hideMethod: "fadeOut"
                                };
                                toastr.error(data.message,"An Error Occured!");
                            }, 1300);
                        }
                    },error: function(data) {
                        //$("#spinner-login").hide();
                        setTimeout(function () {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.error(data.message,"An Error Occured!");

                        }, 1300);
                    }
                })
            }
            return;
        }
        $.ajax({
            method: "GET",
            url: api+"/superadmin/"+type+"request/"+borrow_id+"?key="+$scope.user.user_access_token,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    var loan_details = ($scope.notifications)[index];
                    $(".staff-actions-"+index).hide();
                    $(".staff-spinner-"+index).hide();
                    $scope.notifications.splice(index,1);
                    //console.log("data sent for approval is ",data);
                    $scope.total_notifications--;
                    $scope.$apply();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.info(data.message);
                    }, 1300);
                }
                else{
                    $(".staff-actions-"+index).show();
                    $(".staff-spinner-"+index).hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $(".staff-actions-"+index).show();
                $(".staff-spinner-"+index).hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        });
    }

    $scope.tenure = function (start,end){
        var startMonth = parseInt(start.substring(5,7));
        var endMonth = parseInt(end.substring(5,7));
        var startYear = parseInt(start.substring(0,4));
        var endYear = parseInt(end.substring(0,4));
        if (startYear != endYear){
            var result = (12-startMonth)+endMonth;
            return result + " months";
        }
        else {
            return endMonth-startMonth + " months";
        }
    }

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);
