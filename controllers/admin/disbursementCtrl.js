/**
 * Created by Ukemeabasi on 09/09/2016.
 */

'use strict';

app.controller('disbursementCtrl', ['$scope', '$rootScope', '$http', 'api', '$timeout',function($scope, $rootScope, $http, api,$timeout){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;
    $scope.paymentoptions=[{id:1,type:"Transfer Single"},{id:2,type:"Bank Deposit"},{id:3,type:"Online Transfer"},{id:4,type:"Internet Banking"},{id:5,type:""}];
    $(".loader").show();

    //get disbursement options
    $http({method:'GET',url:api+'/superadmin/getdisbursementoptions?key='+$scope.user.user_access_token})
    	.success(function(data){
    		//console.log('The disbursement option is ',data.data);
    		$scope.paymentoptions = data.data;
    	})
    	.error( function( data )
        { console.log(data) });
    //payment option type
    $http({method:'GET',url:api+'/superadmin/getpaymentoptiontype?key='+$scope.user.user_access_token})
        .success(function(data){
            //debugger;
            if (data.status){
                $scope.KegowpaymentOption = data.data;
            }
        })
    //gets all latest disbursements
    $http({method: 'GET', url:api+'/superadmin/getallrequest?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            $(".loader").hide();
            if(data.status == true){
                //debugger;
                $scope.disbursements = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.pageSize = 10;


    $scope.disburse = function (borrow_id,index,loan_amount,user_login_id,period) {
        $(".staff-actions-"+index).hide();
        $(".staff-spinner-"+index).show();
        $scope.disburseamount = loan_amount;
        //debugger;
        $http({method:'GET',url:api+'/superadmin/getuserbankfromid/'+user_login_id+"?key="+$scope.user.user_access_token})
            .success(function(data){
                if(data.status){
                    //debugger;
                    $timeout(function(){
                        $scope.$apply(function(){
                            $scope.bankdetails = data.data;
                        });
                    },0);

                }
            })
        $("#disbursemodal").modal("show");
        $scope.monthly =interestCalc($scope.disburseamount,period,0.075,0,0.015,0,0);
        //console.log('Scope monthly is ',$scope.monthly);
        $scope.disbursefinally = function(paymentoption){
          var datatoSend = {};

          if ($scope.paymentoption.payment_option_name == 'Kegow') {
            $scope.kegow_transaction_type.payment;
            datatoSend.kegow_transaction_type = $scope.kegow_transaction_type.payment.payment_option_type_id;
          }
          // console.log(datatoSend.kegow_transaction_type);
          // debugger;
          	datatoSend.payment_option_id=$scope.paymentoption.payment_option_id;
          	datatoSend.reference_id = $scope.referenceid;
          	datatoSend.payment_amount = $scope.disburseamount;
            datatoSend.monthly = $scope.monthly;
            var m = new Date();
            var month = new Array();
            month[1] = "January";
            month[2] = "February";
            month[3] = "March";
            month[4] = "April";
            month[5] = "May";
            month[6] = "June";
            month[7] = "July";
            month[8] = "August";
            month[9] = "September";
            month[10] = "October";
            month[11] = "November";
            month[12] = "December";
            var current_month = (m.getMonth() + 1);
            var months_array = [];
            var v = current_month;
            for (var i = 0; i < $scope.monthly.length; i++) {
              v += 1;
              months_array.push(month[v]);
            }
            datatoSend.months_to_pay = months_array;
            console.log(datatoSend);
            // return false;
        	$.ajax({
        		method:'POST',
        		url:api+'/superadmin/confirmborrowdisburse/'+borrow_id+"?key="+$scope.user.user_access_token,
        		data: datatoSend,
        		contentType:"application/x-www-form-urlencoded",
        		success:function(data,status){
        			if(data.status == true){
        				$(".staff-actions-"+index).hide();
	                    $(".staff-spinner-"+index).hide();
	                    $scope.disbursements.splice(index,1);
	                    $("#disbursemodal").hide();
	                    $scope.$apply();
	        			setTimeout(function () {
	                        toastr.options = {
	                            closeButton: true,
	                            debug: false,
	                            positionClass: "toast-top-right",
	                            onclick: null,
	                            showDuration: 1000,
	                            hideDuration: 1000,
	                            timeOut: 5000,
	                            extendedTimeOut: 1000,
	                            showEasing: "swing",
	                            hideEasing: "linear",
	                            showMethod: "fadeIn",
	                            hideMethod: "fadeOut"
	                        };
	                        toastr.success(data.message,"Loan Disbursed Successfully");
	                    }, 1300);
        			}else{
        				setTimeout(function () {
	                        toastr.options = {
	                            closeButton: true,
	                            debug: false,
	                            positionClass: "toast-top-right",
	                            onclick: null,
	                            showDuration: 1000,
	                            hideDuration: 1000,
	                            timeOut: 5000,
	                            extendedTimeOut: 1000,
	                            showEasing: "swing",
	                            hideEasing: "linear",
	                            showMethod: "fadeIn",
	                            hideMethod: "fadeOut"
	                        };
	                        toastr.error(data.message,"An Error Occured!");
	                    }, 1300);
        			}
        		},
		        error: function(data) {
	                $(".staff-actions-"+index).show();
	                $(".staff-spinner-"+index).hide();
	                setTimeout(function () {
	                    toastr.options = {
	                        closeButton: true,
	                        debug: false,
	                        positionClass: "toast-top-right",
	                        onclick: null,
	                        showDuration: 1000,
	                        hideDuration: 1000,
	                        timeOut: 5000,
	                        extendedTimeOut: 1000,
	                        showEasing: "swing",
	                        hideEasing: "linear",
	                        showMethod: "fadeIn",
	                        hideMethod: "fadeOut"
	                    };
	                    toastr.error(data.message,"An Error Occured!");
	                }, 1300);
	            }
        	})
        }
        /*$.ajax({
            method: "GET",
            url: api+"/superadmin/confirmborrowdisburse/"+borrow_id+"?key="+$scope.user.user_access_token,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    var loan_details = ($scope.disbursements)[index];
                    $(".staff-actions-"+index).hide();
                    $(".staff-spinner-"+index).hide();
                    $scope.disbursements.splice(index,1);
                    $scope.$apply();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.info(borrow_data.message,"Disbursement Successful!");
                    }, 1300);
                }
                else{
                    $(".staff-actions-"+index).show();
                    $(".staff-spinner-"+index).hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $(".staff-actions-"+index).show();
                $(".staff-spinner-"+index).hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        });*/
    }

    $scope.tenure = function (start,end){
        var startMonth = parseInt(start.substring(5,7));
        var endMonth = parseInt(end.substring(5,7));
        var startYear = parseInt(start.substring(0,4));
        var endYear = parseInt(end.substring(0,4));
        if (startYear != endYear){
            var result = (12-startMonth)+endMonth;
            return result + " months";
        }
        else {
            return endMonth-startMonth + " months";
        }
    }

    $scope.generatefromtable = function() {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+$scope.issuer.name+" Cards");
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson('editable');
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+$scope.issuer.name+" Cards.pdf");
    }
}]);
