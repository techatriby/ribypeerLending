/**
 * Created by Ukemeabasi and Sunmonu-Adedeji Olawale on 24/07/2016.
 */

'use strict';

app.controller('dashboardCtrl', ['$scope', '$http', 'api', 'sliderCtrller', '$timeout', '$rootScope', function($scope, $http, api, sliderCtrller, $timeout, $rootScope) {
    var loginDetails = window.localStorage.getItem("loginDetails");
    if (!loginDetails) {
        window.location.href = "#/home";
    }

    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;

    $scope.sliderCtrller = new sliderCtrller({ scope: $scope });
    $scope.sliderCtrller.start();

    $scope.lend_amount = 50000;
    $scope.lend_duration = 4;
    $scope.lend_interest_rate = 3;
    $scope.base_lend_interest = 9.8;
    $scope.lend_returns;
    $scope.min_lend = 50000;
    $scope.max_lend = 1000000;
    $scope.lend_step = 5000;
    $scope.min_lend_duration = 1;
    $scope.max_lend_duration = 4;
    $scope.loan_amount = 20000;
    $scope.loan_duration = 4;
    $scope.period = 0;
    $scope.borrowers = [];
    $scope.totalPaidbyUser;
    $scope.totalrepayment;

    $scope.Calculator = function(amount, months, interestrat, seviceCharge, insuranc, priorityinterest, fines) {
        var amountPayableMonthly = amount / months;
        var seviceCharge = 3000;
        var insurance = insuranc;
        var interestrate = interestrat;
        var firstMonthCharges = amountPayableMonthly + (interestrate * amount) + seviceCharge + (insurance * amount) + priorityinterest + fines;
        var firstMonthChargesRounded = firstMonthCharges.toFixed(2);

    }

    $("#loan_active_none").hide();
    //$("#loan_active").hide();
    $scope.priority = "0.00";

    //gets user bankdetails
    $http({ method: 'GET', url: api + '/user/getuserbankdetails?key=' + $scope.user.user_access_token }).success(function(data) {
        //debugger;
        //$scope.bank = data.data;
    }).error(function(data) {});

    $http({ method: 'GET', url: api + '/user/getborrowreason?key=' + $scope.user.user_access_token })
        .success(function(data) {
            $scope.reasons = data.data;
        })
        //gets active loan application
    $http({ method: 'GET', url: api + '/user/activeloan?key=' + $scope.user.user_access_token })
        .success(function(data) {
            //debugger;
            $scope.activeloanstatus = data.status;
            if (!data.status) {
                return;
            }

            $scope.activeloan = data.data;
            //$scope.activeloan.date_of_r = new Date($scope.activeloan.date_of_request).toDateString();
            $scope.activeloan.date_of_r = moment($scope.activeloan.date_of_request).format("ddd MMM Do YYYY");
            $scope.repayment = interestCalc(Number($scope.activeloan.loan_amount), Number($scope.activeloan.period), Number($scope.activeloan.interest_rate), Number($scope.activeloan.fixed_charge), Number($scope.activeloan.insurance_rate), Number($scope.activeloan.priority), Number($scope.activeloan.fine));

            $scope.totalrepayment = $scope.repayment.reduce(function(prev, curr) {
                return Number(prev) + Number(curr);
            });

            $scope.totalrepaymentRounded = $scope.totalrepayment.toFixed(2).toLocaleString();

            /*if(data.status == true){
                $scope.loan_active = data.data;
                // console.log('Date made is ',data.data.date_of_request.replace(/-/g,"/"));
                var dated = new Date(data.data.date_of_request);
                $scope.date_of_request = dated.toDateString();
                console.log("date is ",$scope.date_of_request);
                //$scope.date_of_request = data.data.date_of_request.replace(/-/g,"/");
                if ($scope.loan_active != undefined){
                    $("#loan_active_none").hide();
                    $("#loan_active").show();
                }
                else{
                    $("#loan_active_none").show();
                    $("#loan_active").hide();
                }
            }
            else{
                // console.log(data);
                $("#loan_active_none").show();
                $("#loan_active").hide();
            }*/
        })
        .error(function(data) {
            // console.log(data)
            $("#loan_active_none").show();
            //$("#loan_active").hide();
        });

    $(".table-data").hide();
    $(".loader").show();
    //gets list of all borrowed requests
    $http({ method: 'GET', url: api + '/user/getuserloanhistory?key=' + $scope.user.user_access_token })
        .success(function(data) {
            $scope.borrows = data.data;
            if (data.status == true) {
                data.data = data.data.forEach(function(item, index, array) {
                    //return item.date_of_r = new Date(item.date_of_request).toDateString();
                    return item.date_of_r = moment(item.date_of_request).format("ddd MMM Do YYYY");
                });

                $(".table-data").show();
                $(".loader").hide();
            } else {
                //console.log('Data for loanhistory loan',data);

                console.log(data)
            }
        })
        .error(function(data) { console.log(data) });

    //gets employees status
    $http({ method: 'GET', url: api + '/user/getemployeestatus?key=' + $scope.user.user_access_token })
        .success(function(data) {
            if (data.status == true) {
                $scope.list_employee_status = data.data;
            } else {
                console.log(data)
            }
        })
        .error(function(data) { console.log(data) });

    $(".table-data2").hide();
    $(".loader2").show();
    //gets list of all staff lend requests
    $http({ method: 'GET', url: api + '/user/getuserlendhistory?key=' + $scope.user.user_access_token })
        .success(function(data) {
            if (data.status == true) {

                $scope.lends = data.data;
                $(".table-data2").show();
                $(".loader2").hide();
            } else {
                //console.log(data)
            }
        })
        .error(function(data) { console.log(data) });


    $scope.authenticate = function(id, pos, type) {
            $(".pending-confirmed" + pos).hide();
            $(".pending-deleted" + pos).hide();
            $(".pending-request" + pos).show();
            var pos = pos;
            $.ajax({
                method: "DELETE",
                url: api + "/admin/" + type + "user/" + id + "?key=" + $scope.user.user_access_token,
                contentType: "application/x-www-form-urlencoded",
                success: function(data, status) {
                    if (data.status == true) {
                        $scope.pendings.splice(pos, 1);
                        $(".pending-request" + pos).hide();
                        $(".pending" + pos).show();
                        setTimeout(function() {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.success(data.message, "Account Authentication Successful!");

                        }, 1300);
                    } else {
                        $(".pending-confirmed" + pos).show();
                        $(".pending-deleted" + pos).show();
                        $(".pending-request" + pos).hide();
                        setTimeout(function() {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.error(data.message, "An Error Occured!");

                        }, 1300);
                    }
                },
                error: function(data) {
                    $(".pending-confirmed" + pos).show();
                    $(".pending-deleted" + pos).show();
                    $(".pending-request" + pos).hide();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message, "An Error Occured!");

                    }, 1300);
                }
            });
        }
        //Fetching list of banks
    $http({ method: 'GET', url: api + '/user/getallbank?key=' + $scope.user.user_access_token })
        .success(function(data) {
            //debugger;
            if (data.status == true) {
                $scope.list_bank = data.data;
            } else {
                //console.log(data)
            }
        })
        .error(function(data) { console.log(data) });

    $scope.callValidate = function() {
        var bank_number = $scope.bank.account_number;
        var sort_code = $scope.bank.bank_details.bank_code;
        $http({ method: 'GET', url: api + '/user/verifybank/' + bank_number + '/' + sort_code + '?key=' + $scope.user.user_access_token })
            .success(function(data) {
                if (data.status) {
                    //debugger;
                    $timeout(function() {
                        $scope.$apply(function() {
                            $scope.bank.account_name = data.data.accountname;
                        });
                    }, 0);
                    /*$scope.$watch(data.status,function(){

                    });*/
                }

            })
    }

    /*$scope.list_bank = [{name:"First Bank of Nigeria"}, {name:"Access Bank"}, {name:"Zenith Bank"}, {name:"Diamond Bank"},
        {name:"United Bank of Africa"}, {name:"Eco Bank"}, {name:"Skye Bank"}, {name:"First City Monument Bank"},
        {name:"Fidelity Bank"}, {name:"Stanbic Bank"}, {name:"Sterling Bank"}, {name:"Union Bank"}, {name:"Wema Bank"},
        {name:"Citi Bank"}, {name:"Standard Chartered Bank"}, {name:"Mainstreet Bank"}, {name:"Unity Bank"},
        {name:"Keystone Bank"}, {name:"Heritage Bank"}, {name:"Aso Bank"}, {name:"Jaiz Bank"}, {name:"Jubilee Bank"},{name:"Guaranty Trust Bank"}];*/

    $("#spinner-bank-detail").hide();
    $("#error-bank-detail").hide();
    $scope.updateBank = function(bank) {
        bank.bank_name = bank.bank_details.bank_name;
        bank.bank_code = bank.bank_details.bank_code;
        $("#spinner-bank-detail").show();
        //debugger;
        $.ajax({
            method: "POST",
            url: api + "/user/supplybankdetails?key=" + $scope.user.user_access_token,
            data: bank,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status) {
                if (data.status == true) {
                    $("#spinner-bank-detail").hide();
                    $("#bank_detail").modal("hide");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.info(data.message, "Bank Detail Updated Successfully!");

                    }, 1300);
                } else {
                    $("#spinner-bank-detail").hide();
                    $("#error-bank-detail").show();
                    $scope.bank_error = data.message;
                }
            },
            error: function(data) {
                $("#spinner-bank-detail").hide();
                $("#error-bank-detail").show();
                $scope.bank_error = data.message;
            }
        });
    }

    $scope.cancelLoan = function(loan) {

        // $("#spinner-bank-detail").show();
        //debugger;
        loan.status_id = '0';
        // console.log(loan)
        $.ajax({
            method: "POST",
            url: api + "/user/cancelloan?key=" + $scope.user.user_access_token,
            data: { transaction_id: loan.transaction_id },
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status) {
                // console.log(data);
                if (data.status == true) {
                    $("#spinner-bank-detail").hide();
                    $("#bank_detail").modal("hide");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.info(data.message);

                    }, 1300);
                } else {
                    loan.status_id = '2';
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.info(data.message);

                    }, 1300);
                }
            },
            error: function(data) {
                loan.status_id = '2';
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.info('An Error Occured');

                }, 1300);
            }
        });
    }

    $("#spinner-edit-profile").hide();
    $("#error").hide();
    $scope.editProfile = function(profile) {
        $("#spinner-edit-profile").show();
        $.ajax({
            method: "POST",
            url: api + "/user/updateprofile?key=" + $scope.user.user_access_token,
            data: profile,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status) {
                if (data.status == true) {
                    $("#spinner-edit-profile").hide();
                    $("#edit_profile").modal("hide");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message, "Profile Updated Successfully!");

                    }, 1300);
                } else {
                    $("#spinner-edit-profile").hide();
                    $("#error").show();
                    $scope.profile_error = data.message;
                }
            },
            error: function(data) {
                $("#spinner-edit-profile").hide();
                $("#error").show();
                $scope.profile_error = data.message;
            }
        });
    }

    //--## Handle Loan Operations #--/
    $scope.$watch('loan_amount', function() {
        $scope.updateLoanTotal($scope);
    });

    $scope.$watch('service_charge', function() {
        $scope.updateLoanTotal($scope);
    });

    $scope.$watch('loan_duration', function() {
        $scope.updateLoanTotal($scope);
    });

    $scope.$watch('priority', function() {
        $scope.updateLoanTotal($scope);
    });


    $("#spinner-borrow").hide();
    $("#error-borrow").hide();

    var modal = document.getElementById('loan-note-modal-preview');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("loan-note-close")[0];

    // When the user clicks on the button, open the modal
    btn.onclick = function() {
        $scope.generateLoanNote();
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    $scope.previewLoanNote = function() {
        $("#loan_transaction").modal("hide");
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }




    $scope.generateLoanNote = function() {
        $("#loan_transaction").modal("hide");
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        var transaction_fee;
        var amount = Number($scope.loan_amount);
        if (amount < 50999) {
            transaction_fee = 3000;
        } else if (amount >= 51000 && amount <= 100999) {
            transaction_fee = 4000;
        } else if (amount >= 101000 && amount < 150999) {
            transaction_fee = 7000;
        } else if (amount > 151000) {
            transaction_fee = 10000;
        }
        // console.log('Loan Amount:',$scope.loan_amount);
        // console.log('Transaction Amount:',transaction_fee);
        $scope.transaction_charge = transaction_fee;
        // console.log('Transaction Charge',$scope.service_charge);
        $scope.isLoanPorated = function() {
                var date = new Date;
                if (date.getDate() > 15) {
                    return true;
                } else {
                    return false;
                }
            }
            // $scope.monthly = interestCalc(Number($scope.loan_amount),Number($scope.loan_duration),0.075,0,0.015,0,0);
        $scope.monthly = window.repaymentborrowmonthly;
        // console.log("monthly Amount repaymentborrowmonthly", window.repaymentborrowmonthly);
        // console.log('Loan Total:', $scope.loan_total);
        // console.log('Loan Duration: ', $scope.loan_duration);
        // console.log('Interest Rate: ',$scope.interest_rate);
        // console.log('Transaction Charge: ',$scope.transaction_charge);
        // console.log('Insurance: ',$scope.insurance);
        // console.log('Priority: ', $scope.priority);
        // console.log('Fine: ',$scope.fine);
        // console.log('Loan Amount: ', $scope.loan_amount);

        // console.log('Montly Payment: ', $scope.monthly);
        var amountPayableMonthly = $scope.loan_amount / $scope.loan_duration;
        $scope.principal_charge = [parseFloat($scope.loan_amount).toFixed(2)];

        for (var month = 1; month < $scope.loan_duration; month++) {
            var remainingPrincipal = $scope.loan_amount - (month * amountPayableMonthly);
            $scope.principal_charge.push(parseFloat(remainingPrincipal).toFixed(2));
        }

        var principal_repayment = $scope.principal_charge;
        $scope.principal_repayment_holder = [];
        $scope.monthly_interest_holder = [];
        $scope.insurance_charges_holder = [];
        $scope.priority_charges_holder = [];
        $scope.fines_charges_holder = [];
        $scope.transaction_charges_holder = [];
        if (Number($scope.priority)) {
            $scope.interest_rate = 0.0;
        }
        for (var i = 0; i < principal_repayment.length; i++) {
            $scope.principal_repayment_holder.push(principal_repayment[principal_repayment.length - 1]);
            $scope.monthly_interest_holder.push(parseFloat(principal_repayment[i] * $scope.interest_rate).toFixed(2));
            $scope.insurance_charges_holder.push(parseFloat(principal_repayment[i] * $scope.insurance).toFixed(2));
            $scope.priority_charges_holder.push(parseFloat(principal_repayment[i] * $scope.priority).toFixed(2));
            $scope.fines_charges_holder.push(parseFloat(0).toFixed(2));
            $scope.transaction_charges_holder.push(parseFloat(0).toFixed(2));
        }
        if ($scope.fine) {
            $scope.fines_charges_holder[0] = $scope.fine;
        }
        $scope.transaction_charges_holder[0] = $scope.transaction_charge;

        if ($scope.isLoanPorated()) {
            var pr = parseFloat((((15 / 30) * $scope.loan_amount) * 0.075));
            $scope.monthly_interest_holder[0] = Number($scope.monthly_interest_holder[0]) + Number(pr);
        }
        // console.log('Fines Interest Repayment', $scope.fines_charges_holder);
        // console.log('Transaction Interest Repayment', $scope.transaction_charges_holder);
        // console.log('Monthly Interest Repayment', $scope.monthly_interest_holder);
        // console.log('Insurance Interest Repayment', $scope.insurance_charges_holder);
        // console.log('Priority Interest Repayment', $scope.priority_charges_holder);
        // console.log('Principal Repayment', $scope.principal_repayment_holder);
        // console.log('Principal Payment: ', $scope.principal_charge);
        $scope.fines_charges_holder_sum = $scope.fines_charges_holder.reduce(function(a, b) {
            return (parseFloat(a) + parseFloat(b)).toFixed(2);
        });

        $scope.monthly_sum = parseFloat($scope.loan_total).toFixed(2);

        $scope.transaction_charges_holder_sum = $scope.transaction_charges_holder.reduce(function(a, b) {
            return (parseFloat(a) + parseFloat(b)).toFixed(2);
        });

        $scope.monthly_interest_holder_sum = $scope.monthly_interest_holder.reduce(function(a, b) {
            return (parseFloat(a) + parseFloat(b)).toFixed(2);
        });

        $scope.insurance_charges_holder_sum = $scope.insurance_charges_holder.reduce(function(a, b) {
            return (parseFloat(a) + parseFloat(b)).toFixed(2);
        });

        $scope.priority_charges_holder_sum = $scope.priority_charges_holder.reduce(function(a, b) {
            return (parseFloat(a) + parseFloat(b)).toFixed(2);
        });

        $scope.principal_repayment_holder_sum = $scope.principal_repayment_holder.reduce(function(a, b) {
            return (parseFloat(a) + parseFloat(b)).toFixed(2);
        });

        $scope.principal_charge_sum = $scope.principal_charge.reduce(function(a, b) {
            return (parseFloat(a) + parseFloat(b)).toFixed(2);
        });

        $scope.payment_total_charges = (parseFloat($scope.insurance_charges_holder_sum) + parseFloat($scope.transaction_charges_holder_sum) + parseFloat($scope.fines_charges_holder_sum));
        console.log($scope.payment_total_charges);

        $scope.loan_repayment = (parseFloat($scope.monthly_interest_holder_sum) + parseFloat($scope.priority_charges_holder_sum) + parseFloat($scope.loan_amount));
        console.log($scope.loan_repayment);

        console.log('Total Rate: ', $scope.effective_interest_rate);
        console.log('Found is ', $scope.effrate);
    }


    $scope.apply = function() {
        $("#borrow-text").hide();
        $("#spinner-borrow").show();
        $scope.priority = "0.00";
        var loan_amount = $scope.loan_amount;
        var loan_duration = $scope.loan_duration;
        $scope.hasborrow_error = false;
        //gets user bankdetails
        $http({ method: 'GET', url: api + '/user/getuserbankdetails?key=' + $scope.user.user_access_token }).success(function(data) {
            if (!data.status) {
                setTimeout(function() {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message, "You should please fill in your bank details");
                }, 1300);
            }
        }).error(function(data) {
            console.log("error is ", data);
        });
        if (Number($scope.priority)) {
            $scope.interest_rate = 0.0;
        }
        $.ajax({
            method: "POST",
            url: api + "/user/borrow?key=" + $scope.user.user_access_token,
            data: {
                loan_amount: loan_amount,
                amount_to_pay: $scope.loan_total,
                date_of_request: processDate(Date.now(), 0),
                date_to_pay: processDate(Date.now(), loan_duration),
                period: $scope.loan_duration,
                interest_rate: $scope.interest_rate,
                fixed_charge: $scope.service_charge,
                insurance_rate: $scope.insurance,
                priority: $scope.priority,
                reason_id: $scope.reason_id.reason_id,
                other_reasons: $scope.other_reasons,
                fine: $scope.fine,
                principal_repayment_holder: $scope.principal_repayment_holder,
                monthly_interest_holder: $scope.monthly_interest_holder,
                insurance_charges_holder: $scope.insurance_charges_holder,
                priority_charges_holder: $scope.priority_charges_holder,
                fines_charges_holder: $scope.fines_charges_holder,
                transaction_charges_holder: $scope.transaction_charges_holder,
                monthly: $scope.monthly,
                principal_charge: $scope.principal_charge,
                payment_total_charges: $scope.payment_total_charges,
                loan_repayment: $scope.loan_repayment,
                total_repayment_rate: $scope.effective_interest_rate,
                effrate: $scope.effrate,
            },
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status) {
                if (data.status == true) {
                    $scope.borrows.push({ email: $scope.user.user_email, amount_to_pay: $scope.loan_total, loan_amount: $scope.loan_amount, period: $scope.loan_duration, date_of_r: new Date().toDateString() });
                    $("#spinner-borrow").hide();
                    $("#borrow-text").show();
                    modal.style.display = "none";
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message, "Loan Application Successful!");
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }, 1300);
                } else {
                    $("#spinner-borrow").hide();
                    $("#borrow-text").show();
                    $scope.borrow_error = data.message;
                    $scope.$apply();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message, "You are not permitted to take a Loan! ");
                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-borrow").hide();
                $("#error-borrow").show();
                $("#borrow-text").show();
            }
        });
    }

    $("#spinner-lend").hide();
    $("#error-lend").hide();
    //--## Handle Lend Operations #--/
    $scope.$watch('lend_amount', function() {
        $scope.updateLendTotal();
    });

    $scope.$watch('lend_duration', function() {
        $scope.updateLendTotal();
    });


    $scope.updateLendTotal = function() {

        var amount = parseFloat($scope.lend_amount);
        var period = parseInt($scope.lend_duration);
        var baseinterest2 = $scope.base_lend_interest;
        var total, rate, principal_repayment;
        principal_repayment = amount / period;
        //debugger;
        if (period == 1) {
            $scope.lend_returns = (0.04875 * principal_repayment) + amount;
        } else {
            var lend_repayments = [];
            lend_repayments.push(amount);
            var interst_rates = [];
            for (var i = 1; i < period; i++) {
                var reducing_balance = (i * principal_repayment).toFixed(2);
                lend_repayments.push(Number(reducing_balance));
            }
            //total = 0.03*amount;
            total = baseinterest2 * amount;
            lend_repayments.forEach(function(item, index, array) {
                var monthly = Number(item * 0.04875).toFixed(2);
                interst_rates.push(monthly);
            });
            var transaction_charges = 0 * period;

            var interest_payments = interst_rates.reduce(function(prev, curr, index, array) {
                return Number(prev) + Number(curr);
            });
            var payback = (principal_repayment * period) + interest_payments - transaction_charges;

            $scope.lend_returns = payback;
            var lend_interest_t = (payback - amount);
            var final_lend_interst = (lend_interest_t / amount) * 100;
            var finale_lend_rate = final_lend_interst.toFixed(2);
            $scope.base_lend_interest = finale_lend_rate;
        }

    }

    $scope.invest = function() {
        $("#lend-text").hide();
        $("#spinner-lend").show();
        var lend_amount = parseInt($("#lend-amount").val());
        var amount_to_receive = (lend_amount + lend_amount * 0.05);
        $.ajax({
            method: "POST",
            url: api + "/user/lend?key=" + $scope.user.user_access_token,
            data: { lend_amount: $scope.lend_amount, amount_to_receive: $scope.lend_returns, period: $scope.lend_duration },
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status) {
                if (data.status == true) {
                    $("#spinner-lend").hide();
                    $("#lend-text").show();
                    $scope.lends.push({ email: $scope.user.user_email, amount_to_receive: amount_to_receive, lend_amount: lend_amount, date_lend: processDate(Date.now(), 0) })
                    $scope.$apply();
                    $("#loan_transaction").modal("hide");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message, "Loan Invested Successful!");

                    }, 1300);
                } else {
                    $("#spinner-lend").hide();
                    $("#error-lend").show();
                    $("#lend-text").show();
                    $scope.lend_error = data.message;
                }
            },
            error: function(data) {
                $("#spinner-lend").hide();
                $("#error-lend").show();
                $("#lend-text").show();
                $scope.lend_error = data.message;
            }
        });
    }

    //gets profile image
    $http({ method: 'GET', url: api + '/user/getimage?key=' + $scope.user.user_access_token })
        .success(function(data) {
            if (data.status == true) {
                $scope.profile_image = data.data.user_image;
                console.log($scope.profile_image)
            } else {
                console.log(data)
            }
        })
        .error(function(data) {

        });

    $("#spinner-image-upload").hide();
    $("#error-image-upload").hide();

    var base_url_encode = api.split('api');
    $scope.base_url = base_url_encode[0];

    $scope.previewMyLoanNote = function(loan_note_file_path) {
        var url = 'https://docs.google.com/viewer?url=' + $scope.base_url + 'api/web/loannotes/' + loan_note_file_path;
        var win = window.open(url, '_blank');
        win.focus();
    }

    $scope.uploadImage = function() {
        $("#spinner-image-upload").show();
        var image = $scope.myFile;
        var formData = new FormData();
        formData.append("user_pics", image);
        $.ajax({
            method: "POST",
            url: api + "/user/updatepicture?key=" + $scope.user.user_access_token,
            data: formData,
            contentType: false,
            async: false,
            processData: false,
            success: function(data, status) {
                if (data.status == true) {
                    $("#spinner-image-upload").hide();
                    $("#upload_image").modal("hide");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message, "Image Updated Successfully!");

                    }, 1300);
                } else {
                    $("#spinner-image-upload").hide();
                    $("#error-image-upload").show();
                    $scope.profile_error = data.message;
                }
            },
            error: function(data) {
                $("#spinner-image-upload").hide();
                $("#error-image-upload").show();
                $scope.profile_error = data.message;
            }
        });
    }

    $scope.forgotPassword = function(user) {
        $("#spinner-forgot-password").show();
        $("#error-forgot-password").hide();
        //return;
        $.ajax({
            method: "POST",
            url: api + "/user/confirmpassword?key=" + $scope.user.user_access_token,
            data: user,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status) {
                if (data.status == true) {
                    $("#spinner-forgot-password").hide();
                    $("#error-forgot-password").hide();
                    $("#forgot_password").modal("hide");
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('#re_enter_password').modal('show');
                    $scope.changePassword = function(change) {
                        $('#spinner-changepassword').show();
                        $.ajax({
                            method: "POST",
                            url: api + "/user/changepassword?key=" + $scope.user.user_access_token,
                            data: change,
                            contentType: 'application/x-www-form-urlencoded',
                            success: function(data, status) {
                                if (data.status) {

                                    $('#spinner-changepassword').hide();
                                    $('#re_enter_password').modal('hide');
                                    setTimeout(function() {
                                        toastr.options = {
                                            closeButton: true,
                                            debug: false,
                                            positionClass: "toast-top-right",
                                            onclick: null,
                                            showDuration: 1000,
                                            hideDuration: 1000,
                                            timeOut: 5000,
                                            extendedTimeOut: 1000,
                                            showEasing: "swing",
                                            hideEasing: "linear",
                                            showMethod: "fadeIn",
                                            hideMethod: "fadeOut"
                                        };
                                        toastr.success("Password has been successfully changed !", data.message);
                                        window.localStorage.removeItem("loginDetails");
                                        window.location.href = "#/";
                                    }, 1300);
                                } else {
                                    setTimeout(function() {
                                        toastr.options = {
                                            closeButton: true,
                                            debug: false,
                                            positionClass: "toast-top-right",
                                            onclick: null,
                                            showDuration: 1000,
                                            hideDuration: 1000,
                                            timeOut: 5000,
                                            extendedTimeOut: 1000,
                                            showEasing: "swing",
                                            hideEasing: "linear",
                                            showMethod: "fadeIn",
                                            hideMethod: "fadeOut"
                                        };
                                        toastr.info(data.message, "Password Is Too Short");
                                    }, 1300);
                                }
                            },
                            error: function() {
                                setTimeout(function() {
                                    toastr.options = {
                                        closeButton: true,
                                        debug: false,
                                        positionClass: "toast-top-right",
                                        onclick: null,
                                        showDuration: 1000,
                                        hideDuration: 1000,
                                        timeOut: 5000,
                                        extendedTimeOut: 1000,
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut"
                                    };
                                    toastr.error(data.message, "An Error Occured! Please Try Again!!!");
                                }, 1300);
                            }
                        })
                    }
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message, "Password Reset has been initiated!");
                    }, 1300);

                } else {
                    $("#spinner-forgot-password").hide();
                    //$("#error-forgot-password").show();
                    setTimeout(function() {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error("The password entered isn't your password ", data.message);
                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-forgot-password").hide();
            }
        });
    }


    var processDate = function(date, duration) {

        function addMonths(dateObj, num) {

            var currentMonth = dateObj.getMonth();
            dateObj.setMonth(dateObj.getMonth() + num)

            if (dateObj.getMonth() != ((currentMonth + num) % 12)) {
                dateObj.setDate(0);
            }
            return dateObj;
        }

        if (date != undefined) {
            var date = new Date(date);

            if (parseInt(duration) > 0) date = addMonths(date, duration);
            // GET YYYY, MM AND DD FROM THE DATE OBJECT
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString();
            var dd = date.getDate().toString();
            var hrs = date.getHours().toString();
            var mins = (date.getMinutes() + 1).toString();
            var secs = date.getSeconds().toString();

            // Add preceeding '0'
            //Shorter conversion
            mm = ("0" + mm).slice(-2);
            dd = ("0" + dd).slice(-2);
            hrs = ("0" + hrs).slice(-2);
            mins = ("0" + mins).slice(-2);
            secs = ("0" + secs).slice(-2);

            //CONCAT THE STRINGS IN YYYY-MM-DD FORMAT
            return yyyy + '/' + mm + '/' + dd + ' ' + hrs + ':' + mins + ':' + secs;
            /*
            // CONVERT mm AND dd INTO chars
            var mmChars = mm.split('');
            var ddChars = dd.split('');
            var hrsChars = hrs.split('');
            var minsChars = mins.split('');
            var secsChars = secs.split('');

            // CONCAT THE STRINGS IN YYYY-MM-DD FORMAT
            return (parseInt(mmChars[1] ? mm : "0" + mmChars[0])+parseInt(duration)) + '/' + (ddChars[1] ? dd : "0" + ddChars[0]) + '/' + yyyy + ' '
                +(hrsChars[1] ? dd : "0" + hrsChars[0]) + ':' + (minsChars[1] ? mm : "0" + minsChars[0]) + ':' + (secsChars[1] ? mm : "0" + secsChars[0]);
                */
        } else {
            return 0;
        }
    };

    $(document).on('click', '#close-preview', function() {
        $('.image-preview').popover('hide');
        // Hover befor close the preview
        $('.image-preview').hover(
            function() {
                $('.image-preview').popover('show');
            },
            function() {
                $('.image-preview').popover('hide');
            }
        );
    });

    $(function() {
        // Create the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class", "close pull-right");
        // Set the popover default content
        $('.image-preview').popover({
            trigger: 'manual',
            html: true,
            title: "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
            content: "There's no image",
            placement: 'bottom'
        });
        // Clear event
        $('.image-preview-clear').click(function() {
            $('.image-preview').attr("data-content", "").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse");
        });
        // Create the preview image
        $(".image-preview-input input:file").change(function() {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function(e) {
                $(".image-preview-input-title").text("Change");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
            }
            reader.readAsDataURL(file);
        });
        //
    });
}]);