/**
 * Created by Ukemeabasi and Sunmonu-Adedeji Olawale (walesunmonu@gmail.com) on 23/07/2016.
 */

'use strict';

app.controller('homeCtrl', ['$scope', '$http', 'api', 'sliderCtrller', function($scope, $http, api,sliderCtrller){
	$scope.loginDetails= window.localStorage.getItem("loginDetails");
    $scope.interestCalc = window.interestCalc;
    $scope.loan_amount=20000;
    $scope.loan_duration=4;
    $scope.period = 0;
    // window.neweffectiverate = function(principal,period,monthlyinterestrate,priority){
    //     var amountmonthly = principal/period;
    //     var monthlypayments = [];
    //     var monthlyintrests = [];
    //     var prioritypayments = [];
    //     for(var month=0;month<period;month++){
    //         var paypermonth = principal - (month*amountmonthly);
    //         var interstpermonth = monthlyinterestrate * paypermonth;
    //         var prioritypermonth = priority * paypermonth;
    //         monthlyintrests.push(interstpermonth);
    //         monthlypayments.push(paypermonth);
    //         prioritypayments.push(prioritypermonth);
    //     }
    //     console.log('monthlypayments are ',monthlypayments);
    //     var totalinterests = monthlyintrests.reduce(function(prev,curr){
    //         return Number(prev) +Number(curr);
    //     });
    //     var totalpriorities = prioritypayments.reduce(function(prev,curr){
    //         return Number(prev)+Number(curr);
    //     });
    //     var payment = principal + totalinterests + totalpriorities;
    //     console.log("So payment for effeciveee interst rate is ",payment);
    //     console.log('monthlyintrests are ',monthlyintrests);
    //     var percent = ((payment - principal)/principal)/period;
    //     console.log("Percent is ",percent);
    //     return percent;

    // }
    $scope.effrate = neweffectiverate($scope.loan_amount,$scope.loan_duration,0.05,0)
    $scope.sliderCtrller = new sliderCtrller({scope:$scope});
    $scope.sliderCtrller.start();

    $scope.$watch('loan_amount',function(){
        $scope.updateLoanTotal($scope);
    });

    $scope.$watch('loan_duration',function(){
        $scope.updateLoanTotal($scope);
    });

    $scope.$watch('priority',function(){
        $scope.updateLoanTotal($scope);
    });


    $("#owl-demo").owlCarousel({
        //navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        navigation:false,
        autoPlay: true
    });

    //gets all registered companies
    $http({method: 'GET', url:api+'/company/company?key=92215a06d3349532a5669db8dacb1b06'})
        .success( function( data )
        {
            if(data.status == true){
                $scope.list_companies = data.data;
            }
            else{
                //console.log(data)
            }
        })
        .error( function( data )
        { 
        	console.log('Error is ',data)

         });

    $("#spinner-signup").hide();
    $("#signup-text").show();
    $scope.signUp = function (user) {
        $("#spinner-signup").show();
        $("#signup-text").hide();
        $.ajax({
            method: "POST",
            url: api+"/account/signup?key=92215a06d3349532a5669db8dacb1b06",
            data: user,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    $("#spinner-signup").hide();
                    $("#signup-text").show();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Signup was Successful!");

                    }, 1300);
                    $scope.login(user);
                }
                else{
                    $("#spinner-signup").hide();
                    $("#signup-text").show();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-signup").hide();
                $("#signup-text").show();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");
                }, 1300);
            }
        });
    }

    $("#spinner-login").hide();
    $("#login-text").show();
    $scope.login = function (client) {
        $("#spinner-login").show();
        $("#login-text").hide();
        $.ajax({
            method: "POST",
            url: api+"/login/login?key=92215a06d3349532a5669db8dacb1b06",
            data: client,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    var user = data.data;
                    $.ajax({
                        method: "GET",
                        url: api+"/user/getuserdetail?key="+data.data.user_access_token,
                        contentType: "application/x-www-form-urlencoded",
                        success: function(data, status){
                            if (data.status == true) {
                                $("#spinner-login").hide();
                                $("#login-text").show();
                                var loginDetails = {"user":user, "profile":data.data};
                                window.localStorage.setItem("loginDetails",JSON.stringify(loginDetails));
                                setTimeout(function () {
                                    toastr.options = {
                                        closeButton: true,
                                        debug: false,
                                        positionClass: "toast-top-right",
                                        onclick: null,
                                        showDuration: 1000,
                                        hideDuration: 1000,
                                        timeOut: 5000,
                                        extendedTimeOut: 1000,
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut"
                                    };
                                    toastr.success(data.message,"Login was Successful!");

                                }, 1300);
                                switch (user.user_type_id){
                                    case 1:{
                                        window.location.href = "#/admin/dashboard"; break;
                                    }
                                    case 2:{
                                        window.location.href = "#/transaction"; break;
                                    }
                                    case 3:{
                                        window.location.href = "#/dashboard"; break;
                                    }
                                    case 4:{
                                    	window.location.href = "#/admin/dashboard"; break;
                                    }
                                    case 5:{
                                    	window.location.href = "#/admin/dashboard"; break;
                                    }
                                    
                                    case 6:{
                                        window.location.href = "#/transaction"; break;
                                    }
                                    case 7:{
                                        
                                        window.location.href = "#/transaction"; break;
                                    }
                                }
                            }
                            else{
                                $("#spinner-login").hide();
                                $("#login-text").show();
                                setTimeout(function () {
                                    toastr.options = {
                                        closeButton: true,
                                        debug: false,
                                        positionClass: "toast-top-right",
                                        onclick: null,
                                        showDuration: 1000,
                                        hideDuration: 1000,
                                        timeOut: 5000,
                                        extendedTimeOut: 1000,
                                        showEasing: "swing",
                                        hideEasing: "linear",
                                        showMethod: "fadeIn",
                                        hideMethod: "fadeOut"
                                    };
                                    toastr.error(data.message,"An Error Occured!");

                                }, 1300);
                            }
                        },
                        error: function(data) {
                        	console.log('Error is ',data);
                            $("#spinner-login").hide();
                            $("#login-text").show();
                            setTimeout(function () {
                                toastr.options = {
                                    closeButton: true,
                                    debug: false,
                                    positionClass: "toast-top-right",
                                    onclick: null,
                                    showDuration: 1000,
                                    hideDuration: 1000,
                                    timeOut: 5000,
                                    extendedTimeOut: 1000,
                                    showEasing: "swing",
                                    hideEasing: "linear",
                                    showMethod: "fadeIn",
                                    hideMethod: "fadeOut"
                                };
                                toastr.error(data.message,"An Error Occured!");

                            }, 1300);
                        }
                    });
                }
                else{
                    $("#spinner-login").hide();
                    $("#login-text").show();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-login").hide();
                $("#login-text").show();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        });
    }
    
    $scope.borrow = function () {
        var loginDetails = window.localStorage.getItem("loginDetails");
        if (loginDetails) {
            window.location.href = "#/dashboard";
        }
        else {
            window.location.href =  "#/update-profile";
        }
    }
    var processDate = function (date,duration) {

        function addMonths(dateObj, num) {

            var currentMonth = dateObj.getMonth();
            dateObj.setMonth(dateObj.getMonth() + num)

            if (dateObj.getMonth() != ((currentMonth + num) % 12)){
                dateObj.setDate(0);
            }
            return dateObj;
        }

        if (date != undefined){
            var date = new Date(date);

            if(parseInt(duration)>0)date = addMonths(date,duration);
            // GET YYYY, MM AND DD FROM THE DATE OBJECT
            var yyyy = date.getFullYear().toString();
            var mm = (date.getMonth() + 1).toString();
            var dd = date.getDate().toString();
            var hrs = date.getHours().toString();
            var mins = (date.getMinutes() + 1).toString();
            var secs = date.getSeconds().toString();

            // Add preceeding '0'
            //Shorter conversion
            mm = ("0" + mm).slice(-2);
            dd = ("0" + dd).slice(-2);
            hrs = ("0" + hrs).slice(-2);
            mins = ("0" + mins).slice(-2);
            secs = ("0" + secs).slice(-2);

            //CONCAT THE STRINGS IN YYYY-MM-DD FORMAT
            return yyyy+'/'+mm+'/'+dd+' '+hrs+':'+mins+':'+secs;
            /*
            // CONVERT mm AND dd INTO chars
            var mmChars = mm.split('');
            var ddChars = dd.split('');
            var hrsChars = hrs.split('');
            var minsChars = mins.split('');
            var secsChars = secs.split('');

            // CONCAT THE STRINGS IN YYYY-MM-DD FORMAT
            return (parseInt(mmChars[1] ? mm : "0" + mmChars[0])+parseInt(duration)) + '/' + (ddChars[1] ? dd : "0" + ddChars[0]) + '/' + yyyy + ' '
                +(hrsChars[1] ? dd : "0" + hrsChars[0]) + ':' + (minsChars[1] ? mm : "0" + minsChars[0]) + ':' + (secsChars[1] ? mm : "0" + secsChars[0]);
                */
        }
        else {
            return 0;
        }
    };
    $scope.apply = function(){
        //console.log("I just wan apply sha");
        $scope.loginDetails= window.localStorage.getItem("loginDetails");
        var logindee = JSON.parse($scope.loginDetails);
        //console.log("The usertype is ",logindee.user.user_type_id);
        if(!$scope.loginDetails){
            //console.log("Thius is present location " ,location.href);
            //console.log("The second tab is ",$(".nav-tabs a:eq(3)").text());
            $(".nav-tabs a:eq(2)").tab("show");
            //$(".nav-tabs a:last").tab('show');
            //console.log("Tabs should move");
        }else{
            if(logindee.user.user_type_id ===2){
                    setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error("You are not authorized to apply for a Loan");
                }, 1300);
                return;
            }
            $.ajax({
                method: "POST",
                url: api+"/user/borrow?key="+logindee.user.user_access_token,
                data: {loan_amount:$scope.loan_amount, 
                    amount_to_pay:$scope.loan_total,
                    date_of_request:processDate(Date.now(),0), date_to_pay:processDate(Date.now(),$scope.loan_duration),
                    period:$scope.loan_duration,
                    interest_rate:$scope.interest_rate,
                    fixed_charge:$scope.service_charge,
                    insurance_rate:$scope.insurance,
                    priority:$scope.priority,
                    fine:$scope.fine},
                contentType: "application/x-www-form-urlencoded",
                success: function(data, status){
                    if (data.status == true) {
                        $scope.borrowers.push({email:$scope.user.user_email, amount_to_pay:$scope.loan_total, loan_amount:$scope.loan_amount, date_to_pay:processDate(Date.now(),$scope.loan_duration)});
                        $scope.$apply();
                        /*$("#spinner-borrow").hide();
                        $("#borrow-text").show();
                        $("#loan_transaction").modal("hide");
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();*/
                       
                        setTimeout(function () {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.success(data.message,"Loan Application Successful!");
                        }, 1300);
                    }
                    else{
                        //console.log("Error message ",data.message);
                        //$("#spinner-borrow").hide();
                        //$scope.borrow_error = data.message;
                        //$scope.hasborrow_error = true;
                        $scope.$apply();
                        setTimeout(function () {
                            toastr.options = {
                                closeButton: true,
                                debug: false,
                                positionClass: "toast-top-right",
                                onclick: null,
                                showDuration: 1000,
                                hideDuration: 1000,
                                timeOut: 5000,
                                extendedTimeOut: 1000,
                                showEasing: "swing",
                                hideEasing: "linear",
                                showMethod: "fadeIn",
                                hideMethod: "fadeOut"
                            };
                            toastr.error(data.message);
                        }, 1300);
                        //$("#error-borrow").show();
                        //$("#borrow-text").show();
                        // console.log("This is the error message",$scope.borrow_error);
                    }
                },
                error: function(data) {
                    $("#spinner-borrow").hide();
                    $("#error-borrow").show();
                    $("#borrow-text").show();
                    // $scope.borrow_error = data.message;
                    // console.log('The message you are meant to see ',$scope.borrow_error);
                }
            });
        }
    }
    $scope.opentab = function(){
        $(".nav-tabs a:eq(1)").tab('show');
    }
    $scope.openReset = function(){
        //$(".nav-tabs a:eq(1)").tab('show');
        $(".nav-tabs a:last").tab('show');
    }
    $("#reset-spinner").hide();
    $scope.reset = function(user){
    	//console.log('The user password is ',user);
    	//console.log("User is ",$scope.user, "and details are ",$scope.loginDetails);
    	$('#reset-text').hide();
    	$('#reset-spinner').show();
    	$.ajax({
    		method: "POST",
            url: api+"/user/forgotpassword?key=92215a06d3349532a5669db8dacb1b06",
            data:{user_email:user.user_email},
            contentType: "application/x-www-form-urlencoded",
            success:function(data,status){
            	if(data.status== true){
            		$('#reset-text').show();
    				$('#reset-spinner').hide();
            		setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Password Reset Successful!");
                    }, 1300);
            	}else{
            		$('#reset-text').show();
    				$('#reset-spinner').hide();
            		setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"Password couldn't be reset. Please try again");
                    }, 1300);
            	}
            },error:function(data){
            	$('#reset-text').show();
    			$('#reset-spinner').hide();
            	setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error("An error occured.Please try again");
                }, 1300);
            }
            
    	})
    }

}]);