/*
	Created by Sunmonu-Adedeji Olawale on 10.27.2016 
*/
'use strict';

app.controller('forgotCtrl',['$scope','$http','api','$location',function($scope,$http,api,$location){
	//console.log("Forgot Controller Loaded ",document.location.hash, 'query string begins at ',document.location.hash.indexOf('?'));
	var url = document.location.hash	
	var queryindex = document.location.hash.indexOf('?');
	//console.log("Rest of parameter is ",url.slice(queryindex+1));
	var querystring = url.slice(queryindex+1);
	var array = querystring.split('&');
	//console.log('Array is ',array);
	var data = {};
	var finalArray = [];
	for(var i=0;i<array.length;i++){
		//console.log(array[i].split('=')[1]);
		finalArray.push(array[i].split('=')[1]);
	}
	//console.log('Final array is ',finalArray);
	/*data.a = finalArray[0];
	data.u = finalArray[1];
	data.e = finalArray[2];*/
	$('#spinner-reset').hide();
	//console.log('Data is ',data);
	$scope.reset = function(user){
		$('#reset-password').hide();
		$('#spinner-reset').show();
		user.activation_key = finalArray[0];
		user.uniqid = finalArray[1];
		user.expire = finalArray[2];
		$.ajax({
			method:'POST',
			url:api+"/user/resetoldpassword?key=92215a06d3349532a5669db8dacb1b06",
			data:user,
			contentType: "application/x-www-form-urlencoded",
			success:function(data,status){
				if(data.status == true){
					setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Password Reset Was Successful!");
                        window.location.href = "#/";
                    }, 1300);
                    $('#reset-password').show();
                    $('#spinner-reset').hide();
				}else{
					$('#spinner-reset').hide();
					setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");
                    }, 1300);
                    $('#reset-password').show();
                    $('#spinner-reset').hide();
				}
			},
			error: function(data) {
                $("#spinner-login").hide();
	                setTimeout(function () {
	                    toastr.options = {
	                        closeButton: true,
	                        debug: false,
	                        positionClass: "toast-top-right",
	                        onclick: null,
	                        showDuration: 1000,
	                        hideDuration: 1000,
	                        timeOut: 5000,
	                        extendedTimeOut: 1000,
	                        showEasing: "swing",
	                        hideEasing: "linear",
	                        showMethod: "fadeIn",
	                        hideMethod: "fadeOut"
	                    };
	                    toastr.error(data.message,"An Error Occured!");
                }, 1300);
	            $('#reset-password').show();
                $('#spinner-reset').hide();
        	}
		})
	}
}])