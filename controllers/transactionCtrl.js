'use strict';

app.controller('transactionCtrl', ['$scope', '$http', 'api', function($scope, $http, api){
    $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
    $scope.profile = (JSON.parse(window.localStorage.getItem("loginDetails"))).profile;
    //$scope.declineReasons = {id:1,"Staff Leaving within the loan period":false,id:2,"Staff currently has a loan outside the platform":false,"Not Credit Worthy":false};
    //console.log("The user is ",$scope.user);
    //$scope.declineReasonChosen =[];
    $scope.declineReasonschosenn=[];
    $scope.borrowkey;

    // Get all repayments History
    $http({method: 'GET', url:api+'/admin/getallrequest?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
            	$(".loader").hide();
                $scope.repayments = data.data;
                console.log("repayments are ",$scope.repayments);
                //$scope.repayments = [];
                for (var i=0; i<$scope.repayments.length; i++){
                    //$(".loader").hide();
                    var repayperiod = Number($scope.repayments[i].period);
                    $scope.repayments[i].repaymonths = [];
                    console.log("Period is ",repayperiod);
                    for(var j=1;j<=repayperiod;j++){
                    	$scope.repayments[i].repaymonths.push(j);
                    }
                    //var tenure = $scope.tenure(repayments[i].date_of_request,repayments[i].date_to_pay);
                    /*var months = [];
                    for (var j=1; j<=tenure; j++){
                        months.push({id:j})
                    }
                    $scope.repayments.push({data:repayments[i], months:months});*/
                }
                $scope.repayments.forEach(function(item,index,array){
                    //console.log('Transaction id is ',item.transaction_id);

                    $http({method:'GET',url:api+'/admin/checkborrowrepaymentstatus/'+item.transaction_id+'?key='+$scope.user.user_access_token})
                        .success(function(data){
                            if(data.status == true){
                                item.repaymentdetails = data.data;
                                console.log("The data gotten from the transaction id is ",data.data);
                                //$scope.repaymentOptions = data.data;
                            }
                        })
                        .error(function(data){
                            console.log("Error is ",data);
                    })
                    console.log('item is ',item);
                })
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });




        $http({method:'GET',url:api+'/admin/getrepaymentoptions?key='+$scope.user.user_access_token})
        	.success(function(data){
        		if(data.status == true){
        			console.log("The data retreived is ",data.data);
        			$scope.repaymentOptions = data.data;
        		}
        	})
        	.error(function(data){
        		console.log("Error is ",data);
        	})

          $scope.disbursed  = function (data) {
          	//console.log("Filter ",data);
              return data.approve_flag == 3;
          }



           Array.prototype.remove = function() {
              var what, a = arguments, L = a.length, ax;
              while (L && this.length) {
                  what = a[--L];
                  while ((ax = this.indexOf(what)) !== -1) {
                      this.splice(ax, 1);
                  }
              }
              return this;
          };


          var containsObject = function (obj, list) {
              var i;
              for (i = 0; i < list.length; i++) {
                  if (list[i] === obj) {
                      // return callback(true);
                      return true;
                  }
              }

              // return callback(false);
              return false;
          }
          $scope.items = [];
          $scope.checkBox = function(borrow_id, repayment_amount, index, firstname, lastname){
            var repayment_month = index+1;
            var newItems = {borrow_id: borrow_id, month: repayment_month, payment_amount: repayment_amount, firstname: firstname, lastname: lastname};
            var check = ($.grep($scope.items, function(element) {
              return element.borrow_id == newItems.borrow_id;
            }))[0];
            function arrayObjectIndexOf(myArray, searchTerm, property) {
                for(var i = 0, len = myArray.length; i < len; i++) {
                    if (myArray[i][property] === searchTerm) return i;
                }
                return -1;
            }
            console.log("First Check:", check);
            if (check == undefined) {
              $scope.items.push(newItems);
            }
            else {
                var index = arrayObjectIndexOf($scope.items, newItems.borrow_id, borrow_id);
                $scope.items.splice(index,1);
            }
            // $scope.items = ItrrItems;
            // console.log(ItrrItems)
            console.log('Items Scope is', $scope.items);
            console.log("Second Check:", check);
          }
          $scope.payNow = function (Objitems) {
            $("#repaymodal").modal("show");

             for (var i = 0; i < Objitems.length; i++){
               console.log(Objitems[i]);
             }
          }


          // Password Reset Function

          $scope.forgotPassword = function (user) {
              $("#spinner-forgot-password").show();
              $("#error-forgot-password").hide();
              //return;
              $.ajax({
                  method: "POST",
                  url: api+"/admin/confirmpassword?key="+$scope.user.user_access_token,
                  data: user,
                  contentType: "application/x-www-form-urlencoded",
                  success: function(data, status){
                      if (data.status == true) {
                          $("#spinner-forgot-password").hide();
                          $("#error-forgot-password").hide();
                          $("#forgot_password").modal("hide");
                          $('body').removeClass('modal-open');
                          $('.modal-backdrop').remove();
                          $('#re_enter_password').modal('show');
                          $scope.changePassword = function(change){
                              $('#spinner-changepassword').show();
                              $.ajax({
                                  method:"POST",
                                  url:api+"/admin/changepassword?key="+$scope.user.user_access_token,
                                  data:change,
                                  contentType:'application/x-www-form-urlencoded',
                                  success:function(data,status){
                                      if(data.status){

                                          $('#spinner-changepassword').hide();
                                          $('#re_enter_password').modal('hide');
                                          setTimeout(function () {
                                              toastr.options = {
                                                  closeButton: true,
                                                  debug: false,
                                                  positionClass: "toast-top-right",
                                                  onclick: null,
                                                  showDuration: 1000,
                                                  hideDuration: 1000,
                                                  timeOut: 5000,
                                                  extendedTimeOut: 1000,
                                                  showEasing: "swing",
                                                  hideEasing: "linear",
                                                  showMethod: "fadeIn",
                                                  hideMethod: "fadeOut"
                                              };
                                              toastr.success("Password has been successfully changed !",data.message);
                                              window.localStorage.removeItem("loginDetails");
                                              window.location.href = "#/";
                                          }, 1300);
                                      }else{
                                          setTimeout(function () {
                                          toastr.options = {
                                                  closeButton: true,
                                                  debug: false,
                                                  positionClass: "toast-top-right",
                                                  onclick: null,
                                                  showDuration: 1000,
                                                  hideDuration: 1000,
                                                  timeOut: 5000,
                                                  extendedTimeOut: 1000,
                                                  showEasing: "swing",
                                                  hideEasing: "linear",
                                                  showMethod: "fadeIn",
                                                  hideMethod: "fadeOut"
                                              };
                                              toastr.info(data.message,"Password Is Too Short");
                                          }, 1300);
                                      }
                                  },error:function(){
                                      $('#spinner-changepassword').hide();
                                      setTimeout(function () {
                                          toastr.options = {
                                              closeButton: true,
                                              debug: false,
                                              positionClass: "toast-top-right",
                                              onclick: null,
                                              showDuration: 1000,
                                              hideDuration: 1000,
                                              timeOut: 5000,
                                              extendedTimeOut: 1000,
                                              showEasing: "swing",
                                              hideEasing: "linear",
                                              showMethod: "fadeIn",
                                              hideMethod: "fadeOut"
                                          };
                                          toastr.error(data.message,"An Error Occured! Please Try Again!!!");
                                      }, 1300);
                                  }
                              })
                          }
                          setTimeout(function () {
                              toastr.options = {
                                  closeButton: true,
                                  debug: false,
                                  positionClass: "toast-top-right",
                                  onclick: null,
                                  showDuration: 1000,
                                  hideDuration: 1000,
                                  timeOut: 5000,
                                  extendedTimeOut: 1000,
                                  showEasing: "swing",
                                  hideEasing: "linear",
                                  showMethod: "fadeIn",
                                  hideMethod: "fadeOut"
                              };
                              toastr.success(data.message, "Password Reset has been initiated!");
                          }, 1300);

                      }
                      else{
                          $("#spinner-forgot-password").hide();
                          //$("#error-forgot-password").show();
                          setTimeout(function () {
                              toastr.options = {
                                  closeButton: true,
                                  debug: false,
                                  positionClass: "toast-top-right",
                                  onclick: null,
                                  showDuration: 1000,
                                  hideDuration: 1000,
                                  timeOut: 5000,
                                  extendedTimeOut: 1000,
                                  showEasing: "swing",
                                  hideEasing: "linear",
                                  showMethod: "fadeIn",
                                  hideMethod: "fadeOut"
                              };
                              toastr.error("The password entered isn't your password " ,data.message);
                          }, 1300);
                      }
                  },
                  error: function(data) {
                      $("#spinner-forgot-password").hide();
                  }
              });
          }


          // $scope.repayd = function(borrow_id,index){
            $scope.repaymentModalClose = function(){
              $("#pay-action").show();
              $("#pay-spinner").hide();
            }

            var base_url_encode = api.split('api');
            $scope.base_url = base_url_encode[0];
            


            $scope.previewBorrowerLoanNote = function(loan_note_file_path){
              var url = 'https://docs.google.com/viewer?url='+$scope.base_url+'api/web/loannotes/'+loan_note_file_path;
              var win = window.open(url, '_blank');
              win.focus();
            }

            $scope.repayd = function(Objitems){
          	$("#repaymodal").modal("show");
            $("#pay-action").hide();
            $("#pay-spinner").show();
              function sumArray(a, b) {
                  return a + b;
              }

              $scope.totalPay = function(Objitems){
                var payStack = [];
                for (var i = 0; i < Objitems.length; i++){
                   payStack.push(parseFloat(Objitems[i].payment_amount));
                }
                var totalSum = payStack.reduce(sumArray, 0);
                return totalSum;
              }

              $scope.confirmrepay = function(payment_option){
                for (var i = 0; i < Objitems.length; i++){
          		console.log('The amount repaid is ',Objitems[i].payment_amount);
          		console.log("The payment object is ",payment_option,"and refernce_id ",$scope.referenceid);
          		var postObject = {};
              let firstname = Objitems[i].firstname;
              let lastname = Objitems[i].lastname;
          		postObject.repayment_amount = Objitems[i].payment_amount;
          		postObject.payment_option_id = payment_option.payment_option_id;
          		postObject.reference_id = $scope.referenceid;
          		postObject.month = Objitems[i].month;
          		postObject.payment_description = payment_option.payment_option_name;
          		$.ajax({
          			method:"POST",
          			url:api+"/admin/confirmloanrepayment/"+Objitems[i].borrow_id+"?key="+$scope.user.user_access_token,
          			data:postObject,
          			contentType:"application/x-www-form-urlencoded",
          			success:function(data,status){
          				if(data.status == true){
          					/*$(".staff-actions-"+index).hide();
      	                    $(".staff-spinner-"+index).hide();
      	                    $scope.disbursements.splice(index,1);*/
      	                    $("#repaymodal").hide();
      	                    $scope.$apply();
      	        			setTimeout(function() {
      	                        toastr.options = {
      	                            closeButton: true,
      	                            debug: false,
      	                            positionClass: "toast-top-right",
      	                            onclick: null,
      	                            showDuration: 1000,
      	                            hideDuration: 1000,
      	                            timeOut: 5000,
      	                            extendedTimeOut: 1000,
      	                            showEasing: "swing",
      	                            hideEasing: "linear",
      	                            showMethod: "fadeIn",
      	                            hideMethod: "fadeOut"
      	                        };
      	                        toastr.success(data.message,firstname+" "+lastname+" Loan Repayment Confirmed");
      	                    }, 1300);
                            setTimeout(function(){
                              window.location.reload(true);
                            }, 5000);
          				}else{
          					setTimeout(function () {
      	                        toastr.options = {
      	                            closeButton: true,
      	                            debug: false,
      	                            positionClass: "toast-top-right",
      	                            onclick: null,
      	                            showDuration: 1000,
      	                            hideDuration: 1000,
      	                            timeOut: 5000,
      	                            extendedTimeOut: 1000,
      	                            showEasing: "swing",
      	                            hideEasing: "linear",
      	                            showMethod: "fadeIn",
      	                            hideMethod: "fadeOut"
      	                        };
      	                        toastr.error(data.message,"An Error Occured!");
      	                    }, 1300);
          				}
          			},error:function(data){
          				/*$(".staff-actions-"+index).show();
      	                $(".staff-spinner-"+index).hide();*/
      	                setTimeout(function () {
      	                    toastr.options = {
      	                        closeButton: true,
      	                        debug: false,
      	                        positionClass: "toast-top-right",
      	                        onclick: null,
      	                        showDuration: 1000,
      	                        hideDuration: 1000,
      	                        timeOut: 5000,
      	                        extendedTimeOut: 1000,
      	                        showEasing: "swing",
      	                        hideEasing: "linear",
      	                        showMethod: "fadeIn",
      	                        hideMethod: "fadeOut"
      	                    };
      	                    toastr.error(data.message,"An Error Occured!");
      	                }, 1300);
          			}
          		})

          	}
          }
          	//alert('Hello ',borrow_id);
          }
    //gets reason for company loan decline
    $http({method: 'GET', url:api+'/admin/gettransactionremarks?key='+$scope.user.user_access_token})
    .success( function( data )
    {
        //console.log("the transaction remarks are ",data.data);
        $scope.declineReasons = data.data;
        $scope.checkAll = $scope.declineReasons.map(function(item){
        	return item.remark_id;
        });
        //console.log("Here is the value for check all ",$scope.checkAll);
    });

    //Get All Lenders Data
    $http({method: 'GET', url:api+'/admin/getalllenders?key='+$scope.user.user_access_token})
    .success( function( data )
    {
    	//console.log('The new lenders api is ',data.data);
        if(data.status == true){
        	$scope.lenderslist = data.data;
        }
    }).error( function(data){
    	//console.log('The error from fetching lenders data is ',data);
        console.log(data)
    });


    $scope.declinebutton = function(){

    }
    //gets all registered companies
    $http({method: 'GET', url:api+'/company/companieswithid?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.list_companies = data.data;
                $scope.company = ($.grep($scope.list_companies, function (e) { return e.company_id == $scope.profile.company_id}))[0];
            }
            else{
                //console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });


    //gets total lent for a company
    $http({method: 'GET', url:api+'/admin/totalamountlent?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_lent = data.data;
            }
            else{
                //console.log(data)
            }
        })
        .error( function(data){
        	console.log(data)
        });

    //gets total lent for a company
    $http({method: 'GET', url:api+'/admin/totalamountdeclined?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            //console.log("Data from his declined ",data);
            if(data.status == true){
                $scope.total_declined = data.data;
            }
            else{
                //console.log(data)
            }
        })
        .error( function(data){
            console.log(data)
        });



    //gets total borrowed for a company
    $http({method: 'GET', url:api+'/admin/totalamountborrowed?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_borrowed = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });


    //get company levels
    $http({method:"GET",url:api+'/admin/getadminlevels?key='+$scope.user.user_access_token})
    	.success(function(data){
    		//console.log("Data for payments are ",data);
    	})
    	.error(function(data){
    		//console.log("error from fetchg is ",data);
    	});

         $http({method:"GET",url:api+'/user/payment?key='+$scope.user.user_access_token})
        .success(function(data){
            //console.log("Data for payments are ",data);
        })
        .error(function(data){
            //console.log("error from fetchg is ",data);
        });
    //gets total requested for a company
    $http({method: 'GET', url:api+'/admin/totalamountrequested?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_requested = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //gets total staffs for a company
    $http({method: 'GET', url:api+'/admin/totalstaff?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.total_staff = data.data;
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //gets list of all latest requests
    $(".loader1").show();
    $(".table-data1").hide();
    $http({method: 'GET', url:api+'/admin/fetchborrowerrequest?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.latests = data.data;
                //console.log("Latests are ",$scope.latests);
                $(".table-data1").show();
                $(".loader1").hide();
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //gets list of all borrowed requests
    $(".loader2").show();
    $(".table-data2").hide();
    $http({method: 'GET', url:api+'/admin/lendhistory?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
                $scope.lenders = data.data;
                $(".table-data2").show();
                $(".loader2").hide();
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    //gets list of all borrowed requests
    $(".loader3").show();
    $(".table-data3").hide();
    $http({method: 'GET', url:api+'/admin/loanhistory?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            if(data.status == true){
            	//console.log("The borrows are ",data.data);
                $scope.borrows = data.data;
                if(data.data){
                	$scope.borrows = $scope.borrows.forEach(function(item,index,array){
                        //$scope.activeloan.date_of_r = moment($scope.activeloan.date_of_request).format("ddd MMM Do YYYY");
                        return item.date_of_r = moment(item.date_of_request).format("ddd MMM Do YYYY");
            			//return item.date_of_r = new Date(item.date_of_request).toDateString();
            		});
                }
                $scope.borrow_data = data.data;
                //console.log('Borrow dat is ',$scope.borrow_data);
                $(".table-data3").show();
                $(".loader3").hide();
            }
            else{
                console.log(data)
            }
        })
        .error( function( data )
        { console.log(data) });

    $scope.authenticate = function (borrow_id,index,type) {

        //console.log("type is ",type);
        //console.log("Borrow_id is ",borrow_id);
        $scope.borrowkey = borrow_id;
        //gets reason for company loan decline
        $http({method: 'GET', url:api+'/admin/gettransactionremarks?key='+$scope.user.user_access_token})
        .success( function( data )
        {
            //console.log("the transaction remarks are ",data);
            // if(data.status == true){
            //     $scope.total_lent = data.data;
            // }
            // else{
            //     console.log(data)
            // }
        })
        .error( function(data){
            console.log(data)
        });
        	if(type == "approve"){
        		$(".staff-actions-"+index).hide();
        		$(".staff-spinner-"+index).show();
	        	$.ajax({
	            method: "GET",
	            url: api+"/admin/"+type+"borrowrequest/"+borrow_id+"?key="+$scope.user.user_access_token,
	            contentType: "application/x-www-form-urlencoded",
	            success: function(data, status){
	                if (data.status == true) {
	                    var loan_details = ($scope.latests)[index];
	                    $(".staff-actions-"+index).hide();
	                    $(".staff-spinner-"+index).hide();
	                    $scope.latests.splice(index,1);
	                    $scope.$apply();
	                    setTimeout(function () {
	                        toastr.options = {
	                            closeButton: true,
	                            debug: false,
	                            positionClass: "toast-top-right",
	                            onclick: null,
	                            showDuration: 1000,
	                            hideDuration: 1000,
	                            timeOut: 5000,
	                            extendedTimeOut: 1000,
	                            showEasing: "swing",
	                            hideEasing: "linear",
	                            showMethod: "fadeIn",
	                            hideMethod: "fadeOut"
	                        };
	                        //toastr.info(data.message,type+" was Successful!");
	                        toastr.info("Loan has been Approved");
	                    }, 1300);
	                }
	                else{
	                    $(".staff-actions-"+index).show();
	                    $(".staff-spinner-"+index).hide();
	                    setTimeout(function () {
	                        toastr.options = {
	                            closeButton: true,
	                            debug: false,
	                            positionClass: "toast-top-right",
	                            onclick: null,
	                            showDuration: 1000,
	                            hideDuration: 1000,
	                            timeOut: 5000,
	                            extendedTimeOut: 1000,
	                            showEasing: "swing",
	                            hideEasing: "linear",
	                            showMethod: "fadeIn",
	                            hideMethod: "fadeOut"
	                        };
	                        toastr.error(data.message,"An Error Occured!");

	                    }, 1300);
	                }
	            },
	            error: function(data) {
	                $(".staff-actions-"+index).show();
	                $(".staff-spinner-"+index).hide();
	                setTimeout(function () {
	                    toastr.options = {
	                        closeButton: true,
	                        debug: false,
	                        positionClass: "toast-top-right",
	                        onclick: null,
	                        showDuration: 1000,
	                        hideDuration: 1000,
	                        timeOut: 5000,
	                        extendedTimeOut: 1000,
	                        showEasing: "swing",
	                        hideEasing: "linear",
	                        showMethod: "fadeIn",
	                        hideMethod: "fadeOut"
	                    };
	                    toastr.error(data.message,"An Error Occured!");

	                }, 1300);
	            }
        	});
        }else{
        	$("#declinemodal").modal('show');
        	$scope.declinereason = function(){
		    	var data = {};
		    	//console.log("here are the reason chosen ",$scope.declineReasons,"and the borrow key is ",$scope.borrowkey);
		    	if($scope.others || $scope.declineReasonschosenn.length >0){
		    		if($scope.others){
		    			//$scope.declineReasonschosenn.push($scope.others);
		    			data.process_description = $scope.others;
		    		}else{
		    			data.process_description = "";
		    		}
		    		data.process_remarks = $scope.declineReasonschosenn.toString();

		    		//console.log("The reasons are ",data);
		    		//console.log("Here are the reasons for declining the loan",$scope.declineReasonschosenn);

				    //return;
		    		$.ajax({
			            method: "POST",
			            url: api+"/admin/rejectborrowrequest/"+$scope.borrowkey+"?key="+$scope.user.user_access_token,
			            data: data,
		            	contentType: "application/x-www-form-urlencoded",
			            success: function(data, status){
			            	//console.log("Success Data is ",data);
			                if (data.status == true) {
			                   	$("#declinemodal").modal("hide");
			                   	$scope.latests.splice(index,1);
				    			//console.log("Splice Successful");
				    			$scope.$apply();
			                    //$('body').removeClass('modal-open');
			                    //$('.modal-backdrop').remove();
			                    setTimeout(function () {
			                        toastr.options = {
			                            closeButton: true,
			                            debug: false,
			                            positionClass: "toast-top-right",
			                            onclick: null,
			                            showDuration: 1000,
			                            hideDuration: 1000,
			                            timeOut: 5000,
			                            extendedTimeOut: 1000,
			                            showEasing: "swing",
			                            hideEasing: "linear",
			                            showMethod: "fadeIn",
			                            hideMethod: "fadeOut"
			                        };
			                        toastr.success(data.message,"Loan Declined Successfully!");
			                        //console.log("Index now is ",index);
				                   	//console.log("Latests are ",$scope.latests);

			                    }, 1300);
			                }
			                else{
			                    $("#spinner-image-upload").hide();
			                    $("#error-image-upload").show();
			                    $scope.profile_error = data.message;
			                }
			            },
			            error: function(data) {
			                $("#spinner-image-upload").hide();
			                $("#error-image-upload").show();
			                $scope.profile_error = data.message;
			            }
		        	});
		    	}else{
		    		setTimeout(function () {
		                toastr.options = {
		                    closeButton: true,
		                    debug: false,
		                    positionClass: "toast-top-right",
		                    onclick: null,
		                    showDuration: 1000,
		                    hideDuration: 1000,
		                    timeOut: 5000,
		                    extendedTimeOut: 1000,
		                    showEasing: "swing",
		                    hideEasing: "linear",
		                    showMethod: "fadeIn",
		                    hideMethod: "fadeOut"
		                };
		                toastr.error("You can't decline the loan for no reason!");
		            }, 1300);

		    	}
		    };
        }
    }

    $scope.tenure = function (start,end){
        var startMonth = parseInt(start.substring(5,7));
        var endMonth = parseInt(end.substring(5,7));
        var startYear = parseInt(start.substring(0,4));
        var endYear = parseInt(end.substring(0,4));
        if (startYear != endYear){
            var result = (12-startMonth)+endMonth;
            return result + " months";
        }
        else {
            return endMonth-startMonth + " months";
        }
    }

    $scope.filterTransactions = function(value) {
        switch (parseInt(value)){
            case 1:{
                $scope.borrows = $.grep($scope.borrow_data, function (e) { return e.status_id == 1}); break;
            }
            case 2:{
                $scope.borrows = $.grep($scope.borrow_data, function (e) { return e.status_id == 2}); break;
            }
            case 3:{
            	 $scope.borrows = $.grep($scope.borrow_data, function (e) { return e.status_id == 2 || e.status_id == 1});break;
            }
        }
    }

    $.fn.digits = function(){
        return this.each(function(){
            $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
        })
    }

    $scope.generatefromtable = function(name,type,table_id) {
        var data = [], height = 0, doc;
        doc = new jsPDF('p', 'pt', 'a4', true);
        doc.setFont("times", "normal");
        doc.setFontSize(20);
        doc.text(20, 40, "List of "+name+" "+type);
        doc.setFontSize(12);
        data = [];
        data = doc.tableToJson(table_id);
        height = doc.drawTable(data, {
            xstart : 10,
            ystart : 10,
            tablestart : 60,
            marginleft : 10,
            xOffset : 10,
            yOffset : 15
        });
        doc.save("List of "+name+" "+type+".pdf");
    }
    $scope.generate2fromtable = function(name){
        /*var columns = ["#","NAME","AMOUNT LENT","AMOUNT TO PAY","STATUS","DATE REQUESTED","LOAN TENURE"];
        var data = [
            [1,"Denmark"]
        ];*/
        var doc = new jsPDF('p','pt');
        //doc.text(20, 40, "Borrow History For "+name+ "Staff");
        //var elem = document.getElementById('editable2');
        var data = doc.autoTableHtmlToJson(document.getElementById('editable2'));
        var opts = {margin:0,beforePageContent:function(data){
            doc.text("Borrow History For "+name+ "Staff",40,30);
        }};
        doc.autoTable(data.columns,data.rows,opts);
        doc.save(name + "Staff Borrow History.pdf");

    }
}]);
