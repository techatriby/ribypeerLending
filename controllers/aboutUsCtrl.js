'use strict';

app.controller('aboutUsCtrl', ['$scope', '$http', 'api', function($scope, $http, api){
	$scope.contact = function(){
		var data = {};
		data.name = $scope.firstname +''+ $scope.lastname;
		data.email = $scope.email;
		data.phone = $scope.phone;
		data.message = $scope.message;
		$.ajax({
            method: "POST",
            url: api+"/contact/contactus",
            data: data,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status) {
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Thanks for the feedback.Well Appreciated");

                    }, 1300);
                }
                else{
       				setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An error has occured!");

                    }, 1300);
                }
            },
            error: function(data) {
               setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An error has occured!");

                    }, 1300);
            }
        });
	}
}]);