/**
 * Created by Ukemeabasi and Sunmonu-Adedehji Olawale on 08/08/2016.
 */
'use strict';

app.controller('resetCtrl', ['$scope', '$http', 'api', function($scope, $http, api){
    $("#reset-password").validate({
        rules: {
            new_password: {
                required: true,
                minlength: 6,
                maxlength: 10,

            } ,

            confirm_password: {
                equalTo: "#new_password",
                minlength: 6,
                maxlength: 10
            }


        },
        messages:{
            password: {
                required:"the password is required"
            }
        }
    });

    $("#spinner-login").hide();
    $("#reset-password").hide();
    $("#reset-login").show();
    $scope.login = function (client) {
    	$("#spinner-login").show();
        $.ajax({
            method: "POST",
            url: api+"/login/login?key=92215a06d3349532a5669db8dacb1b06",
            data: client,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
                if (data.status == true) {
                    var user = data.data;
                    var loginDetails = {"user":user, "profile":data.data};
                    window.localStorage.setItem("loginDetails",JSON.stringify(loginDetails));
                     
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Login was Successful!");

                    }, 1300);
                    $("#spinner-login").hide();
                    $("#reset-password").show();
                    $("#reset-login").hide();
                }
                else{
                    $("#spinner-login").hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-login").hide();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        });
    }

    $("#spinner-reset").hide();
    $scope.resetPassword = function (user) {
        $scope.user = (JSON.parse(window.localStorage.getItem("loginDetails"))).user;
        $("#spinner-reset").show();
        $.ajax({
            method: "POST",
            url: api+"/user/changepassword?key="+$scope.user.user_access_token,
            data: user,
            contentType: "application/x-www-form-urlencoded",
            success: function(data, status){
            	//console.log("Data from server is ",data);
                if (data.status == true) {
                    $("#spinner-reset").hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.success(data.message,"Password Reset was Successful!");

                    }, 1300);
                    window.localStorage.removeItem("loginDetails");
                    window.location.href = "#/login";
                }
                else{
                    $("#spinner-reset").hide();
                    setTimeout(function () {
                        toastr.options = {
                            closeButton: true,
                            debug: false,
                            positionClass: "toast-top-right",
                            onclick: null,
                            showDuration: 1000,
                            hideDuration: 1000,
                            timeOut: 5000,
                            extendedTimeOut: 1000,
                            showEasing: "swing",
                            hideEasing: "linear",
                            showMethod: "fadeIn",
                            hideMethod: "fadeOut"
                        };
                        toastr.error(data.message,"An Error Occured!");

                    }, 1300);
                }
            },
            error: function(data) {
                $("#spinner-signup").hide();
                $("#signup-text").show();
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        debug: false,
                        positionClass: "toast-top-right",
                        onclick: null,
                        showDuration: 1000,
                        hideDuration: 1000,
                        timeOut: 5000,
                        extendedTimeOut: 1000,
                        showEasing: "swing",
                        hideEasing: "linear",
                        showMethod: "fadeIn",
                        hideMethod: "fadeOut"
                    };
                    toastr.error(data.message,"An Error Occured!");

                }, 1300);
            }
        });
    }
}]);