/**
 * Created by Emmanuel.Adigun on 8/1/2016.
 */

$(function(){

    var total = new Highcharts.Chart({
        chart: {
            renderTo: 'ntotal',
            type: 'column',
            height: 225

        },
        title: {
            text: "",
            style: {
                color: '#15848F',
                fontWeight: 'bold'
            }
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: []
        },
        series: [{
            name: 'Total Number of users per company',
            data: []
        }
        ]
    });


    var totalno = [];

    $.getJSON('http://localhost/ribypeerlending/api/web/api/v1/superadmin/totalusers?key=99912347866d0999a3239890232', function(data){
        totalno = data.data;
        $("#total").text(totalno);

    });

    var borrower_no = [];

    $.getJSON('http://localhost/ribypeerlending/api/web/api/v1/superadmin/totalborrowers?key=99912347866d0999a3239890232', function(data){
        borrower_no = data.data;
        $("#borrow").text(borrower_no);

    });

    var lender_no = [];

    $.getJSON('http://localhost/ribypeerlending/api/web/api/v1/superadmin/totallenders?key=99912347866d0999a3239890232', function(data){
        lender_no = data.data;
        $("#lend").text(lender_no);

    });

    var approve_no = [];

    $.getJSON('http://localhost/ribypeerlending/api/web/api/v1/superadmin/totalusersapprove?key=99912347866d0999a3239890232', function(data){
        approve_no = data.data;
        $("#approve").text(approve_no);

    });

    var reject_no = [];

    $.getJSON('http://localhost/ribypeerlending/api/web/api/v1/superadmin/totalusersreject?key=99912347866d0999a3239890232', function(data){
        reject_no = data.data;
        $("#reject").text(reject_no);

    });

    $.getJSON('http://localhost/ribypeerlending/api/web/api/v1/superadmin/totaluserspercompany?key=99912347866d0999a3239890232', function(data){
        var no_user = data.data;
        var name = _.pluck(no_user, "company_name"),
            user = _.pluck(no_user, "number_of_user");

        total.xAxis[0].setCategories(name);
        total.series[0].setData(user);
        total.redraw();
    })
});

