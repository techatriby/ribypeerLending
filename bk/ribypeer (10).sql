-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2016 at 06:54 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ribypeer`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_log`
--

CREATE TABLE `admin_log` (
  `admin_log_id` int(11) NOT NULL,
  `admin_log_details` varchar(500) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bench_mark`
--

CREATE TABLE `bench_mark` (
  `bench_mark_id` int(11) NOT NULL,
  `bench_mark_min` varchar(40) NOT NULL,
  `bench_mark_max` varchar(40) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bench_mark`
--

INSERT INTO `bench_mark` (`bench_mark_id`, `bench_mark_min`, `bench_mark_max`, `created_date`, `modified_date`) VALUES
(1, '50000', '300000', '2016-07-24 07:56:47', '2016-07-24 07:56:47');

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `borrow_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `loan_amount` int(11) NOT NULL,
  `amount_to_pay` int(11) NOT NULL,
  `date_of_request` datetime NOT NULL,
  `date_to_pay` datetime NOT NULL,
  `status_id` int(11) NOT NULL,
  `approve_flag` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borrow`
--

INSERT INTO `borrow` (`borrow_id`, `company_id`, `user_id`, `loan_amount`, `amount_to_pay`, `date_of_request`, `date_to_pay`, `status_id`, `approve_flag`) VALUES
(1, 1, 2, 5000, 6000, '2016-07-25 17:53:04', '2016-12-30 00:00:00', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `company_info` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `company_info`) VALUES
(1, 'Venture Garden Group', NULL),
(2, 'ESL', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_status`
--

CREATE TABLE `employee_status` (
  `employee_id` int(11) NOT NULL,
  `employee_status_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_status`
--

INSERT INTO `employee_status` (`employee_id`, `employee_status_name`) VALUES
(1, 'contract'),
(2, 'part-time'),
(3, 'full-time');

-- --------------------------------------------------------

--
-- Table structure for table `lend`
--

CREATE TABLE `lend` (
  `lend_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lend_amount` int(11) NOT NULL,
  `date_lend` datetime NOT NULL,
  `amount_to_receive` int(11) NOT NULL,
  `date_to_receive` datetime DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `approve_flag` int(11) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lend`
--

INSERT INTO `lend` (`lend_id`, `company_id`, `user_id`, `lend_amount`, `date_lend`, `amount_to_receive`, `date_to_receive`, `status_id`, `approve_flag`, `modified_date`) VALUES
(1, 1, 2, 5000, '2016-07-25 17:53:15', 6000, NULL, 2, 1, '2016-07-25 17:53:15');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `status_name`) VALUES
(1, 'unactive'),
(2, 'active'),
(3, 'unconfirm');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `user_id` int(11) NOT NULL,
  `title` varchar(10) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `middlename` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `phone_number` varchar(14) NOT NULL,
  `email` varchar(80) NOT NULL,
  `company_id` int(11) NOT NULL,
  `address` varchar(350) DEFAULT NULL,
  `gender` text,
  `birthdate` date DEFAULT NULL,
  `user_type_id` int(11) NOT NULL,
  `user_image_url` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '2',
  `employee_status_id` int(11) DEFAULT NULL,
  `user_login_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `title`, `firstname`, `middlename`, `lastname`, `phone_number`, `email`, `company_id`, `address`, `gender`, `birthdate`, `user_type_id`, `user_image_url`, `created_date`, `modified_date`, `status_id`, `employee_status_id`, `user_login_id`) VALUES
(1, NULL, NULL, NULL, NULL, '08010101010', 'abdrkasali@gmail.com', 1, NULL, NULL, NULL, 3, '', '2016-07-23 23:52:27', '2016-07-23 23:52:27', 3, NULL, 9),
(2, 'mr', 'adeola', 'ademola', 'olumide', '07069324903', 'abdr@gmail.com', 1, '12 success estate', 'male', '0000-00-00', 3, '', '2016-07-24 01:11:55', '2016-07-24 02:45:34', 2, 2, 10),
(3, 'mr', 'ola', 'omo', 'ade', '09090909090', 'alozrazaq@gmail.com', 1, 'hello', 'male', '0000-00-00', 3, '', '2016-07-24 07:48:23', '2016-07-24 14:57:24', 2, 3, 11),
(4, NULL, NULL, NULL, NULL, '09090909090', 'alrazaqwalixy@gmail.com', 1, NULL, NULL, NULL, 3, '', '2016-07-24 09:24:06', '2016-07-24 09:24:06', 3, NULL, 12);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `user_login_id` int(11) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_password` varchar(60) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '2',
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `access_token` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_login_id`, `user_email`, `user_password`, `user_type_id`, `status_id`, `created_date`, `modified_date`, `access_token`) VALUES
(9, 'abdrkasali@gmail.com', '4f7e4ae8486f849cf8cf03180ff483d1539eb8f9', 3, 3, '2016-07-23 23:52:27', '2016-07-23 23:52:27', '87e737ce18a9f94a7e2d6cabde9258acc13c8700684508a75ddf2cb22002c688'),
(10, 'abdr@gmail.com', '4f7e4ae8486f849cf8cf03180ff483d1539eb8f9', 3, 2, '2016-07-24 01:11:55', '2016-07-24 02:45:34', 'b7c73a12ca6217b8377cc55c7a16bd5d9592a584d810dc441ed6a5fd223d6d93'),
(11, 'alozrazaq@gmail.com', 'c2e698114574d781d122e33bf31f255125f549fb', 3, 2, '2016-07-24 07:48:23', '2016-07-24 14:57:24', 'b7911ec8ff4f8d58e78068634de32623c4d921f78cc4b6a30012df8be614f5e9'),
(12, 'alrazaqwalixy@gmail.com', '4f7e4ae8486f849cf8cf03180ff483d1539eb8f9', 3, 3, '2016-07-24 09:24:06', '2016-07-24 09:24:06', '6200a27e14a616a5465b74443d36abd9f7594a1f5990434b5ab901c9f82b572f');

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `user_type_id` int(11) NOT NULL,
  `user_type_name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`user_type_id`, `user_type_name`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'General User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`admin_log_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indexes for table `bench_mark`
--
ALTER TABLE `bench_mark`
  ADD PRIMARY KEY (`bench_mark_id`),
  ADD KEY `bench_mark_id` (`bench_mark_id`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`borrow_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `user_id_2` (`user_id`),
  ADD KEY `company_id_2` (`company_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `employee_status`
--
ALTER TABLE `employee_status`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `lend`
--
ALTER TABLE `lend`
  ADD PRIMARY KEY (`lend_id`),
  ADD KEY `company_id` (`company_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `user_type_id_2` (`user_type_id`),
  ADD KEY `employee_status_id` (`employee_status_id`),
  ADD KEY `user_login_id` (`user_login_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`user_login_id`),
  ADD KEY `user_type_id` (`user_type_id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`user_type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `admin_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bench_mark`
--
ALTER TABLE `bench_mark`
  MODIFY `bench_mark_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `borrow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `employee_status`
--
ALTER TABLE `employee_status`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `lend`
--
ALTER TABLE `lend`
  MODIFY `lend_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `user_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `user_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_log`
--
ALTER TABLE `admin_log`
  ADD CONSTRAINT `admin_log_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `user_details` (`user_id`),
  ADD CONSTRAINT `admin_log_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`);

--
-- Constraints for table `borrow`
--
ALTER TABLE `borrow`
  ADD CONSTRAINT `borrow_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `borrow_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`),
  ADD CONSTRAINT `borrow_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`);

--
-- Constraints for table `lend`
--
ALTER TABLE `lend`
  ADD CONSTRAINT `lend_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `lend_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`),
  ADD CONSTRAINT `lend_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`);

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`user_type_id`),
  ADD CONSTRAINT `user_details_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`),
  ADD CONSTRAINT `user_details_ibfk_3` FOREIGN KEY (`employee_status_id`) REFERENCES `employee_status` (`employee_id`),
  ADD CONSTRAINT `user_details_ibfk_4` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `user_details_ibfk_5` FOREIGN KEY (`user_login_id`) REFERENCES `user_login` (`user_login_id`);

--
-- Constraints for table `user_login`
--
ALTER TABLE `user_login`
  ADD CONSTRAINT `user_login_ibfk_2` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`user_type_id`),
  ADD CONSTRAINT `user_login_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
